VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cls_POSConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Property Let Prop_LC_Edicion(ByVal pValor As String)
    gLC_Edicion = pValor
End Property

Property Get Prop_LC_Edicion() As String
    Prop_LC_Edicion = gLC_Edicion
End Property

Property Let Prop_LC_NumPOS(ByVal pValor As String)
    gLC_NumPOS = pValor
End Property

Property Get Prop_LC_NumPOS() As String
    Prop_LC_NumPOS = gLC_NumPOS
End Property

Public Sub IniciarConfigurador(pCodLocalidad As String, pConexionStringAdm As String, pConexionStringPOS As String, _
Optional pUserLogin As String = "", Optional pUserPass As String = "")

    On Error GoTo Error
    
    TmpVal = GetEnvironmentVariable("ProgramW6432")
    
    Select Case Len(Trim(TmpVal))
        Case 0 ' Default / No Especificado.
            WindowsArchitecture = [32Bits]
        Case Else
            WindowsArchitecture = [64Bits]
    End Select
    
    Screen.MousePointer = 11
    X = Screen.Width / 15
    Y = Screen.Height / 15
    
    If X < 800 And Y < 600 Then
       Call Mensaje(True, "Su pantalla debe tener una resoluci�n m�nima de 800x600 " & Chr(13) & "Configure su Monitor en el Panel de Control")
    End If
    
    If App.PrevInstance = True Then
        Call Mensaje(True, "Una instancia del programa ya se encuentra en ejecuci�n.")
        Exit Sub
    End If
    
    Ent.BDD.ConnectionString = pConexionStringAdm
    Ent.POS.ConnectionString = pConexionStringPOS
    Ent.BDD.Open
    Ent.POS.Open
    
    Sucursal = pCodLocalidad
        
    Screen.MousePointer = 0
    
    Inicio = True
    
    If pUserLogin <> "" And pUserPass <> "" Then
        AutoLogin = True
        Inicio_Usuario_Login = pUserLogin
        inicio_Usuario_Contrase�a = pUserPass
    Else
        AutoLogin = False
    End If
    
    frm_Config_POS1.Show vbModal
    
    Ent.BDD.Close
    Ent.POS.Close
    
    Exit Sub

Error:
    
    Debug.Print ERR.Description
    
    Call Mensaje(True, "Se ha producido un error, verifique las conexiones.")
    
    Exit Sub

End Sub
