VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmUbicacion 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4335
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   8460
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmUbicacion.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   8460
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   9225
      TabIndex        =   8
      Top             =   421
      Width           =   9255
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   1080
         Left            =   240
         TabIndex        =   9
         Top             =   0
         Width           =   8000
         Begin MSComctlLib.Toolbar tlb_Config 
            Height          =   810
            Left            =   90
            TabIndex        =   10
            Top             =   120
            Width           =   7700
            _ExtentX        =   13573
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "Cancelar la configuración actual [F7]"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "Grabar esta configuración [F4]"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "salir"
                  Object.ToolTipText     =   "Salir del configurador [F12]"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "Ayuda del Configurador [F1]"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuración de Ubicaciones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6555
         TabIndex        =   6
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   2280
      Left            =   300
      TabIndex        =   2
      Top             =   1800
      Width           =   7875
      Begin VB.TextBox Txtcodigo 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   615
         MaxLength       =   10
         TabIndex        =   0
         Top             =   600
         Width           =   3570
      End
      Begin VB.TextBox TxtUbicacion 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   615
         MaxLength       =   255
         TabIndex        =   1
         Top             =   1470
         Width           =   6540
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Código"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   615
         TabIndex        =   4
         Top             =   345
         Width           =   585
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripción de Ubicación"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   615
         TabIndex        =   3
         Top             =   1215
         Width           =   2115
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   4035
      Top             =   1320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmUbicacion.frx":628A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmUbicacion.frx":801C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmUbicacion.frx":9DAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmUbicacion.frx":BB40
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmUbicacion.frx":D8D2
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmUbicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Activate()
    Call Cancelar
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case vbShiftMask
        Case Else
            Select Case KeyCode
                Case vbKeyF4
                    Call Grabar
                Case vbKeyF7
                    Call Cancelar
                Case vbKeyF12
                    AddNode = False
                    Unload Me
                    Exit Sub
                Case vbKeyReturn
                    Send_Keys Chr(vbKeyTab), True
                Case vbKeyEscape
            End Select
    End Select
End Sub

Public Sub Grabar()
    
    On Error GoTo ErrGrabar
    
    Dim rsGrabar As New ADODB.Recordset, Cont As Integer, Cuantos As Integer, Consecutivos As Object, NwCode As String
    
    Txtcodigo.Text = Replace(Txtcodigo.Text, " ", vbNullString)
    
    If Txtcodigo.Text = vbNullString Then
        Call Mensaje(True, "Debe ingresar un código. Adicionalmente, No se permiten espacios.")
        Exit Sub
    End If
    
    Call Apertura_Recordset(True, rsGrabar)
    
    rsGrabar.Open "SELECT * FROM MA_SITIO WHERE cs_CodigoSitio = '" & Txtcodigo.Text & "'", _
    Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
    
    If Not IsNumeric(Txtcodigo.Text) And rsGrabar.EOF Then
        Txtcodigo.Text = "." & Txtcodigo.Text
    End If
    
    Set Consecutivos = CreateObject("recsun.cls_datos")
    
    Ent.POS.BeginTrans
    
    If rsGrabar.EOF Then
    
        Call Apertura_Recordset(True, rsGrabar)
        
        rsGrabar.Open "SELECT * FROM MA_SITIO WHERE cs_CodigoSitio = '" & Txtcodigo.Text & "'", _
        Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
    
    End If
    
    If rsGrabar.EOF Then
        rsGrabar.AddNew
    End If
    
    rsGrabar!cs_CodigoSitio = Me.Txtcodigo.Text
    rsGrabar!cs_CodigoRelacion = NodoTextoKey
    rsGrabar!cu_descripcion = TxtUbicacion.Text
    rsGrabar.UpdateBatch
    
    Ent.POS.CommitTrans
    
    lcKey = "|S|" & NodoTextoKey
    Clave = "|U|" & rsGrabar!cs_CodigoSitio
    lcTexto = rsGrabar!cu_descripcion
    AddNode = True
    rsGrabar.Close
    
    Unload Me
    
    Exit Sub
    
ErrGrabar:
    
    Mensaje True, "Ha ocurrido un error al grabar, por favor reporte lo siguiente:" & ERR.Description
    
    Unload Me
    
End Sub

Public Sub Cancelar()
    
    Dim rsGrabar As New ADODB.Recordset, Consecutivos As Object
    
    Set Consecutivos = CreateObject("recsun.cls_datos")
    
    Call Apertura_Recordset(True, rsGrabar)
    
    AddNode = False
    
    If Trim(Me.Txtcodigo.Text) <> "" Then
        
        rsGrabar.Open "SELECT * FROM MA_SITIO WHERE cs_CodigoSitio = '" & Trim(Me.Txtcodigo.Text) & "'", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not rsGrabar.EOF Then
            NodoTextoKey = rsGrabar!cs_CodigoRelacion
            Txtcodigo.Text = rsGrabar!cs_CodigoSitio
            TxtUbicacion.Text = rsGrabar!cu_descripcion
            Me.Txtcodigo.Enabled = False
        Else
            TxtUbicacion.Text = ""
            Txtcodigo.Text = "|N|" & Consecutivos.NO_CONSECUTIVO(ProducID, Ent.POS, "cs_codigositio", False)
        End If
        
        rsGrabar.Close
        
    Else
        TxtUbicacion.Text = ""
        Txtcodigo.Text = ""
    End If
    
    SeleccionarTexto TxtUbicacion
    
    'Me.Caption = "Configuración de Administración para el Nivel '" & NodoTexto & "'"
    
End Sub

Private Sub Form_Load()
    
    'Me.lbl_Organizacion.Caption = stellar_mensaje(10134, True) 'configuracion de zona
    'Label1.Caption = stellar_mensaje(10133, True) 'Descripción de Ubicación
    
    Me.lbl_Organizacion.Caption = Stellar_Mensaje(10210, True) 'configuracion de mesa
    Label1.Caption = Stellar_Mensaje(143, True) 'Descripción de mesa
    Me.tlb_Config.Buttons(1).Caption = Stellar_Mensaje(6, True) 'cancelar
    Me.tlb_Config.Buttons(2).Caption = Stellar_Mensaje(103, True) 'grabar
    Me.tlb_Config.Buttons(3).Caption = Stellar_Mensaje(54, True) 'salir
    Me.tlb_Config.Buttons(4).Caption = Stellar_Mensaje(7, True) 'ayuda
    tlb_Config.Buttons("Teclado").Caption = Stellar_Mensaje(10080, True) ' Teclado
    Label2.Caption = Stellar_Mensaje(142, True) 'codigo
    
End Sub

Private Sub tlb_Config_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case LCase(Button.Key)
        Case "grabar"
            Send_Keys Chr(vbKeyF4), True
        Case "cancelar"
            Send_Keys Chr(vbKeyF7), True
        Case "salir"
            Send_Keys Chr(vbKeyF12), True
        Case "ayuda"
            Send_Keys Chr(vbKeyF1)
        Case Is = LCase("Teclado")
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = Txtcodigo
            
            TecladoWindows mCtl
                        
    End Select
End Sub

Private Sub Txtcodigo_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyReturn Then
        Call BuscarSitio
    End If
End Sub

Private Sub BuscarSitio()
    
    Dim rsGrabar As New ADODB.Recordset, Consecutivos As Object
    
    Set Consecutivos = CreateObject("recsun.cls_datos")
    
    Call Apertura_Recordset(True, rsGrabar)
    
    AddNode = False
    
    If Trim(Me.Txtcodigo.Text) <> "" Then
        rsGrabar.Open "SELECT * FROM MA_SITIO WHERE cs_CodigoSitio = '" & Trim(Me.Txtcodigo.Text) & "'", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not rsGrabar.EOF Then
            If NodoTextoKey <> rsGrabar!cs_CodigoRelacion Then
                Call Mensaje(True, "La Ubicación pertenece a otra Relación.")
                Txtcodigo.Text = ""
                TxtUbicacion.Text = ""
                Exit Sub
            End If
            
            NodoTextoKey = rsGrabar!cs_CodigoRelacion
            
            Txtcodigo.Text = rsGrabar!cs_CodigoSitio
            TxtUbicacion.Text = rsGrabar!cu_descripcion
        Else
            TxtUbicacion.Text = ""
        End If
        
        rsGrabar.Close
    Else
        TxtUbicacion.Text = ""
        Txtcodigo.Text = ""
    End If
    
End Sub
