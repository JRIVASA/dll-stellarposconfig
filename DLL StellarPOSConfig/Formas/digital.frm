VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Begin VB.Form digital 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n Digital"
   ClientHeight    =   8370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11355
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "digital.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   11355
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComCtl3.CoolBar CoolBar1 
      Height          =   795
      Left            =   90
      TabIndex        =   91
      Top             =   7470
      Width           =   11085
      _ExtentX        =   19553
      _ExtentY        =   1402
      BandCount       =   1
      _CBWidth        =   11085
      _CBHeight       =   795
      _Version        =   "6.0.8450"
      Child1          =   "Frame1"
      MinHeight1      =   735
      Width1          =   2715
      NewRow1         =   0   'False
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   30
         TabIndex        =   92
         Top             =   30
         Width           =   10965
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   675
            Left            =   30
            TabIndex        =   93
            Top             =   30
            Width           =   10905
            _ExtentX        =   19235
            _ExtentY        =   1191
            ButtonWidth     =   1455
            ButtonHeight    =   1191
            Appearance      =   1
            Style           =   1
            ImageList       =   "Icono_Apagado"
            DisabledImageList=   "Icono_deshabilitado"
            HotImageList    =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Cancelar"
                  Key             =   "cancelar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "grabar"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Ayuda"
                  Key             =   "ayuda"
                  ImageIndex      =   3
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame datos_subboton 
      Caption         =   " Datos del sub-bot�n # "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   2145
      Left            =   5700
      TabIndex        =   83
      Top             =   5280
      Width           =   5475
      Begin VB.CommandButton scancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   3030
         TabIndex        =   88
         Top             =   1740
         Width           =   1095
      End
      Begin VB.CommandButton sgrabar 
         Caption         =   "&Grabar"
         Height          =   315
         Left            =   4230
         TabIndex        =   87
         Top             =   1740
         Width           =   1095
      End
      Begin VB.TextBox subboton_producto 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   150
         MaxLength       =   15
         TabIndex        =   75
         Top             =   570
         Width           =   1755
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Producto"
         Height          =   195
         Index           =   14
         Left            =   150
         TabIndex        =   86
         Top             =   360
         Width           =   750
      End
      Begin VB.Label subboton_descri 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   345
         Left            =   150
         TabIndex        =   85
         Top             =   1260
         Width           =   5145
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         Height          =   195
         Index           =   13
         Left            =   150
         TabIndex        =   84
         Top             =   1020
         Width           =   990
      End
   End
   Begin VB.Frame datos_boton 
      Caption         =   " Datos del bot�n # "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   2145
      Left            =   120
      TabIndex        =   78
      Top             =   5280
      Width           =   5475
      Begin VB.CommandButton bcancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   3000
         TabIndex        =   90
         Top             =   1740
         Width           =   1095
      End
      Begin VB.CommandButton bgarbar 
         Caption         =   "&Grabar"
         Height          =   315
         Left            =   4200
         TabIndex        =   89
         Top             =   1740
         Width           =   1095
      End
      Begin VB.Frame Frame3 
         Caption         =   "Es un Producto"
         Height          =   705
         Left            =   120
         TabIndex        =   81
         Top             =   960
         Width           =   5175
         Begin VB.TextBox boton_producto 
            CausesValidation=   0   'False
            Height          =   285
            Left            =   90
            MaxLength       =   15
            TabIndex        =   38
            Top             =   300
            Width           =   1725
         End
         Begin VB.Label boton_descri 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1890
            TabIndex        =   82
            Top             =   300
            Width           =   3195
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Es un Grupo"
         Height          =   675
         Left            =   120
         TabIndex        =   79
         Top             =   240
         Width           =   5175
         Begin VB.CheckBox grp_btn 
            Alignment       =   1  'Right Justify
            Caption         =   "No"
            CausesValidation=   0   'False
            Height          =   255
            Left            =   90
            TabIndex        =   36
            Top             =   315
            Width           =   525
         End
         Begin VB.TextBox nombre_grupo 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            Height          =   285
            Left            =   1620
            MaxLength       =   15
            TabIndex        =   37
            Top             =   300
            Width           =   3465
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Nombre"
            Height          =   195
            Index           =   10
            Left            =   780
            TabIndex        =   80
            Top             =   345
            Width           =   675
         End
      End
   End
   Begin VB.Frame panel2 
      Caption         =   "Panel Secundario"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   5055
      Left            =   5700
      TabIndex        =   77
      Top             =   150
      Width           =   5475
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   1
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   2
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   3
         Left            =   2745
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   4
         Left            =   3630
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   5
         Left            =   4515
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   7
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   6
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   8
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   9
         Left            =   2745
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   10
         Left            =   3630
         Style           =   1  'Graphical
         TabIndex        =   49
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   11
         Left            =   4515
         Style           =   1  'Graphical
         TabIndex        =   50
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   19
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   18
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   20
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   59
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   21
         Left            =   2745
         Style           =   1  'Graphical
         TabIndex        =   60
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   22
         Left            =   3630
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   23
         Left            =   4515
         Style           =   1  'Graphical
         TabIndex        =   62
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   13
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   12
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   14
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   15
         Left            =   2745
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   16
         Left            =   3630
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   17
         Left            =   4515
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   31
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   70
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   30
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   69
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   32
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   71
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   33
         Left            =   2745
         Style           =   1  'Graphical
         TabIndex        =   72
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   34
         Left            =   3630
         Style           =   1  'Graphical
         TabIndex        =   73
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   35
         Left            =   4515
         Picture         =   "digital.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   74
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   25
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   64
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   24
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   26
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   65
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   27
         Left            =   2745
         Style           =   1  'Graphical
         TabIndex        =   66
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   28
         Left            =   3630
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton sbtn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   29
         Left            =   4515
         Style           =   1  'Graphical
         TabIndex        =   68
         Top             =   3420
         Width           =   855
      End
   End
   Begin VB.Frame PANEL1 
      Caption         =   "Panel Primario "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   5055
      Left            =   120
      TabIndex        =   76
      Top             =   150
      Width           =   5475
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   35
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   34
         Left            =   3660
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   33
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   32
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   31
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   30
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   4200
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   29
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   28
         Left            =   3660
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   27
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   26
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   25
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   24
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   3420
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   17
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   16
         Left            =   3660
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   15
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   14
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   13
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   12
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   1860
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   23
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   22
         Left            =   3660
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   21
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   20
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   19
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   18
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   11
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   10
         Left            =   3660
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   9
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   8
         Left            =   1890
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   7
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   6
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   5
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   4
         Left            =   3660
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   3
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   2
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton btn_1 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Index           =   1
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   300
         Width           =   855
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1560
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":0614
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":12F0
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":1FCC
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":2CA8
            Key             =   "inter"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   840
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":2EF4
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":3BD0
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":48AC
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":5588
            Key             =   "inter"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_deshabilitado 
      Left            =   120
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":57D4
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":64B0
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":718C
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "digital.frx":7E68
            Key             =   "inter"
         EndProperty
      EndProperty
   End
   Begin VB.Menu boton 
      Caption         =   "Boton"
      Visible         =   0   'False
      Begin VB.Menu configurar_boton 
         Caption         =   "&Configurar"
      End
      Begin VB.Menu eliminar_boton 
         Caption         =   "&Eliminar"
      End
   End
   Begin VB.Menu subboton 
      Caption         =   "SubBoton"
      Visible         =   0   'False
      Begin VB.Menu configurar_subboton 
         Caption         =   "&Configurar"
      End
      Begin VB.Menu eliminar_subboton 
         Caption         =   "&Eliminar"
      End
   End
End
Attribute VB_Name = "digital"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub BARRA_PANEL1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            
        Case Is = "GRABAR"
            
        Case Is = "AYUDA"
    End Select
End Sub


Private Sub bcancelar_Click()
    Call cancelar
End Sub

Private Sub bgarbar_Click()
        If boton_producto.Text = "" And grp_btn.Value = 0 Then
            Call mensaje(True, "Debe seleccionar un producto o configurar un grupo...!")
            Exit Sub
        ElseIf grp_btn.Value = 1 And nombre_grupo.Text = "" Then
            Call mensaje(True, "Debe escribir un nombre para el grupo...!")
            Exit Sub
        End If
        Ent.POS.BeginTrans
            
            numero = CInt(Right(Trim(datos_boton), 2))
            '*******************************************************
            '*** busca el boyton en la temporal de botones
            '*******************************************************
            Call apertura_recordset(False, rsbotones)
            rsbotones.Open "select * from ma_botones_tmp where c_caja = '" & NCAJA & "' and n_boton = " & numero, Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rsbotones.EOF Then '------ EXISTE
                If rsbotones!B_GRUPO = True Then '------ ES GRUPO
                    btn_1(numero).Caption = nombre_grupo.Text
                    btn_1(numero).Tag = "Grupo"
                    btn_1(numero).ToolTipText = "Grupo"
                    If grp_btn.Value = 1 Then '--- VERIFICA SI SE MANTIENE EL GRUPO
                        '*** borrar los datos actuales
                        Ent.POS.Execute "delete tr_botones_tmp where c_caja = '" & codigo.Text & "' and n_boton = " & numero
                        
                        '*** abre la tabla tmp de subbotones
                        Call apertura_recordset(False, rstrbotones)
                        rstrbotones.Open "tr_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                        
                        '*** abre la tabla tmp DE TMP de subbotones
                        '***  actualiza la tr_botones_tmp
                        Call apertura_recordset(False, rstrbotones_TMP)
                        rstrbotones_TMP.Open "tr_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                        If Not rstrbotones_TMP.EOF Then
                            Do Until rstrbotones_TMP.EOF
                                rstrbotones.AddNew
                                    rstrbotones!c_codigo = rstrbotones_TMP!c_codigo
                                    rstrbotones!c_caja = rstrbotones_TMP!c_caja
                                    rstrbotones!n_boton = rstrbotones_TMP!n_boton
                                    rstrbotones!n_subboton = rstrbotones_TMP!n_subboton
                                rstrbotones.UpdateBatch
                                
                                rstrbotones_TMP.Delete
                                rstrbotones_TMP.UpdateBatch
                                rstrbotones_TMP.MoveNext
                            Loop
                        End If
                    Else
                        '************************************************
                        '****** ACTUALIZA EL BOTON
                        '************************************************
                        rsbotones.Update
                            rsbotones!B_GRUPO = 0
                            rsbotones!c_codigo = boton_producto
                        rsbotones.UpdateBatch
                        
                        btn_1(numero).Caption = boton_descri
                        btn_1(numero).Tag = boton_producto
                        btn_1(numero).ToolTipText = boton_descri
                        
                        '************************************************
                        '****** ELIMINA LOS SUB-BOTONES DEL GRUPO
                        '************************************************
                        Ent.POS.Execute "delete tr_botones_tmp where c_caja = '" & codigo.Text & "' and n_boton = " & numero
                        Ent.POS.Execute "delete tr_botones_tmp1"
                    End If
                Else '----- NO ES GRUPO
                    If grp_btn.Value = 1 Then
                        '*** actualizo el boton
                        rsbotones.Update
                            rsbotones!B_GRUPO = 1
                            rsbotones!c_codigo = boton_producto
                        rsbotones.UpdateBatch
                        
                        btn_1(numero).Caption = nombre_grupo.Text
                        btn_1(numero).Tag = "Grupo"
                        btn_1(numero).ToolTipText = "Grupo"
                        
                        '*** abre la tabla tmp de subbotones tmp
                        Call apertura_recordset(False, rstrbotones)
                        rstrbotones.Open "tr_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                        
                        '*** abre la tabla tmp tmp de subbotones tmp1
                        Call apertura_recordset(False, rstrbotones_TMP)
                        rstrbotones_TMP.Open "tr_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                        If Not rstrbotones_TMP.EOF Then
                            Do Until rstrbotones_TMP.EOF
                                rstrbotones.AddNew
                                    rstrbotones!c_codigo = rstrbotones_TMP!c_codigo
                                    rstrbotones!c_caja = rstrbotones_TMP!c_caja
                                    rstrbotones!n_boton = rstrbotones_TMP!n_boton
                                    rstrbotones!n_subboton = rstrbotones_TMP!n_subboton
                                rstrbotones.UpdateBatch
                                
                                rstrbotones.Delete
                                rstrbotones.UpdateBatch
                                rstrbotones.MoveNext
                            Loop
                        End If
                    
                    Else
                        rsbotones.Update
                            rsbotones!c_codigo = boton_producto
                        rsbotones.UpdateBatch
                        btn_1(numero).Caption = boton_descri.Caption
                        btn_1(numero).Tag = boton_producto.Text
                        btn_1(numero).ToolTipText = boton_descri.Caption
                    
                    End If
                End If
                '**** fin del boton encrontrado
            Else '**** else de la busqueda
                '******* comienzo del boton nuevo
                rsbotones.AddNew
                    rsbotones!c_codigo = boton_producto.Text
                    rsbotones!c_caja = NCAJA
                    rsbotones!B_GRUPO = grp_btn.Value
                    rsbotones!n_boton = numero
                    rsbotones!nombre_grupo = nombre_grupo.Text
                rsbotones.UpdateBatch
                
                If grp_btn.Value = 1 Then
                    btn_1(numero).Caption = nombre_grupo.Text
                    btn_1(numero).Tag = "Grupo"
                    btn_1(numero).ToolTipText = "Grupo"
                    Call apertura_recordset(False, rstrbotones)
                    rstrbotones.Open "tr_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    
                    Call apertura_recordset(False, rstrbotones_TMP)
                    rstrbotones_TMP.Open "TR_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    
                    If Not rstrbotones_TMP.EOF Then
                        Do Until rstrbotones_TMP.EOF
                            rstrbotones.AddNew
                                rstrbotones!c_codigo = rstrbotones_TMP!c_codigo
                                rstrbotones!c_caja = rstrbotones_TMP!c_caja
                                rstrbotones!n_boton = rstrbotones_TMP!n_boton
                                rstrbotones!n_subboton = rstrbotones_TMP!n_subboton
                            rstrbotones.UpdateBatch
                            
                            rstrbotones_TMP.Delete
                            rstrbotones_TMP.UpdateBatch
                            rstrbotones_TMP.MoveNext
                        Loop
                        
                    End If
                Else
                    btn_1(numero).Caption = boton_descri
                    btn_1(numero).Tag = boton_producto
                    btn_1(numero).ToolTipText = boton_descri
                    Call apertura_recordset(False, rstrbotones_TMP)
                    rstrbotones_TMP.Open "TR_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    If Not rstrbotones_TMP.EOF Then
                        Do Until rstrbotones_TMP.EOF
                            rstrbotones_TMP.Delete
                            rstrbotones_TMP.UpdateBatch
                            rstrbotones_TMP.MoveNext
                        Loop
                    End If
                
                End If
            End If
        Ent.POS.CommitTrans
        Call cancelar

End Sub

Private Sub boton_producto_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            KeyAscii = 0
            SendKeys Chr$(9)
    End Select
End Sub

Private Sub boton_producto_LostFocus()
    Call buscar_producto(boton_producto, boton_descri)
End Sub

Private Sub btn_1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = 93
            If btn_1(Index).Tag = "" Then
                eliminar_boton.Enabled = False
            Else
                eliminar_boton.Enabled = True
            End If
            datos_boton.Caption = " Datos del bot�n # " & Format(Index, "00")
            PopupMenu boton
            
    End Select
End Sub

Private Sub btn_1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Button
        Case Is = vbKeyRButton
            If btn_1(Index).Tag = "" Then
                eliminar_boton.Enabled = False
            Else
                eliminar_boton.Enabled = True
            End If
            datos_boton.Caption = " Datos del bot�n # " & Format(Index, "00")
            PopupMenu boton
    End Select
End Sub

Private Sub configurar_boton_Click()
    Dim RSPRODUCTOS As New ADODB.Recordset
'    barra_menu.Buttons(2).Enabled = False
    PANEL1.Enabled = False
    datos_boton.Enabled = True
    numero = CInt(Right(Trim(datos_boton.Caption), 2))
    If UCase(btn_1(numero).Tag) = "GRUPO" Then
        nombre_grupo.Text = btn_1(numero).Caption
        Call apertura_recordset(True, rstrbotones)
        rstrbotones.Open "select * from tr_botones_tmp where c_caja = '" & NCAJA & "' and n_boton = " & numero & " order by c_caja,n_boton,n_subBoton", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
        rstrbotones.ActiveConnection = Nothing
        
        If Not rstrbotones.EOF Then
            Call apertura_recordset(False, rstrbotones_TMP)
            rstrbotones_TMP.Open "tr_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
            Do Until rstrbotones.EOF
                Call apertura_recordset(True, RSPRODUCTOS)
                RSPRODUCTOS.Open "select c_descri from ma_productos where c_codigo = '" & rstrbotones!c_codigo & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                Set RSPRODUCTOS.ActiveConnection = Nothing
                sbtn_1(rstrbotones!n_subboton).Caption = RSPRODUCTOS!c_descri
                sbtn_1(rstrbotones!n_subboton).ToolTipText = RSPRODUCTOS!c_descri
                sbtn_1(rstrbotones!n_subboton).Tag = rstrbotones!c_codigo
                
                rstrbotones_TMP.AddNew
                    rstrbotones_TMP!c_codigo = rstrbotones!c_codigo
                    rstrbotones_TMP!c_caja = rstrbotones!c_caja
                    rstrbotones_TMP!n_boton = rstrbotones!n_boton
                    rstrbotones_TMP!n_subboton = rstrbotones!n_subboton
                rstrbotones_TMP.UpdateBatch
                rstrbotones.MoveNext
            Loop
            Call cerrar_recordset(RSPRODUCTOS)
            Call cerrar_recordset(rstrbotones)
            Call cerrar_recordset(rstrbotones_TMP)
        End If
        
        
        PANEL1.Enabled = False
        grp_btn.Value = 1
        boton_producto.Text = ""
        boton_descri.Caption = ""
    Else
        grp_btn.Value = 0
        boton_producto.Text = btn_1(numero).Tag
        boton_descri.Caption = btn_1(numero).ToolTipText
    End If
    
End Sub

Private Sub configurar_subboton_Click()
    numero = CInt(Right(Trim(datos_subboton), 2))
    datos_boton.Enabled = False
    datos_subboton.Enabled = True
    subboton_producto.Text = sbtn_1(numero).Tag
    subboton_descri.Caption = sbtn_1(numero).ToolTipText
End Sub

Private Sub eliminar_boton_Click()
    numero = CInt(Right(Trim(datos_boton), 2))
    Ent.POS.BeginTrans
        Call apertura_recordset(False, rstrbotones_TMP)
        rstrbotones_TMP.Open "SELECT * from tr_botones_tmp1 WHERE n_boton  = " & numero, Ent.POS, adOpenDynamic, adLockBatchOptimistic, ADCMDTETX
        If Not rstrbotones_TMP.EOF Then
            Do Until rstrbotones_TMP.EOF
                rstrbotones_TMP.Delete
                rstrbotones_TMP.UpdateBatch
                rstrbotones_TMP.MoveNext
            Loop
        End If
        
        Call apertura_recordset(False, rstrbotones_TMP)
        rstrbotones_TMP.Open "SELECT * from tr_botones_tmp WHERE n_boton = " & numero, Ent.POS, adOpenDynamic, adLockBatchOptimistic, ADCMDTETX
        If Not rstrbotones_TMP.EOF Then
            Do Until rstrbotones_TMP.EOF
                rstrbotones_TMP.Delete
                rstrbotones_TMP.UpdateBatch
                rstrbotones_TMP.MoveNext
            Loop
        End If
        
        Call apertura_recordset(False, rstrbotones_TMP)
        rstrbotones_TMP.Open "SELECT * from ma_botones_tmp WHERE n_boton = " & numero, Ent.POS, adOpenDynamic, adLockBatchOptimistic, ADCMDTETX
        If Not rstrbotones_TMP.EOF Then
            rstrbotones_TMP.Delete
            rstrbotones_TMP.UpdateBatch
        End If
        
    Ent.POS.CommitTrans
    btn_1(numero).Caption = ""
    btn_1(numero).Tag = ""
    btn_1(numero).ToolTipText = ""
    Call cerrar_recordset(rstrbotones_TMP)

End Sub
Private Sub eliminar_subboton_Click()
    numero = CInt(Right(Trim(datos_subboton), 2))
    NUMERO1 = CInt(Right(Trim(datos_boton), 2))
    Ent.POS.BeginTrans
        Call apertura_recordset(False, rstrbotones_TMP)
        rstrbotones_TMP.Open "SELECT * from tr_botones_tmp1 WHERE C_CAJA = '" & NCAJA & "' AND N_SUBBOTON = " & numero & " AND N_BOTON = " & NUMERO1, Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
        If Not rstrbotones_TMP.EOF Then
            rstrbotones_TMP.Delete
            rstrbotones_TMP.UpdateBatch
        End If
    Ent.POS.CommitTrans
        sbtn_1(numero).Caption = ""
        sbtn_1(numero).Tag = ""
        sbtn_1(numero).ToolTipText = ""
    Call cerrar_recordset(rstrbotones_TMP)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
        Select Case KeyCode
            Case Is = vbKeyC
                Call Form_KeyDown(vbKeyF7, 0)
            Case Is = vbKeyG
                Call Form_KeyDown(vbKeyF4, 0)
            Case Is = vbKeyA
        End Select
    Else
        Select Case KeyCode
            Case Is = vbKeyF7
                If Panel2.Enabled = True Then
                    bcancelar_Click
                End If
                Call CANCELAR_BOTONES
                Call CARGAR_BOTONES
                
            Case Is = vbKeyF4
                Call GRABAR
        End Select
    End If
    
End Sub

Private Sub Form_Load()
    Me.Caption = Me.Caption & " de la Caja No. " & NCAJA & " ..."
    Call CARGAR_BOTONES
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set digital = Nothing
End Sub

Private Sub grp_btn_Click()
    numero = CInt(Right(Trim(datos_boton), 2))
    Select Case grp_btn.Value
        Case Is = 0
            grp_btn.Caption = "No"
            nombre_grupo.Text = ""
            nombre_grupo.Enabled = False
            boton_producto.Text = IIf(btn_1(numero).Tag = "Grupo", "", btn_1(numero).Tag)
            boton_descri.Caption = IIf(btn_1(numero).Tag = "Grupo", "", btn_1(numero).ToolTipText)
            boton_producto.Enabled = True
            Panel2.Enabled = False
        Case Is = 1
            grp_btn.Caption = "Si"
            nombre_grupo.Text = btn_1(numero).Caption
            nombre_grupo.Enabled = True
            boton_producto.Text = ""
            boton_descri.Caption = ""
            boton_producto.Enabled = False
            Panel2.Enabled = True
    End Select
End Sub

Private Sub cancelar()
    grp_btn.Value = 0
    boton_producto.Text = ""
    boton_descri.Caption = ""
    datos_boton.Enabled = False
    PANEL1.Enabled = True
    datos_boton.Caption = " Datos del bot�n # "
    For i = 0 To 34
        sbtn_1(i).Caption = ""
        sbtn_1(i).ToolTipText = ""
        sbtn_1(i).Tag = ""
    Next
    Ent.POS.BeginTrans
        Call apertura_recordset(False, rstrbotones)
        rstrbotones.Open "tr_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
        If Not rstrbotones.EOF Then
            Do Until rstrbotones.EOF
                rstrbotones.Delete
                rstrbotones.UpdateBatch
                rstrbotones.MoveNext
            Loop
        End If
        Call cerrar_recordset(rstrbotones)
    Ent.POS.CommitTrans
End Sub

Private Sub sbtn_1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = 93
            If Index = 35 Then Exit Sub
            If sbtn_1(Index).Tag = "" Then
                eliminar_subboton.Enabled = False
            Else
                eliminar_subboton.Enabled = True
            End If
            datos_subboton.Caption = " Datos del bot�n # " & Format(Index, "00")
            PopupMenu subboton
        
    End Select
End Sub

Private Sub sbtn_1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Index = 35 Then Exit Sub
    If sbtn_1(Index).Tag = "" Then
        eliminar_subboton.Enabled = False
    Else
        eliminar_subboton.Enabled = True
    End If
    datos_subboton.Caption = " Datos del bot�n # " & Format(Index, "00")
    PopupMenu subboton
End Sub


Private Sub scancelar_Click()
            datos_subboton.Enabled = False
            datos_boton.Enabled = True
            Panel2.Enabled = True
            subboton_producto.Text = ""
            subboton_descri.Caption = ""

End Sub

Private Sub sgrabar_Click()
            If subboton_producto.Text = "" Then
                Call mensaje(True, "Debe configurar el bot�n seleccionando un producto...!")
                Exit Sub
            End If
            Call apertura_recordset(False, rstrbotones)
            rstrbotones.Open "select * from tr_botones_tmp1 where c_caja = '" & NCAJA & "' and n_boton = " & CInt(Right(Trim(datos_boton.Caption), 2)) & " and n_subboton = " & CInt(Right(Trim(datos_subboton.Caption), 2)), Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rstrbotones.EOF Then
                rstrbotones.Update
            Else
                rstrbotones.AddNew
                rstrbotones!c_caja = NCAJA
                rstrbotones!n_boton = CInt(Right(Trim(datos_boton), 2))
                rstrbotones!n_subboton = CInt(Right(Trim(datos_subboton), 2))
            End If
            rstrbotones!c_codigo = subboton_producto.Text
            rstrbotones.UpdateBatch
            sbtn_1(CInt(Right(Trim(datos_subboton), 2))).Caption = subboton_descri.Caption
            sbtn_1(CInt(Right(Trim(datos_subboton), 2))).Tag = subboton_producto.Text
            sbtn_1(CInt(Right(Trim(datos_subboton), 2))).ToolTipText = subboton_descri.Caption
            datos_subboton.Enabled = False
            datos_boton.Enabled = True
            Panel2.Enabled = True
            subboton_descri.Caption = ""
            subboton_producto.Text = ""

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
            
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF7, 0)
    End Select
End Sub


Private Sub GRABAR()
    Ent.POS.BeginTrans
        Call apertura_recordset(False, rsbotones_TMP)
        rsbotones_TMP.Open "ma_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
        
        Ent.POS.Execute ("delete ma_botones where c_caja = '" & NCAJA & "'")
        
        If Not rsbotones_TMP.EOF Then
            
            Call apertura_recordset(False, rstrbotones)
            rstrbotones.Open "ma_botones", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
            
            Do Until rsbotones_TMP.EOF
                rstrbotones.AddNew
                    rstrbotones!c_codigo = rsbotones_TMP!c_codigo
                    rstrbotones!c_caja = rsbotones_TMP!c_caja
                    rstrbotones!n_boton = rsbotones_TMP!n_boton
                    rstrbotones!B_GRUPO = rsbotones_TMP!B_GRUPO
                    rstrbotones!nombre_grupo = rsbotones_TMP!nombre_grupo
                    
                rstrbotones.UpdateBatch
                
                rsbotones_TMP.Delete
                rsbotones_TMP.UpdateBatch
                rsbotones_TMP.MoveNext
            Loop
        End If
                    
        Call apertura_recordset(False, rstrbotones_TMP)
        rstrbotones_TMP.Open "tr_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
        
        Ent.POS.Execute ("delete TR_botones where c_caja = '" & NCAJA & "'")
        
        If Not rstrbotones_TMP.EOF Then
            '*** abre la tabla tmp de subbotones
            Call apertura_recordset(False, rstrbotones)
            rstrbotones.Open "tr_botones", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
            Do Until rstrbotones_TMP.EOF
                rstrbotones.AddNew
                    rstrbotones!c_codigo = rstrbotones_TMP!c_codigo
                    rstrbotones!c_caja = rstrbotones_TMP!c_caja
                    rstrbotones!n_boton = rstrbotones_TMP!n_boton
                    rstrbotones!n_subboton = rstrbotones_TMP!n_subboton
                rstrbotones.UpdateBatch
                
                rstrbotones_TMP.Delete
                rstrbotones_TMP.UpdateBatch
                rstrbotones_TMP.MoveNext
            Loop
        End If
    Ent.POS.CommitTrans
    Unload Me
End Sub


Private Sub CANCELAR_BOTONES()
    Ent.POS.BeginTrans
        Call apertura_recordset(False, rsbotones)
        rsbotones.Open "MA_BOTONES_TMP", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
        If Not rsbotones.EOF Then
            Do Until rsbotones.EOF
                rsbotones.Delete
                rsbotones.UpdateBatch
                rsbotones.MoveNext
            Loop
        End If
        
        Call apertura_recordset(False, rsbotones)
        rsbotones.Open "TR_BOTONES_TMP", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
        If Not rsbotones.EOF Then
            Do Until rsbotones.EOF
                rsbotones.Delete
                rsbotones.UpdateBatch
                rsbotones.MoveNext
            Loop
        End If
        
        Call apertura_recordset(False, rsbotones)
        rsbotones.Open "TR_BOTONES_TMP1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
        If Not rsbotones.EOF Then
            Do Until rsbotones.EOF
                rsbotones.Delete
                rsbotones.UpdateBatch
                rsbotones.MoveNext
            Loop
        End If
    Ent.POS.CommitTrans

End Sub


Private Sub CARGAR_BOTONES()
            Ent.POS.BeginTrans
                Ent.POS.Execute "delete ma_botones_TMP"
                Ent.POS.Execute "delete tr_botones_TMP"
                Ent.POS.Execute "delete tr_botones_TMP1"
                For i = 0 To 35
                    btn_1(i).Caption = ""
                    btn_1(i).Tag = ""
                    btn_1(i).ToolTipText = ""
                    If i < 35 Then
                        sbtn_1(i).Tag = ""
                        sbtn_1(i).Caption = ""
                        sbtn_1(i).ToolTipText = ""
                    End If
                Next
                
                Call apertura_recordset(True, rsbotones)
                rsbotones.Open "select * from ma_botones where c_caja = '" & NCAJA & "' order by c_caja,n_boton", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
                Set rsbotones.ActiveConnection = Nothing
                
                Call apertura_recordset(False, rsbotones_TMP)
                rsbotones_TMP.Open "ma_botones_TMP", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                
                If Not rsbotones.EOF Then
                    Do Until rsbotones.EOF
                        rsbotones_TMP.AddNew
                            rsbotones_TMP!c_caja = rsbotones!c_caja
                            rsbotones_TMP!n_boton = rsbotones!n_boton
                            rsbotones_TMP!B_GRUPO = rsbotones!B_GRUPO
                            rsbotones_TMP!c_codigo = rsbotones!c_codigo
                            rsbotones_TMP!nombre_grupo = rsbotones!nombre_grupo
                        rsbotones_TMP.UpdateBatch
                        
                        If rsbotones!B_GRUPO = True Then
                            btn_1(rsbotones!n_boton).Caption = rsbotones!nombre_grupo
                            btn_1(rsbotones!n_boton).ToolTipText = rsbotones!nombre_grupo
                            btn_1(rsbotones!n_boton).Tag = "Grupo"
                        Else
                            Call apertura_recordset(True, RSPRODUCTOS)
                            RSPRODUCTOS.Open "select c_descri from ma_productos where c_codigo = '" & rsbotones!c_codigo & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            Set RSPRODUCTOS.ActiveConnection = Nothing
                            btn_1(rsbotones!n_boton).Caption = RSPRODUCTOS!c_descri
                            btn_1(rsbotones!n_boton).ToolTipText = RSPRODUCTOS!c_descri
                            btn_1(rsbotones!n_boton).Tag = rsbotones!c_codigo
                        End If
                        rsbotones.MoveNext
                    Loop
            
                    Call apertura_recordset(True, rsbotones)
                    rsbotones.Open "select * from tr_botones where c_caja = '" & NCAJA & "' order by c_caja,n_boton", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
                    Set rsbotones.ActiveConnection = Nothing
                    
                    Call apertura_recordset(False, rsbotones_TMP)
                    rsbotones_TMP.Open "tr_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    
                    If Not rsbotones.EOF Then
                        Do Until rsbotones.EOF
                            rsbotones_TMP.AddNew
                                rsbotones_TMP!c_codigo = rsbotones!c_codigo
                                rsbotones_TMP!c_caja = rsbotones!c_caja
                                rsbotones_TMP!n_boton = rsbotones!n_boton
                                rsbotones_TMP!n_subboton = rsbotones!n_subboton
                            rsbotones_TMP.UpdateBatch
                            rsbotones.MoveNext
                        Loop
                    End If
                End If
            Ent.POS.CommitTrans
            Call cerrar_recordset(RSPRODUCTOS)
            Call cerrar_recordset(rsbotones)
            Call cerrar_recordset(rsbotones_TMP)

End Sub




            

Private Sub subboton_producto_LostFocus()
    Call buscar_producto(subboton_producto, subboton_descri)
End Sub
Private Sub subboton_producto_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            KeyAscii = 0
            SendKeys Chr$(9)
    End Select
End Sub

Private Sub buscar_producto(campo_cod As Control, campo_des As Control)
    Dim codigo_prod As String
    Call apertura_recordset(True, rseureka)
    rseureka.Open "select * from MA_CODIGOS where c_codigo = '" & Trim(campo_cod) & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly
    Set rseureka.ActiveConnection = Nothing
        If Not rseureka.EOF Then
            codigo_prod = rseureka!c_codnasa
            Call cerrar_recordset(rseureka)
            Call apertura_recordset(True, rseureka)
            rseureka.Open "select c_codigo,c_descri,n_activo from MA_productos where c_codigo = '" & codigo_prod & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly
            Set rseureka.ActiveConnection = Nothing
            '///////
            If rseureka.EOF = True And rseureka.BOF = True Then
                Call mensaje(True, "No existe el producto...!")
                campo_cod = ""
                campo_cod.Caption = ""
                campo_cod.SetFocus
                Call cerrar_recordset(rseureka)
                Exit Sub
            End If
            '/////////

            If rseureka!n_activo = 0 Then
                boton_producto = ""
                Call mensaje(True, "El productos se encuentra suspendido")
                campo_cod = ""
'                campo_cod= ""
                campo_cod.SetFocus
                Exit Sub
            End If
            campo_des.Caption = rseureka!c_descri
            campo_cod.Text = rseureka!c_codigo

        Else
            Call mensaje(True, "No existe el producto...!")
            campo_cod = ""
            campo_cod.Text = ""
            campo_cod.SetFocus
            Call cerrar_recordset(rseureka)
        End If

End Sub

