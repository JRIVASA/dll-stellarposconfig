VERSION 5.00
Begin VB.Form Cambio_Password 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2805
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   7350
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "cambio_password.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   2805
   ScaleWidth      =   7350
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   360
      TabIndex        =   9
      Top             =   600
      Width           =   6615
      Begin VB.TextBox verify_password 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         IMEMode         =   3  'DISABLE
         Left            =   2670
         PasswordChar    =   "*"
         TabIndex        =   4
         ToolTipText     =   "Ingrese su Contraseņa de Seguridad"
         Top             =   1935
         Visible         =   0   'False
         Width           =   3615
      End
      Begin VB.TextBox new_password 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         IMEMode         =   3  'DISABLE
         Left            =   2670
         PasswordChar    =   "*"
         TabIndex        =   3
         ToolTipText     =   "Ingrese su Contraseņa de Seguridad"
         Top             =   1440
         Visible         =   0   'False
         Width           =   3615
      End
      Begin VB.TextBox txt_usuario 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2640
         TabIndex        =   0
         ToolTipText     =   "Ingrese su Nombre de Usuario Registrado"
         Top             =   120
         Width           =   3615
      End
      Begin VB.TextBox txt_contraseņa 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         IMEMode         =   3  'DISABLE
         Left            =   2640
         PasswordChar    =   "*"
         TabIndex        =   1
         ToolTipText     =   "Ingrese su Contraseņa de Seguridad"
         Top             =   600
         Width           =   3615
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Verificar Contraseņa:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   360
         TabIndex        =   13
         Top             =   1935
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Nueva Contraseņa              :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   390
         TabIndex        =   12
         Top             =   1455
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Contraseņa              :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   600
         Width           =   2055
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre de Usuario  :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   120
         Width           =   2055
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8355
         TabIndex        =   7
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.CommandButton cmd_cancelar 
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   3990
      TabIndex        =   2
      Top             =   2010
      Width           =   1635
   End
   Begin VB.CommandButton cmd_aceptar 
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   1740
      TabIndex        =   6
      Top             =   2010
      Width           =   1635
   End
   Begin VB.Image CmdTeclado 
      Height          =   600
      Left            =   6240
      MouseIcon       =   "cambio_password.frx":628A
      MousePointer    =   99  'Custom
      Picture         =   "cambio_password.frx":6594
      Stretch         =   -1  'True
      Top             =   1975
      Width           =   600
   End
End
Attribute VB_Name = "Cambio_Password"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Cmd_Aceptar_Click()
    If new_password = verify_password Then
        Call Apertura_Recordset(False, rsEureka)
        
        'rseureka.Open "select * from ma_usuarios where login_name = '" & txt_usuario & "'  and password = '" & txt_contraseņa.Text & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic
        rsEureka.Open "select * from ma_usuarios where login_name = '" & txt_usuario & "'  and password = '" & txt_contraseņa.Text & "'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic
        
        If Not rsEureka.EOF Then
            rsEureka.Update
            rsEureka!Password = new_password.Text
            rsEureka.UpdateBatch
            
            Unload Me
        Else
            Call Mensaje(True, "El Usuario no existe en el sistema.")
            rsEureka.Close
            Set rsEureka = Nothing
            new_password.SetFocus
        End If
    End If
End Sub

Private Sub cmd_cancelar_Click()
    If rsUsuarios.State = adStateOpen Then rsUsuarios.Close
    Set Cambio_Password = Nothing
    Unload Me
End Sub

Private Sub CmdTeclado_Click()
    
    Dim mCtl As Object
    
    If Not Me.ActiveControl Is Nothing Then
        If TypeOf Me.ActiveControl Is TextBox Then
            Set mCtl = Me.ActiveControl
        End If
    End If
    
    If mCtl Is Nothing Then Set mCtl = txt_usuario
    
    TecladoWindows mCtl
    
End Sub

Private Sub Form_Load()
    Me.lbl_Organizacion = "Cambio de Contraseņa."
    Me.Label1.Caption = stellar_mensaje(2, True) 'Usuario
    Me.Label2.Caption = stellar_mensaje(3, True) 'contraseņa
    
    Me.Label4.Caption = stellar_mensaje(10182, True) 'Nuevas contraseņa
    Me.Label3.Caption = stellar_mensaje(10183, True) 'Repita la contraseņa
    
    Me.cmd_Aceptar.Caption = stellar_mensaje(5, True)
    Me.cmd_Cancelar.Caption = stellar_mensaje(6, True)
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set Cambio_Password = Nothing
End Sub


Private Sub new_password_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        verify_password.Enabled = True
        verify_password.SetFocus
    End If
End Sub

Private Sub txt_contraseņa_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If txt_usuario = "" Then txt_usuario = " "
        If txt_contraseņa = "" Then txt_contraseņa = " "
        
        Call Apertura_Recordset(False, rsUsuarios)
        
        rsUsuarios.Open "select * from ma_usuarios where login_name = '" & txt_usuario & "' and password = '" & txt_contraseņa & "'", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic
        
        If Not rsUsuarios.EOF Then
            cmd_Aceptar.top = 3200
            cmd_Cancelar.top = 3200
            Cambio_Password.Height = 3945
            Me.Frame1.Height = 2415
            verify_password.Visible = True
            Label3.Visible = True
            Label4.Visible = True
            new_password.Visible = True
            new_password.SetFocus
        Else
            Call Mensaje(True, "El Usuario no existe en el sistema.")
            txt_usuario = ""
            txt_usuario.SetFocus
        End If
    End If
End Sub

Private Sub verify_password_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If new_password = verify_password Then
            cmd_Aceptar.Enabled = True
            cmd_Aceptar.SetFocus
        Else
            verify_password.SetFocus
        End If
    End If
End Sub
