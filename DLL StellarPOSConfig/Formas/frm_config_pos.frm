VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Begin VB.Form frm_config_pos 
   BorderStyle     =   0  'None
   ClientHeight    =   8970
   ClientLeft      =   15
   ClientTop       =   300
   ClientWidth     =   11970
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   1300
   Icon            =   "frm_config_pos.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   Picture         =   "frm_config_pos.frx":030A
   ScaleHeight     =   598
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   798
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.TreeView ARBOL 
      Height          =   6825
      HelpContextID   =   1300
      Left            =   150
      TabIndex        =   0
      Top             =   1170
      Width           =   3285
      _ExtentX        =   5794
      _ExtentY        =   12039
      _Version        =   393217
      Indentation     =   176
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "iconos"
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList iconos 
      Left            =   1950
      Top             =   -150
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   48
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":7EE6
            Key             =   "org"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":833A
            Key             =   "caja"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":A016
            Key             =   "reportes"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":A332
            Key             =   "pos"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":B386
            Key             =   "solicitud"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":C002
            Key             =   "subnivel"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":C456
            Key             =   "user"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":C56A
            Key             =   "compras"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":CBCE
            Key             =   "inventario"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":D1A2
            Key             =   "printer3"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":D4BE
            Key             =   "produccion"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":DAFE
            Key             =   "ventas"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":E1C2
            Key             =   "tesoreria"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":E7F6
            Key             =   "cxp"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":EE4A
            Key             =   "cxc"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":F4CE
            Key             =   "compra"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":106AA
            Key             =   "orden"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":10F3E
            Key             =   "fichero"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":11E1E
            Key             =   "estadistic"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":126D6
            Key             =   "recepcion"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":13372
            Key             =   "traslado"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1400E
            Key             =   "ajuste"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":149EA
            Key             =   "invfisico"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":15666
            Key             =   "transferen"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":15982
            Key             =   "manufactur"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":15C9E
            Key             =   "presupuest"
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":15FBA
            Key             =   "pedidos"
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":162D6
            Key             =   "notentrega"
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":165F2
            Key             =   "facturacio"
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1690E
            Key             =   "promocione"
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":16C2A
            Key             =   "ctlcaja"
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":16F46
            Key             =   "movimiento"
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":17262
            Key             =   "hablador"
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1757E
            Key             =   "listado"
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1789A
            Key             =   "listado1"
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":17BB6
            Key             =   "listado11"
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":17ED2
            Key             =   "listado10"
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":181EE
            Key             =   "listado2"
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1850A
            Key             =   "listado3"
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":18826
            Key             =   "listado4"
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":18B42
            Key             =   "listado5"
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":18E5E
            Key             =   "listado6"
         EndProperty
         BeginProperty ListImage43 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1917A
            Key             =   "listado7"
         EndProperty
         BeginProperty ListImage44 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":19496
            Key             =   "listado8"
         EndProperty
         BeginProperty ListImage45 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":197B2
            Key             =   "listado9"
         EndProperty
         BeginProperty ListImage46 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":19ACE
            Key             =   "Balanzas"
         EndProperty
         BeginProperty ListImage47 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":19DEA
            Key             =   "printer1"
         EndProperty
         BeginProperty ListImage48 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1A106
            Key             =   "printer2"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab FICHAS 
      CausesValidation=   0   'False
      Height          =   5895
      HelpContextID   =   1300
      Left            =   3450
      TabIndex        =   1
      Top             =   1200
      Width           =   8445
      _ExtentX        =   14896
      _ExtentY        =   10398
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      Tab             =   4
      TabsPerRow      =   5
      TabHeight       =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&General"
      TabPicture(0)   =   "frm_config_pos.frx":1A422
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fdescripcion"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Botones"
      TabPicture(1)   =   "frm_config_pos.frx":1A43E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fconfig"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Dispositivos de &Entrada"
      TabPicture(2)   =   "frm_config_pos.frx":1A45A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FBALANZA"
      Tab(2).Control(1)=   "FSCANNER"
      Tab(2).Control(2)=   "fbanda"
      Tab(2).Control(3)=   "fllaves"
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "&Dispositivos de Salida"
      TabPicture(3)   =   "frm_config_pos.frx":1A476
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FGAVETA"
      Tab(3).Control(1)=   "FDISPLAY"
      Tab(3).Control(2)=   "fimpresora"
      Tab(3).ControlCount=   3
      TabCaption(4)   =   "Configuraci�n Digital"
      TabPicture(4)   =   "frm_config_pos.frx":1A492
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "datos_subboton"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "datos_boton"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "Panel2"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).Control(3)=   "PANEL1"
      Tab(4).Control(3).Enabled=   0   'False
      Tab(4).ControlCount=   4
      Begin VB.Frame fconfig 
         Caption         =   "Botones"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   5175
         Left            =   -74670
         TabIndex        =   256
         Top             =   510
         Width           =   7185
         Begin VB.ComboBox cellave 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A4AE
            Left            =   5820
            List            =   "frm_config_pos.frx":1A4C1
            TabIndex        =   315
            Text            =   "Combo1"
            Top             =   4740
            Width           =   705
         End
         Begin VB.ComboBox CLLLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A4D4
            Left            =   5820
            List            =   "frm_config_pos.frx":1A4E7
            TabIndex        =   312
            Text            =   "Combo1"
            Top             =   4410
            Width           =   705
         End
         Begin VB.ComboBox PRLLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A4FA
            Left            =   5820
            List            =   "frm_config_pos.frx":1A50D
            TabIndex        =   311
            Text            =   "Combo1"
            Top             =   4080
            Width           =   705
         End
         Begin VB.ComboBox DLLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A520
            Left            =   5820
            List            =   "frm_config_pos.frx":1A533
            TabIndex        =   310
            Text            =   "Combo1"
            Top             =   3750
            Width           =   705
         End
         Begin VB.ComboBox SLLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A546
            Left            =   5820
            List            =   "frm_config_pos.frx":1A559
            TabIndex        =   309
            Text            =   "Combo1"
            Top             =   3420
            Width           =   705
         End
         Begin VB.ComboBox ELLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A56C
            Left            =   5820
            List            =   "frm_config_pos.frx":1A57F
            TabIndex        =   308
            Text            =   "Combo1"
            Top             =   3075
            Width           =   705
         End
         Begin VB.ComboBox RLLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A592
            Left            =   5820
            List            =   "frm_config_pos.frx":1A5A5
            TabIndex        =   307
            Text            =   "Combo1"
            Top             =   2745
            Width           =   705
         End
         Begin VB.ComboBox BLLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A5B8
            Left            =   5820
            List            =   "frm_config_pos.frx":1A5CB
            TabIndex        =   306
            Text            =   "Combo1"
            Top             =   2430
            Width           =   705
         End
         Begin VB.ComboBox CLLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A5DE
            Left            =   5820
            List            =   "frm_config_pos.frx":1A5F1
            TabIndex        =   305
            Text            =   "Combo1"
            Top             =   2100
            Width           =   705
         End
         Begin VB.ComboBox LLLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A604
            Left            =   5820
            List            =   "frm_config_pos.frx":1A617
            TabIndex        =   304
            Text            =   "Combo1"
            Top             =   1770
            Width           =   705
         End
         Begin VB.ComboBox PLLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A62A
            Left            =   5820
            List            =   "frm_config_pos.frx":1A63D
            TabIndex        =   303
            Text            =   "Combo1"
            Top             =   1440
            Width           =   705
         End
         Begin VB.ComboBox ALLAVE 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A650
            Left            =   5820
            List            =   "frm_config_pos.frx":1A663
            TabIndex        =   302
            Text            =   "Combo1"
            Top             =   1110
            Width           =   705
         End
         Begin VB.CheckBox espera 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   280
            Top             =   3135
            Width           =   615
         End
         Begin VB.CheckBox cantidad 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   279
            Top             =   2160
            Width           =   615
         End
         Begin VB.CheckBox precios 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   278
            Top             =   4140
            Width           =   615
         End
         Begin VB.CheckBox productos 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   277
            Top             =   1500
            Width           =   615
         End
         Begin VB.CheckBox reintegro 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   276
            Top             =   2805
            Width           =   615
         End
         Begin VB.CheckBox devolucion 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   275
            Top             =   3810
            Width           =   615
         End
         Begin VB.CheckBox limite 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   274
            Top             =   1830
            Width           =   615
         End
         Begin VB.CheckBox cliente 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   273
            Top             =   4470
            Width           =   615
         End
         Begin VB.CheckBox ayuda 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   272
            Top             =   1155
            Width           =   615
         End
         Begin VB.CheckBox balanzas 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   271
            Top             =   2490
            Width           =   615
         End
         Begin VB.CheckBox suspender 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3510
            TabIndex        =   270
            Top             =   3450
            Width           =   615
         End
         Begin VB.ComboBox ANIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A676
            Left            =   4440
            List            =   "frm_config_pos.frx":1A698
            TabIndex        =   269
            Text            =   "Combo1"
            Top             =   1110
            Width           =   705
         End
         Begin VB.ComboBox PNIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A6BA
            Left            =   4440
            List            =   "frm_config_pos.frx":1A6DC
            TabIndex        =   268
            Text            =   "Combo1"
            Top             =   1440
            Width           =   705
         End
         Begin VB.ComboBox LNIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A6FE
            Left            =   4440
            List            =   "frm_config_pos.frx":1A720
            TabIndex        =   267
            Text            =   "Combo1"
            Top             =   1770
            Width           =   705
         End
         Begin VB.ComboBox CNIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A742
            Left            =   4440
            List            =   "frm_config_pos.frx":1A764
            TabIndex        =   266
            Text            =   "Combo1"
            Top             =   2100
            Width           =   705
         End
         Begin VB.ComboBox BNIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A786
            Left            =   4440
            List            =   "frm_config_pos.frx":1A7A8
            TabIndex        =   265
            Text            =   "Combo1"
            Top             =   2430
            Width           =   705
         End
         Begin VB.ComboBox RNIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A7CA
            Left            =   4440
            List            =   "frm_config_pos.frx":1A7EC
            TabIndex        =   264
            Text            =   "Combo1"
            Top             =   2745
            Width           =   705
         End
         Begin VB.ComboBox ENIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A80E
            Left            =   4440
            List            =   "frm_config_pos.frx":1A830
            TabIndex        =   263
            Text            =   "Combo1"
            Top             =   3075
            Width           =   705
         End
         Begin VB.ComboBox SNIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A852
            Left            =   4440
            List            =   "frm_config_pos.frx":1A874
            TabIndex        =   262
            Text            =   "Combo1"
            Top             =   3420
            Width           =   705
         End
         Begin VB.ComboBox DNIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A896
            Left            =   4440
            List            =   "frm_config_pos.frx":1A8B8
            TabIndex        =   261
            Text            =   "Combo1"
            Top             =   3750
            Width           =   705
         End
         Begin VB.ComboBox PRNIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A8DA
            Left            =   4440
            List            =   "frm_config_pos.frx":1A8FC
            TabIndex        =   260
            Text            =   "Combo1"
            Top             =   4080
            Width           =   705
         End
         Begin VB.ComboBox CLNIVEL 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A91E
            Left            =   4440
            List            =   "frm_config_pos.frx":1A940
            TabIndex        =   259
            Text            =   "Combo1"
            Top             =   4410
            Width           =   705
         End
         Begin VB.ComboBox nopen 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            IntegralHeight  =   0   'False
            ItemData        =   "frm_config_pos.frx":1A962
            Left            =   4440
            List            =   "frm_config_pos.frx":1A984
            TabIndex        =   258
            Text            =   "Combo1"
            Top             =   780
            Width           =   705
         End
         Begin VB.ComboBox nclose 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frm_config_pos.frx":1A9A6
            Left            =   4440
            List            =   "frm_config_pos.frx":1A9C8
            TabIndex        =   257
            Text            =   "Combo1"
            Top             =   4740
            Width           =   705
         End
         Begin VB.Label Label67 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "LLAVE"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000002&
            Height          =   195
            Left            =   5910
            TabIndex        =   314
            Top             =   420
            Width           =   525
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "POSICION"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000002&
            Height          =   195
            Left            =   5700
            TabIndex        =   313
            Top             =   270
            Width           =   900
         End
         Begin VB.Label Label19 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Compras en Espera"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   297
            Top             =   3135
            Width           =   1695
         End
         Begin VB.Label Label20 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Devoluciones"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   296
            Top             =   3810
            Width           =   1140
         End
         Begin VB.Label Label21 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "L�mite de Compra"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   295
            Top             =   1830
            Width           =   1530
         End
         Begin VB.Label Label22 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Cantidad de Productos"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   294
            Top             =   2160
            Width           =   1935
         End
         Begin VB.Label Label23 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Consultar Productos"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   293
            Top             =   1500
            Width           =   1725
         End
         Begin VB.Label Label24 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Consulta de Precios"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   292
            Top             =   4140
            Width           =   1695
         End
         Begin VB.Label Label25 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Reintegro"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   540
            TabIndex        =   291
            Top             =   2805
            Width           =   825
         End
         Begin VB.Label Label26 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Cliente Frecuente"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   290
            Top             =   4470
            Width           =   1500
         End
         Begin VB.Label Label28 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ayuda"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   289
            Top             =   1155
            Width           =   540
         End
         Begin VB.Label Label30 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Balanzas"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   288
            Top             =   2490
            Width           =   765
         End
         Begin VB.Label Label32 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Suspender"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   287
            Top             =   3450
            Width           =   915
         End
         Begin VB.Label Label33 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "ACTIVAR"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000002&
            Height          =   195
            Left            =   3300
            TabIndex        =   286
            Top             =   270
            Width           =   795
         End
         Begin VB.Label Label35 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "CONTRASE�A"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000002&
            Height          =   195
            Left            =   4260
            TabIndex        =   285
            Top             =   270
            Width           =   1200
         End
         Begin VB.Label Label36 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "NIVEL"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000002&
            Height          =   195
            Left            =   4530
            TabIndex        =   284
            Top             =   420
            Width           =   510
         End
         Begin VB.Label Label37 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "ACCION"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000002&
            Height          =   195
            Left            =   510
            TabIndex        =   283
            Top             =   270
            Width           =   720
         End
         Begin VB.Label Label38 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Apertura de Caja"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   282
            Top             =   810
            Width           =   1485
         End
         Begin VB.Label Label39 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Cierre de Caja"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   510
            TabIndex        =   281
            Top             =   4800
            Width           =   1275
         End
      End
      Begin VB.Frame fllaves 
         Caption         =   "Llaves de Seguridad"
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   795
         Left            =   -74910
         TabIndex        =   192
         Top             =   4650
         Width           =   8175
         Begin VB.OptionButton oposllaves 
            Caption         =   "OLE POS"
            Height          =   195
            Left            =   765
            TabIndex        =   243
            Top             =   390
            Width           =   1155
         End
         Begin VB.OptionButton nollaves 
            Caption         =   "No"
            Height          =   195
            Left            =   180
            TabIndex        =   242
            Top             =   390
            Width           =   555
         End
         Begin VB.Frame foposl 
            ClipControls    =   0   'False
            Height          =   555
            Left            =   2040
            TabIndex        =   193
            Top             =   180
            Width           =   6045
            Begin VB.TextBox oposl 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1200
               MaxLength       =   50
               TabIndex        =   194
               Top             =   180
               Width           =   4725
            End
            Begin VB.Label Label45 
               Caption         =   "OPOS Llaves"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   60
               TabIndex        =   195
               Top             =   210
               Width           =   2865
            End
         End
      End
      Begin VB.Frame fdescripcion 
         Enabled         =   0   'False
         Height          =   5205
         Left            =   -74700
         TabIndex        =   122
         Top             =   550
         Width           =   7815
         Begin VB.CheckBox cordenar 
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3180
            TabIndex        =   220
            Top             =   2850
            Width           =   495
         End
         Begin VB.CheckBox cagrupar 
            Caption         =   "Si"
            Height          =   195
            Left            =   6840
            TabIndex        =   219
            Top             =   2850
            Width           =   495
         End
         Begin VB.CheckBox forma_continua 
            Alignment       =   1  'Right Justify
            Caption         =   "Papel de Forma Continua"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   270
            TabIndex        =   218
            Top             =   2460
            Width           =   3105
         End
         Begin VB.TextBox no_lineas 
            Alignment       =   1  'Right Justify
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   6810
            Locked          =   -1  'True
            TabIndex        =   217
            Text            =   "33"
            Top             =   2460
            Width           =   435
         End
         Begin VB.TextBox linea 
            BackColor       =   &H80000018&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   1770
            MaxLength       =   30
            TabIndex        =   216
            Top             =   3180
            Width           =   5805
         End
         Begin VB.TextBox linea 
            BackColor       =   &H80000018&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   1770
            MaxLength       =   30
            TabIndex        =   215
            Top             =   3510
            Width           =   5805
         End
         Begin VB.TextBox linea 
            BackColor       =   &H80000018&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   1770
            MaxLength       =   30
            TabIndex        =   214
            Top             =   3840
            Width           =   5805
         End
         Begin VB.TextBox linea 
            BackColor       =   &H80000018&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   1770
            MaxLength       =   30
            TabIndex        =   213
            Top             =   4170
            Width           =   5805
         End
         Begin VB.TextBox linea 
            BackColor       =   &H80000018&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   1770
            MaxLength       =   30
            TabIndex        =   212
            Top             =   4500
            Width           =   5805
         End
         Begin VB.CheckBox opos 
            Caption         =   "No"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3690
            TabIndex        =   133
            Top             =   870
            Width           =   555
         End
         Begin VB.TextBox codigo 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1050
            MaxLength       =   3
            TabIndex        =   2
            Text            =   "1"
            Top             =   180
            Width           =   525
         End
         Begin VB.TextBox descri 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3510
            MaxLength       =   50
            TabIndex        =   3
            Top             =   180
            Width           =   4245
         End
         Begin VB.CheckBox dato_numerico 
            Caption         =   "No"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3690
            TabIndex        =   5
            Top             =   1170
            Width           =   555
         End
         Begin VB.CheckBox fondo_pos 
            Caption         =   "No"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3690
            TabIndex        =   6
            Top             =   1470
            Width           =   555
         End
         Begin VB.TextBox coddeposito 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3240
            TabIndex        =   7
            Top             =   1770
            Width           =   1305
         End
         Begin VB.CommandButton view 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4560
            TabIndex        =   8
            Top             =   1770
            Width           =   315
         End
         Begin VB.TextBox lbl_deposito 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4890
            Locked          =   -1  'True
            TabIndex        =   123
            TabStop         =   0   'False
            Top             =   1770
            Width           =   2865
         End
         Begin VB.CheckBox lineal_digital 
            Caption         =   "No"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3690
            TabIndex        =   4
            Top             =   570
            Width           =   555
         End
         Begin ComCtl2.UpDown uplineas 
            Height          =   315
            Left            =   7245
            TabIndex        =   221
            Top             =   2460
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   556
            _Version        =   327681
            Value           =   10
            OrigLeft        =   3540
            OrigTop         =   690
            OrigRight       =   3780
            OrigBottom      =   1035
            Max             =   100
            Min             =   10
            SyncBuddy       =   -1  'True
            Wrap            =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   0   'False
         End
         Begin VB.Label Label29 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ordenar Productos"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   270
            TabIndex        =   229
            Top             =   2850
            Width           =   1605
         End
         Begin VB.Label Label27 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Agrupar Productos"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3930
            TabIndex        =   228
            Top             =   2850
            Width           =   1590
         End
         Begin VB.Label Label58 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "N�mero de L�neas"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3930
            TabIndex        =   227
            Top             =   2520
            Width           =   1545
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Comentario  1"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   270
            TabIndex        =   226
            Top             =   3225
            Width           =   1230
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Comentario 2"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   270
            TabIndex        =   225
            Top             =   3555
            Width           =   1170
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Comentario 3"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   270
            TabIndex        =   224
            Top             =   3885
            Width           =   1170
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Comentario 4"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   270
            TabIndex        =   223
            Top             =   4215
            Width           =   1170
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Comentario 5"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   270
            TabIndex        =   222
            Top             =   4545
            Width           =   1170
         End
         Begin VB.Label Label57 
            AutoSize        =   -1  'True
            Caption         =   "Si"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3450
            TabIndex        =   211
            Top             =   2520
            Width           =   165
         End
         Begin VB.Label Label61 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Standard OLE POS "
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   270
            TabIndex        =   134
            Top             =   870
            Width           =   1650
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   270
            TabIndex        =   129
            Top             =   225
            Width           =   600
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2280
            TabIndex        =   128
            Top             =   225
            Width           =   990
         End
         Begin VB.Label Label56 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "C�digo del Producto Alfan�merico"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   270
            TabIndex        =   127
            Top             =   1170
            Width           =   2910
         End
         Begin VB.Label Label34 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Solicitar Fondo al Abrir Punto de Venta"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   270
            TabIndex        =   126
            Top             =   1470
            Width           =   3315
         End
         Begin VB.Label Label59 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Dep�sito para Descargar Ventas"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   300
            TabIndex        =   125
            Top             =   1800
            Width           =   2790
         End
         Begin VB.Label Label60 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "POS Digital"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   270
            TabIndex        =   124
            Top             =   570
            Width           =   960
         End
      End
      Begin VB.Frame fimpresora 
         Caption         =   " Impresora "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   2115
         Left            =   -74880
         TabIndex        =   119
         Top             =   460
         Width           =   8175
         Begin VB.OptionButton serialimpresora 
            Caption         =   "Serial"
            Height          =   195
            Left            =   2070
            TabIndex        =   255
            Top             =   330
            Width           =   705
         End
         Begin VB.OptionButton noimpresora 
            Caption         =   "Paralela"
            Height          =   195
            Left            =   120
            TabIndex        =   254
            Top             =   330
            Width           =   885
         End
         Begin VB.OptionButton oposimpresora 
            Caption         =   "OLE POS"
            Height          =   195
            Left            =   1020
            TabIndex        =   253
            Top             =   330
            Width           =   1035
         End
         Begin VB.Frame foposi 
            ClipControls    =   0   'False
            Height          =   825
            Left            =   2820
            TabIndex        =   250
            Top             =   120
            Width           =   5265
            Begin VB.ComboBox Rotar 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frm_config_pos.frx":1A9EA
               Left            =   3420
               List            =   "frm_config_pos.frx":1A9FA
               Style           =   2  'Dropdown List
               TabIndex        =   299
               Top             =   480
               Width           =   1005
            End
            Begin VB.CheckBox cheques 
               Caption         =   "No Imprime Cheques"
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   90
               TabIndex        =   298
               Top             =   480
               Width           =   2145
            End
            Begin VB.TextBox oposi 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1560
               MaxLength       =   50
               TabIndex        =   251
               Top             =   150
               Width           =   3615
            End
            Begin VB.Label Label65 
               BackStyle       =   0  'Transparent
               Caption         =   "Grados"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   4470
               TabIndex        =   301
               Top             =   540
               Width           =   645
            End
            Begin VB.Label Label65 
               BackStyle       =   0  'Transparent
               Caption         =   "Rotaci�n"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   2580
               TabIndex        =   300
               Top             =   540
               Width           =   735
            End
            Begin VB.Label Label65 
               Caption         =   "OPOS Impresora"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   60
               TabIndex        =   252
               Top             =   180
               Width           =   1545
            End
         End
         Begin VB.Frame fserialimpresora 
            ClipControls    =   0   'False
            Height          =   675
            Left            =   90
            TabIndex        =   200
            Top             =   870
            Width           =   7995
            Begin VB.ComboBox IPuerto 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AA0F
               Left            =   150
               List            =   "frm_config_pos.frx":1AA1F
               Style           =   2  'Dropdown List
               TabIndex        =   205
               Top             =   330
               Width           =   1485
            End
            Begin VB.ComboBox IBaudios 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AA3B
               Left            =   1710
               List            =   "frm_config_pos.frx":1AA51
               Style           =   2  'Dropdown List
               TabIndex        =   204
               Top             =   330
               Width           =   1485
            End
            Begin VB.ComboBox IParidad 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AA7C
               Left            =   3270
               List            =   "frm_config_pos.frx":1AA89
               Style           =   2  'Dropdown List
               TabIndex        =   203
               Top             =   330
               Width           =   1485
            End
            Begin VB.ComboBox IDato 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AAA2
               Left            =   4830
               List            =   "frm_config_pos.frx":1AAB5
               Style           =   2  'Dropdown List
               TabIndex        =   202
               Top             =   330
               Width           =   1485
            End
            Begin VB.ComboBox IParada 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AAC8
               Left            =   6390
               List            =   "frm_config_pos.frx":1AAD5
               Style           =   2  'Dropdown List
               TabIndex        =   201
               Top             =   330
               Width           =   1485
            End
            Begin VB.Label Label51 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Puerto"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   150
               TabIndex        =   210
               Top             =   150
               Width           =   555
            End
            Begin VB.Label Label52 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Baudios"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   1710
               TabIndex        =   209
               Top             =   150
               Width           =   675
            End
            Begin VB.Label Label53 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Paridad"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   3270
               TabIndex        =   208
               Top             =   150
               Width           =   645
            End
            Begin VB.Label Label54 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Dato"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   4830
               TabIndex        =   207
               Top             =   150
               Width           =   1050
            End
            Begin VB.Label Label55 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Parada"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   6390
               TabIndex        =   206
               Top             =   150
               Width           =   1245
            End
         End
         Begin VB.Frame fhojilla 
            ClipControls    =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000002&
            Height          =   585
            Left            =   90
            TabIndex        =   120
            Top             =   1470
            Width           =   7995
            Begin VB.CheckBox hsi 
               Caption         =   "No utilizo Hojilla para el corte del papel "
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   120
               TabIndex        =   199
               Top             =   210
               Width           =   3705
            End
            Begin VB.TextBox hojilla_caracter 
               CausesValidation=   0   'False
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   6300
               MaxLength       =   60
               TabIndex        =   9
               Top             =   180
               Width           =   1515
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Car�cter de Control "
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   4500
               TabIndex        =   121
               Top             =   240
               Width           =   1770
            End
         End
      End
      Begin VB.Frame FDISPLAY 
         Caption         =   " Display"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   1395
         Left            =   -74880
         TabIndex        =   108
         Top             =   4410
         Width           =   8175
         Begin VB.OptionButton OPOSDISPLAY 
            Caption         =   "OLE POS"
            Height          =   195
            Left            =   825
            TabIndex        =   249
            Top             =   360
            Width           =   1155
         End
         Begin VB.OptionButton NODISPLAY 
            Caption         =   "No"
            Height          =   195
            Left            =   210
            TabIndex        =   248
            Top             =   360
            Width           =   555
         End
         Begin VB.OptionButton SERIALDISPLAY 
            Caption         =   "Serial"
            Height          =   195
            Left            =   2040
            TabIndex        =   247
            Top             =   360
            Width           =   765
         End
         Begin VB.Frame FOPOSD 
            ClipControls    =   0   'False
            Height          =   555
            Left            =   2880
            TabIndex        =   189
            Top             =   180
            Width           =   5205
            Begin VB.TextBox OPOSD 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1380
               MaxLength       =   50
               TabIndex        =   190
               Top             =   180
               Width           =   3735
            End
            Begin VB.Label Label44 
               Caption         =   "OPOS Display"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   60
               TabIndex        =   191
               Top             =   210
               Width           =   1275
            End
         End
         Begin VB.Frame FSERIALD 
            ClipControls    =   0   'False
            Height          =   675
            Left            =   90
            TabIndex        =   178
            Top             =   660
            Width           =   7995
            Begin VB.ComboBox DPuerto 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AAE4
               Left            =   180
               List            =   "frm_config_pos.frx":1AAF4
               Style           =   2  'Dropdown List
               TabIndex        =   183
               Top             =   300
               Width           =   1395
            End
            Begin VB.ComboBox DBaudios 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AB10
               Left            =   1740
               List            =   "frm_config_pos.frx":1AB26
               Style           =   2  'Dropdown List
               TabIndex        =   182
               Top             =   300
               Width           =   1395
            End
            Begin VB.ComboBox DParidad 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AB51
               Left            =   3300
               List            =   "frm_config_pos.frx":1AB5E
               Style           =   2  'Dropdown List
               TabIndex        =   181
               Top             =   300
               Width           =   1395
            End
            Begin VB.ComboBox DDato 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AB77
               Left            =   4860
               List            =   "frm_config_pos.frx":1AB8A
               Style           =   2  'Dropdown List
               TabIndex        =   180
               Top             =   300
               Width           =   1395
            End
            Begin VB.ComboBox DParada 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AB9D
               Left            =   6420
               List            =   "frm_config_pos.frx":1ABAA
               Style           =   2  'Dropdown List
               TabIndex        =   179
               Top             =   300
               Width           =   1395
            End
            Begin VB.Label Label31 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Puerto"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   180
               TabIndex        =   188
               Top             =   120
               Width           =   555
            End
            Begin VB.Label Label40 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Baudios"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   1740
               TabIndex        =   187
               Top             =   120
               Width           =   675
            End
            Begin VB.Label Label41 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Paridad"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   3300
               TabIndex        =   186
               Top             =   120
               Width           =   645
            End
            Begin VB.Label Label42 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Dato"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   4860
               TabIndex        =   185
               Top             =   120
               Width           =   1050
            End
            Begin VB.Label Label43 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Parada"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   6450
               TabIndex        =   184
               Top             =   120
               Width           =   1245
            End
         End
      End
      Begin VB.Frame fbanda 
         Caption         =   " Lector de Banda Magn�tica "
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   1425
         Left            =   -74910
         TabIndex        =   107
         Top             =   3240
         Width           =   8175
         Begin VB.OptionButton serialbanda 
            Caption         =   "Serial"
            Height          =   195
            Left            =   2010
            TabIndex        =   241
            Top             =   360
            Width           =   765
         End
         Begin VB.OptionButton nobanda 
            Caption         =   "No"
            Height          =   195
            Left            =   240
            TabIndex        =   240
            Top             =   360
            Width           =   555
         End
         Begin VB.OptionButton Oposbanda 
            Caption         =   "OLE POS"
            Height          =   195
            Left            =   825
            TabIndex        =   239
            Top             =   360
            Width           =   1155
         End
         Begin VB.Frame foposb 
            ClipControls    =   0   'False
            Height          =   555
            Left            =   2880
            TabIndex        =   175
            Top             =   180
            Width           =   5205
            Begin VB.TextBox oposb 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1200
               MaxLength       =   50
               TabIndex        =   176
               Top             =   180
               Width           =   3915
            End
            Begin VB.Label Label64 
               Caption         =   "OPOS Banda"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   60
               TabIndex        =   177
               Top             =   210
               Width           =   2865
            End
         End
         Begin VB.Frame fserialb 
            ClipControls    =   0   'False
            Height          =   705
            Left            =   90
            TabIndex        =   164
            Top             =   660
            Width           =   7995
            Begin VB.ComboBox bmpuerto 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1ABB9
               Left            =   150
               List            =   "frm_config_pos.frx":1ABC9
               Style           =   2  'Dropdown List
               TabIndex        =   169
               Top             =   330
               Width           =   1485
            End
            Begin VB.ComboBox bmbaudios 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1ABE5
               Left            =   1710
               List            =   "frm_config_pos.frx":1ABFB
               Style           =   2  'Dropdown List
               TabIndex        =   168
               Top             =   330
               Width           =   1485
            End
            Begin VB.ComboBox bmparidad 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AC26
               Left            =   3270
               List            =   "frm_config_pos.frx":1AC33
               Style           =   2  'Dropdown List
               TabIndex        =   167
               Top             =   330
               Width           =   1485
            End
            Begin VB.ComboBox bmdato 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AC4C
               Left            =   4830
               List            =   "frm_config_pos.frx":1AC5F
               Style           =   2  'Dropdown List
               TabIndex        =   166
               Top             =   330
               Width           =   1485
            End
            Begin VB.ComboBox bmparada 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AC72
               Left            =   6390
               List            =   "frm_config_pos.frx":1AC7F
               Style           =   2  'Dropdown List
               TabIndex        =   165
               Top             =   330
               Width           =   1485
            End
            Begin VB.Label Label46 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Puerto"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   180
               Left            =   150
               TabIndex        =   174
               Top             =   150
               Width           =   555
            End
            Begin VB.Label Label47 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Baudios"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   180
               Left            =   1740
               TabIndex        =   173
               Top             =   150
               Width           =   675
            End
            Begin VB.Label Label48 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Paridad"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   180
               Left            =   3300
               TabIndex        =   172
               Top             =   150
               Width           =   645
            End
            Begin VB.Label Label49 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Dato"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   180
               Left            =   4860
               TabIndex        =   171
               Top             =   150
               Width           =   1050
            End
            Begin VB.Label Label50 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Parada"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   180
               Left            =   6390
               TabIndex        =   170
               Top             =   150
               Width           =   1245
            End
         End
      End
      Begin VB.Frame FSCANNER 
         Caption         =   " Scanner  "
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   1425
         Left            =   -74910
         TabIndex        =   106
         Top             =   405
         Width           =   8175
         Begin VB.OptionButton serialscanner 
            Caption         =   "Serial"
            Height          =   195
            Left            =   1980
            TabIndex        =   235
            Top             =   390
            Width           =   765
         End
         Begin VB.OptionButton noscanner 
            Caption         =   "No"
            Height          =   195
            Left            =   210
            TabIndex        =   234
            Top             =   390
            Width           =   555
         End
         Begin VB.OptionButton Oposscanner 
            Caption         =   "OLE POS"
            CausesValidation=   0   'False
            Enabled         =   0   'False
            Height          =   195
            Left            =   795
            TabIndex        =   233
            Top             =   390
            Width           =   1155
         End
         Begin VB.Frame foposs 
            ClipControls    =   0   'False
            Height          =   555
            Left            =   2970
            TabIndex        =   230
            Top             =   180
            Width           =   5145
            Begin VB.TextBox oposs 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1380
               MaxLength       =   50
               TabIndex        =   231
               Top             =   180
               Width           =   3675
            End
            Begin VB.Label Label62 
               Caption         =   "OPOS Scanner"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   60
               TabIndex        =   232
               Top             =   210
               Width           =   1305
            End
         End
         Begin VB.Frame fserials 
            ClipControls    =   0   'False
            Height          =   705
            Left            =   120
            TabIndex        =   135
            Top             =   660
            Width           =   7995
            Begin VB.ComboBox sPuerto 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AC8E
               Left            =   60
               List            =   "frm_config_pos.frx":1AC9E
               Style           =   2  'Dropdown List
               TabIndex        =   141
               Top             =   330
               Width           =   1215
            End
            Begin VB.ComboBox SBaudios 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1ACBA
               Left            =   1380
               List            =   "frm_config_pos.frx":1ACD0
               Style           =   2  'Dropdown List
               TabIndex        =   140
               Top             =   330
               Width           =   1215
            End
            Begin VB.ComboBox SParidad 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1ACFB
               Left            =   2715
               List            =   "frm_config_pos.frx":1AD08
               Style           =   2  'Dropdown List
               TabIndex        =   139
               Top             =   330
               Width           =   1215
            End
            Begin VB.ComboBox SDato 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AD21
               Left            =   4035
               List            =   "frm_config_pos.frx":1AD34
               Style           =   2  'Dropdown List
               TabIndex        =   138
               Top             =   330
               Width           =   1215
            End
            Begin VB.ComboBox SParada 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AD47
               Left            =   5370
               List            =   "frm_config_pos.frx":1AD54
               Style           =   2  'Dropdown List
               TabIndex        =   137
               Top             =   330
               Width           =   1215
            End
            Begin VB.TextBox sPrefijo 
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6690
               MaxLength       =   40
               TabIndex        =   136
               Top             =   330
               Width           =   1215
            End
            Begin VB.Label Label14 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Puerto"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   60
               TabIndex        =   147
               Top             =   150
               Width           =   555
            End
            Begin VB.Label Label15 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Baudios"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   1380
               TabIndex        =   146
               Top             =   150
               Width           =   675
            End
            Begin VB.Label Label16 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Paridad"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   2730
               TabIndex        =   145
               Top             =   150
               Width           =   645
            End
            Begin VB.Label Label17 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Dato"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   4050
               TabIndex        =   144
               Top             =   150
               Width           =   1050
            End
            Begin VB.Label Label18 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Parada"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   5370
               TabIndex        =   143
               Top             =   150
               Width           =   1245
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Prefijo Ctrl"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   6720
               TabIndex        =   142
               Top             =   150
               Width           =   930
            End
         End
      End
      Begin VB.Frame FBALANZA 
         Caption         =   " Balanza "
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   1395
         Left            =   -74910
         TabIndex        =   105
         Top             =   1830
         Width           =   8175
         Begin VB.OptionButton Oposbalanza 
            Caption         =   "OLE POS"
            CausesValidation=   0   'False
            Enabled         =   0   'False
            Height          =   195
            Left            =   795
            TabIndex        =   238
            Top             =   390
            Width           =   1155
         End
         Begin VB.OptionButton nobalanza 
            Caption         =   "No"
            Height          =   195
            Left            =   180
            TabIndex        =   237
            Top             =   390
            Width           =   555
         End
         Begin VB.OptionButton serialbalanza 
            Caption         =   "Serial"
            Height          =   195
            Left            =   2010
            TabIndex        =   236
            Top             =   390
            Width           =   765
         End
         Begin VB.Frame foposba 
            ClipControls    =   0   'False
            Height          =   555
            Left            =   2910
            TabIndex        =   161
            Top             =   180
            Width           =   5205
            Begin VB.TextBox oposba 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1410
               MaxLength       =   50
               TabIndex        =   162
               Top             =   180
               Width           =   3705
            End
            Begin VB.Label Label63 
               Caption         =   "OPOS  Balanza"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   90
               TabIndex        =   163
               Top             =   180
               Width           =   1335
            End
         End
         Begin VB.Frame fserialba 
            ClipControls    =   0   'False
            Height          =   675
            Left            =   120
            TabIndex        =   148
            Top             =   660
            Width           =   7995
            Begin VB.ComboBox bParada 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AD63
               Left            =   5310
               List            =   "frm_config_pos.frx":1AD70
               Style           =   2  'Dropdown List
               TabIndex        =   155
               Top             =   300
               Width           =   1215
            End
            Begin VB.ComboBox bDato 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AD7F
               Left            =   4005
               List            =   "frm_config_pos.frx":1AD92
               Style           =   2  'Dropdown List
               TabIndex        =   154
               Top             =   300
               Width           =   1215
            End
            Begin VB.ComboBox bParidad 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1ADA5
               Left            =   2685
               List            =   "frm_config_pos.frx":1ADB2
               Style           =   2  'Dropdown List
               TabIndex        =   153
               Top             =   300
               Width           =   1215
            End
            Begin VB.ComboBox bBaudios 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1ADCB
               Left            =   1380
               List            =   "frm_config_pos.frx":1ADE1
               Style           =   2  'Dropdown List
               TabIndex        =   152
               Top             =   300
               Width           =   1215
            End
            Begin VB.ComboBox bPuerto 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               ItemData        =   "frm_config_pos.frx":1AE0C
               Left            =   60
               List            =   "frm_config_pos.frx":1AE1C
               Style           =   2  'Dropdown List
               TabIndex        =   151
               Top             =   300
               Width           =   1215
            End
            Begin VB.TextBox bprefijo 
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6630
               MaxLength       =   40
               TabIndex        =   149
               Top             =   300
               Width           =   1275
            End
            Begin VB.Label Label9 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Parada"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   5310
               TabIndex        =   160
               Top             =   120
               Width           =   1245
            End
            Begin VB.Label Label10 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Dato"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   4005
               TabIndex        =   159
               Top             =   120
               Width           =   1050
            End
            Begin VB.Label Label11 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Paridad"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   2700
               TabIndex        =   158
               Top             =   120
               Width           =   645
            End
            Begin VB.Label Label12 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Baudios"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   1380
               TabIndex        =   157
               Top             =   120
               Width           =   675
            End
            Begin VB.Label Label13 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Puerto"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   60
               TabIndex        =   156
               Top             =   120
               Width           =   555
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Prefijo Ctrl"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   6630
               TabIndex        =   150
               Top             =   120
               Width           =   930
            End
         End
      End
      Begin VB.Frame PANEL1 
         Caption         =   "Panel Primario "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   3705
         Left            =   150
         TabIndex        =   104
         Top             =   470
         Width           =   4005
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   35
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   53
            Top             =   3090
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   29
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   47
            Top             =   2520
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   34
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   52
            Top             =   3090
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   28
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   46
            Top             =   2520
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   33
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   51
            Top             =   3090
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   27
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   45
            Top             =   2520
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   32
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   50
            Top             =   3090
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   26
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   2520
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   31
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   49
            Top             =   3090
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   25
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   43
            Top             =   2520
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   30
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   48
            Top             =   3090
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   24
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   2520
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   1
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   19
            Top             =   300
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   0
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   18
            Top             =   300
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   6
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   24
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   12
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   1380
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   18
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   36
            Top             =   1950
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   7
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   25
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   13
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   31
            Top             =   1380
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   19
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   37
            Top             =   1950
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   2
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   20
            Top             =   300
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   8
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   26
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   14
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   1380
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   20
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   38
            Top             =   1950
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   3
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   21
            Top             =   300
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   9
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   27
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   15
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   1380
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   21
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   39
            Top             =   1950
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   4
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   22
            Top             =   300
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   10
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   16
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   34
            Top             =   1380
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   22
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   40
            Top             =   1950
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   5
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   23
            Top             =   300
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   11
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   29
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   17
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   35
            Top             =   1380
            Width           =   615
         End
         Begin VB.CommandButton btn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   23
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   41
            Top             =   1950
            Width           =   615
         End
      End
      Begin VB.Frame Panel2 
         Caption         =   "Panel Secundario "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   3705
         Left            =   4260
         TabIndex        =   103
         Top             =   470
         Width           =   4005
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   24
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   81
            Top             =   2550
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   30
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   87
            Top             =   3120
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   25
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   82
            Top             =   2550
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   31
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   88
            Top             =   3120
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   26
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   83
            Top             =   2550
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   32
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   89
            Top             =   3120
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   27
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   84
            Top             =   2550
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   33
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   90
            Top             =   3120
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   28
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   85
            Top             =   2550
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   34
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   91
            Top             =   3120
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   29
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   86
            Top             =   2550
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            Caption         =   "&Salir"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   35
            Left            =   3240
            Picture         =   "frm_config_pos.frx":1AE38
            Style           =   1  'Graphical
            TabIndex        =   92
            Top             =   3120
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   23
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   80
            Top             =   1980
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   17
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   74
            Top             =   1410
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   11
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   68
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   5
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   62
            Top             =   270
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   22
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   79
            Top             =   1980
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   16
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   73
            Top             =   1410
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   10
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   67
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   4
            Left            =   2610
            Style           =   1  'Graphical
            TabIndex        =   61
            Top             =   270
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   21
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   78
            Top             =   1980
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   15
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   72
            Top             =   1410
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   9
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   66
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   3
            Left            =   1980
            Style           =   1  'Graphical
            TabIndex        =   60
            Top             =   270
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   20
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   77
            Top             =   1980
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   14
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   71
            Top             =   1410
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   8
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   65
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   2
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   59
            Top             =   270
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   19
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   76
            Top             =   1980
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   13
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   70
            Top             =   1410
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   7
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   64
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   1
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   58
            Top             =   270
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   18
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   75
            Top             =   1980
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   12
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   69
            Top             =   1410
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   6
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   63
            Top             =   840
            Width           =   615
         End
         Begin VB.CommandButton sbtn_1 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   0
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   57
            Top             =   270
            Width           =   615
         End
      End
      Begin VB.Frame datos_boton 
         Caption         =   " Datos del bot�n # "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   1665
         Left            =   150
         TabIndex        =   99
         Top             =   4190
         Width           =   3975
         Begin VB.TextBox nombre_grupo 
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   990
            MaxLength       =   15
            TabIndex        =   55
            Top             =   450
            Width           =   1905
         End
         Begin VB.CheckBox grp_btn 
            Alignment       =   1  'Right Justify
            Caption         =   "Grupo de Botones"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   54
            Top             =   210
            Width           =   1935
         End
         Begin VB.TextBox boton_producto 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   150
            MaxLength       =   15
            TabIndex        =   56
            Top             =   1020
            Width           =   1725
         End
         Begin MSComctlLib.Toolbar BARRA_PANEL1 
            Height          =   570
            Left            =   3000
            TabIndex        =   130
            Top             =   180
            Width           =   885
            _ExtentX        =   1561
            _ExtentY        =   1005
            ButtonWidth     =   1455
            ButtonHeight    =   953
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "Cancela la configuraci�n actual"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "GRABAR"
                  Object.ToolTipText     =   "Guarda la configuraci�n actual"
                  ImageIndex      =   2
               EndProperty
            EndProperty
            BorderStyle     =   1
            MousePointer    =   10
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00808080&
            X1              =   90
            X2              =   2955
            Y1              =   810
            Y2              =   810
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   150
            TabIndex        =   132
            Top             =   480
            Width           =   675
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   150
            TabIndex        =   102
            Top             =   840
            Width           =   750
         End
         Begin VB.Label boton_descri 
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   150
            TabIndex        =   101
            Top             =   1320
            Width           =   2745
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "No"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   2190
            TabIndex        =   100
            Top             =   240
            Width           =   225
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00FFFFFF&
            BorderWidth     =   2
            X1              =   90
            X2              =   2955
            Y1              =   810
            Y2              =   810
         End
      End
      Begin VB.Frame datos_subboton 
         Caption         =   " Datos del sub-bot�n # "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   1665
         Left            =   4260
         TabIndex        =   95
         Top             =   4190
         Width           =   3975
         Begin VB.TextBox subboton_producto 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   150
            MaxLength       =   15
            TabIndex        =   93
            Top             =   660
            Width           =   1725
         End
         Begin MSComctlLib.Toolbar BARRA_PANEL2 
            Height          =   705
            Left            =   3030
            TabIndex        =   131
            Top             =   180
            Width           =   855
            _ExtentX        =   1508
            _ExtentY        =   1244
            ButtonWidth     =   1455
            ButtonHeight    =   1191
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            ImageList       =   "Icono_Apagado"
            DisabledImageList=   "Icono_deshabilitado"
            HotImageList    =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "Cancela la configuraci�n actual"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "GRABAR"
                  Object.ToolTipText     =   "Guarda la configuraci�n actual"
                  ImageIndex      =   2
               EndProperty
            EndProperty
            BorderStyle     =   1
            MousePointer    =   10
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   180
            TabIndex        =   98
            Top             =   990
            Width           =   990
         End
         Begin VB.Label subboton_descri 
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   150
            TabIndex        =   97
            Top             =   1230
            Width           =   2805
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   180
            TabIndex        =   96
            Top             =   450
            Width           =   750
         End
      End
      Begin VB.Frame FGAVETA 
         Caption         =   " Gaveta para Dinero "
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   1845
         Left            =   -74880
         TabIndex        =   109
         Top             =   2565
         Width           =   8175
         Begin VB.OptionButton serialgaveta 
            Caption         =   "Serial"
            Height          =   195
            Left            =   2040
            TabIndex        =   246
            Top             =   390
            Width           =   765
         End
         Begin VB.OptionButton nogaveta 
            Caption         =   "No"
            CausesValidation=   0   'False
            Height          =   195
            Left            =   210
            TabIndex        =   245
            Top             =   390
            Width           =   555
         End
         Begin VB.OptionButton oposgaveta 
            Caption         =   "OLE POS"
            Height          =   195
            Left            =   825
            TabIndex        =   244
            Top             =   390
            Width           =   1155
         End
         Begin VB.Frame foposg 
            ClipControls    =   0   'False
            Height          =   555
            Left            =   2850
            TabIndex        =   196
            Top             =   180
            Width           =   5205
            Begin VB.TextBox oposg 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1260
               MaxLength       =   50
               TabIndex        =   197
               Top             =   180
               Width           =   3855
            End
            Begin VB.Label Label65 
               Caption         =   "OPOS Gaveta"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   60
               TabIndex        =   198
               Top             =   210
               Width           =   1275
            End
         End
         Begin VB.Frame FSERIALg 
            ClipControls    =   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   675
            Left            =   1380
            TabIndex        =   111
            Top             =   660
            Width           =   6675
            Begin VB.ComboBox GParada 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frm_config_pos.frx":1B2B2
               Left            =   5370
               List            =   "frm_config_pos.frx":1B2BF
               Style           =   2  'Dropdown List
               TabIndex        =   16
               Top             =   300
               Width           =   1215
            End
            Begin VB.ComboBox GDato 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frm_config_pos.frx":1B2CE
               Left            =   4050
               List            =   "frm_config_pos.frx":1B2E1
               Style           =   2  'Dropdown List
               TabIndex        =   15
               Top             =   300
               Width           =   1215
            End
            Begin VB.ComboBox GParidad 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frm_config_pos.frx":1B2F4
               Left            =   2730
               List            =   "frm_config_pos.frx":1B301
               Style           =   2  'Dropdown List
               TabIndex        =   14
               Top             =   300
               Width           =   1215
            End
            Begin VB.ComboBox GBaudios 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frm_config_pos.frx":1B31A
               Left            =   1410
               List            =   "frm_config_pos.frx":1B330
               Style           =   2  'Dropdown List
               TabIndex        =   13
               Top             =   300
               Width           =   1215
            End
            Begin VB.ComboBox GPuerto 
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frm_config_pos.frx":1B35B
               Left            =   90
               List            =   "frm_config_pos.frx":1B36B
               Style           =   2  'Dropdown List
               TabIndex        =   12
               Top             =   300
               Width           =   1215
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Parada"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   5370
               TabIndex        =   116
               Top             =   120
               Width           =   1245
            End
            Begin VB.Label Label7 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Bits de Dato"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   4050
               TabIndex        =   115
               Top             =   120
               Width           =   1050
            End
            Begin VB.Label Label5 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Paridad"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   2730
               TabIndex        =   114
               Top             =   120
               Width           =   645
            End
            Begin VB.Label Label6 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Baudios"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   1410
               TabIndex        =   113
               Top             =   120
               Width           =   675
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Puerto"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   90
               TabIndex        =   112
               Top             =   120
               Width           =   555
            End
         End
         Begin VB.Frame ftipo 
            Caption         =   "Tipo"
            ClipControls    =   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000002&
            Height          =   1035
            Left            =   90
            TabIndex        =   110
            Top             =   750
            Width           =   1275
            Begin VB.OptionButton tconexion 
               Caption         =   "Serial"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Index           =   0
               Left            =   60
               TabIndex        =   10
               Top             =   330
               Value           =   -1  'True
               Width           =   885
            End
            Begin VB.OptionButton tconexion 
               Caption         =   "Paralela"
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Index           =   1
               Left            =   60
               TabIndex        =   11
               Top             =   690
               Width           =   1035
            End
         End
         Begin VB.Frame fparalelog 
            ClipControls    =   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   525
            Left            =   1380
            TabIndex        =   117
            Top             =   1260
            Width           =   6675
            Begin VB.TextBox Gcontrol 
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   3270
               MaxLength       =   60
               TabIndex        =   17
               Top             =   150
               Width           =   2325
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Car�cter de Control de Apertua"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   210
               TabIndex        =   118
               Top             =   210
               Width           =   2715
            End
         End
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1560
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1B387
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1C063
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1CD3F
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1DA1B
            Key             =   "inter"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   840
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1DC67
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1E943
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":1F61F
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":202FB
            Key             =   "inter"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_deshabilitado 
      Left            =   120
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":20547
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":21223
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":21EFF
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos.frx":22BDB
            Key             =   "inter"
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar CoolBar1 
      Height          =   960
      Left            =   0
      TabIndex        =   316
      Top             =   0
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   1693
      BandCount       =   1
      _CBWidth        =   8415
      _CBHeight       =   960
      _Version        =   "6.0.8450"
      Child1          =   "Frame1"
      MinHeight1      =   60
      Width1          =   375
      NewRow1         =   0   'False
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   900
         Left            =   30
         TabIndex        =   317
         Top             =   30
         Width           =   8295
         Begin MSComctlLib.Toolbar barra_menu 
            Height          =   705
            Left            =   90
            TabIndex        =   318
            Top             =   60
            Width           =   8175
            _ExtentX        =   14420
            _ExtentY        =   1244
            ButtonWidth     =   1455
            ButtonHeight    =   1191
            Appearance      =   1
            Style           =   1
            ImageList       =   "Icono_Apagado"
            DisabledImageList=   "Icono_deshabilitado"
            HotImageList    =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "Cancela la configuraci�n actual"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "GRABAR"
                  Object.ToolTipText     =   "Guarda la configuraci�n actual"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Ayuda"
                  Key             =   "AYUDA"
                  Object.ToolTipText     =   "Ayuda del SIstema"
                  ImageIndex      =   3
               EndProperty
            EndProperty
            BorderStyle     =   1
         End
      End
   End
   Begin VB.Label lbl_salir 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Salir"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   10500
      MouseIcon       =   "frm_config_pos.frx":22E27
      MousePointer    =   99  'Custom
      TabIndex        =   94
      ToolTipText     =   "Salir del Sistema"
      Top             =   8280
      UseMnemonic     =   0   'False
      Width           =   1125
   End
   Begin VB.Menu opcion 
      Caption         =   "opcion"
      Begin VB.Menu agregar 
         Caption         =   "agregar"
         Begin VB.Menu add 
            Caption         =   "Sub-nivel"
            Index           =   0
         End
         Begin VB.Menu add 
            Caption         =   "Caja"
            Index           =   1
         End
      End
      Begin VB.Menu GUION 
         Caption         =   "-"
      End
      Begin VB.Menu eliminar 
         Caption         =   "Eliminar"
      End
      Begin VB.Menu config 
         Caption         =   "Configurar"
      End
      Begin VB.Menu GUION3 
         Caption         =   "-"
      End
      Begin VB.Menu cambiar 
         Caption         =   "Cambiar Nombre"
      End
   End
   Begin VB.Menu boton 
      Caption         =   "Boton"
      Visible         =   0   'False
      Begin VB.Menu configurar_boton 
         Caption         =   "Configurar"
      End
      Begin VB.Menu eliminar_boton 
         Caption         =   "Eliminar"
      End
   End
   Begin VB.Menu subboton 
      Caption         =   "Subboton"
      Visible         =   0   'False
      Begin VB.Menu configurar_subboton 
         Caption         =   "&Configurar"
      End
      Begin VB.Menu eliminar_subboton 
         Caption         =   "&Eliminar"
      End
   End
End
Attribute VB_Name = "frm_config_pos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub add_Click(Index As Integer)
    Select Case Index
        Case Is = 0
                Newdpto.Show vbModal
                If addnode Then ''verifica que se deba ingresar un node subnivel
                    '************************************
                    Call apertura_recordset(False, rsmenu)
                    rsmenu.Open "SELECT org_caja FROM MA_CONSECUTIVOS", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
                    CLAVE = rsmenu!org_caja
                    rsmenu.Update
                        rsmenu!org_caja = rsmenu!org_caja + 1
                    rsmenu.UpdateBatch
                    '*******************************************
                    Call apertura_recordset(False, rsmenu)
                    rsmenu.Open "ma_estruc_caja", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    rsmenu.AddNew
                        rsmenu!texto = lctexto
                        rsmenu!CLAVE = "N" & CLAVE
                    rsmenu.UpdateBatch
                    '***************************************************
                    Set lcnode = ARBOL.Nodes.add(lckey, tvwChild, Key:="N" & CLAVE, Text:=lctexto, Image:="subnivel")
                    lcnode.Tag = "N" & CLAVE
                End If
        
        Case Is = 1
            glgrabar = True
            config = False
            ARBOL.Enabled = False
            fdescripcion.Enabled = True
            codigo.Enabled = True
            Call ACTIVAR_FRAME(True)
            FICHAS.Tab = 0
            codigo.SetFocus
            
    End Select
End Sub

Private Sub ARBOL_Click()
'    lckey = ARBOL.SelectedItem.Key
'    lctag = ARBOL.SelectedItem.Tag
'    lcnodo = ARBOL.SelectedItem.Index
'    lccount = ARBOL.SelectedItem.Children
End Sub
Private Sub ARBOL_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = 93 'BOTON DE PROPIEDADES
            Call ARBOL_MouseDown(vbRightButton, 0, 0, 0)
    End Select
End Sub
Private Sub ARBOL_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Button
        Case Is = 1
        
        Case Is = 2
            If lckey = "Root" Then
                agregar.Enabled = True
                add(0).Enabled = True
                add(1).Enabled = False
                eliminar.Enabled = False
                cambiar = False
                config.Enabled = False
            ElseIf Mid(lckey, 1, 1) = "N" Then
                agregar.Enabled = True
                add(0).Enabled = False
                add(1).Enabled = True
                If lccount <> 0 Then
                    config.Enabled = True
                    eliminar.Enabled = False
                Else
                    config.Enabled = False
                    eliminar.Enabled = True
                End If
                cambiar = True
                
            ElseIf Mid(lckey, 1, 1) = "C" Then
                agregar.Enabled = False
                add(0).Enabled = False
                add(1).Enabled = False
                eliminar.Enabled = True
                cambiar = False
                config = True
            End If
            PopupMenu opcion
    End Select

End Sub
Private Sub ARBOL_NodeClick(ByVal Node As MSComctlLib.Node)
    lckey = Node.Key
    lctag = Node.Tag
    lcnodo = Node.Index
    lccount = Node.Children
    lcindex = Node.Index
    If Mid(Node.Key, 1, 1) = "R" Or Mid(Node.Key, 1, 1) = "N" Then
        descri.Text = ""
        codigo.Text = "000"
        Call llenar_config
    Else
        CAJA = Mid(lckey, 2, Len(lckey) - 1)
        Call apertura_recordset(True, rscaja)
        rscaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & CAJA & "'", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
        Set rscaja.ActiveConnection = Nothing
        If Not rscaja.EOF Then
            Call LLENAR_CAJA
        Else
            Call llenar_config
        End If
        Call cerrar_recordset(rscaja)
    End If
End Sub
Private Sub ayuda_Click()
    Select Case ayuda.Value
        Case Is = 0
'            Call ACTIVA(ANIVEL, False)
            ayuda.Caption = Replace(ayuda.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(ANIVEL, True)
            ayuda.Caption = Replace(ayuda.Caption, "No", "Si", 1)
    End Select
End Sub
Private Sub ACTIVA(Control As Object, Activar As Boolean)
    Control.Enabled = Activar
End Sub
Private Sub balanzas_Click()
    Select Case balanzas.Value
        Case Is = 0
'            Call ACTIVA(BNIVEL, False)
            balanzas.Caption = Replace(balanzas.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(BNIVEL, True)
            balanzas.Caption = Replace(balanzas.Caption, "No", "Si", 1)
    End Select

End Sub
Private Sub barra_menu_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
        
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
        
        Case Is = "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
    End Select
End Sub
Private Sub sub_cancelar()
    Call ACTIVAR_FRAME(False)
    Call cerrar_recordset(rsbotones)
    Call cerrar_recordset(rstrbotones)
    ARBOL.Enabled = True
    ARBOL.SetFocus
    SendKeys "{ENTER}"
    
End Sub
Private Sub BARRA_PANEL1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call cancelar
            
            
        Case Is = "GRABAR"
            If boton_producto.Text = "" And grp_btn.Value = 0 Then
                Call mensaje(True, "Debe seleccionar un producto o configurar un grupo...!")
                Exit Sub
            ElseIf grp_btn.Value = 1 And nombre_grupo.Text = "" Then
                Call mensaje(True, "Debe escribir un nombre para el grupo...!")
                Exit Sub
            End If
            Ent.POS.BeginTrans
                
                numero = CInt(Right(Trim(datos_boton), 2))
                '*******************************************************
                '*** busca el boyton en la temporal de botones
                '*******************************************************
                Call apertura_recordset(False, rsbotones)
                rsbotones.Open "select * from ma_botones_tmp where c_caja = '" & codigo.Text & "' and n_boton = " & numero, Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
                If Not rsbotones.EOF Then '------ EXISTE
                    If rsbotones!B_GRUPO = True Then '------ ES GRUPO
                        btn_1(numero).Caption = nombre_grupo.Text
                        btn_1(numero).Tag = "Grupo"
                        btn_1(numero).ToolTipText = "Grupo"
                        If grp_btn.Value = 1 Then '--- VERIFICA SI SE MANTIENE EL GRUPO
                            '*** borrar los datos actuales
                            Ent.POS.Execute "delete tr_botones_tmp where c_caja = '" & codigo.Text & "' and n_boton = " & numero
                            
                            '*** abre la tabla tmp de subbotones
                            Call apertura_recordset(False, rstrbotones)
                            rstrbotones.Open "tr_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                            
                            '*** abre la tabla tmp DE TMP de subbotones
                            '***  actualiza la tr_botones_tmp
                            Call apertura_recordset(False, rstrbotones_TMP)
                            rstrbotones_TMP.Open "tr_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                            If Not rstrbotones_TMP.EOF Then
                                Do Until rstrbotones_TMP.EOF
                                    rstrbotones.AddNew
                                        rstrbotones!c_codigo = rstrbotones_TMP!c_codigo
                                        rstrbotones!c_caja = rstrbotones_TMP!c_caja
                                        rstrbotones!n_boton = rstrbotones_TMP!n_boton
                                        rstrbotones!n_subboton = rstrbotones_TMP!n_subboton
                                    rstrbotones.UpdateBatch
                                    
                                    rstrbotones_TMP.Delete
                                    rstrbotones_TMP.UpdateBatch
                                    rstrbotones_TMP.MoveNext
                                Loop
                            End If
                        Else
                            '************************************************
                            '****** ACTUALIZA EL BOTON
                            '************************************************
                            rsbotones.Update
                                rsbotones!B_GRUPO = 0
                                rsbotones!c_codigo = boton_producto
                            rsbotones.UpdateBatch
                            
                            btn_1(numero).Caption = boton_descri
                            btn_1(numero).Tag = boton_producto
                            btn_1(numero).ToolTipText = boton_descri
                            
                            '************************************************
                            '****** ELIMINA LOS SUB-BOTONES DEL GRUPO
                            '************************************************
                            Ent.POS.Execute "delete tr_botones_tmp where c_caja = '" & codigo.Text & "' and n_boton = " & numero
                            Ent.POS.Execute "delete tr_botones_tmp1"
                        End If
                    Else '----- NO ES GRUPO
                        If grp_btn.Value = 1 Then
                            '*** actualizo el boton
                            rsbotones.Update
                                rsbotones!B_GRUPO = 1
                                rsbotones!c_codigo = boton_producto
                            rsbotones.UpdateBatch
                            
                            btn_1(numero).Caption = nombre_grupo.Text
                            btn_1(numero).Tag = "Grupo"
                            btn_1(numero).ToolTipText = "Grupo"
                            
                            '*** abre la tabla tmp de subbotones tmp
                            Call apertura_recordset(False, rstrbotones)
                            rstrbotones.Open "tr_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                            
                            '*** abre la tabla tmp tmp de subbotones tmp1
                            Call apertura_recordset(False, rstrbotones_TMP)
                            rstrbotones_TMP.Open "tr_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                            If Not rstrbotones_TMP.EOF Then
                                Do Until rstrbotones_TMP.EOF
                                    rstrbotones.AddNew
                                        rstrbotones!c_codigo = rstrbotones_TMP!c_codigo
                                        rstrbotones!c_caja = rstrbotones_TMP!c_caja
                                        rstrbotones!n_boton = rstrbotones_TMP!n_boton
                                        rstrbotones!n_subboton = rstrbotones_TMP!n_subboton
                                    rstrbotones.UpdateBatch
                                    
                                    rstrbotones.Delete
                                    rstrbotones.UpdateBatch
                                    rstrbotones.MoveNext
                                Loop
                            End If
                        
                        Else
                            rsbotones.Update
                                rsbotones!c_codigo = boton_producto
                            rsbotones.UpdateBatch
                            btn_1(numero).Caption = boton_descri.Caption
                            btn_1(numero).Tag = boton_producto.Text
                            btn_1(numero).ToolTipText = boton_descri.Caption
                        
                        End If
                    End If
                    '**** fin del boton encrontrado
                Else '**** else de la busqueda
                    '******* comienzo del boton nuevo
                    rsbotones.AddNew
                        rsbotones!c_codigo = boton_producto.Text
                        rsbotones!c_caja = codigo.Text
                        rsbotones!B_GRUPO = grp_btn.Value
                        rsbotones!n_boton = numero
                        rsbotones!nombre_grupo = nombre_grupo.Text
                    rsbotones.UpdateBatch
                    
                    If grp_btn.Value = 1 Then
                        btn_1(numero).Caption = nombre_grupo.Text
                        btn_1(numero).Tag = "Grupo"
                        btn_1(numero).ToolTipText = "Grupo"
                        Call apertura_recordset(False, rstrbotones)
                        rstrbotones.Open "tr_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                        
                        Call apertura_recordset(False, rstrbotones_TMP)
                        rstrbotones_TMP.Open "TR_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                        
                        If Not rstrbotones_TMP.EOF Then
                            Do Until rstrbotones_TMP.EOF
                                rstrbotones.AddNew
                                    rstrbotones!c_codigo = rstrbotones_TMP!c_codigo
                                    rstrbotones!c_caja = rstrbotones_TMP!c_caja
                                    rstrbotones!n_boton = rstrbotones_TMP!n_boton
                                    rstrbotones!n_subboton = rstrbotones_TMP!n_subboton
                                rstrbotones.UpdateBatch
                                
                                rstrbotones_TMP.Delete
                                rstrbotones_TMP.UpdateBatch
                                rstrbotones_TMP.MoveNext
                            Loop
                            
                        End If
                    Else
                        btn_1(numero).Caption = boton_descri
                        btn_1(numero).Tag = boton_producto
                        btn_1(numero).ToolTipText = boton_descri
                        Call apertura_recordset(False, rstrbotones_TMP)
                        rstrbotones_TMP.Open "TR_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                        If Not rstrbotones_TMP.EOF Then
                            Do Until rstrbotones_TMP.EOF
                                rstrbotones_TMP.Delete
                                rstrbotones_TMP.UpdateBatch
                                rstrbotones_TMP.MoveNext
                            Loop
                        End If
                    
                    End If
                End If
            Ent.POS.CommitTrans
            Call cancelar
            
        Case Is = "AYUDA"
    End Select
End Sub

Private Sub BARRA_PANEL2_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            barra_menu.Buttons(2).Enabled = True
            datos_subboton.Enabled = False
            datos_boton.Enabled = True
            panel2.Enabled = True
            subboton_producto.Text = ""
            subboton_descri.Caption = ""
            
        Case Is = "GRABAR"
            If subboton_producto.Text = "" Then
                Call mensaje(True, "Debe configurar el bot�n seleccionando un producto...!")
                Exit Sub
            End If
            Call apertura_recordset(False, rstrbotones)
            rstrbotones.Open "select * from tr_botones_tmp1 where c_caja = '" & codigo.Text & "' and n_boton = " & CInt(Right(Trim(datos_boton.Caption), 2)) & " and n_subboton = " & CInt(Right(Trim(datos_subboton.Caption), 2)), Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rstrbotones.EOF Then
                rstrbotones.Update
            Else
                rstrbotones.AddNew
                rstrbotones!c_caja = codigo.Text
                rstrbotones!n_boton = CInt(Right(Trim(datos_boton), 2))
                rstrbotones!n_subboton = CInt(Right(Trim(datos_subboton), 2))
            End If
            rstrbotones!c_codigo = subboton_producto.Text
            rstrbotones.UpdateBatch
            sbtn_1(CInt(Right(Trim(datos_subboton), 2))).Caption = subboton_descri.Caption
            sbtn_1(CInt(Right(Trim(datos_subboton), 2))).Tag = subboton_producto.Text
            sbtn_1(CInt(Right(Trim(datos_subboton), 2))).ToolTipText = subboton_descri.Caption
            barra_menu.Buttons(2).Enabled = True
            datos_subboton.Enabled = False
            datos_boton.Enabled = True
            panel2.Enabled = True
            subboton_descri.Caption = ""
            subboton_producto.Text = ""
        
        Case Is = "AYUDA"
    End Select
        
End Sub



Private Sub noscanner_Click()
    foposs.Enabled = False
    fserials.Enabled = False

End Sub

Private Sub opos_Click()
    Select Case opos.Value
        Case Is = 0
            opos.Caption = "No"
            
            nogaveta.Value = True
            nollaves.Value = True
            NODISPLAY.Value = True
            noscanner.Value = True
            nobalanza.Value = True
            nobanda.Value = True
            Call OBJETOS_OPOS(False)
            
            DoEvents
        Case Is = 1
            opos.Caption = "Si"
            Call OBJETOS_OPOS(True)
    
    End Select

End Sub

Private Sub nobanda_Click()
    foposb.Enabled = False
    fserialb.Enabled = False
End Sub

Private Sub serialbanda_Click()
    foposb.Enabled = False
    fserialb.Enabled = True

End Sub


Private Sub Oposbanda_Click()
    If opos.Value = 0 Then
        nobanda.Value = True
    Else
        foposb.Enabled = True
        fserialb.Enabled = False
    End If
End Sub

Private Sub nollaves_Click()
    foposl.Enabled = False

End Sub

Private Sub oposllaves_Click()
    foposl.Enabled = True
End Sub

Private Sub Oposscanner_Click()
    If opos.Value = 0 Then
        noscanner.Value = True
    Else
        foposs.Enabled = True
        fserials.Enabled = False
    End If

End Sub


Private Sub serialdisplay_Click()
    FOPOSD.Enabled = False
    FSERIALD.Enabled = True

End Sub
Private Sub Oposdisplay_Click()
    FOPOSD.Enabled = True
    FSERIALD.Enabled = False
End Sub
Private Sub nodisplay_Click()
    FOPOSD.Enabled = False
    FSERIALD.Enabled = False
End Sub

Private Sub Oposbalanza_Click()
    If opos.Value = 0 Then
        nobalanza.Value = True
    Else
        foposba.Enabled = True
        fserialba.Enabled = False
    End If
End Sub

Private Sub nobalanza_Click()
    foposba.Enabled = False
    fserialba.Enabled = False

End Sub

Private Sub serialbalanza_Click()
    foposba.Enabled = False
    fserialba.Enabled = True

End Sub


Private Sub serialscanner_Click()
    foposs.Enabled = False
    fserials.Enabled = True
End Sub




Private Sub boton_producto_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            KeyAscii = 0
            SendKeys Chr$(9)
    End Select
End Sub

Private Sub boton_producto_LostFocus()
    Call buscar_producto(boton_producto, boton_descri)
End Sub


Private Sub btn_1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If btn_1(Index).Tag = "" Then
        eliminar_boton.Enabled = False
    Else
        eliminar_boton.Enabled = True
    End If
    datos_boton.Caption = " Datos del bot�n # " & Format(Index, "00")
    PopupMenu boton
End Sub

Private Sub cantidad_Click()
    Select Case cantidad.Value
        Case Is = 0
'            Call ACTIVA(CNIVEL, False)
            cantidad.Caption = Replace(cantidad.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(CNIVEL, True)
            cantidad.Caption = Replace(cantidad.Caption, "No", "Si", 1)
    End Select

End Sub




Private Sub cliente_Click()
    Select Case cliente.Value
        Case Is = 0
'            Call ACTIVA(CLNIVEL, False)
            cliente.Caption = Replace(cliente.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(CLNIVEL, True)
            cliente.Caption = Replace(cliente.Caption, "No", "Si", 1)
    End Select
End Sub

Private Sub coddeposito_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            tabla = "ma_deposito"
            campo_op1 = "c_coddeposito"
            campo_op2 = "c_descripcion"
            TITULO = "D E P O S I T O S"
            Set forma = frm_config_pos
            forma.Tag = "DEPOSITOS"
            VIEW_CONSULTAS.Show vbModal
    End Select
End Sub

Private Sub coddeposito_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            KeyAscii = 0
            SendKeys Chr(9)
            
    End Select
End Sub

Private Sub coddeposito_LostFocus()
    If coddeposito.Text <> "" Then
        Call buscar_deposito
    End If
End Sub

Private Sub codigo_GotFocus()
    codigo.SelStart = 0
    codigo.SelLength = Len(codigo.Text)
End Sub

Private Sub codigo_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 8, 48 To 57
        
        Case Is = 13
            KeyAscii = 0
            SendKeys Chr$(9)
            
        Case Else
            KeyAscii = 0
            Beep
    End Select
End Sub

Private Sub codigo_LostFocus()
    If codigo.Text = "" Or Format(codigo.Text, "00#") = "000" Then
        If codigo.Enabled = True Then codigo.SetFocus
        Exit Sub
    End If
    Call apertura_recordset(True, rscaja)
    rscaja.Open "select * from ma_caja where c_codigo = '" & Format(codigo.Text, "000") & "'", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
    Set rscaja.ActiveConnection = Nothing
    If Not rscaja.EOF Then
        MsgBox "La caja ya fue creada en el sistema", vbInformation + vbOKOnly, "Mensaje Stellar"
        codigo.Text = "000"
    Else
        descri.Text = "Caja # " & Format(codigo.Text, "00#")
        If descri.Enabled = True Then descri.SetFocus
    End If
    Call cerrar_recordset(rscaja)
End Sub

Private Sub config_Click()
    Select Case Mid(lckey, 1, 1)
        Case Is = "N"
            fdescripcion.Enabled = False
            Call ACTIVAR_FRAME(True)
            config_group = True
            lineal_digital.Value = 0
            lineal_digital.Enabled = False
            glgrabar = False
            
        Case Is = "C"
            Dim RSPRODUCTOS As New ADODB.Recordset
            config_group = False
            glgrabar = False
            ARBOL.Enabled = False
            codigo.Enabled = False
            fdescripcion.Enabled = True
            Call ACTIVAR_FRAME(True)
            FICHAS.Tab = 0
            Ent.POS.BeginTrans
                Ent.POS.Execute "delete ma_botones_TMP"
                Ent.POS.Execute "delete tr_botones_TMP"
                Ent.POS.Execute "delete tr_botones_TMP1"
                For i = 0 To 35
                    btn_1(i).Caption = ""
                    btn_1(i).Tag = ""
                    btn_1(i).ToolTipText = ""
                    If i < 35 Then
                        sbtn_1(i).Tag = ""
                        sbtn_1(i).Caption = ""
                        sbtn_1(i).ToolTipText = ""
                    End If
                Next
                
                Call apertura_recordset(True, rsbotones)
                rsbotones.Open "select * from ma_botones where c_caja = '" & codigo.Text & "' order by c_caja,n_boton", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
                Set rsbotones.ActiveConnection = Nothing
                
                Call apertura_recordset(False, rsbotones_TMP)
                rsbotones_TMP.Open "ma_botones_TMP", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                
                If Not rsbotones.EOF Then
                    Do Until rsbotones.EOF
                        rsbotones_TMP.AddNew
                            rsbotones_TMP!c_caja = rsbotones!c_caja
                            rsbotones_TMP!n_boton = rsbotones!n_boton
                            rsbotones_TMP!B_GRUPO = rsbotones!B_GRUPO
                            rsbotones_TMP!c_codigo = rsbotones!c_codigo
                            rsbotones_TMP!nombre_grupo = rsbotones!nombre_grupo
                        rsbotones_TMP.UpdateBatch
                        
                        If rsbotones!B_GRUPO = True Then
                            btn_1(rsbotones!n_boton).Caption = rsbotones!nombre_grupo
                            btn_1(rsbotones!n_boton).ToolTipText = rsbotones!nombre_grupo
                            btn_1(rsbotones!n_boton).Tag = "Grupo"
                        Else
                            Call apertura_recordset(True, RSPRODUCTOS)
                            RSPRODUCTOS.Open "select c_descri from ma_productos where c_codigo = '" & rsbotones!c_codigo & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                            Set RSPRODUCTOS.ActiveConnection = Nothing
                            btn_1(rsbotones!n_boton).Caption = RSPRODUCTOS!c_descri
                            btn_1(rsbotones!n_boton).ToolTipText = RSPRODUCTOS!c_descri
                            btn_1(rsbotones!n_boton).Tag = rsbotones!c_codigo
                        End If
                        rsbotones.MoveNext
                    Loop
            
                    Call apertura_recordset(True, rsbotones)
                    rsbotones.Open "select * from tr_botones where c_caja = '" & codigo.Text & "' order by c_caja,n_boton", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
                    Set rsbotones.ActiveConnection = Nothing
                    
                    Call apertura_recordset(False, rsbotones_TMP)
                    rsbotones_TMP.Open "tr_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    
                    If Not rsbotones.EOF Then
                        Do Until rsbotones.EOF
                            rsbotones_TMP.AddNew
                                rsbotones_TMP!c_codigo = rsbotones!c_codigo
                                rsbotones_TMP!c_caja = rsbotones!c_caja
                                rsbotones_TMP!n_boton = rsbotones!n_boton
                                rsbotones_TMP!n_subboton = rsbotones!n_subboton
                            rsbotones_TMP.UpdateBatch
                            rsbotones.MoveNext
                        Loop
                    End If
                End If
            Ent.POS.CommitTrans
            Call cerrar_recordset(RSPRODUCTOS)
            Call cerrar_recordset(rsbotones)
            Call cerrar_recordset(rsbotones_TMP)
            
    End Select
End Sub

Private Sub configurar_boton_Click()
    Dim RSPRODUCTOS As New ADODB.Recordset
    barra_menu.Buttons(2).Enabled = False
    datos_boton.Enabled = True
    numero = CInt(Right(Trim(datos_boton.Caption), 2))
    If UCase(btn_1(numero).Tag) = "GRUPO" Then
        nombre_grupo.Text = btn_1(numero).Caption
        Call apertura_recordset(True, rstrbotones)
        rstrbotones.Open "select * from tr_botones_tmp where c_caja = '" & codigo.Text & "' and n_boton = " & numero & " order by c_caja,n_boton,n_subBoton", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
        rstrbotones.ActiveConnection = Nothing
        
        If Not rstrbotones.EOF Then
            Call apertura_recordset(False, rstrbotones_TMP)
            rstrbotones_TMP.Open "tr_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
            Do Until rstrbotones.EOF
                Call apertura_recordset(True, RSPRODUCTOS)
                RSPRODUCTOS.Open "select c_descri from ma_productos where c_codigo = '" & rstrbotones!c_codigo & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
                Set RSPRODUCTOS.ActiveConnection = Nothing
                sbtn_1(rstrbotones!n_subboton).Caption = RSPRODUCTOS!c_descri
                sbtn_1(rstrbotones!n_subboton).ToolTipText = RSPRODUCTOS!c_descri
                sbtn_1(rstrbotones!n_subboton).Tag = rstrbotones!c_codigo
                
                rstrbotones_TMP.AddNew
                    rstrbotones_TMP!c_codigo = rstrbotones!c_codigo
                    rstrbotones_TMP!c_caja = rstrbotones!c_caja
                    rstrbotones_TMP!n_boton = rstrbotones!n_boton
                    rstrbotones_TMP!n_subboton = rstrbotones!n_subboton
                rstrbotones_TMP.UpdateBatch
                rstrbotones.MoveNext
            Loop
            Call cerrar_recordset(RSPRODUCTOS)
            Call cerrar_recordset(rstrbotones)
            Call cerrar_recordset(rstrbotones_TMP)
        End If
        
        
        PANEL1.Enabled = False
        grp_btn.Value = 1
        boton_producto.Text = ""
        boton_descri.Caption = ""
    Else
        grp_btn.Value = 0
        boton_producto.Text = btn_1(numero).Tag
        boton_descri.Caption = btn_1(numero).ToolTipText
    End If
    
End Sub

Private Sub configurar_subboton_Click()
    numero = CInt(Right(Trim(datos_subboton), 2))
    datos_boton.Enabled = False
    datos_subboton.Enabled = True
    subboton_producto.Text = sbtn_1(numero).Tag
    subboton_descri.Caption = sbtn_1(numero).ToolTipText
End Sub



Private Sub dato_numerico_Click()
    Select Case dato_numerico.Value
        Case Is = 0
            dato_numerico.Caption = "No"
        Case Is = 1
            dato_numerico.Caption = "Si"
    End Select
End Sub


Private Sub devolucion_Click()
    Select Case devolucion.Value
        Case Is = 0
'            Call ACTIVA(DNIVEL, False)
            devolucion.Caption = Replace(devolucion.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(DNIVEL, True)
            devolucion.Caption = Replace(devolucion.Caption, "No", "Si", 1)
    End Select

End Sub


Private Sub eliminar_boton_Click()
    numero = CInt(Right(Trim(datos_boton), 2))
    Ent.POS.BeginTrans
        Call apertura_recordset(False, rstrbotones_TMP)
        rstrbotones_TMP.Open "SELECT * from tr_botones_tmp1 WHERE n_boton  = " & numero, Ent.POS, adOpenDynamic, adLockBatchOptimistic, ADCMDTETX
        If Not rstrbotones_TMP.EOF Then
            Do Until rstrbotones_TMP.EOF
                rstrbotones_TMP.Delete
                rstrbotones_TMP.UpdateBatch
                rstrbotones_TMP.MoveNext
            Loop
        End If
        
        Call apertura_recordset(False, rstrbotones_TMP)
        rstrbotones_TMP.Open "SELECT * from tr_botones_tmp WHERE n_boton = " & numero, Ent.POS, adOpenDynamic, adLockBatchOptimistic, ADCMDTETX
        If Not rstrbotones_TMP.EOF Then
            Do Until rstrbotones_TMP.EOF
                rstrbotones_TMP.Delete
                rstrbotones_TMP.UpdateBatch
                rstrbotones_TMP.MoveNext
            Loop
        End If
        
        Call apertura_recordset(False, rstrbotones_TMP)
        rstrbotones_TMP.Open "SELECT * from ma_botones_tmp WHERE n_boton = " & numero, Ent.POS, adOpenDynamic, adLockBatchOptimistic, ADCMDTETX
        If Not rstrbotones_TMP.EOF Then
            rstrbotones_TMP.Delete
            rstrbotones_TMP.UpdateBatch
        End If
        
    Ent.POS.CommitTrans
    btn_1(numero).Caption = ""
    btn_1(numero).Tag = ""
    btn_1(numero).ToolTipText = ""
    Call cerrar_recordset(rstrbotones_TMP)

End Sub

Private Sub eliminar_Click()
    Select Case Mid(lckey, 1, 1)
        Case Is = "N"
            Call apertura_recordset(False, rsmenu)
            rsmenu.Open "select * from ma_estruc_caja where clave = '" & lckey & "'", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rsmenu.EOF Then
                rsmenu.Delete
                rsmenu.UpdateBatch
            End If
            ARBOL.Nodes.Remove (lcindex)
            
        Case Is = "C"
    End Select
End Sub

Private Sub eliminar_subboton_Click()
    numero = CInt(Right(Trim(datos_subboton), 2))
    NUMERO1 = CInt(Right(Trim(datos_boton), 2))
    Ent.POS.BeginTrans
        Call apertura_recordset(False, rstrbotones_TMP)
        rstrbotones_TMP.Open "SELECT * from tr_botones_tmp1 WHERE C_CAJA = '" & codigo.Text & "' AND N_SUBBOTON = " & numero & " AND N_BOTON = " & NUMERO1, Ent.POS, adOpenDynamic, adLockBatchOptimistic, ADCMDTETX
        If Not rstrbotones_TMP.EOF Then
            rstrbotones_TMP.Delete
            rstrbotones_TMP.UpdateBatch
        End If
    Ent.POS.CommitTrans
        sbtn_1(numero).Caption = ""
        sbtn_1(numero).Tag = ""
        sbtn_1(numero).ToolTipText = ""
    Call cerrar_recordset(rstrbotones_TMP)
End Sub

Private Sub espera_Click()
    Select Case espera.Value
        Case Is = 0
'            Call ACTIVA(ENIVEL, False)
            espera.Caption = Replace(espera.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(ENIVEL, True)
            espera.Caption = Replace(espera.Caption, "No", "Si", 1)
    End Select

End Sub

Private Sub fondo_pos_Click()
    Select Case fond_pos.Value
        Case Is = 0
            fondo_pos.Caption = "No"
        Case Is = 1
            fond_pos.Caption = "Si"
    End Select

End Sub

Private Sub Form_Activate()
    
    If INICIO = True Then
        frm_Login_Configuracion.Show vbModal
        If fCancelar = True Then Unload Me
    End If
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
        Select Case KeyCode
            Case Is = vbKeyC
                Call Form_KeyDown(vbKeyF7, 0)
            Case Is = vbKeyG
                Call Form_KeyDown(vbKeyF4, 0)
            Case Is = vbKeyS
                Call Form_KeyDown(vbKeyF12, 0)
            Case Is = vbKeyA
        End Select
    Else
        Select Case KeyCode
            Case Is = vbKeyF4
                If barra_menu.Buttons(2).Enabled = False Then Exit Sub
                Call GRABAR_CAJA
                fdescripcion.Enabled = False
                Call sub_cancelar
            
            Case Is = vbKeyF7
                If barra_menu.Buttons(1).Enabled = False Then Exit Sub
                fdescripcion.Enabled = False
                Call sub_cancelar
                Ent.POS.BeginTrans
                    Call apertura_recordset(False, rsbotones)
                    rsbotones.Open "MA_BOTONES_TMP", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    If Not rsbotones.EOF Then
                        Do Until rsbotones.EOF
                            rsbotones.Delete
                            rsbotones.UpdateBatch
                            rsbotones.MoveNext
                        Loop
                    End If
                    
                    Call apertura_recordset(False, rsbotones)
                    rsbotones.Open "TR_BOTONES_TMP", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    If Not rsbotones.EOF Then
                        Do Until rsbotones.EOF
                            rsbotones.Delete
                            rsbotones.UpdateBatch
                            rsbotones.MoveNext
                        Loop
                    End If
                    
                    Call apertura_recordset(False, rsbotones)
                    rsbotones.Open "TR_BOTONES_TMP1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    If Not rsbotones.EOF Then
                        Do Until rsbotones.EOF
                            rsbotones.Delete
                            rsbotones.UpdateBatch
                            rsbotones.MoveNext
                        Loop
                    End If
                Ent.POS.CommitTrans
            
            Case Is = vbKeyF12
                Unload Me
        End Select
    End If
End Sub

Private Sub Form_Load()
    Call ACTIVAR_FRAME(False)
    Call llenar_config
    Set lcnode = ARBOL.Nodes.add(, , "Root", "Root", "org")
    lcnode.Tag = "Root"
    
    Call apertura_recordset(False, rsmenu)
    rsmenu.Open "select * from ma_estruc_caja order by texto", Ent.POS, adOpenDynamic, adLockReadOnly, adCmdText
    If Not rsmenu.EOF Then
        Do Until rsmenu.EOF
            Call crear_menu("Root", rsmenu!CLAVE, rsmenu!texto, "subnivel")
            rsmenu.MoveNext
        Loop
        Call apertura_recordset(False, rsmenu)
        rsmenu.Open "select * from ma_caja order by c_codigo", Ent.POS, adOpenDynamic, adLockReadOnly, adCmdText
        If Not rsmenu.EOF Then
            Do Until rsmenu.EOF
                CLAVE = "C" & rsmenu!c_codigo
                rel = rsmenu!c_relacion
                Call crear_menu(rel, CLAVE, rsmenu!c_desc_caja, "caja")
                rsmenu.MoveNext
            Loop
        End If
    End If
    lckey = "Root"
'    FICHAS.TabEnabled(2) = False
'    FICHAS.TabEnabled(3) = False
'    FICHAS.TabEnabled(4) = False
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lbl_salir.FontBold = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frm_config_pos = Nothing
    Unload Me
End Sub


Public Sub crear_menu(campo1, campo2, campo3, campo4)
    Set lcnode = ARBOL.Nodes.add(campo1, tvwChild, campo2, campo3, campo4)
    lcnode.Tag = campo2
End Sub

Private Sub forma_continua_Click()
    If forma_continua.Value = 1 Then
        no_lineas.Enabled = True
        uplineas.Enabled = True
    Else
        no_lineas.Enabled = False
        uplineas.Enabled = False
    End If
End Sub

Private Sub grp_btn_Click()
    numero = CInt(Right(Trim(datos_boton), 2))
    Select Case grp_btn.Value
        Case Is = 0
            nombre_grupo.Text = ""
            nombre_grupo.Enabled = False
            Label2(12).Caption = "No"
            boton_producto.Text = IIf(btn_1(numero).Tag = "Grupo", "", btn_1(numero).Tag)
            boton_descri.Caption = IIf(btn_1(numero).Tag = "Grupo", "", btn_1(numero).ToolTipText)
            boton_producto.Enabled = True
            panel2.Enabled = False
        Case Is = 1
            nombre_grupo.Text = btn_1(numero).Caption
            nombre_grupo.Enabled = True
            Label2(12).Caption = "Si"
            boton_producto.Text = ""
            boton_descri.Caption = ""
            boton_producto.Enabled = False
            panel2.Enabled = True
    End Select
End Sub


Private Sub noimpresora_Click()
    fserialimpresora.Enabled = False
    foposi.Enabled = False
    fhojilla.Enabled = True
End Sub


Private Sub oposimpresora_Click()
    fserialimpresora.Enabled = False
    foposi.Enabled = True
    hsi_Click
End Sub

Private Sub serialimpresora_Click()
    fserialimpresora.Enabled = True
    foposi.Enabled = False
    fhojilla.Enabled = True
    hsi_Click
End Sub

Private Sub cheques_Click()
    Select Case cheques.Value
        Case Is = 0
            resp = Replace(cheques.Caption, "Si", "No", 1)
            Rotar.Enabled = False
            cheques.Caption = resp
        Case Is = 1
            resp = Replace(cheques.Caption, "No", "Si", 1)
            Rotar.Enabled = True
            cheques.Caption = resp
    End Select

End Sub
Private Sub hsi_Click()
    Select Case hsi.Value
        Case Is = 0
            hojilla_caracter.Enabled = False
            resp = Replace(hsi.Caption, "Si", "No", 1)
            hsi.Caption = resp
        Case Is = 1
            If oposimpresora.Value = False Then
                hojilla_caracter.Enabled = True
            Else
                hojilla_caracter.Enabled = False
            End If
            resp = Replace(hsi.Caption, "No", "Si", 1)
            hsi.Caption = resp
    End Select
End Sub


Private Sub lbl_salir_Click()
    Unload Me
End Sub

Private Sub lbl_salir_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lbl_salir.FontBold = True
End Sub


Private Sub limite_Click()
    Select Case limite.Value
        Case Is = 0
'            Call ACTIVA(LNIVEL, False)
            limite.Caption = Replace(limite.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(LNIVEL, True)
            limite.Caption = Replace(limite.Caption, "No", "Si", 1)
    End Select

End Sub

Private Sub lineal_digital_Click()
    Select Case lineal_digital.Value
        Case Is = 0
            lineal_digital.Caption = "No"
            FICHAS.TabEnabled(4) = False
            PANEL1.Enabled = False
            panel2.Enabled = False
        Case Is = 1
            lineal_digital.Caption = "Si"
            FICHAS.TabEnabled(4) = True
            PANEL1.Enabled = True
            panel2.Enabled = False
    End Select

End Sub



Private Sub precios_Click()
    Select Case precios.Value
        Case Is = 0
'            Call ACTIVA(PRNIVEL, False)
            precios.Caption = Replace(precios.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(PRNIVEL, True)
            precios.Caption = Replace(precios.Caption, "No", "Si", 1)
    End Select

End Sub

Private Sub productos_Click()
    Select Case productos.Value
        Case Is = 0
'            Call ACTIVA(PNIVEL, False)
            productos.Caption = Replace(productos.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(PNIVEL, True)
            productos.Caption = Replace(productos.Caption, "No", "Si", 1)
    End Select

End Sub


Private Sub reintegro_Click()
    Select Case reintegro.Value
        Case Is = 0
'            Call ACTIVA(RNIVEL, False)
            reintegro.Caption = Replace(reintegro.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(RNIVEL, True)
            reintegro.Caption = Replace(reintegro.Caption, "No", "Si", 1)
    End Select

End Sub


Private Sub sbtn_1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Index = 35 Then Exit Sub
    If sbtn_1(Index).Tag = "" Then
        eliminar_subboton.Enabled = False
    Else
        eliminar_subboton.Enabled = True
    End If
    datos_subboton.Caption = " Datos del bot�n # " & Format(Index, "00")
    PopupMenu subboton
End Sub


Private Sub suspender_Click()
    Select Case suspender.Value
        Case Is = 0
'            Call ACTIVA(SNIVEL, False)
            suspender.Caption = Replace(suspender.Caption, "Si", "No", 1)
        Case Is = 1
'            Call ACTIVA(SNIVEL, True)
            suspender.Caption = Replace(suspender.Caption, "No", "Si", 1)
    End Select

End Sub
Private Sub Gactivar(VALOR As Boolean)
    FCSERIAL.Enabled = VALOR
    GPuerto.Enabled = VALOR
    GBaudios.Enabled = VALOR
    GParidad.Enabled = VALOR
    GDato.Enabled = VALOR
    GParada.Enabled = VALOR
End Sub

Private Sub bactivar(VALOR As Boolean)
    bPuerto.Enabled = VALOR
    bBaudios.Enabled = VALOR
    bParidad.Enabled = VALOR
    bDato.Enabled = VALOR
    bParada.Enabled = VALOR
    bprefijo.Enabled = VALOR
End Sub
Private Sub Dactivar(VALOR As Boolean)
    DPuerto.Enabled = VALOR
    DBaudios.Enabled = VALOR
    DParidad.Enabled = VALOR
    DDato.Enabled = VALOR
    DParada.Enabled = VALOR
    clinea.Enabled = VALOR
    nlinea.Enabled = VALOR
End Sub

Private Sub iactivar(VALOR As Boolean)
    IPuerto.Enabled = VALOR
    IBaudios.Enabled = VALOR
    IParidad.Enabled = VALOR
    IDato.Enabled = VALOR
    IParada.Enabled = VALOR
End Sub

Private Sub Sactivar(VALOR As Boolean)
    sPuerto.Enabled = VALOR
    SBaudios.Enabled = VALOR
    SParidad.Enabled = VALOR
    SDato.Enabled = VALOR
    SParada.Enabled = VALOR
    sPrefijo.Enabled = VALOR
End Sub


Private Sub llenar_config()
    
'    copen.Value = 0
'    cclose.Value = 0
'
    ayuda.Value = 0
    productos.Value = 0
    limite.Value = 0
    cantidad.Value = 0
    balanzas.Value = 0
    espera.Value = 0
    suspender.Value = 0
    devolucion.Value = 0
    precios.Value = 0
    cliente.Value = 0

'    ACONTRASE�A.Value = 0
'    PCONTRASE�A.Value = 0
'    LCONTRASE�A.Value = 0
'    CCONTRASE�A.Value = 0
'    BCONTRASE�A.Value = 0
'    ECONTRASE�A.Value = 0
'    SCONTRASE�A.Value = 0
'    DCONTRASE�A.Value = 0
'    PRCONTRASE�A.Value = 0
'    CLCONTRASE�A.Value = 0
    nopen.Text = "1"
    nclose.Text = "1"
    
    ANIVEL.Text = "1"
    PNIVEL.Text = "1"
    LNIVEL.Text = "1"
    CNIVEL.Text = "1"
    BNIVEL.Text = "1"
    rnivel.Text = "1"
    ENIVEL.Text = "1"
    SNIVEL.Text = "1"
    DNIVEL.Text = "1"
    PRNIVEL.Text = "1"
    CLNIVEL.Text = "1"
    nopen.Text = "1"
    nclose.Text = "1"
    
    IPuerto.Text = "COM1"
    IBaudios.Text = "9600"
    IParidad.Text = "Ninguna"
    IDato.Text = "4"
    IParada.Text = "1"
    
    GPuerto.Text = "COM1"
    GBaudios.Text = "9600"
    GParidad.Text = "Ninguna"
    GDato.Text = "4"
    GParada.Text = "1"
    
    DPuerto.Text = "COM1"
    DBaudios.Text = "9600"
    DParidad.Text = "Ninguna"
    DDato.Text = "4"
    DParada.Text = "1"
'    nlinea.Text = "2"
'    clinea.Text = "2"
    
    bPuerto.Text = "COM1"
    bBaudios.Text = "9600"
    bParidad.Text = "Ninguna"
    bDato.Text = "4"
    bParada.Text = "1"
    bprefijo.Text = "w"
    
    sPuerto.Text = "COM1"
    SBaudios.Text = "9600"
    SParidad.Text = "Ninguna"
    SDato.Text = "4"
    SParada.Text = "1"
    sPrefijo.Text = ""
    
    cordenar.Value = 0
    cagrupar.Value = 0
    
    For i = 0 To 4
        linea(i).Text = ""
    Next

End Sub

Private Sub LLENAR_CAJA()
    Dim RSPRODUCTOS As New ADODB.Recordset
    
    codigo.Text = Format(rscaja!c_codigo, "000")
    descri.Text = rscaja!c_desc_caja
    
'    copen.Value = IIf(rscaja!copenpos = True, 1, 0)
'    cclose.Value = IIf(rscaja!cclosepos = True, 1, 0)
    
    dato_numerico.Value = IIf(rscaja!dato_string = True, 1, 0)
    
    ayuda.Value = IIf(rscaja!bayuda = True, 1, 0)
    productos.Value = IIf(rscaja!bproductos = True, 1, 0)
    limite.Value = IIf(rscaja!blimite = True, 1, 0)
    cantidad.Value = IIf(rscaja!bcantidad = True, 1, 0)
    balanzas.Value = IIf(rscaja!Bbalanza = True, 1, 0)
    reintegro.Value = IIf(rscaja!Breintegro = True, 1, 0)
    espera.Value = IIf(rscaja!bespera = True, 1, 0)
    suspender.Value = IIf(rscaja!bsuspender = True, 1, 0)
    devolucion.Value = IIf(rscaja!bdevolucion = True, 1, 0)
    precios.Value = IIf(rscaja!bprecios = True, 1, 0)
    cliente.Value = IIf(rscaja!bcliente = True, 1, 0)

'    ACONTRASE�A.Value = IIf(rscaja!cayuda = True, 1, 0)
'    PCONTRASE�A.Value = IIf(rscaja!cproductos = True, 1, 0)
'    LCONTRASE�A.Value = IIf(rscaja!climite = True, 1, 0)
'    CCONTRASE�A.Value = IIf(rscaja!ccantidad = True, 1, 0)
'    RCONTRASE�A.Value = IIf(rscaja!creintegro = True, 1, 0)
'    BCONTRASE�A.Value = IIf(rscaja!cbalanza = True, 1, 0)
'    ECONTRASE�A.Value = IIf(rscaja!cespera = True, 1, 0)
'    SCONTRASE�A.Value = IIf(rscaja!csuspender = True, 1, 0)
'    DCONTRASE�A.Value = IIf(rscaja!cdevolucion = True, 1, 0)
'    PRCONTRASE�A.Value = IIf(rscaja!cprecios = True, 1, 0)
'    CLCONTRASE�A.Value = IIf(rscaja!ccliente = True, 1, 0)
    
    ANIVEL.Text = rscaja!nayuda
    PNIVEL.Text = rscaja!nproductos
    LNIVEL.Text = rscaja!nlimite
    CNIVEL.Text = rscaja!ncantidad
    BNIVEL.Text = rscaja!nbalanza
    rnivel.Text = rscaja!nreintegro
    ENIVEL.Text = rscaja!nespera
    SNIVEL.Text = rscaja!nsuspender
    DNIVEL.Text = rscaja!ndevolucion
    PRNIVEL.Text = rscaja!nprecios
    CLNIVEL.Text = rscaja!ncliente
    nopen.Text = rscaja!nopenpos
    nclose.Text = rscaja!nopenpos
    
    
    ALLAVE.Text = rscaja!Layuda
    PLLAVE.Text = rscaja!Lproductos
    LLLAVE.Text = rscaja!Llimite
    CLLAVE.Text = rscaja!Lcantidad
    BLLAVE.Text = rscaja!Lbalanza
    rllave.Text = rscaja!Lreintegro
    ELLAVE.Text = rscaja!Lespera
    SLLAVE.Text = rscaja!Lsuspender
    DLLAVE.Text = rscaja!Ldevolucion
    PRLLAVE.Text = rscaja!Lprecios
    CLLLAVE.Text = rscaja!Lcliente
    cellave.Text = rscaja!LCERRAR
    
    Select Case rscaja!gabeta
        Case 0
            nogaveta.Value = True
        Case 1
            oposgaveta.Value = True
        Case 2
            serialgaveta.Value = True
    End Select
    
    If rscaja!gserial = True Then
        tconexion(0).Value = True
    Else
        tconexion(1).Value = True
    End If
    
    GPuerto.Text = rscaja!GPuerto
    GBaudios.Text = rscaja!GBaudios
    Select Case rscaja!GParidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    GParidad.Text = Paridad
    GDato.Text = rscaja!Gbit
    GParada.Text = rscaja!GParada
    
    
    Select Case rscaja!DISPLAY
        Case 0
            NODISPLAY.Value = True
        Case 1
            OPOSDISPLAY.Value = True
        Case 2
            SERIALDISPLAY.Value = True

    End Select
    DPuerto.Text = rscaja!DPuerto
    DBaudios.Text = rscaja!DBaudios
    Select Case rscaja!DParidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    DParidad.Text = Paridad
    DDato.Text = rscaja!dbit
    DParada.Text = rscaja!DParada
    
'    nlinea.Text = rscaja!dno_lineas
'    clinea.Text = rscaja!dca_lineas
    
    
    
    
    Select Case rscaja!IMPRESORA
        Case 0
            noimpresora.Value = True
        Case 1
            oposimpresora.Value = True
        Case 2
            serialimpresora.Value = True
        
    End Select
    IPuerto.Text = rscaja!IPuerto
    IBaudios.Text = rscaja!IBaudios
    Select Case rscaja!IParidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    IParidad.Text = Paridad
    IDato.Text = rscaja!IBit
    IParada.Text = rscaja!IParada
    hsi.Value = IIf(rscaja!ihojilla = True, 1, 0)
    hsi_Click
    hojilla_caracter = IIf(IsNull(rscaja!hojilla_caracter), "", rscaja!hojilla_caracter)
    
    
    Select Case rscaja!balanza
        Case 0
            nobalanza.Value = True
        Case 1
            Oposbalanza.Value = True
        Case 2
            serialbalanza.Value = True
        
    End Select
    bPuerto.Text = rscaja!bPuerto
    bBaudios.Text = rscaja!bBaudios
    Select Case rscaja!bParidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    bParidad.Text = Paridad
    bDato.Text = rscaja!Bbit
    bParada.Text = rscaja!bParada
    bprefijo.Text = IIf(IsNull(rscaja!BCONTROL), "", rscaja!BCONTROL)
    
    
    Select Case rscaja!banda
        Case 0
            nobanda.Value = True
        Case 1
            Oposbanda.Value = True
        Case 2
            serialbanda.Value = True
        
    End Select
    bmpuerto.Text = IIf(IsNull(rscaja!bmpuerto), "COM1", rscaja!bmpuerto)
    bmbaudios.Text = IIf(IsNull(rscaja!bmbaudios), "9600", rscaja!bmbaudios)
    Select Case rscaja!bmparidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    bmparidad.Text = Paridad
    bmdato.Text = rscaja!bmbit
    bmparada.Text = rscaja!bmparada
    
    Select Case rscaja!scanner
        Case 0
            noscanner.Value = True
        Case 1
            Oposscanner.Value = True
        Case 2
            serialscanner.Value = True
        
    End Select
    sPuerto.Text = rscaja!sPuerto
    SBaudios.Text = rscaja!SBaudios
    Select Case rscaja!SParidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    SParidad.Text = Paridad
    SDato.Text = rscaja!Sbit
    SParada.Text = rscaja!SParada
    sPrefijo.Text = IIf(IsNull(rscaja!SCONTROL), "", rscaja!SCONTROL)
    
    Select Case rscaja!llaves
        Case 0
            nollaves.Value = True
        Case 1
            oposllaves.Value = True
    End Select
    
    
    
    cordenar.Value = IIf(rscaja!imp_ordenada = True, 1, 0)
    cagrupar.Value = IIf(rscaja!imp_agrupada = True, 1, 0)
    
    forma_continua.Value = IIf(rscaja!forma_continua = True, 1, 0)
    no_lineas.Text = rscaja!no_lineas
    linea(0).Text = IIf(IsNull(rscaja!MENSAJE1), "", rscaja!MENSAJE1)
    linea(1).Text = IIf(IsNull(rscaja!MENSAJE2), "", rscaja!MENSAJE2)
    linea(2).Text = IIf(IsNull(rscaja!MENSAJE3), "", rscaja!MENSAJE3)
    linea(3).Text = IIf(IsNull(rscaja!MENSAJE4), "", rscaja!MENSAJE4)
    linea(4).Text = IIf(IsNull(rscaja!MENSAJE5), "", rscaja!MENSAJE5)

    fondo_pos.Value = IIf(rscaja!B_SOLICITAR_FONDO = True, 1, 0)
    coddeposito.Text = IIf(IsNull(rscaja!c_coddeposito), "", rscaja!c_coddeposito)
    lineal_digital.Value = IIf(rscaja!B_POS_DIGITAL = False, 0, 1)
    cheques.Value = IIf(rscaja!IMP_CHEQUES = True, 1, 0)
    Rotar.Text = rscaja!ROTACION
    
    opos.Value = IIf(rscaja!b_opos = False, 0, 1)
    opos_Click
    OPOSD.Text = IIf(IsNull(rscaja!C_OposD), "", rscaja!C_OposD)
    oposg.Text = IIf(IsNull(rscaja!C_OposG), "", rscaja!C_OposG)
    oposs.Text = IIf(IsNull(rscaja!C_OposS), "", rscaja!C_OposS)
    oposba.Text = IIf(IsNull(rscaja!C_OposBA), "", rscaja!C_OposBA)
    oposb.Text = IIf(IsNull(rscaja!C_OposB), "", rscaja!C_OposB)
    oposl.Text = IIf(IsNull(rscaja!C_OposL), "", rscaja!C_OposL)
    oposi.Text = IIf(IsNull(rscaja!C_OposI), "", rscaja!C_OposI)
'    opost.Text = IIf(IsNull(rscaja!C_OposT), "", rscaja!C_OposT)
    
    
'    Call buscar_deposito
    
End Sub


Private Sub ACTIVAR_FRAME(VALOR As Boolean)
    barra_menu.Buttons(1).Enabled = VALOR
    barra_menu.Buttons(2).Enabled = VALOR
    barra_menu.Buttons(4).Enabled = IIf(VALOR = True, False, True)
    fconfig.Enabled = VALOR
    FGAVETA.Enabled = VALOR
    FSCANNER.Enabled = VALOR
    FBALANZA.Enabled = VALOR
    fbanda.Enabled = VALOR
    FDISPLAY.Enabled = VALOR
    fimpresora.Enabled = VALOR
    fllaves.Enabled = VALOR
End Sub


Private Sub GRABAR_CAJA()
    If coddeposito.Text = "" Then
        Call mensaje(True, "Debe seleccionar un Dep�sito para las ventas...!")
        coddeposito.SetFocus
        Exit Sub
    End If
    Ent.POS.BeginTrans
        If config_group = False Then
            Call apertura_recordset(False, rscaja)
            rscaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & codigo.Text & "'", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If glgrabar = False Then
                rscaja.Update
                rscaja!c_codigo = Format(codigo.Text, "00#")
            Else
                Call apertura_recordset(False, rscaja)
                rscaja.Open "ma_caja", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                rscaja.AddNew
                rscaja!c_codigo = Format(codigo.Text, "00#")
                rscaja!ncierre = 1
                rscaja!c_relacion = lckey
                CLAVE = "C" & Format(codigo.Text, "000")
            End If
            rscaja!B_POS_DIGITAL = lineal_digital.Value
            rscaja!c_desc_caja = UCase(descri.Text)
            
            Call grabar_detalle
            rscaja.UpdateBatch
            
            
            Call apertura_recordset(False, rsbotones_TMP)
            rsbotones_TMP.Open "ma_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
            
            Ent.POS.Execute ("delete ma_botones where c_caja = '" & codigo.Text & "'")
            
            If Not rsbotones_TMP.EOF Then
                
                Call apertura_recordset(False, rstrbotones)
                rstrbotones.Open "ma_botones", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                
                Do Until rsbotones_TMP.EOF
                    rstrbotones.AddNew
                        rstrbotones!c_codigo = rsbotones_TMP!c_codigo
                        rstrbotones!c_caja = rsbotones_TMP!c_caja
                        rstrbotones!n_boton = rsbotones_TMP!n_boton
                        rstrbotones!B_GRUPO = rsbotones_TMP!B_GRUPO
                        rstrbotones!nombre_grupo = rsbotones_TMP!nombre_grupo
                        
                    rstrbotones.UpdateBatch
                    
                    rsbotones_TMP.Delete
                    rsbotones_TMP.UpdateBatch
                    rsbotones_TMP.MoveNext
                Loop
            End If
                        
            Call apertura_recordset(False, rstrbotones_TMP)
            rstrbotones_TMP.Open "tr_botones_tmp", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
            
            Ent.POS.Execute ("delete TR_botones where c_caja = '" & codigo.Text & "'")
            
            If Not rstrbotones_TMP.EOF Then
                '*** abre la tabla tmp de subbotones
                Call apertura_recordset(False, rstrbotones)
                rstrbotones.Open "tr_botones", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                Do Until rstrbotones_TMP.EOF
                    rstrbotones.AddNew
                        rstrbotones!c_codigo = rstrbotones_TMP!c_codigo
                        rstrbotones!c_caja = rstrbotones_TMP!c_caja
                        rstrbotones!n_boton = rstrbotones_TMP!n_boton
                        rstrbotones!n_subboton = rstrbotones_TMP!n_subboton
                    rstrbotones.UpdateBatch
                    
                    rstrbotones_TMP.Delete
                    rstrbotones_TMP.UpdateBatch
                    rstrbotones_TMP.MoveNext
                Loop
            End If
            
            If glgrabar = False Then
                ARBOL.Nodes.Item(lcindex).Text = UCase(descri.Text)
            Else
                Call crear_menu(lckey, CLAVE, UCase(descri.Text), "caja")
            End If
            
        Else
            Call apertura_recordset(False, rscaja)
            rscaja.Open "select * from ma_caja where c_relacion = '" & lckey & "'", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rscaja.EOF Then
                Do Until rscaja.EOF
                    rscaja.Update
                        Call grabar_detalle
                    rscaja.UpdateBatch
                    rscaja.MoveNext
                Loop
            End If
        End If
    Ent.POS.CommitTrans
    Call cerrar_recordset(rscaja)
    Call cerrar_recordset(rsbotones)
    Call cerrar_recordset(rsbotones_TMP)
    Call cerrar_recordset(rstrbotones)
    Call cerrar_recordset(rstrbotones_TMP)
End Sub

Private Sub grabar_detalle()
    rscaja!dato_string = IIf(dato_numerico.Value = 1, 1, 0)
    rscaja!bayuda = IIf(ayuda.Value = 1, 1, 0)
    rscaja!bproductos = IIf(productos.Value = 1, 1, 0)
    rscaja!blimite = IIf(limite.Value = 1, 1, 0)
    rscaja!bcantidad = IIf(cantidad.Value = 1, 1, 0)
    rscaja!Bbalanza = IIf(balanzas.Value = 1, 1, 0)
    rscaja!Breintegro = IIf(reintegro.Value = 1, 1, 0)
    rscaja!bespera = IIf(espera.Value = 1, 1, 0)
    rscaja!bsuspender = IIf(suspender.Value = 1, 1, 0)
    rscaja!bdevolucion = IIf(devolucion.Value = 1, 1, 0)
    rscaja!bprecios = IIf(precios.Value = 1, 1, 0)
    rscaja!bcliente = IIf(cliente.Value = 1, 1, 0)

'    rscaja!copenpos = IIf(copen.Value = 1, 1, 0)
'    rscaja!cayuda = IIf(ACONTRASE�A.Value = 1, 1, 0)
'    rscaja!cproductos = IIf(PCONTRASE�A.Value = 1, 1, 0)
'    rscaja!climite = IIf(LCONTRASE�A.Value = 1, 1, 0)
'    rscaja!ccantidad = IIf(CCONTRASE�A.Value = 1, 1, 0)
'    rscaja!cbalanza = IIf(BCONTRASE�A.Value = 1, 1, 0)
'    rscaja!creintegro = IIf(RCONTRASE�A.Value = 1, 1, 0)
'    rscaja!cespera = IIf(ECONTRASE�A.Value = 1, 1, 0)
'    rscaja!csuspender = IIf(SCONTRASE�A.Value = 1, 1, 0)
'    rscaja!cdevolucion = IIf(DCONTRASE�A.Value = 1, 1, 0)
'    rscaja!cprecios = IIf(PRCONTRASE�A.Value = 1, 1, 0)
'    rscaja!ccliente = IIf(CLCONTRASE�A.Value = 1, 1, 0)
'    rscaja!cclosepos = IIf(cclose.Value = 1, 1, 0)
    
    rscaja!copenpos = IIf(CInt(nopen.Text) <> 0, 1, 0)
    rscaja!cayuda = IIf(CInt(ANIVEL.Text) <> 0, 1, 0)
    rscaja!cproductos = IIf(CInt(PNIVEL.Text) <> 0, 1, 0)
    rscaja!climite = IIf(CInt(LNIVEL.Text) <> 0, 1, 0)
    rscaja!ccantidad = IIf(CInt(CNIVEL.Text) <> 0, 1, 0)
    rscaja!cbalanza = IIf(CInt(BNIVEL.Text) <> 0, 1, 0)
    rscaja!creintegro = IIf(CInt(rnivel.Text) <> 0, 1, 0)
    rscaja!cespera = IIf(CInt(ENIVEL.Text) <> 0, 1, 0)
    rscaja!csuspender = IIf(CInt(SNIVEL.Text) <> 0, 1, 0)
    rscaja!cdevolucion = IIf(CInt(DNIVEL.Text) <> 0, 1, 0)
    rscaja!cprecios = IIf(CInt(PNIVEL.Text) <> 0, 1, 0)
    rscaja!ccliente = IIf(CInt(CNIVEL.Text) <> 0, 1, 0)
    rscaja!cclosepos = IIf(CInt(nopen.Text) <> 0, 1, 0)
    
    rscaja!nopenpos = CInt(nopen.Text)
    rscaja!nayuda = CInt(ANIVEL.Text)
    rscaja!nproductos = CInt(PNIVEL.Text)
    rscaja!nlimite = CInt(LNIVEL.Text)
    rscaja!ncantidad = CInt(CNIVEL.Text)
    rscaja!nbalanza = CInt(BNIVEL.Text)
    rscaja!nreintegro = CInt(rnivel.Text)
    rscaja!nespera = CInt(ENIVEL.Text)
    rscaja!nsuspender = CInt(SNIVEL.Text)
    rscaja!ndevolucion = CInt(DNIVEL.Text)
    rscaja!nprecios = CInt(PNIVEL.Text)
    rscaja!ncliente = CInt(CNIVEL.Text)
    rscaja!nclosepos = CInt(nclose.Text)
    
    
    rscaja!Layuda = CInt(ALLAVE.Text)
    rscaja!Lproductos = CInt(PLLAVE.Text)
    rscaja!Llimite = CInt(LLLAVE.Text)
    rscaja!Lcantidad = CInt(CLLAVE.Text)
    rscaja!Lbalanza = CInt(BLLAVE.Text)
    rscaja!Lreintegro = CInt(rllave.Text)
    rscaja!Lespera = CInt(ELLAVE.Text)
    rscaja!Lsuspender = CInt(SLLAVE.Text)
    rscaja!Ldevolucion = CInt(DLLAVE.Text)
    rscaja!Lprecios = CInt(PLLAVE.Text)
    rscaja!Lcliente = CInt(CLLAVE.Text)
    rscaja!LCERRAR = CInt(cellave.Text)
    
    
    
    If nogaveta.Value = True Then
        numero = 0
    ElseIf oposgaveta.Value = True Then
        numero = 1
    Else
        numero = 2
    End If
    rscaja!gabeta = numero
    rscaja!gserial = tconexion(0).Value
    
    rscaja!GPuerto = GPuerto.Text
    rscaja!GBaudios = GBaudios.Text
    Select Case GParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    rscaja!GParidad = Paridad
    rscaja!Gbit = CInt(GDato.Text)
    rscaja!GParada = CDbl(GParada.Text)
    rscaja!Gcontrol = IIf(IsNull(Gcontrol.Text), "", Gcontrol.Text)
    
    If NODISPLAY.Value = True Then
        numero = 0
    ElseIf OPOSDISPLAY.Value = True Then
        numero = 1
    Else
        numero = 2
    End If
    rscaja!DISPLAY = numero
    rscaja!DPuerto = DPuerto.Text
    rscaja!DBaudios = DBaudios.Text
    Select Case DParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    rscaja!DParidad = Paridad
    rscaja!dbit = CInt(DDato.Text)
    rscaja!DParada = CDbl(DParada.Text)
    
    If noimpresora.Value = True Then
        numero = 0
    ElseIf oposimpresora.Value = True Then
        numero = 1
    Else
        numero = 2
    End If
    rscaja!IMPRESORA = numero
    rscaja!IPuerto = IPuerto.Text
    rscaja!IBaudios = IBaudios.Text
    Select Case IParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    rscaja!IParidad = Paridad
    rscaja!IBit = CInt(IDato.Text)
    rscaja!IParada = CDbl(IParada.Text)
    rscaja!ihojilla = hsi.Value
    rscaja!hojilla_caracter = hojilla_caracter
    rscaja!IMP_CHEQUES = cheques.Value
    rscaja!ROTACION = CInt(Rotar.Text)
'    rscaja!dno_lineas = CInt(nlinea.Text)
'    rscaja!dca_lineas = CInt(clinea.Text)
    
    
    If nobalanza.Value = True Then
        numero = 0
    ElseIf Oposbalanza.Value = True Then
        numero = 1
    Else
        numero = 2
    End If
    rscaja!balanza = numero
    rscaja!bPuerto = bPuerto.Text
    rscaja!bBaudios = bBaudios.Text
    Select Case bParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    rscaja!bParidad = Paridad
    rscaja!Bbit = CInt(bDato.Text)
    rscaja!bParada = CDbl(bParada.Text)
    rscaja!BCONTROL = IIf(IsNull(bprefijo.Text), "", bprefijo.Text)
    
    
    If noscanner.Value = True Then
        numero = 0
    ElseIf Oposscanner.Value = True Then
        numero = 1
    Else
        numero = 2
    End If
    rscaja!scanner = numero
    rscaja!sPuerto = sPuerto.Text
    rscaja!SBaudios = SBaudios.Text
    Select Case SParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    rscaja!SParidad = Paridad
    rscaja!Sbit = CInt(SDato.Text)
    rscaja!SParada = CDbl(SParada.Text)
    rscaja!SCONTROL = IIf(IsNull(sPrefijo.Text), "", sPrefijo.Text)
    
    If nobanda.Value = True Then
        numero = 0
    ElseIf Oposbanda.Value = True Then
        numero = 1
    Else
        numero = 2
    End If
    rscaja!banda = numero
    rscaja!bmpuerto = sPuerto.Text
    rscaja!bmbaudios = SBaudios.Text
    Select Case bmparidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    rscaja!bmparidad = Paridad
    rscaja!bmbit = CInt(SDato.Text)
    rscaja!bmparada = CDbl(SParada.Text)
    
    If nollaves.Value = True Then
        numero = 0
    ElseIf oposllaves.Value = True Then
        numero = 1
    End If
    rscaja!llaves = numero
    
    
    rscaja!imp_ordenada = IIf(cordenar.Value = 1, 1, 0)
    rscaja!imp_agrupada = IIf(cagrupar.Value = 1, 1, 0)
    
    rscaja!forma_continua = IIf(forma_continua.Value = 1, 1, 0)
    rscaja!no_lineas = Int(no_lineas.Text)
    
    rscaja!MENSAJE1 = IIf(IsNull(linea(0).Text), "", linea(0).Text)
    rscaja!MENSAJE2 = IIf(IsNull(linea(1).Text), "", linea(1).Text)
    rscaja!MENSAJE3 = IIf(IsNull(linea(2).Text), "", linea(2).Text)
    rscaja!MENSAJE4 = IIf(IsNull(linea(3).Text), "", linea(3).Text)
    rscaja!MENSAJE5 = IIf(IsNull(linea(4).Text), "", linea(4).Text)
    
    rscaja!B_SOLICITAR_FONDO = IIf(fondo_pos.Value = 1, 1, 0)
    rscaja!c_coddeposito = coddeposito.Text
    
    rscaja!b_opos = IIf(opos.Value = 1, 1, 0)
    rscaja!C_OposD = OPOSD.Text
    
    rscaja!C_OposG = oposg.Text
    rscaja!C_OposS = oposs.Text
    rscaja!C_OposB = oposb.Text
    rscaja!C_OposBA = oposba.Text
    rscaja!C_OposL = oposl.Text
    rscaja!C_OposI = oposi.Text
'    rscaja!C_OposT = opost.Text
End Sub

Private Sub Text2_Change()

End Sub

Private Sub view_Click()
    coddeposito.SetFocus
    Call coddeposito_KeyDown(vbKeyF2, 0)
End Sub


Private Sub cancelar()
    barra_menu.Buttons(2).Enabled = True
    grp_btn.Value = 0
    boton_producto.Text = ""
    boton_descri.Caption = ""
    datos_boton.Enabled = False
    PANEL1.Enabled = True
    datos_boton.Caption = " Datos del bot�n # "
    For i = 0 To 34
        sbtn_1(i).Caption = ""
        sbtn_1(i).ToolTipText = ""
        sbtn_1(i).Tag = ""
    Next
    Ent.POS.BeginTrans
        Call apertura_recordset(False, rstrbotones)
        rstrbotones.Open "tr_botones_tmp1", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
        If Not rstrbotones.EOF Then
            Do Until rstrbotones.EOF
                rstrbotones.Delete
                rstrbotones.UpdateBatch
                rstrbotones.MoveNext
            Loop
        End If
        Call cerrar_recordset(rstrbotones)
    Ent.POS.CommitTrans
End Sub

Private Sub subboton_producto_LostFocus()
    Call buscar_producto(subboton_producto, subboton_descri)
End Sub
Private Sub subboton_producto_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            KeyAscii = 0
            SendKeys Chr$(9)
    End Select
End Sub

Private Sub buscar_producto(campo_cod As Control, campo_des As Control)
    Dim codigo_prod As String
    Call apertura_recordset(True, rseureka)
    rseureka.Open "select * from MA_CODIGOS where c_codigo = '" & Trim(campo_cod) & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly
    Set rseureka.ActiveConnection = Nothing
        If Not rseureka.EOF Then
            codigo_prod = rseureka!c_codnasa
            Call cerrar_recordset(rseureka)
            Call apertura_recordset(True, rseureka)
            rseureka.Open "select c_codigo,c_descri,n_activo from MA_productos where c_codigo = '" & codigo_prod & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly
            Set rseureka.ActiveConnection = Nothing
            '///////
            If rseureka.EOF = True And rseureka.BOF = True Then
                Call mensaje(True, "No existe el producto...!")
                campo_cod = ""
                campo_cod.Caption = ""
                campo_cod.SetFocus
                Call cerrar_recordset(rseureka)
                Exit Sub
            End If
            '/////////

            If rseureka!n_activo = 0 Then
                boton_producto = ""
                Call mensaje(True, "El productos se encuentra suspendido")
                campo_cod = ""
'                campo_cod= ""
                campo_cod.SetFocus
                Exit Sub
            End If
            campo_des.Caption = rseureka!c_descri
            campo_cod.Text = rseureka!c_codigo

        Else
            Call mensaje(True, "No existe el producto...!")
            campo_cod = ""
            campo_cod.Text = ""
            campo_cod.SetFocus
            Call cerrar_recordset(rseureka)
        End If

End Sub


Private Sub OBJETOS_OPOS(VALOR As Boolean)
    oposgaveta.Enabled = VALOR
    OPOSDISPLAY.Enabled = VALOR
    'Oposscanner.Enabled = VALOR
    'Oposbalanza.Enabled = VALOR
    Oposbanda.Enabled = VALOR
    oposllaves.Enabled = VALOR
    
    ALLAVE.Enabled = VALOR
    PLLAVE.Enabled = VALOR
    LLLAVE.Enabled = VALOR
    CLLAVE.Enabled = VALOR
    BLLAVE.Enabled = VALOR
    ELLAVE.Enabled = VALOR
    rllave.Enabled = VALOR
    SLLAVE.Enabled = VALOR
    DLLAVE.Enabled = VALOR
    PRLLAVE.Enabled = VALOR
    CLLLAVE.Enabled = VALOR


End Sub
