VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "comct232.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form general 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9975
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   13965
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "general.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9975
   ScaleWidth      =   13965
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1050
      ScaleWidth      =   14025
      TabIndex        =   67
      Top             =   421
      Width           =   14055
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   1080
         Left            =   0
         TabIndex        =   68
         Top             =   0
         Width           =   13700
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   300
            TabIndex        =   69
            Top             =   120
            Width           =   13000
            _ExtentX        =   22939
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "F4 Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "F7 Cancelar"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "F1 Ayuda del SIstema"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   4
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   64
      Top             =   0
      Width           =   14040
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   11595
         TabIndex        =   66
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Datos Generales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   65
         Top             =   75
         Width           =   5415
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Configuraci�n del Tickets de Impresi�n "
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   1815
      Left            =   210
      TabIndex        =   19
      Top             =   7920
      Width           =   13560
      Begin VB.TextBox no_lineas 
         Alignment       =   1  'Right Justify
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   3390
         Locked          =   -1  'True
         TabIndex        =   7
         Text            =   "33"
         Top             =   690
         Width           =   405
      End
      Begin VB.CheckBox forma_continua 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Utiliza Papel de Forma Continua"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   225
         TabIndex        =   6
         Top             =   345
         Width           =   3465
      End
      Begin VB.CheckBox cagrupar 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   8475
         TabIndex        =   9
         Top             =   735
         Width           =   615
      End
      Begin VB.CheckBox cordenar 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   8475
         TabIndex        =   8
         Top             =   375
         Width           =   615
      End
      Begin ComCtl2.UpDown uplineas 
         Height          =   315
         Left            =   3795
         TabIndex        =   20
         Top             =   690
         Width           =   240
         _ExtentX        =   450
         _ExtentY        =   556
         _Version        =   327681
         Value           =   10
         OrigLeft        =   3540
         OrigTop         =   690
         OrigRight       =   3780
         OrigBottom      =   1035
         Max             =   100
         Min             =   10
         SyncBuddy       =   -1  'True
         Wrap            =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   0   'False
      End
      Begin VB.TextBox Observaciones 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   450
         Left            =   1620
         MaxLength       =   512
         MultiLine       =   -1  'True
         TabIndex        =   52
         Top             =   1155
         Width           =   11685
      End
      Begin VB.Label Label18 
         BackStyle       =   0  'Transparent
         Caption         =   " Configuraci�n del Tickets de Impresi�n "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   76
         Top             =   0
         Width           =   4215
      End
      Begin VB.Label Label57 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3780
         TabIndex        =   25
         Top             =   390
         Width           =   165
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Comentario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   0
         Left            =   240
         TabIndex        =   24
         Top             =   1155
         Width           =   990
      End
      Begin VB.Label Label58 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "N�mero de L�neas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   23
         Top             =   750
         Width           =   1545
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Agrupar Productos al Imprimir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5565
         TabIndex        =   22
         Top             =   735
         Width           =   2595
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ordenar Productos al Imprimir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5565
         TabIndex        =   21
         Top             =   375
         Width           =   2610
      End
   End
   Begin VB.Frame fdescripcion 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      Height          =   5970
      Left            =   210
      TabIndex        =   10
      Top             =   1665
      Width           =   13560
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Caption         =   "Frame3"
         ForeColor       =   &H80000008&
         Height          =   970
         Left            =   8520
         TabIndex        =   70
         Top             =   4740
         Width           =   4815
         Begin MSComCtl2.DTPicker Hora_cierre 
            Height          =   345
            Left            =   300
            TabIndex        =   71
            Top             =   490
            Width           =   1875
            _ExtentX        =   3307
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   -2147483633
            CalendarForeColor=   5790296
            CalendarTitleForeColor=   5790296
            Format          =   234291202
            CurrentDate     =   38069
         End
         Begin MSComCtl2.DTPicker Hora_Cierre_Max 
            Height          =   345
            Left            =   2580
            TabIndex        =   72
            Top             =   490
            Width           =   1875
            _ExtentX        =   3307
            _ExtentY        =   609
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   -2147483633
            CalendarForeColor=   5790296
            CalendarTitleForeColor=   5790296
            Format          =   234291202
            CurrentDate     =   38069
         End
         Begin VB.Label Label15 
            BackStyle       =   0  'Transparent
            Caption         =   "No permitir cerrar la caja si hay mesas abiertas entre las:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   435
            Left            =   240
            TabIndex        =   73
            Top             =   15
            UseMnemonic     =   0   'False
            Width           =   4110
         End
      End
      Begin VB.TextBox TxtConsumo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   7870
         Locked          =   -1  'True
         MaxLength       =   255
         TabIndex        =   62
         Text            =   "0"
         Top             =   4125
         Width           =   465
      End
      Begin MSComCtl2.UpDown Actualizar 
         Height          =   360
         Left            =   3855
         TabIndex        =   60
         Top             =   4050
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   635
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "TxtActualizar"
         BuddyDispid     =   196630
         OrigLeft        =   3945
         OrigTop         =   2970
         OrigRight       =   4200
         OrigBottom      =   3255
         Max             =   100
         Min             =   1
         SyncBuddy       =   -1  'True
         Wrap            =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.TextBox TxtActualizar 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3405
         Locked          =   -1  'True
         MaxLength       =   255
         TabIndex        =   58
         Text            =   "0"
         Top             =   4050
         Width           =   465
      End
      Begin VB.CommandButton cmd_info 
         Caption         =   "&Info"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1155
         Left            =   11715
         Picture         =   "general.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   1980
         Width           =   1290
      End
      Begin VB.TextBox TxtImpresoraDeCheques 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3420
         MaxLength       =   255
         TabIndex        =   55
         Top             =   3045
         Width           =   5730
      End
      Begin VB.CommandButton Cmd_Impresora_Cheques 
         Height          =   360
         Left            =   9270
         Picture         =   "general.frx":800C
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   3045
         Width           =   360
      End
      Begin VB.ComboBox TipoPuertoCheque 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   9735
         Style           =   2  'Dropdown List
         TabIndex        =   53
         Top             =   3045
         Width           =   1425
      End
      Begin VB.ComboBox CampoBusqueda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11145
         Style           =   2  'Dropdown List
         TabIndex        =   51
         Top             =   3585
         Width           =   1980
      End
      Begin VB.ComboBox TipoPrecio 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   6765
         Style           =   2  'Dropdown List
         TabIndex        =   49
         Top             =   3585
         Width           =   1860
      End
      Begin VB.CheckBox chk_AdminClientes 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3555
         TabIndex        =   46
         Top             =   3600
         Width           =   795
      End
      Begin VB.Frame FrameLocales 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Caption         =   " Comandas Locales  "
         ForeColor       =   &H80000008&
         Height          =   970
         Left            =   4455
         TabIndex        =   41
         Top             =   4740
         Width           =   3780
         Begin VB.CheckBox LLLevar 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Caption         =   "Imprimir Comandas Llevar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   195
            Left            =   195
            TabIndex        =   45
            Top             =   615
            Width           =   3165
         End
         Begin VB.CheckBox LComandas 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Caption         =   "Imprimir Comandas"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   195
            Left            =   195
            TabIndex        =   44
            Top             =   280
            Width           =   3165
         End
         Begin VB.Label Label16 
            BackStyle       =   0  'Transparent
            Caption         =   " Comandas Locales  "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   0
            TabIndex        =   74
            Top             =   0
            Width           =   1815
         End
      End
      Begin VB.Frame FrameRemotas 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Caption         =   " Comandas Remotas  "
         ForeColor       =   &H80000008&
         Height          =   970
         Left            =   285
         TabIndex        =   40
         Top             =   4740
         Width           =   3900
         Begin VB.CheckBox RLlevar 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Caption         =   "Imprimir Comandas Llevar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   195
            Left            =   240
            TabIndex        =   43
            Top             =   615
            Width           =   3165
         End
         Begin VB.CheckBox RComandas 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Caption         =   "Imprimir Comandas"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   240
            TabIndex        =   42
            Top             =   280
            Width           =   3165
         End
         Begin VB.Label Label17 
            BackStyle       =   0  'Transparent
            Caption         =   " Comandas Remotas  "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   0
            TabIndex        =   75
            Top             =   0
            Width           =   1935
         End
      End
      Begin VB.ComboBox TipoPuerto 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   9735
         Style           =   2  'Dropdown List
         TabIndex        =   39
         Top             =   2625
         Width           =   1425
      End
      Begin VB.CheckBox Autodeclaracion 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   8520
         TabIndex        =   37
         Top             =   855
         Width           =   675
      End
      Begin MSComDlg.CommonDialog Impresoras 
         Left            =   1725
         Top             =   240
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton cmd_imprimir 
         Height          =   360
         Left            =   9270
         Picture         =   "general.frx":880E
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   2625
         Width           =   360
      End
      Begin VB.TextBox TxtUbicacion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3420
         MaxLength       =   255
         TabIndex        =   34
         Top             =   2625
         Width           =   5730
      End
      Begin VB.TextBox TxtMoneda 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3420
         TabIndex        =   32
         Top             =   1785
         Width           =   1905
      End
      Begin VB.CommandButton cmd_moneda 
         Height          =   360
         Left            =   5430
         Picture         =   "general.frx":9010
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   1800
         Width           =   360
      End
      Begin VB.TextBox Lbl_Moneda 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   5910
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   1800
         Width           =   5250
      End
      Begin VB.CheckBox cClaveOpen 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   12750
         TabIndex        =   28
         Top             =   1350
         Width           =   675
      End
      Begin VB.CheckBox cVxV 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3510
         TabIndex        =   26
         Top             =   1335
         Width           =   675
      End
      Begin VB.TextBox lbl_deposito 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   5910
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   2205
         Width           =   5250
      End
      Begin VB.CommandButton view 
         Height          =   360
         Left            =   5430
         Picture         =   "general.frx":9812
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   2205
         Width           =   360
      End
      Begin VB.TextBox coddeposito 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3420
         TabIndex        =   4
         Top             =   2205
         Width           =   1905
      End
      Begin VB.CheckBox fondo_pos 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   8520
         TabIndex        =   3
         Top             =   1320
         Width           =   675
      End
      Begin VB.CheckBox dato_numerico 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   12750
         TabIndex        =   2
         Top             =   855
         Width           =   675
      End
      Begin VB.TextBox descri 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3420
         MaxLength       =   50
         TabIndex        =   0
         Top             =   300
         Width           =   9585
      End
      Begin VB.TextBox codigo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1080
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   11
         Text            =   "1"
         Top             =   300
         Width           =   525
      End
      Begin VB.CheckBox opos 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3510
         TabIndex        =   1
         Top             =   870
         Width           =   675
      End
      Begin MSComCtl2.UpDown Consumo 
         Height          =   360
         Left            =   8350
         TabIndex        =   61
         Top             =   4125
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   635
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "TxtConsumo"
         BuddyDispid     =   196629
         OrigLeft        =   3945
         OrigTop         =   2970
         OrigRight       =   4200
         OrigBottom      =   3255
         Max             =   100
         Min             =   1
         SyncBuddy       =   -1  'True
         Wrap            =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "M�ximo Impresiones de Comanda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4605
         TabIndex        =   63
         Top             =   4140
         Width           =   2910
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Transacciones para actualizaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   165
         TabIndex        =   59
         Top             =   4065
         Width           =   2835
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ubicaci�n Impresora de Cheques"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   180
         TabIndex        =   56
         Top             =   3060
         Width           =   2820
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Buscar por"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   9540
         TabIndex        =   50
         Top             =   3600
         Width           =   915
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo de Precio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4620
         TabIndex        =   48
         Top             =   3630
         Width           =   1230
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Administra Clientes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   180
         TabIndex        =   47
         Top             =   3600
         Width           =   1650
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Autodeclaraci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4635
         TabIndex        =   38
         Top             =   855
         Width           =   1365
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ubicaci�n Impresora Facturaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   180
         TabIndex        =   36
         Top             =   2640
         Width           =   2805
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Moneda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   180
         TabIndex        =   33
         Top             =   1845
         Width           =   675
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Requiere de clave de supervisor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   9450
         TabIndex        =   29
         Top             =   1350
         Width           =   2745
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "El POS Maneja Ventas por Volumen"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   210
         TabIndex        =   27
         Top             =   1335
         Width           =   3060
      End
      Begin VB.Label Label59 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito para Descargar Ventas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   180
         TabIndex        =   18
         Top             =   2235
         Width           =   2760
      End
      Begin VB.Label Label34 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Solicitar Fondo al Abrir Punto de Venta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4620
         TabIndex        =   17
         Top             =   1320
         Width           =   3330
      End
      Begin VB.Label Label56 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo del Producto Alfan�merico"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   9420
         TabIndex        =   16
         Top             =   855
         Width           =   2895
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   2310
         TabIndex        =   15
         Top             =   345
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   180
         TabIndex        =   14
         Top             =   345
         Width           =   585
      End
      Begin VB.Label Label61 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Standard OLE POS "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   180
         TabIndex        =   13
         Top             =   870
         Width           =   1650
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   5340
      Top             =   1230
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "general.frx":A014
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "general.frx":BDA6
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "general.frx":DB38
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "general.frx":F8CA
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "general"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cagrupar_Click()
    'cagrupar.Caption = IIf(cagrupar.Value = 0, "No", "Si")
End Sub

Private Sub cClaveOpen_Click()
    'cClaveOpen.Caption = IIf(cClaveOpen.Value = 0, "No", "Si")
End Sub

Private Sub Cmd_Impresora_Cheques_Click()
    
    Dim PTest As Boolean, DefPrinter As String
    
    On Error GoTo Falla_Local
    
    Impresoras.FileName = ""
    Impresoras.Flags = cdlPDDisablePrintToFile Or cdlPDNoPageNums
    Impresoras.ShowPrinter
    
    If Me.TipoPuertoCheque.ListIndex = 1 Then
        DefPrinter = Printer.Port
    Else
        DefPrinter = Printer.DeviceName
    End If
    
    If DefPrinter <> "" Then
        'Mensajes.PressBoton = True
        'PTest = Mensajes.Mensaje("�Probar la conexi�n con la impresora?", True)
        Mensaje True, StellarMensaje(10235), True
        
        If Retorno Then
            Printer.Print Replace(Replace(StellarMensaje(10262), "$(Line)", vbNewLine), "$(Tab)", vbTab) '"Esta es una prueba del Stellar isFood." & vbNewLine & vbTab & "Esta es una prueba que la conexi�n esta bien." & vbNewLine & vbNewLine & vbTab & "Stellar isFood, la soluci�n acertiva."
            Printer.EndDoc
        End If
        
        TxtImpresoraDeCheques.Text = DefPrinter
    End If
    
    Exit Sub
    
Falla_Local:
    
    'Call Mensajes.Mensaje("Hubo un error de conexi�n con la impresora.", False)
    Mensaje True, StellarMensaje(10236)
    TxtImpresoraDeCheques.Text = ""
    
End Sub

Private Sub cmd_info_Click()
    FrmPosInfo.PosNo = nCaja
    FrmPosInfo.Show vbModal
    Set FrmPosInfo = Nothing
End Sub

Private Sub cmd_moneda_Click()
    TxtMoneda.SetFocus
    Call TxtMoneda_KeyDown(vbKeyF2, 0)
End Sub

Private Sub coddeposito_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        Tecla_pulsada = True
        Call Make_View("ma_deposito", "c_coddeposito", "c_descripcion", UCase(Stellar_Mensaje(10237)), Me, "GENERICO")
        Tecla_pulsada = False
    End If
End Sub

Private Sub cordenar_Click()
    'cordenar.Caption = IIf(cordenar.Value = 0, "No", "Si")
End Sub

Private Sub cVxV_Click()
    'cVxV.Caption = IIf(cVxV.Value = 0, "No", "Si")
End Sub

Private Sub dato_numerico_Click()
    'dato_numerico.Caption = IIf(dato_numerico.Value = 0, "No", "Si")
End Sub

Private Sub fondo_pos_Click()
    'fondo_pos.Caption = IIf(fondo_pos.Value = 0, "No", "Si")
End Sub

Private Sub Form_Load()
    
    Me.Toolbar1.Buttons(1).Caption = Stellar_Mensaje(103) 'grabar
    Me.Toolbar1.Buttons(2).Caption = Stellar_Mensaje(105) 'cancelar
    Me.Toolbar1.Buttons(4).Caption = Stellar_Mensaje(7) 'ayuda
    Toolbar1.Buttons("Teclado").Caption = Stellar_Mensaje(10080, True) ' Teclado

    'TipoPuerto.AddItem "Por Nombre"
    'TipoPuerto.AddItem "Por Puerto"
    TipoPuerto.AddItem Stellar_Mensaje(10131)
    TipoPuerto.AddItem Stellar_Mensaje(10132)
    TipoPuerto.ListIndex = 0
    
    'TipoPuertoCheque.AddItem "Por Nombre"
    'TipoPuertoCheque.AddItem "Por Puerto"
    TipoPuertoCheque.AddItem Stellar_Mensaje(10131)
    TipoPuertoCheque.AddItem Stellar_Mensaje(10132)
    TipoPuertoCheque.ListIndex = 0
    
    'TipoPrecio.AddItem "Del Usuario"
    'TipoPrecio.AddItem "Precio 1"
    'TipoPrecio.AddItem "Precio 2"
    'TipoPrecio.AddItem "Precio 3"
    TipoPrecio.AddItem Stellar_Mensaje(10230)
    TipoPrecio.AddItem Stellar_Mensaje(10175)
    TipoPrecio.AddItem Stellar_Mensaje(10176)
    TipoPrecio.AddItem Stellar_Mensaje(10177)
    TipoPrecio.ListIndex = 0
    
    'CampoBusqueda.AddItem "Codigo"
    'CampoBusqueda.AddItem "Rif"
    'CampoBusqueda.AddItem "Nit"
    'CampoBusqueda.AddItem "Descripci�n"
    CampoBusqueda.AddItem Stellar_Mensaje(10096)
    CampoBusqueda.AddItem Stellar_Mensaje(10231)
    CampoBusqueda.AddItem Stellar_Mensaje(10232)
    CampoBusqueda.AddItem Stellar_Mensaje(143)
    CampoBusqueda.ListIndex = 0
    
    Me.lbl_Organizacion.Caption = Stellar_Mensaje(10162) ' Datos generales
    Me.Label1.Caption = Stellar_Mensaje(10096) 'codigo
    Me.Label3.Caption = Stellar_Mensaje(143) 'Descripcion
    Me.Label61.Caption = Stellar_Mensaje(10211) 'Standard OLE POS
    Me.opos.Caption = Stellar_Mensaje(10144) ' si
    Me.Label8.Caption = Stellar_Mensaje(10212) ' Autodeclaracion
    Me.Autodeclaracion.Caption = Stellar_Mensaje(10144) 'si
    Me.Label56.Caption = Stellar_Mensaje(10213) ' Codigo del producto alfanumerico
    Me.dato_numerico.Caption = Stellar_Mensaje(10144) 'si
    Me.Label4.Caption = Stellar_Mensaje(10214) 'el pos maneja ventas por volumen
    Me.cVxV.Caption = Stellar_Mensaje(10144) 'si
    Me.Label34.Caption = Stellar_Mensaje(10215) 'solicitar fondo al abrir punto de venta
    Me.fondo_pos.Caption = Stellar_Mensaje(10144) 'si
    Me.Label5.Caption = Stellar_Mensaje(10216) 'requiere de clave de supervisor
    Me.cClaveOpen.Caption = Stellar_Mensaje(10144) 'si
    Me.Label6.Caption = Stellar_Mensaje(10217) 'Moneda
    Me.Label59.Caption = Stellar_Mensaje(10165) 'deposito para descargar ventas
    Me.Label7.Caption = Stellar_Mensaje(10219) 'ubicacion impresora facturacion
    Me.Label12.Caption = Stellar_Mensaje(10220) ' ubicacion impresora cheques
    Me.Label9.Caption = Stellar_Mensaje(10166) 'Administra Clientes
    Me.chk_AdminClientes.Caption = Stellar_Mensaje(10144) 'si
    Me.Label10.Caption = Stellar_Mensaje(10167) 'tipo de precio
    Me.Label11.Caption = Replace(Stellar_Mensaje(55), ":", "") 'buscar por
    Me.Label13.Caption = Stellar_Mensaje(10221) 'transacciones para actualizar
    Me.Label14.Caption = Stellar_Mensaje(10222) 'maximo de impreso de consumo
    
    Me.Label16.Caption = Stellar_Mensaje(10226) 'comandas locales
    Me.Label17.Caption = Stellar_Mensaje(10223) 'commandas remotas
    
    Me.RComandas.Caption = Stellar_Mensaje(10224) 'imprimir comandas
    Me.RLlevar.Caption = Stellar_Mensaje(10225) 'imprimir comandas llevar
    Me.LComandas.Caption = Stellar_Mensaje(10224) 'imprimir comandas
    Me.LLLevar.Caption = Stellar_Mensaje(10225) 'imprimir comandas llevar

    Me.Label15.Caption = Stellar_Mensaje(10227) 'no permite cerrar la caja si hay mesas abiertas entre las:

    Me.Label18.Caption = Stellar_Mensaje(10228) ' Configuraci�n del Tickets de Impresi�n
    Me.forma_continua.Caption = Stellar_Mensaje(10169) 'utiliza papel de forma continua
    Me.Label57.Caption = Stellar_Mensaje(10144) 'si
    Me.Label29.Caption = Stellar_Mensaje(10171) 'ordenar productos al imprimir
    Me.cordenar.Caption = Stellar_Mensaje(10144) 'si
    Me.Label58.Caption = Stellar_Mensaje(10229) 'numero de lineas
    Me.Label27.Caption = Stellar_Mensaje(10172) 'agrupar productos al imprimir
    Me.cagrupar.Caption = Stellar_Mensaje(10144) 'si
    Me.Label2(0).Caption = Stellar_Mensaje(10173) 'comentario
    
    'Me.Caption = Me.Caption & " de la Caja No. " & nCaja
    'me.lbl_Organizacion=
    Call Apertura_Recordset(True, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set rsCaja.ActiveConnection = Nothing
    
    If Not rsCaja.EOF Then
        Call Llenar_Caja
    End If
    
    Call Cerrar_Recordset(rsCaja)
    
    descri.SelStart = 0
    descri.SelLength = Len(descri.Text)
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF4
            Call Grabar_Caja
            Unload Me
        Case vbKeyF12, vbKeyF7
            Unload Me
    End Select
End Sub

Private Sub Llenar_Caja()
    
    codigo.Text = rsCaja!c_codigo 'Format(rsCaja!c_codigo, "000")
    descri.Text = rsCaja!c_Desc_Caja
    
    dato_numerico.Value = IIf(rsCaja!Dato_String = True, 1, 0)
    
    opos.Value = IIf(rsCaja!b_Opos = True, 1, 0)
    cordenar.Value = IIf(rsCaja!Imp_Ordenada = True, 1, 0)
    cagrupar.Value = IIf(rsCaja!Imp_Agrupada = True, 1, 0)
    
    forma_continua.Value = IIf(rsCaja!forma_continua = True, 1, 0)
    no_lineas.Text = rsCaja!no_lineas

    fondo_pos.Value = IIf(rsCaja!b_Solicitar_Fondo = True, 1, 0)
    coddeposito.Text = IIf(IsNull(rsCaja!c_CodDeposito), "", rsCaja!c_CodDeposito)
    TxtMoneda.Text = IIf(IsNull(rsCaja!c_CodMoneda), "", rsCaja!c_CodMoneda)
    Observaciones.Text = rsCaja!Observaciones
    EsDigital = IIf(rsCaja!B_POS_Digital = False, 0, 1)
    EsPedidos = IIf(rsCaja!b_POS_Pedido = False, 0, 1)
    TxtActualizar.Text = rsCaja!n_Transacciones
    Actualizar.Value = CInt(rsCaja!n_Transacciones)
    TxtConsumo.Text = CInt(rsCaja!nu_Maximo_Consumo_imprimir)
    Consumo.Value = CInt(rsCaja!nu_Maximo_Consumo_imprimir)
    Hora_cierre.Value = IIf(IsNull(rsCaja!fu_Hora_Cierre), Format("06:00:00", "HH:mm:ss"), rsCaja!fu_Hora_Cierre)
    Hora_Cierre_Max.Value = IIf(IsNull(rsCaja!fu_Hora_Cierre_Max), Format("06:00:00", "HH:mm:ss"), rsCaja!fu_Hora_Cierre_Max)
    
    If EsDigital And Not EsPedidos Then
        'Me.Caption = Me.Caption & " POS FOOD"
        Me.lbl_Organizacion = Me.lbl_Organizacion & " POS FOOD"
    ElseIf EsDigital And EsPedidos Then
        'Me.Caption = Me.Caption & " TERMINAL DE PEDIDOS"
        Me.lbl_Organizacion = Me.lbl_Organizacion & " POE FOOD"
    Else
        Me.lbl_Organizacion = Me.lbl_Organizacion & " POS RETAIL"
        'Me.Caption = Me.Caption & " POS RETAIL"
    End If
    
    cVxV.Value = IIf(rsCaja!b_Ventas_Volumen, 1, 0)
    cClaveOpen.Value = IIf(rsCaja!cOpenPOS, 1, 0)
    TxtUbicacion.Text = rsCaja!cu_Impresora_Facturar
    Autodeclaracion.Value = IIf(rsCaja!bu_AutoDeclaracion, 1, 0)
    Me.TipoPuerto.ListIndex = IIf(rsCaja!bu_Por_Puerto, 1, 0)
    Me.RComandas.Value = IIf(rsCaja!bu_Comanda_Remota, 1, 0)
    Me.RLlevar.Value = IIf(rsCaja!bu_Llevar_Remota, 1, 0)
    Me.LComandas.Value = IIf(rsCaja!bu_Comanda_Local, 1, 0)
    Me.LLLevar.Value = IIf(rsCaja!bu_Llevar_Local, 1, 0)
    Me.chk_AdminClientes.Value = IIf(rsCaja!bu_Administra_Clientes, 1, 0)
    Me.TipoPrecio.ListIndex = rsCaja!nu_Tipo_Precio
    Me.CampoBusqueda.ListIndex = rsCaja!nu_Campo_Busqueda

    Me.TxtImpresoraDeCheques.Text = rsCaja!cu_Impresora_Cheque
    Me.TipoPuertoCheque.ListIndex = IIf(rsCaja!bu_Cheque_Por_Puerto, 1, 0)

    'Me.Chk_shell.Value = IIf(RsCaja!bu_esshell = 1, 1, 0)

    Call Buscar_Deposito
    Call Buscar_Moneda
    
End Sub

Private Sub Buscar_Deposito()
    
    Call Apertura_Recordset(False, rsEureka)
    
    rsEureka.Open "SELECT * FROM MA_DEPOSITO WHERE c_CodDeposito= '" & coddeposito.Text & "' and c_CodLocalidad = '" & Sucursal & "'", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEureka.EOF Then
        lbl_deposito.Text = rsEureka!c_Descripcion
    Else
        'Call Mensaje(True, "El c�digo del dep�sito no existe.")
        lbl_deposito.Text = Stellar_Mensaje(10233)
    End If
    
    Call Cerrar_Recordset(rsEureka)

End Sub

Private Sub Grabar_Caja()
    
    Dim RsBotonesConfig As New ADODB.Recordset, rsBotones As New ADODB.Recordset
    Dim EraDigital As Boolean
    
    If coddeposito.Text = "" Then
        Call Mensaje(True, "Debe seleccionar un Dep�sito para las ventas.")
        coddeposito.SetFocus
        Exit Sub
    End If
    
    Ent.POS.BeginTrans
        
        Call Apertura_Recordset(False, rsCaja)
        
        rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
        Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not rsCaja.EOF Then
    
            EraDigital = rsCaja!B_POS_Digital
            EsPedidos = rsCaja!b_POS_Pedido
            rsCaja!nCierre = 1
            Clave = "|C|" & Format(codigo.Text, "000")
            rsCaja!B_POS_Digital = EsDigital
            rsCaja!b_POS_Pedido = EsPedidos
            rsCaja!c_CodMoneda = TxtMoneda.Text
            rsCaja!b_Opos = opos.Value
            rsCaja!c_Desc_Caja = UCase(descri.Text)
            rsCaja!Imp_Ordenada = IIf(cordenar.Value = 1, 1, 0)
            rsCaja!Imp_Agrupada = IIf(cagrupar.Value = 1, 1, 0)
            rsCaja!Dato_String = dato_numerico.Value = 1
            
            rsCaja!forma_continua = IIf(forma_continua.Value = 1, 1, 0)
            rsCaja!no_lineas = Int(no_lineas.Text)
            
            rsCaja!Observaciones = IIf(IsNull(Observaciones.Text), "", Observaciones.Text)
            rsCaja!b_Solicitar_Fondo = IIf(fondo_pos.Value = 1, 1, 0)
            rsCaja!cOpenPOS = IIf(cClaveOpen.Value = 1, 1, 0)
            rsCaja!c_CodDeposito = coddeposito.Text
            rsCaja!b_Ventas_Volumen = (cVxV.Value = 1)
            rsCaja!cu_Impresora_Facturar = Trim(TxtUbicacion.Text)
            rsCaja!bu_AutoDeclaracion = (Autodeclaracion.Value = 1)
            rsCaja!bu_Por_Puerto = (Me.TipoPuerto.ListIndex = 1)
            rsCaja!bu_Comanda_Remota = (Me.RComandas.Value = 1)
            rsCaja!bu_Llevar_Remota = (Me.RLlevar.Value = 1)
            rsCaja!bu_Comanda_Local = (Me.LComandas.Value = 1)
            rsCaja!bu_Llevar_Local = (Me.LLLevar.Value = 1)
            rsCaja!bu_Administra_Clientes = (Me.chk_AdminClientes.Value = 1)
            rsCaja!nu_Tipo_Precio = Me.TipoPrecio.ListIndex
            rsCaja!nu_Campo_Busqueda = Me.CampoBusqueda.ListIndex
            rsCaja!n_Transacciones = Actualizar.Value
            rsCaja!nu_Maximo_Consumo_imprimir = Consumo.Value
            rsCaja!fu_Hora_Cierre = Hora_cierre.Value
            rsCaja!fu_Hora_Cierre_Max = Hora_Cierre_Max.Value
            
            rsCaja!cu_Impresora_Cheque = Trim(TxtImpresoraDeCheques.Text)
            rsCaja!bu_Cheque_Por_Puerto = (TipoPuertoCheque.ListIndex = 1)

            'RsCaja!bu_esshell = IIf(Me.Chk_shell.Value = 1, 1, 0)
            
            rsCaja.UpdateBatch
            
            ' Chequear Botones
            
            RsBotonesConfig.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "'", _
            Ent.POS, adOpenForwardOnly, adLockPessimistic, adCmdText
            
            If RsBotonesConfig.EOF Then
            
                rsBotones.Open "SELECT * FROM MA_CAJA_BOTONES WHERE TipoPOS = " & IIf(EsDigital And EsPedidos, 2, 1), _
                Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
            
                While Not rsBotones.EOF
                    RsBotonesConfig.AddNew
                    RsBotonesConfig!Caja = nCaja
                    RsBotonesConfig!Posicion = rsBotones!Posicion
                    RsBotonesConfig!Nombre_Boton = rsBotones!Nombre
                    RsBotonesConfig!Tecla = rsBotones!Tecla
                    RsBotonesConfig!Shift = rsBotones!Shift
                    RsBotonesConfig!Activo = True
                    RsBotonesConfig!Llave = False
                    RsBotonesConfig!BLLAVE = rsBotones!BLLAVE
                    RsBotonesConfig!BNIVEL = rsBotones!BNIVEL
                    RsBotonesConfig!bActivo = rsBotones!bActivo
                    RsBotonesConfig.UpdateBatch
                    rsBotones.MoveNext
                Wend
                
                RsBotonesConfig.Close
                rsBotones.Close
            
            End If
        
        End If
            
        'If glgrabar = False Then
            'ARBOL.Nodes.Item(lcindex).Text = UCase(descri.Text)
        'Else
            'Call crear_menu(lckey, clave, UCase(descri.Text), "caja")
        'End If
        
    Ent.POS.CommitTrans
    
    Call Cerrar_Recordset(rsCaja)
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 Then
        Unload Me
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set general = Nothing
End Sub

Private Sub forma_continua_Click()
    If forma_continua.Value = 1 Then
        no_lineas.Enabled = True
        uplineas.Enabled = True
    Else
        no_lineas.Enabled = False
        uplineas.Enabled = False
    End If
    
    'Label57.Caption = IIf(forma_continua.Value = 0, "No", "Si")
End Sub

Private Sub lineal_digital_Click()
    'lineal_digital.Caption = IIf(lineal_digital.Value = 0, "No", "Si")
End Sub

Private Sub opos_Click()
    'opos.Caption = IIf(opos.Value = 0, "No", "Si")
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF12, 0)
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
        Case Is = "AYUDA"
            SendKeys "{F1}"
        Case Is = UCase("Teclado")
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = descri
            
            TecladoWindows mCtl
                        
    End Select
End Sub

Private Sub TxtMoneda_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        Tecla_pulsada = True
        Call Make_View("ma_monedas", "c_codmoneda", "c_descripcion", Stellar_Mensaje(10051), Me, "GENERICO")
        Tecla_pulsada = False
    End If
End Sub

Private Sub view_Click()
    coddeposito.SetFocus
    Call coddeposito_KeyDown(vbKeyF2, 0)
End Sub

Private Sub Buscar_Moneda()

    Call Apertura_Recordset(False, rsEureka)
    
    rsEureka.Open "SELECT * FROM MA_MONEDAS WHERE c_CodMoneda= '" & TxtMoneda.Text & "'", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEureka.EOF Then
        Lbl_Moneda.Text = rsEureka!c_Descripcion
    Else
        'Call Mensaje(True, "El c�digo de la Moneda no existe.")
        Lbl_Moneda.Text = Stellar_Mensaje(10234)
    End If
    
    Call Cerrar_Recordset(rsEureka)

End Sub

Private Sub cmd_imprimir_Click()
    
    Dim PTest As Boolean, DefPrinter As String
    
    On Error GoTo Falla_Local
    
    Impresoras.FileName = ""
    Impresoras.Flags = cdlPDDisablePrintToFile Or cdlPDNoPageNums
    Impresoras.ShowPrinter
    
    If TipoPuerto.ListIndex = 1 Then
        DefPrinter = Printer.Port
    Else
        DefPrinter = Printer.DeviceName
    End If
    
    If DefPrinter <> "" Then
        'Mensajes.PressBoton = True
        'PTest = Mensajes.Mensaje(stellar_mensaje(10235), True)
        'If Mensajes.PressBoton Then
        
        Mensaje True, StellarMensaje(10235), True
        
        If Retorno Then
            Printer.Print Replace(Replace(StellarMensaje(10262), "$(Line)", vbNewLine), "$(Tab)", vbTab) '"Esta es una prueba del Stellar isFood." & vbNewLine & vbTab & "Esta es una prueba que la conexi�n esta bien." & vbNewLine & vbNewLine & vbTab & "Stellar isFood, la soluci�n acertiva."
            Printer.EndDoc
        End If
        
        TxtUbicacion.Text = DefPrinter
    End If
    
    Exit Sub
    
Falla_Local:

    'Call Mensajes.Mensaje(stellar_mensaje(10236), False)
    Mensaje True, StellarMensaje(10236)
    TxtUbicacion.Text = ""
    
End Sub
