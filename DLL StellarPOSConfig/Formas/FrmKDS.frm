VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmKDS 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5970
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   8460
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmKDS.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5970
   ScaleWidth      =   8460
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   9225
      TabIndex        =   10
      Top             =   421
      Width           =   9255
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   1080
         Left            =   240
         TabIndex        =   11
         Top             =   0
         Width           =   8000
         Begin MSComctlLib.Toolbar tlb_Config 
            Height          =   810
            Left            =   90
            TabIndex        =   12
            Top             =   120
            Width           =   7800
            _ExtentX        =   13758
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "Cancelar la configuraci�n actual [F7]"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "Grabar esta configuraci�n [F4]"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "salir"
                  Object.ToolTipText     =   "Salir del configurador [F12]"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "Ayuda del Configurador [F1]"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuraci�n de KDS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6555
         TabIndex        =   8
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   3840
      Left            =   300
      TabIndex        =   4
      Top             =   1800
      Width           =   7875
      Begin VB.TextBox txtPort 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4680
         MaxLength       =   10
         TabIndex        =   17
         Top             =   2265
         Width           =   2250
      End
      Begin VB.ComboBox CmbModeloCompatible 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "FrmKDS.frx":628A
         Left            =   4680
         List            =   "FrmKDS.frx":6291
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   1440
         Width           =   2250
      End
      Begin VB.TextBox TxtDescripcion 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   615
         MaxLength       =   50
         TabIndex        =   3
         Top             =   3090
         Width           =   6570
      End
      Begin VB.TextBox TxtCodigo 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   615
         MaxLength       =   10
         TabIndex        =   0
         Top             =   615
         Width           =   3570
      End
      Begin VB.TextBox TxtID 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   615
         MaxLength       =   10
         TabIndex        =   1
         Top             =   1440
         Width           =   3570
      End
      Begin VB.TextBox TxtIP 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   615
         MaxLength       =   255
         TabIndex        =   2
         Top             =   2265
         Width           =   3570
      End
      Begin VB.Label lblPort 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Puerto (En Blanco: Predeterminado)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4680
         TabIndex        =   18
         Top             =   2010
         Width           =   3090
      End
      Begin VB.Label lblModeloCompatible 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Marca / Modelo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4680
         TabIndex        =   16
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripcion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   615
         TabIndex        =   14
         Top             =   2835
         Width           =   975
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   615
         TabIndex        =   13
         Top             =   360
         Width           =   585
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Numero de Estacion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   615
         TabIndex        =   6
         Top             =   1185
         Width           =   1710
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "IP de la Estacion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   615
         TabIndex        =   5
         Top             =   2010
         Width           =   1410
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   3795
      Top             =   1320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKDS.frx":62A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKDS.frx":8038
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKDS.frx":9DCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKDS.frx":BB5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKDS.frx":D8EE
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmKDS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim clsConfig As New Cls_POSConfig
Dim EstaCargado As Boolean
Dim TmpIP As String
Dim TmpID As String
Dim TmpPort As String
Dim TmpModeloSeleccionado As Long

Public TmpCodigo As String
Public EsModificar As Boolean

Dim Rs As New ADODB.Recordset
Dim rs2 As New ADODB.Recordset
Dim rs3 As New ADODB.Recordset

Private EvitarActivate As Boolean

Private ModelosCompatibles    As Collection

Private Sub IgnorarActivate()
    EvitarActivate = True
End Sub

Private Sub Form_Activate()

    If EvitarActivate Then Exit Sub

    If Not ExisteTabla("MA_KDS", Ent.POS) Then
        IgnorarActivate
        Call Mensaje(True, "Para manejar esta funcionalidad necesita ejecutar los Scripts de Base de Datos relacionados. Si tiene dudas favor de contactar a Soporte Tecnico.")
        Unload Me
        Exit Sub
    End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case vbShiftMask
        Case Else
            Select Case KeyCode
                Case vbKeyF4
                    Call Grabar
                Case vbKeyF7
                    Call Cancelar
                Case vbKeyF12
                    AddNode = False
                    Unload Me
                    Exit Sub
                Case vbKeyReturn
                    Send_Keys Chr(vbKeyTab), True
                Case vbKeyEscape
            End Select
    End Select
End Sub

Public Sub Grabar()
    
    Dim rsGrabar As New ADODB.Recordset, Cont As Integer, Cuantos As Integer, Consecutivos As Object, NwCode As String

    On Error GoTo Error
    
    If EsModificar = True Then ' si entramos desde modificar entonces
        
        If Trim(Me.TxtIP.Text) = "" Or Trim(Me.TxtID.Text) = "" Or Trim(Me.Txtcodigo.Text) = "" Then ' Si esta vacio alguno de los campos sale del proceso
            Call Mensaje(True, "Error, debe rellenar todos los campos.")
            Exit Sub
        End If
        
        Cambio = 0
        
        ' metodo caiman para comprobar si no se han hecho modificaciones,
        'si no se han cambiado ninguno de los campos se sale del proceso
        
        If TmpIP <> Me.TxtIP.Text Then Cambio = Cambio + 1
        If TmpID <> Me.TxtID.Text Then Cambio = Cambio + 1
        If TmpCodigo <> Me.Txtcodigo.Text Then Cambio = Cambio + 1
        If TmpPort <> Me.txtPort.Text Then Cambio = Cambio + 1
        If TmpModeloSeleccionado <> CmbModeloCompatible.ItemData(CmbModeloCompatible.ListIndex) Then _
        Cambio = Cambio + 1
        If Not CBool(Cambio) Then Exit Sub
    
    Else
        'limpiar las variables
        If Trim(Me.TxtIP.Text) = "" Or Trim(Me.TxtID.Text) = "" Then Exit Sub
        TmpIP = ""
        TmpID = ""
        TmpCodigo = ""
        TmpPort = 0
        TmpModeloSeleccionado = 0
    End If
    
    Rs.Open "SELECT COUNT(*) AS IP FROM MA_KDS where IP = '" & Me.TxtIP.Text & "' AND cs_Relacion = '" & NodoTextoKey & "'", _
    Ent.POS, adOpenDynamic, adLockBatchOptimistic

    If Rs!IP > 0 And Me.TxtIP.Text <> TmpIP Then ' si hay algun registro en la consulta y el campo es diferente del tmp, emite un mensaje
        Call Mensaje(True, "No se puede a�adir esta estaci�n, verifique que la direcci�n IP no este repetida en otra estaci�n.")
        CerrarRS
        Exit Sub
    Else
        rs2.Open "select count(*) as ID from MA_KDS where KDS_ID = '" & Me.TxtID.Text & "' AND cs_Relacion = '" & NodoTextoKey & "'", _
        Ent.POS, adOpenDynamic, adLockBatchOptimistic
        
        If rs2!ID > 0 And Me.TxtID.Text <> TmpID = True Then ' igual que el anterior pero con el KDS ID
            Call Mensaje(True, "No se puede a�adir esta estaci�n, verifique que el numero de estaci�n no este repetido.")
            CerrarRS

            Exit Sub
        Else
            rs3.Open "select count(*) as CODIGO from MA_KDS where cs_CODIGO = '" & Me.Txtcodigo.Text & "'", _
            Ent.POS, adOpenDynamic, adLockBatchOptimistic
        
            If rs3!codigo > 0 And Me.Txtcodigo.Text <> TmpCodigo = True Then ' igual pero con el codigo Stellar
                Call Mensaje(True, "No se puede a�adir esta estaci�n, verifique que el codigo no este repetido en otra estaci�n.")
                CerrarRS
                Exit Sub
            End If
        End If
    End If
        
    If Not IsValidIPAddress(Trim(Me.TxtIP.Text)) Then ' funcion para validar si es una ip valida
        Call Mensaje(True, "Formato de IP inv�lido, verifique la direcci�n IP introducida.")
        CerrarRS
        Exit Sub
    End If
    
    If Not IsNumeric(Trim(Me.Txtcodigo.Text)) Then ' funcion para validar si es un entero el codigo
        Call Mensaje(True, "Codigo inv�lido, debe ser un entero.")
        CerrarRS
        Exit Sub
    End If
    
    If Not IsNumeric(Trim(Me.TxtID.Text)) Then ' funcion para validar si es un entero el numero de la estacion
        Call Mensaje(True, "Numero de estacion inv�lido, verifique el n�mero de estaci�n introducido.")
        CerrarRS
        Exit Sub
    End If
    
    IntID = Int(Me.TxtID.Text)
    
    If Me.TxtID.Text <> IntID Then
        Call Mensaje(True, "N�mero de estacion inv�lido, debe ser un entero.")
        CerrarRS
        Exit Sub
    End If
    
    ' si ninguno de los campos esta repetido en la base de datos, procede a grabar
    
    Set Consecutivos = CreateObject("recsun.cls_datos")
    
    Call Apertura_Recordset(True, rsGrabar)
    
    If NodoTextoKey <> "" Then
        
        rsGrabar.Open "SELECT * FROM MA_KDS WHERE cs_Codigo = '" & Trim(Me.Txtcodigo.Text) & "'", _
        Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
            
        Ent.POS.BeginTrans
        
        If rsGrabar.EOF Then
            rsGrabar.AddNew
        End If
        
        'If EsModificar = True And (TxtIP.Text <> rsGrabar!IP) Then
            'Ent.POS.Execute "UPDATE TR_CONSUMO SET cu_KDS = '" & TxtIP.Text & "' WHERE cu_KDS = '" & rsGrabar!IP & "'"
        'End If
        
        rsGrabar!KDS_ID = Trim(Me.TxtID.Text)
        rsGrabar!cs_Relacion = NodoTextoKey
        rsGrabar!IP = TxtIP.Text
        rsGrabar!cs_Descripcion = TxtDescripcion.Text
        rsGrabar!cs_Codigo = Txtcodigo.Text
        rsGrabar!Port = txtPort.Text
        rsGrabar!IDModeloCompatible = CmbModeloCompatible.ItemData(CmbModeloCompatible.ListIndex)
        rsGrabar.UpdateBatch
        
        Ent.POS.CommitTrans
        
        lcKey = "|K|" & NodoTextoKey
        Clave = "|A|" & rsGrabar!cs_Codigo
        lcTexto = rsGrabar!cs_Descripcion
        AddNode = True
        rsGrabar.Close
    
    End If
    
    CerrarRS
    Unload Me
    
    Exit Sub
    
Error:

    Call Mensaje(True, "Ha ocurrido un error al grabar. Por favor reporte lo siguente: " & ERR.Description)
    
End Sub

Public Sub Cancelar()
    
    Dim rsGrabar As New ADODB.Recordset, Consecutivos As Object
    
    Set Consecutivos = CreateObject("recsun.cls_datos")
    
    Call Apertura_Recordset(True, rsGrabar)
    
    AddNode = False
    
    If EsModificar = True Then
        If Trim(Me.Txtcodigo.Text) <> "" Then
            rsGrabar.Open "SELECT * FROM MA_KDS WHERE cs_CODIGO = '" & Trim(Me.Txtcodigo.Text) & "'", _
            Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not rsGrabar.EOF Then
                NodoTextoKey = rsGrabar!cs_Relacion
                TxtID.Text = rsGrabar!KDS_ID
                TxtIP.Text = rsGrabar!IP
                Txtcodigo.Text = rsGrabar!cs_Codigo
                TxtDescripcion.Text = rsGrabar!cs_Descripcion
                txtPort.Text = rsGrabar!Port
                TmpModeloSeleccionado = rsGrabar!IDModeloCompatible
                CargarModelo TmpModeloSeleccionado
            End If
            
            rsGrabar.Close
        Else
            TxtIP.Text = ""
            TxtID.Text = ""
            Txtcodigo.Text = ""
        End If
    End If
    'Me.Caption = "Configuraci�n de Administraci�n para el Nivel '" & NodoTexto & "'"
    
End Sub

Private Sub Form_Load()
    
    'Me.lbl_Organizacion.Caption = stellar_mensaje(10134, True) 'configuracion de zona
    'Label1.Caption = stellar_mensaje(10133, True) 'Descripci�n de Ubicaci�n
    'Me.lbl_Organizacion.Caption = stellar_mensaje(10210, True)  'configuracion de mesa
    
    Label2.Caption = Stellar_Mensaje(10259, True) 'numero de la estacion
    Label3.Caption = Stellar_Mensaje(142, True) 'codigo
    Label1.Caption = Stellar_Mensaje(10260, True) 'IP de la estacion
    Label4.Caption = Stellar_Mensaje(10261, True) 'descripcion
    
    Me.tlb_Config.Buttons(1).Caption = Stellar_Mensaje(6, True) 'cancelar
    Me.tlb_Config.Buttons(2).Caption = Stellar_Mensaje(103, True) 'grabar
    Me.tlb_Config.Buttons(3).Caption = Stellar_Mensaje(54, True) 'salir
    Me.tlb_Config.Buttons(4).Caption = Stellar_Mensaje(7, True) 'ayuda
    tlb_Config.Buttons("Teclado").Caption = Stellar_Mensaje(10080, True) ' Teclado
    
    Set ModelosCompatibles = New Collection
    
    ModelosCompatibles.add Array("Bematech LS 8000", 1)
    
    CmbModeloCompatible.Clear
    
    Dim TmpItem
    
    For Each TmpItem In ModelosCompatibles
        CmbModeloCompatible.AddItem TmpItem(0)
        CmbModeloCompatible.ItemData(CmbModeloCompatible.NewIndex) = TmpItem(1)
    Next
    
    CmbModeloCompatible.ListIndex = 0
    
    Dim rsGrabar As New ADODB.Recordset, Consecutivos As Object
    
    Set Consecutivos = CreateObject("recsun.cls_datos")
    
    Call Apertura_Recordset(True, rsGrabar)
    
    AddNode = False
    
    'se realiza el proceso para rellenar los campos del form con la base de datos si se esta modificando
    If EsModificar = True Then
        
        rsGrabar.Open "SELECT * FROM MA_KDS WHERE cs_CODIGO = '" & TmpCodigo & "'", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not rsGrabar.EOF Then
            NodoTextoKey = rsGrabar!cs_Relacion
            TxtID.Text = rsGrabar!KDS_ID
            TxtIP.Text = rsGrabar!IP
            Txtcodigo.Text = rsGrabar!cs_Codigo
            TxtDescripcion.Text = rsGrabar!cs_Descripcion
            TmpID = rsGrabar!KDS_ID
            TmpIP = rsGrabar!IP
            TmpCodigo = rsGrabar!cs_Codigo
            TmpPort = rsGrabar!Port
            txtPort = TmpPort
            TmpModeloSeleccionado = rsGrabar!IDModeloCompatible
            CargarModelo TmpModeloSeleccionado
        End If
        
        rsGrabar.Close
        
    Else
        TxtIP.Text = ""
        TxtID.Text = ""
    End If
    
End Sub

Private Sub CargarModelo(pIDModelo As Long)
    For i = 0 To CmbModeloCompatible.ListCount - 1
        If pIDModelo = CmbModeloCompatible.ItemData(i) Then
            CmbModeloCompatible.ListIndex = i
        End If
    Next i
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EstaCargado = False
    EvitarActivate = False
End Sub

Private Sub tlb_Config_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case LCase(Button.Key)
        Case "grabar"
            Send_Keys Chr(vbKeyF4), True
        Case "cancelar"
            Send_Keys Chr(vbKeyF7), True
        Case "salir"
            Send_Keys Chr(vbKeyF12), True
        Case "ayuda"
            Send_Keys Chr(vbKeyF1)
        Case LCase("Teclado")
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = Txtcodigo
            
            TecladoWindows mCtl
    End Select
End Sub

Private Sub CerrarRS()
    If Rs.State = 1 Then Rs.Close
    If rs2.State = 1 Then rs2.Close
    If rs3.State = 1 Then rs3.Close
End Sub
