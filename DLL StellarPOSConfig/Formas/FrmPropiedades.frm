VERSION 5.00
Begin VB.Form FrmPropiedades 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3270
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   4980
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmPropiedades.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   4980
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Height          =   975
      Left            =   3720
      Picture         =   "FrmPropiedades.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   2160
      Width           =   975
   End
   Begin VB.CommandButton cmb_Acep 
      Height          =   975
      Left            =   2520
      Picture         =   "FrmPropiedades.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2160
      Width           =   975
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7035
         TabIndex        =   5
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuraci�n de Comandas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   75
         Width           =   5415
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   1335
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   4455
      Begin VB.CheckBox chk_local 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Imprimir Local"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   855
         TabIndex        =   2
         Top             =   240
         Width           =   2370
      End
      Begin VB.CheckBox chk_remoto 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Imprimir Remota"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   840
         TabIndex        =   1
         Top             =   780
         Width           =   2370
      End
   End
End
Attribute VB_Name = "FrmPropiedades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Departamento As String, Grupo As String, SubGrupo As String

Dim rsEstadoActual As New ADODB.Recordset

Dim ChkRemoto_Original As Integer
Dim ChkLocal_Original As Integer

Private Sub Command1_Click()
End Sub

Private Sub cmb_Acep_Click()
    Mensajes.PressBoton = True
'    If (HayCambios) Then
'        If Mensajes.Mensaje("�Desea guardar los cambios?", True) Then
'            Call Grabar
'        End If
'    End If
    If (HayCambios) Then
    Call Mensaje(True, stellar_mensaje(10094), True)
        If (Retorno) Then
            Call Grabar
        End If
    End If
    Unload Me
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    
    Dim cSQL As String
    
    If Departamento <> "" And Grupo <> "" And SubGrupo <> "" Then
        cSQL = "SELECT * FROM MA_IMPRESORA_SUBGRUPO WHERE cs_Departamento = '" & Departamento & "' AND cs_Grupo = '" & Grupo & "' AND cs_Subgrupo = '" & SubGrupo & "' "
    ElseIf Departamento <> "" And Grupo <> "" Then
        cSQL = "SELECT * FROM MA_IMPRESORA_SUBGRUPO WHERE cs_Departamento = '" & Departamento & "' AND cs_Grupo = '" & Grupo & "' "
    ElseIf Departamento <> "" Then
        cSQL = "SELECT * FROM MA_IMPRESORA_SUBGRUPO WHERE cs_Departamento = '" & Departamento & "' "
    Else
        Unload Me
    End If
    
    rsEstadoActual.Open cSQL, Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEstadoActual.EOF Then
        If Not ExisteCampoTabla("nu_Remoto", rsEstadoActual) Or Not ExisteCampoTabla("nu_Local", rsEstadoActual) Then
            Mensaje True, "No existen los campos relacionados en la BD sel Servidor Remoto."
            Unload Me
            Exit Sub
        End If
        Me.chk_remoto.Value = IIf(rsEstadoActual!nu_remoto = 1, 1, 0)
        Me.chk_local.Value = IIf(rsEstadoActual!nu_local = 1, 1, 0)
        ChkRemoto_Original = Me.chk_remoto.Value
        ChkLocal_Original = Me.chk_local.Value
    End If
    
    rsEstadoActual.Close
    
End Sub

Private Sub Grabar()
    Dim FaltaCampo As Boolean
    Dim rsGrabar As New ADODB.Recordset
    Dim Cls_Config As New Cls_POSConfig
    If Departamento <> "" And Grupo <> "" And SubGrupo <> "" Then
        cSQL = "SELECT * FROM MA_IMPRESORA_SUBGRUPO WHERE cs_Departamento = '" & Departamento & "' AND cs_Grupo = '" & Grupo & "' AND cs_Subgrupo = '" & SubGrupo & "' "
    ElseIf Departamento <> "" And Grupo <> "" Then
        cSQL = "SELECT * FROM MA_IMPRESORA_GRUPO WHERE cs_Departamento = '" & Departamento & "' AND cs_Grupo = '" & Grupo & "' "
    ElseIf Departamento <> "" Then
        cSQL = "SELECT * FROM MA_IMPRESORA_DEPARTAMENTO WHERE cs_Departamento = '" & Departamento & "' "
    Else
        Unload Me
    End If
    asd = cSQL
    rsGrabar.Open cSQL, Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
    
    If Not rsGrabar.EOF Then
        If chk_remoto.Value = 0 And chk_local.Value = 0 Then
            Mensajes.PressBoton = True
            
            If Mensajes.Mensaje("�Desea establecer [No Imprimir] en ambas opciones?", True) Then
            
                If ExisteCampoTabla("nu_remoto", rsGrabar) Then
                    rsGrabar!nu_local = chk_local.Value
                Else
                    FaltaCampo = True
                End If
                
                If ExisteCampoTabla("nu_local", rsGrabar) Then
                rsGrabar!nu_local = chk_local.Value
                Else
                    FaltaCampo = True
                End If
                
            Else
                rsGrabar!nu_local = -1
                rsGrabar!nu_remoto = -1
            End If
        Else
            If ExisteCampoTabla("nu_remoto", rsGrabar) Then
                rsGrabar!nu_local = chk_local.Value
            Else
                FaltaCampo = True
            End If
             
            If ExisteCampoTabla("nu_local", rsGrabar) Then
                rsGrabar!nu_local = chk_local.Value
            Else
                FaltaCampo = True
            End If
            
        End If
        
        rsGrabar.Update
    End If
    
    If FaltaCampo Then
        Call Mensaje(False, stellar_mensaje(0))
    End If
    
    rsGrabar.Close
    
End Sub

Private Function HayCambios() As Boolean

    HayCambios = False
    
    If Me.chk_remoto.Value <> ChkRemoto_Original Then HayCambios = True: Exit Function
    If Me.chk_local.Value <> ChkLocal_Original Then HayCambios = True: Exit Function

End Function

Private Sub Form_Load()
    Me.lbl_Organizacion.Caption = stellar_mensaje(10253) 'configuracion de comandas
    Me.chk_local.Caption = stellar_mensaje(10251) 'impresora local
    Me.chk_remoto.Caption = stellar_mensaje(10252) 'impresora  remota
    Me.Command2.Caption = stellar_mensaje(6)
    Me.cmb_Acep.Caption = stellar_mensaje(5)
End Sub
