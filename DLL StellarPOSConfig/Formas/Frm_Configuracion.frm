VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Frm_Configuracion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Punto Restaurant (Configuracion de Grupos y Productos)"
   ClientHeight    =   7575
   ClientLeft      =   285
   ClientTop       =   1170
   ClientWidth     =   10890
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7575
   ScaleWidth      =   10890
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList Treeimagen 
      Left            =   150
      Top             =   6690
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":0000
            Key             =   "DPTO CERRADO"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":0452
            Key             =   "DPTO ABIERTO"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1485
      Top             =   6690
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":08A4
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":1580
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":225C
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":2F38
            Key             =   "inter"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":3184
            Key             =   "Nuevo"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":37FE
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":3E78
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":42CA
            Key             =   "salir"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":43D4
            Key             =   "DPTO CERRADO"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":4826
            Key             =   "DPTO ABIERTO"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   825
      Top             =   6690
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":4C78
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":5954
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":6630
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":730C
            Key             =   "inter"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":7558
            Key             =   "Nuevo"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":7BD2
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":824C
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":869E
            Key             =   "salir"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":87A8
            Key             =   "DPTO CERRADO"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":8BFA
            Key             =   "DPTO ABIERTO"
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   945
      Left            =   60
      TabIndex        =   38
      Top             =   -15
      Width           =   10785
      Begin MSComctlLib.Toolbar tlb_Config 
         Height          =   675
         Left            =   75
         TabIndex        =   39
         Top             =   180
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1191
         ButtonWidth     =   847
         ButtonHeight    =   1191
         Appearance      =   1
         Style           =   1
         ImageList       =   "Icono_Apagado"
         DisabledImageList=   "Icono_Apagado"
         HotImageList    =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Object.ToolTipText     =   "Salir de la Configuracion de Botones"
               ImageIndex      =   8
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Botones"
      ForeColor       =   &H80000002&
      Height          =   6495
      Left            =   2580
      TabIndex        =   1
      Top             =   990
      Width           =   8265
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   35
         Left            =   6825
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   5325
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   34
         Left            =   5475
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   5325
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   33
         Left            =   4125
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   5325
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   32
         Left            =   2775
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   5325
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   31
         Left            =   1425
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   5325
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   30
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   5325
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   29
         Left            =   6825
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   4320
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   28
         Left            =   5475
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   4320
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   27
         Left            =   4125
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   4320
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   26
         Left            =   2775
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   4320
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   25
         Left            =   1425
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   4320
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   24
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   4320
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   23
         Left            =   6825
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   3315
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   22
         Left            =   5475
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   3315
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   21
         Left            =   4125
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   3315
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   20
         Left            =   2775
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   3315
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   19
         Left            =   1425
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   3315
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   18
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   3315
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   17
         Left            =   6825
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   2310
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   16
         Left            =   5475
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   2310
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   15
         Left            =   4125
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   2310
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   14
         Left            =   2775
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2310
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   13
         Left            =   1425
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   2310
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   12
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   2310
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   11
         Left            =   6825
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   1305
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   10
         Left            =   5475
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   1305
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   9
         Left            =   4125
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1305
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   8
         Left            =   2775
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1305
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   7
         Left            =   1425
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   1305
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   6
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   1305
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   5
         Left            =   6825
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   300
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   4
         Left            =   5475
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   300
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   3
         Left            =   4125
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   300
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   2
         Left            =   2775
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   300
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   1
         Left            =   1425
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   300
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1005
         Index           =   0
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   300
         Width           =   1335
      End
   End
   Begin MSComctlLib.TreeView tvw_Arbol 
      Height          =   6390
      Left            =   60
      TabIndex        =   0
      Top             =   1080
      Width           =   2460
      _ExtentX        =   4339
      _ExtentY        =   11271
      _Version        =   393217
      Indentation     =   212
      LabelEdit       =   1
      Style           =   5
      ImageList       =   "Treeimagen"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDragMode     =   1
      OLEDropMode     =   1
   End
   Begin VB.Menu menu 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu mnuAgregar 
         Caption         =   "&Agregar"
         Begin VB.Menu mnuGrupo 
            Caption         =   "&Grupos"
         End
         Begin VB.Menu mnuproducto 
            Caption         =   "&Producto"
         End
      End
      Begin VB.Menu mnuModificar 
         Caption         =   "&Modificar"
      End
      Begin VB.Menu sepa 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuborrar 
         Caption         =   "&Eliminar"
      End
   End
End
Attribute VB_Name = "Frm_Configuracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim POS As Integer          ' Variable que guarda la posicion del nodo
Dim obj  As Object          ' Variable para manejo del treeview
Public fNodoArr_Sel As Variant          'Variable que guarda los datos del nodo seleccionado
Public addnode As Boolean   'Variable que indica  si se va a�adir un nodo o no

Dim lckey As String         ' Variable que indica dependiendo del nodo seleccionado si se pueden a�adir productos o grupos

Dim fcargado As Boolean     'Variable utilizada para controlar el evento mouse click y mouse down
                            ' el evento mouse down se dispara primero y para el correcto funcionamiento del programa se debia
                            'dispara primero el nodeclick entonces se utilizo esta bandera
                            
Private Sub Form_Load()
    Dim mSQL As String
    
    Set obj = CreateObject("recsun.obj_treeview")
    mSQL = "SELECT  * FROM TR_Config_Botones_Temp ORDER BY id"
    DibujarArbol Me.tvw_Arbol, CADENA, mSQL
    fcargado = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set obj = Nothing
End Sub

Private Sub mnuborrar_Click()
    Dim boton As cls_Boton
    Dim mensaje As Object
    Dim mresp As String
    Dim mconexion As New ADODB.Connection
    Dim m_Tabla As String
    
    m_Tabla = "TR_Config_Botones_Temp"
    
    
    mconexion.Open CADENA
    Set mensaje = CreateObject("recsun.OBJ_MENSAJERIA")
    mensaje.mensaje "Esta seguro ", True
    If mensaje.PressBoton = True Then
        Set boton = New cls_Boton
        boton.codigo = CStr(fNodoArr_Sel(2))
        boton.eliminar mconexion, m_Tabla, mresp
        If mresp = vbYes Then
            obj.BorrarTw Me.tvw_Arbol, POS
            Call tvw_Arbol_NodeClick(tvw_Arbol.SelectedItem)
        End If
        Set boton = Nothing
    End If
    Set mensaje = Nothing
    mconexion.Close
 
End Sub

Private Sub mnuGrupo_Click()
    addnode = False
    frm_NewGrupo.Show vbModal
    Dim o As New obj_treeview
    
    If addnode = True Then
        obj.AdicionarTw Me.tvw_Arbol, DatosBoton.Grupo, DatosBoton.codigo, DatosBoton.Nombre, , "DPTO CERRADO"
        Call tvw_Arbol_NodeClick(tvw_Arbol.SelectedItem)
    End If
    
End Sub

Private Sub mnuModificar_Click()
    Dim mSQL As String
    addnode = False
    mSQL = "SELECT  * FROM TR_Config_Botones_Temp ORDER BY id"
    
    Frm_Modificar.Show vbModal
   
    If addnode = True Then
        Me.tvw_Arbol.Nodes.Clear
        DibujarArbol Me.tvw_Arbol, CADENA, mSQL
    End If
End Sub
Private Sub mnuproducto_Click()
    
    addnode = False
    clavepadre = fNodoArr_Sel(2)
    Frm_NewProducto.Show vbModal
           
    If addnode = True Then
        obj.AdicionarTw Me.tvw_Arbol, DatosBoton.Grupo, DatosBoton.codigo, DatosBoton.Nombre, , "DPTO CERRADO"
        Call tvw_Arbol_NodeClick(tvw_Arbol.SelectedItem)
    End If
End Sub



Private Sub tlb_Config_ButtonClick(ByVal Button As MSComctlLib.Button)
     Select Case Button.Index
        Case Is = 1
            Unload Me
    End Select
End Sub

Private Sub tvw_Arbol_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lckey As String
    If fcargado = True Then
        lckey = Mid(CStr(fNodoArr_Sel(2)), 1, 1)
        Select Case Button
            Case Is = 1
            Case Is = 2
                Select Case lckey
                    Case Is = "R"
                        mnuGrupo.Enabled = True
                        mnuAgregar.Enabled = True
                        mnuproducto.Enabled = True
                        mnuModificar.Enabled = True
                        mnuborrar.Enabled = False
                    Case Is = "G"
                         mnuAgregar.Enabled = True
                         mnuGrupo.Enabled = True
                         mnuproducto.Enabled = True
                         mnuModificar.Enabled = True
                         mnuborrar.Enabled = True
                    Case Else
                        mnuAgregar.Enabled = False
                        mnuModificar.Enabled = True
                        mnuborrar.Enabled = True
                End Select
                PopupMenu menu
        End Select
    End If
    fcargado = False
End Sub
Private Sub tvw_Arbol_NodeClick(ByVal Node As MSComctlLib.Node)
    Dim boton As cls_Boton
    Dim min As Long, max As Long
    Dim CONEX As New ADODB.Connection
    Dim m_sql As String
    
    CONEX.Open CADENA
    min = 0
    max = Me.cmd_Botones.Count - 1
    Set boton = New cls_Boton
    fNodoArr_Sel = obj.TomardataTw(Me.tvw_Arbol, Node.Index)
    
    POS = Node.Index
    fcargado = True
    m_sql = "Select * from TR_Config_Botones_Temp where relacion='" & CStr(Frm_Configuracion.fNodoArr_Sel(2)) + "'"
    boton.IniciarBotones Me.cmd_Botones, CONEX, m_sql, min, max
    CONEX.Close
    Set boton = Nothing
End Sub





