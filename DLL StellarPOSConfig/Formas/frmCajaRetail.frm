VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form frmCajaRetail 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9675
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   9150
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCajaRetail.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9675
   ScaleWidth      =   9150
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   53
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Datos Generales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   55
         Top             =   75
         Width           =   5415
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7035
         TabIndex        =   54
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   9825
      TabIndex        =   50
      Top             =   421
      Width           =   9855
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   1080
         Left            =   0
         TabIndex        =   51
         Top             =   0
         Width           =   8670
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   300
            TabIndex        =   52
            Top             =   120
            Width           =   7935
            _ExtentX        =   13996
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "F4 Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "F7 Cancelar"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "F1 Ayuda del SIstema"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   4
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Configuraci�n del Tickets de Impresi�n "
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   3735
      Left            =   210
      TabIndex        =   26
      Top             =   5745
      Width           =   8775
      Begin VB.TextBox linea 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   2100
         MaxLength       =   30
         TabIndex        =   15
         Top             =   3090
         Width           =   5805
      End
      Begin VB.TextBox linea 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   2100
         MaxLength       =   30
         TabIndex        =   14
         Top             =   2640
         Width           =   5805
      End
      Begin VB.TextBox linea 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   2100
         MaxLength       =   30
         TabIndex        =   13
         Top             =   2190
         Width           =   5805
      End
      Begin VB.TextBox linea 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   2100
         MaxLength       =   30
         TabIndex        =   12
         Top             =   1740
         Width           =   5805
      End
      Begin VB.TextBox linea 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   2100
         MaxLength       =   30
         TabIndex        =   11
         Top             =   1290
         Width           =   5805
      End
      Begin VB.TextBox no_lineas 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   360
         Left            =   3270
         Locked          =   -1  'True
         TabIndex        =   8
         Text            =   "33"
         Top             =   720
         Width           =   450
      End
      Begin VB.CheckBox forma_continua 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Utiliza Papel de Forma Continua"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   240
         TabIndex        =   7
         Top             =   390
         Width           =   3225
      End
      Begin VB.CheckBox cagrupar 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   7590
         TabIndex        =   10
         Top             =   810
         Width           =   855
      End
      Begin VB.CheckBox cordenar 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   7590
         TabIndex        =   9
         Top             =   390
         Width           =   855
      End
      Begin ComCtl2.UpDown uplineas 
         Height          =   360
         Left            =   3705
         TabIndex        =   27
         Top             =   720
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   635
         _Version        =   327681
         Value           =   10
         BuddyControl    =   "no_lineas"
         BuddyDispid     =   196616
         OrigLeft        =   3555
         OrigTop         =   600
         OrigRight       =   3795
         OrigBottom      =   915
         Max             =   100
         Min             =   10
         SyncBuddy       =   -1  'True
         Wrap            =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   0   'False
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   3720
         X2              =   8520
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   " Configuraci�n del Tickets de Impresi�n "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   120
         TabIndex        =   56
         Top             =   0
         Width           =   3855
      End
      Begin VB.Label Label57 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3540
         TabIndex        =   36
         Top             =   405
         Width           =   645
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Comentario 5"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   4
         Left            =   480
         TabIndex        =   35
         Top             =   3135
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Comentario 4"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   3
         Left            =   480
         TabIndex        =   34
         Top             =   2685
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Comentario 3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   2
         Left            =   480
         TabIndex        =   33
         Top             =   2235
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Comentario 2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   1
         Left            =   480
         TabIndex        =   32
         Top             =   1785
         Width           =   1155
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Comentario  1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   0
         Left            =   480
         TabIndex        =   31
         Top             =   1335
         Width           =   1215
      End
      Begin VB.Label Label58 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "N�mero de L�neas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   30
         Top             =   780
         Width           =   1545
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Agrupar Productos al Imprimir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4560
         TabIndex        =   29
         Top             =   810
         Width           =   2595
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ordenar Productos al Imprimir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4560
         TabIndex        =   28
         Top             =   390
         Width           =   2610
      End
   End
   Begin VB.Frame fdescripcion 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   3930
      Left            =   210
      TabIndex        =   16
      Top             =   1665
      Width           =   8775
      Begin VB.TextBox TxtActualizar 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3450
         Locked          =   -1  'True
         MaxLength       =   255
         TabIndex        =   48
         Text            =   "0"
         Top             =   2825
         Width           =   1005
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Info"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   6360
         Picture         =   "frmCajaRetail.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   2820
         Width           =   1050
      End
      Begin VB.CommandButton cmd_rtf 
         Caption         =   "&Formatos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   7545
         Picture         =   "frmCajaRetail.frx":800C
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   2820
         Width           =   1050
      End
      Begin VB.TextBox TxtMoneda 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3450
         TabIndex        =   43
         Top             =   1920
         Width           =   1305
      End
      Begin VB.CommandButton cmd_moneda 
         Height          =   360
         Left            =   4890
         Picture         =   "frmCajaRetail.frx":8776
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   1935
         Width           =   360
      End
      Begin VB.TextBox Lbl_Moneda 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   5355
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   1935
         Width           =   3255
      End
      Begin VB.CheckBox cClaveOpen 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   7905
         TabIndex        =   39
         Top             =   1530
         Width           =   795
      End
      Begin VB.CheckBox cVxV 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   7905
         TabIndex        =   37
         Top             =   1110
         Width           =   795
      End
      Begin VB.CheckBox lineal_digital 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3465
         TabIndex        =   1
         Top             =   675
         Width           =   795
      End
      Begin VB.TextBox lbl_deposito 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   5355
         Locked          =   -1  'True
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   2370
         Width           =   3255
      End
      Begin VB.CommandButton view 
         Height          =   360
         Left            =   4890
         Picture         =   "frmCajaRetail.frx":8F78
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   2370
         Width           =   360
      End
      Begin VB.TextBox coddeposito 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3450
         TabIndex        =   5
         Top             =   2370
         Width           =   1305
      End
      Begin VB.CheckBox fondo_pos 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   7905
         TabIndex        =   4
         Top             =   690
         Width           =   795
      End
      Begin VB.CheckBox dato_numerico 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3465
         TabIndex        =   3
         Top             =   1515
         Width           =   795
      End
      Begin VB.TextBox descri 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3390
         MaxLength       =   50
         TabIndex        =   0
         Top             =   180
         Width           =   5145
      End
      Begin VB.TextBox codigo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1170
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   17
         Text            =   "1"
         Top             =   180
         Width           =   525
      End
      Begin VB.CheckBox opos 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3465
         TabIndex        =   2
         Top             =   1095
         Width           =   795
      End
      Begin MSComCtl2.UpDown Actualizar 
         Height          =   360
         Left            =   4455
         TabIndex        =   47
         Top             =   2820
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   635
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "TxtActualizar"
         BuddyDispid     =   196628
         OrigLeft        =   3900
         OrigTop         =   2085
         OrigRight       =   4155
         OrigBottom      =   2370
         Max             =   100
         Min             =   1
         SyncBuddy       =   -1  'True
         Wrap            =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Transacciones para actualizaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   210
         TabIndex        =   49
         Top             =   2825
         Width           =   2835
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Moneda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   210
         TabIndex        =   44
         Top             =   1980
         Width           =   675
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Requiere de clave de supervisor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4365
         TabIndex        =   40
         Top             =   1530
         Width           =   2745
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "El POS Maneja Ventas x Volumen"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4365
         TabIndex        =   38
         Top             =   1110
         Width           =   2865
      End
      Begin VB.Label Label60 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "POS Digital"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   25
         Top             =   675
         Width           =   945
      End
      Begin VB.Label Label59 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito para Descargar Ventas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   210
         TabIndex        =   24
         Top             =   2400
         Width           =   2760
      End
      Begin VB.Label Label34 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Solicitar Fondo al Abrir Punto de Venta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4365
         TabIndex        =   23
         Top             =   690
         Width           =   3330
      End
      Begin VB.Label Label56 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo del Producto Alfan�merico"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   22
         Top             =   1515
         Width           =   2895
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   2160
         TabIndex        =   21
         Top             =   225
         Width           =   1095
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   270
         TabIndex        =   20
         Top             =   225
         Width           =   585
      End
      Begin VB.Label Label61 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Standard OLE POS "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   19
         Top             =   1095
         Width           =   1650
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   60
      Top             =   1590
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCajaRetail.frx":977A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCajaRetail.frx":B50C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCajaRetail.frx":D29E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCajaRetail.frx":F030
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCajaRetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private RtfFormatoFactura As String
Private RtfFacturaPuerto As Boolean

Private RtfFormatoDevolucion As String
Private RtfDevolucionPuerto As Boolean

Private RtfFormatoReintegro As String
Private RtfReintegroPuerto As Boolean

Private RtfFormatoEspera As String
Private RtfEsperaPuerto As Boolean

Private Sub cagrupar_Click()
    'cagrupar.Caption = IIf(cagrupar.Value = 0, "No", "Si")
End Sub

Private Sub cClaveOpen_Click()
    'cClaveOpen.Caption = IIf(cClaveOpen.Value = 0, "No", "Si")
End Sub

Private Sub cmd_moneda_Click()
    TxtMoneda.SetFocus
    Call TxtMoneda_KeyDown(vbKeyF2, 0)
End Sub

Private Sub cmd_rtf_Click()

    FrmFormatosPos.RtfFormatoFactura = RtfFormatoFactura
    FrmFormatosPos.RtfFacturaPuerto = RtfFacturaPuerto

    FrmFormatosPos.RtfFormatoReintegro = RtfFormatoReintegro
    FrmFormatosPos.RtfReintegroPuerto = RtfReintegroPuerto
    
    FrmFormatosPos.RtfFormatoDevolucion = RtfFormatoDevolucion
    FrmFormatosPos.RtfDevolucionPuerto = RtfDevolucionPuerto
    
    FrmFormatosPos.RtfFormatoEspera = RtfFormatoEspera
    FrmFormatosPos.RtfEsperaPuerto = RtfEsperaPuerto

    FrmFormatosPos.Show vbModal
    
    If FrmFormatosPos.Aceptar Then
        
        RtfFormatoFactura = FrmFormatosPos.RtfFormatoFactura
        RtfFacturaPuerto = FrmFormatosPos.RtfFacturaPuerto
    
        RtfFormatoReintegro = FrmFormatosPos.RtfFormatoReintegro
        RtfReintegroPuerto = FrmFormatosPos.RtfReintegroPuerto
        
        RtfFormatoDevolucion = FrmFormatosPos.RtfFormatoDevolucion
        RtfDevolucionPuerto = FrmFormatosPos.RtfDevolucionPuerto
        
        RtfFormatoEspera = FrmFormatosPos.RtfFormatoEspera
        RtfEsperaPuerto = FrmFormatosPos.RtfEsperaPuerto
        
    End If
    
    Set FrmFormatosPos = Nothing
    
End Sub

Private Sub coddeposito_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        Tecla_pulsada = True
        Call Make_View("ma_deposito", "c_coddeposito", "c_descripcion", "D E P O S I T O S", Me, "GENERICO")
        Tecla_pulsada = False
    End If
End Sub

Private Sub Command1_Click()
    FrmPosInfo.PosNo = nCaja
    FrmPosInfo.Show vbModal
    Set FrmPosInfo = Nothing
End Sub

Private Sub cordenar_Click()
    'cordenar.Caption = IIf(cordenar.Value = 0, "No", "Si")
End Sub

Private Sub cVxV_Click()
    'cVxV.Caption = IIf(cVxV.Value = 0, "No", "Si")
End Sub

Private Sub dato_numerico_Click()
    'dato_numerico.Caption = IIf(dato_numerico.Value = 0, "No", "Si")
End Sub

Private Sub fondo_pos_Click()
    'fondo_pos.Caption = IIf(fondo_pos.Value = 0, "No", "Si")
End Sub

Private Sub Form_Load()

    'Me.Caption = Me.Caption & " de la Caja No. " & nCaja
    Me.lbl_Organizacion.Caption = Me.lbl_Organizacion.Caption & " de la Caja No. " & nCaja
    Call Apertura_Recordset(True, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set rsCaja.ActiveConnection = Nothing
    
    If Not rsCaja.EOF Then
        Call Llenar_Caja
    End If
    
    Me.lbl_Organizacion.Caption = Stellar_Mensaje(10162) 'datos generales
    Me.Toolbar1.Buttons(1).Caption = Stellar_Mensaje(103) 'grabar
    Me.Toolbar1.Buttons(2).Caption = Stellar_Mensaje(105) 'cancelar
    Me.Toolbar1.Buttons(4).Caption = Stellar_Mensaje(7) 'ayuda
    Toolbar1.Buttons("Teclado").Caption = Stellar_Mensaje(10080, True) ' Teclado
    
    Me.Label1.Caption = Stellar_Mensaje(142) 'codigo
    Me.Label3.Caption = Stellar_Mensaje(143) 'descripcion
    Me.Label60.Caption = Stellar_Mensaje(10240) 'pos digital
    Me.lineal_digital.Caption = Stellar_Mensaje(10144) 'si
    Me.Label34.Caption = Stellar_Mensaje(10215) 'solicitar fondo al abrir puntos de venta
    Me.fondo_pos.Caption = Stellar_Mensaje(10144) 'si
    Me.Label61.Caption = Stellar_Mensaje(10211) 'Standerd OLE POS
    Me.opos.Caption = Stellar_Mensaje(10144) 'si
    Me.Label4.Caption = Stellar_Mensaje(10214) 'El POS maneja ventas por volumen
    Me.cVxV.Caption = Stellar_Mensaje(10144) 'si
    Me.Label56.Caption = Stellar_Mensaje(10213) 'codigo del producto alfanumerico
    Me.dato_numerico.Caption = Stellar_Mensaje(10144) 'si
    Me.Label5.Caption = Stellar_Mensaje(10216) 'requiere de clave de supervisor
    Me.cClaveOpen.Caption = Stellar_Mensaje(10144) 'si
    
    Me.Label6.Caption = Stellar_Mensaje(10217) 'Moneda
    Me.Label59.Caption = Stellar_Mensaje(10165) 'deposito para descargar ventas
    
    Me.Label13.Caption = Stellar_Mensaje(10221) ' transaccioens para actualizar
    
    Me.Command1.Caption = Stellar_Mensaje(10241) 'info
    Me.cmd_rtf.Caption = Stellar_Mensaje(10242) 'Formatos
    
    Me.Label7.Caption = Stellar_Mensaje(10228) 'Configuracion del ticket de impresion
    
    Me.forma_continua.Caption = Stellar_Mensaje(10169) 'utilizar papel de forma continua
    Me.Label57.Caption = Stellar_Mensaje(10144) 'si
    Me.Label29.Caption = Stellar_Mensaje(10171) ' Ordenar productos al imprimir
    Me.cordenar.Caption = Stellar_Mensaje(10144) 'si
    Me.Label58.Caption = Stellar_Mensaje(10229) 'numero de lineas
    Me.Label27.Caption = Stellar_Mensaje(10172) 'agrupar productos al imprimir
    Me.cagrupar.Caption = Stellar_Mensaje(10144) 'si
    
    Me.Label2(0).Caption = Stellar_Mensaje(10173) & " 1" 'comentario 1
    Me.Label2(1).Caption = Stellar_Mensaje(10173) & " 2" 'comentario 2
    Me.Label2(2).Caption = Stellar_Mensaje(10173) & " 3" 'comentario 3
    Me.Label2(3).Caption = Stellar_Mensaje(10173) & " 4" 'comentario 4
    Me.Label2(4).Caption = Stellar_Mensaje(10173) & " 5" 'comentario 5
    Call Cerrar_Recordset(rsCaja)
    
    Label60.Visible = False
    lineal_digital.Visible = False
    
    descri.SelStart = 0
    descri.SelLength = Len(descri.Text)
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF4
            Call Grabar_Caja
            Unload Me
            
        Case vbKeyF12, vbKeyF7
            Unload Me
    End Select
End Sub

Private Sub Llenar_Caja()
    
    codigo.Text = rsCaja!c_codigo 'Format(rsCaja!c_codigo, "000")
    descri.Text = rsCaja!c_Desc_Caja
    
    dato_numerico.Value = IIf(rsCaja!Dato_String = True, 1, 0)
    
    opos.Value = IIf(rsCaja!b_Opos = True, 1, 0)
    cordenar.Value = IIf(rsCaja!Imp_Ordenada = True, 1, 0)
    cagrupar.Value = IIf(rsCaja!Imp_Agrupada = True, 1, 0)
    
    forma_continua.Value = IIf(rsCaja!forma_continua = True, 1, 0)
    no_lineas.Text = rsCaja!no_lineas
    linea(0).Text = IIf(IsNull(rsCaja!Mensaje1), "", rsCaja!Mensaje1)
    linea(1).Text = IIf(IsNull(rsCaja!Mensaje2), "", rsCaja!Mensaje2)
    linea(2).Text = IIf(IsNull(rsCaja!Mensaje3), "", rsCaja!Mensaje3)
    linea(3).Text = IIf(IsNull(rsCaja!Mensaje4), "", rsCaja!Mensaje4)
    linea(4).Text = IIf(IsNull(rsCaja!Mensaje5), "", rsCaja!Mensaje5)

    fondo_pos.Value = IIf(rsCaja!b_Solicitar_Fondo = True, 1, 0)
    coddeposito.Text = IIf(IsNull(rsCaja!c_CodDeposito), "", rsCaja!c_CodDeposito)
    TxtMoneda.Text = IIf(IsNull(rsCaja!c_CodMoneda), "", rsCaja!c_CodMoneda)
    lineal_digital.Value = IIf(rsCaja!B_POS_Digital = False, 0, 1)
    cVxV.Value = IIf(rsCaja!b_Ventas_Volumen, 1, 0)
    cClaveOpen.Value = IIf(rsCaja!cOpenPOS, 1, 0)
    Actualizar.Value = CInt(rsCaja!n_Transacciones)

    RtfFormatoFactura = rsCaja!cs_FormatoFactura
    RtfFacturaPuerto = rsCaja!bs_FacturaPuerto
    
    RtfFormatoDevolucion = rsCaja!cs_FormatoDevolucion
    RtfDevolucionPuerto = rsCaja!bs_DevolucionPuerto
    
    RtfFormatoReintegro = rsCaja!cs_FormatoReintegro
    RtfReintegroPuerto = rsCaja!bs_ReintegroPuerto
    
    RtfFormatoEspera = rsCaja!cs_FormatoEspera
    RtfEsperaPuerto = rsCaja!bs_EsperaPuerto

    Call Buscar_Deposito
    Call Buscar_Moneda
    
End Sub

Private Sub Buscar_Deposito()

    Call Apertura_Recordset(False, rsEureka)
    
    rsEureka.Open "SELECT * FROM MA_DEPOSITO WHERE c_CodDeposito= '" & coddeposito.Text & "' and c_CodLocalidad = '" & Sucursal & "'", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEureka.EOF Then
        lbl_deposito.Text = rsEureka!c_Descripcion
    Else
        'Call mensaje(True, "El c�digo del Dep�sito no existe.")
        lbl_deposito.Text = ""
    End If
    
    Call Cerrar_Recordset(rsEureka)

End Sub

Private Sub Grabar_Caja()

    If coddeposito.Text = "" Then
        Call Mensaje(True, "Debe seleccionar un Dep�sito para las ventas.")
        coddeposito.SetFocus
        Exit Sub
    End If
    
    Ent.POS.BeginTrans
    
    Call Apertura_Recordset(False, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
    Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not rsCaja.EOF Then
        rsCaja.Update
            rsCaja!nCierre = 1
           'rscaja!c_relacion = lckey
            Clave = "C" & Format(codigo.Text, "000")
            rsCaja!B_POS_Digital = lineal_digital.Value
            rsCaja!c_CodMoneda = TxtMoneda.Text
            rsCaja!b_Opos = opos.Value
            rsCaja!c_Desc_Caja = UCase(descri.Text)
            rsCaja!Imp_Ordenada = IIf(cordenar.Value = 1, 1, 0)
            rsCaja!Imp_Agrupada = IIf(cagrupar.Value = 1, 1, 0)
            rsCaja!Dato_String = dato_numerico.Value = 1
            
            rsCaja!forma_continua = IIf(forma_continua.Value = 1, 1, 0)
            rsCaja!no_lineas = Int(no_lineas.Text)
            rsCaja!n_Transacciones = Actualizar.Value

            rsCaja!Mensaje1 = IIf(IsNull(linea(0).Text), "", linea(0).Text)
            rsCaja!Mensaje2 = IIf(IsNull(linea(1).Text), "", linea(1).Text)
            rsCaja!Mensaje3 = IIf(IsNull(linea(2).Text), "", linea(2).Text)
            rsCaja!Mensaje4 = IIf(IsNull(linea(3).Text), "", linea(3).Text)
            rsCaja!Mensaje5 = IIf(IsNull(linea(4).Text), "", linea(4).Text)
            
            rsCaja!b_Solicitar_Fondo = IIf(fondo_pos.Value = 1, 1, 0)
            rsCaja!cOpenPOS = IIf(cClaveOpen.Value = 1, 1, 0)
            rsCaja!c_CodDeposito = coddeposito.Text
            rsCaja!b_Ventas_Volumen = (cVxV.Value = 1)
            
            rsCaja!cs_FormatoFactura = RtfFormatoFactura
            rsCaja!bs_FacturaPuerto = RtfFacturaPuerto
            
            rsCaja!cs_FormatoDevolucion = RtfFormatoDevolucion
            rsCaja!bs_DevolucionPuerto = RtfDevolucionPuerto
            
            rsCaja!cs_FormatoReintegro = RtfFormatoReintegro
            rsCaja!bs_ReintegroPuerto = RtfReintegroPuerto
            
            rsCaja!cs_FormatoEspera = RtfFormatoEspera
            rsCaja!bs_EsperaPuerto = RtfEsperaPuerto
            
        rsCaja.UpdateBatch
        
    End If
        
'            If glgrabar = False Then
'                ARBOL.Nodes.Item(lcindex).Text = UCase(descri.Text)
'            Else
'                Call crear_menu(lckey, clave, UCase(descri.Text), "caja")
'            End If

    Ent.POS.CommitTrans
    
    Call Cerrar_Recordset(rsCaja)
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 Then
        Unload Me
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmCajaRetail = Nothing
End Sub

Private Sub forma_continua_Click()
    If forma_continua.Value = 1 Then
        no_lineas.Enabled = True
        uplineas.Enabled = True
    Else
        no_lineas.Enabled = False
        uplineas.Enabled = False
    End If
    
    'Label57.Caption = IIf(forma_continua.Value = 0, "No", "Si")
End Sub


Private Sub lineal_digital_Click()
    'lineal_digital.Caption = IIf(lineal_digital.Value = 0, "No", "Si")
End Sub

Private Sub opos_Click()
    'opos.Caption = IIf(opos.Value = 0, "No", "Si")
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF12, 0)
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
        Case Is = "AYUDA"
            SendKeys "{F1}"
        Case Is = UCase("Teclado")
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = descri
            
            TecladoWindows mCtl
            
    End Select
End Sub

Private Sub TxtMoneda_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        Tecla_pulsada = True
        Call Make_View("ma_monedas", "c_codmoneda", "c_descripcion", "M O N E D A S", Me, "GENERICO")
        Tecla_pulsada = False
    End If
End Sub

Private Sub view_Click()
    coddeposito.SetFocus
    Call coddeposito_KeyDown(vbKeyF2, 0)
End Sub

Private Sub Buscar_Moneda()
    
    Call Apertura_Recordset(False, rsEureka)
    
    rsEureka.Open "select * from ma_monedas where c_codmoneda= '" & TxtMoneda.Text & "'", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEureka.EOF Then
        Lbl_Moneda.Text = rsEureka!c_Descripcion
    Else
        'Call mensaje(True, "El c�digo de la moneda no existe.")
        Lbl_Moneda.Text = ""
    End If
    
    Call Cerrar_Recordset(rsEureka)

End Sub
