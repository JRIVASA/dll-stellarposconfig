VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form ficha_dgs 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "#"
   ClientHeight    =   6555
   ClientLeft      =   480
   ClientTop       =   1065
   ClientWidth     =   11220
   Icon            =   "Ficha_Dgs.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6555
   ScaleWidth      =   11220
   Begin TabDlg.SSTab FDPTO 
      CausesValidation=   0   'False
      Height          =   1845
      Left            =   5310
      TabIndex        =   1
      Top             =   60
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   3254
      _Version        =   393216
      Tabs            =   1
      TabHeight       =   520
      Enabled         =   0   'False
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Departamentos"
      TabPicture(0)   =   "Ficha_Dgs.frx":1CCA
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "C�digo(1)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "C�digo(16)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "C�digo(0)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "C�digo(4)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "DCAMPO(0)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "DCAMPO(1)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "DCAMPO(3)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "DCAMPO(2)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).ControlCount=   8
      Begin VB.TextBox DCAMPO 
         BackColor       =   &H80000018&
         Height          =   285
         Index           =   2
         Left            =   4470
         MaxLength       =   10
         TabIndex        =   4
         Top             =   630
         Width           =   1215
      End
      Begin VB.TextBox DCAMPO 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Index           =   3
         Left            =   90
         MaxLength       =   250
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   5
         Top             =   1200
         Width           =   5625
      End
      Begin VB.TextBox DCAMPO 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1395
         MaxLength       =   50
         TabIndex        =   3
         Top             =   630
         Width           =   2925
      End
      Begin VB.TextBox DCAMPO 
         BackColor       =   &H80000018&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   60
         MaxLength       =   5
         TabIndex        =   2
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo *"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Index           =   4
         Left            =   60
         TabIndex        =   27
         Top             =   420
         Width           =   1215
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Index           =   0
         Left            =   4470
         TabIndex        =   26
         Top             =   420
         Width           =   1215
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "Observaci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Index           =   16
         Left            =   60
         TabIndex        =   25
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label C�digo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n *"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   195
         Index           =   1
         Left            =   1380
         TabIndex        =   24
         Top             =   420
         Width           =   1320
      End
   End
   Begin MSComctlLib.ImageList iconos 
      Left            =   4740
      Top             =   6420
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":1CE6
            Key             =   "raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":213A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":259A
            Key             =   "dpto"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":29EE
            Key             =   "dpto_open"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":2E4E
            Key             =   "grupo"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":32A2
            Key             =   "grupo_open"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":36F6
            Key             =   "subgrupo"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab FGRUPO 
      CausesValidation=   0   'False
      Height          =   1845
      Left            =   5310
      TabIndex        =   6
      Top             =   1920
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   3254
      _Version        =   393216
      Tabs            =   1
      TabHeight       =   520
      Enabled         =   0   'False
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Grupos"
      TabPicture(0)   =   "Ficha_Dgs.frx":3B4E
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "C�digo(3)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "C�digo(5)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "C�digo(6)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "C�digo(2)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Gcampo(2)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Gcampo(3)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Gcampo(1)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Gcampo(0)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).ControlCount=   8
      Begin VB.TextBox Gcampo 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         MaxLength       =   5
         TabIndex        =   7
         Top             =   630
         Width           =   1215
      End
      Begin VB.TextBox Gcampo 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1440
         MaxLength       =   50
         TabIndex        =   8
         Top             =   630
         Width           =   2925
      End
      Begin VB.TextBox Gcampo 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Index           =   3
         Left            =   90
         MaxLength       =   250
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Top             =   1200
         Width           =   5625
      End
      Begin VB.TextBox Gcampo 
         BackColor       =   &H80000018&
         Height          =   285
         Index           =   2
         Left            =   4470
         MaxLength       =   10
         TabIndex        =   9
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Index           =   2
         Left            =   4440
         TabIndex        =   19
         Top             =   420
         Width           =   1215
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "Observaci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Index           =   6
         Left            =   90
         TabIndex        =   18
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label C�digo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n *"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   195
         Index           =   5
         Left            =   1410
         TabIndex        =   17
         Top             =   420
         Width           =   1320
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo *"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Index           =   3
         Left            =   150
         TabIndex        =   16
         Top             =   420
         Width           =   1215
      End
   End
   Begin TabDlg.SSTab FSUBGRUPO 
      CausesValidation=   0   'False
      Height          =   1845
      Left            =   5310
      TabIndex        =   11
      Top             =   3780
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   3254
      _Version        =   393216
      Tabs            =   1
      TabHeight       =   520
      Enabled         =   0   'False
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Subgrupos"
      TabPicture(0)   =   "Ficha_Dgs.frx":3B6A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "C�digo(7)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "C�digo(8)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "C�digo(9)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "C�digo(10)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Scampo(2)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Scampo(3)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Scampo(1)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Scampo(0)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).ControlCount=   8
      Begin VB.TextBox Scampo 
         BackColor       =   &H80000018&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         MaxLength       =   5
         TabIndex        =   12
         Top             =   630
         Width           =   1215
      End
      Begin VB.TextBox Scampo 
         BackColor       =   &H80000018&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1425
         MaxLength       =   50
         TabIndex        =   13
         Top             =   630
         Width           =   2925
      End
      Begin VB.TextBox Scampo 
         BackColor       =   &H80000018&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Index           =   3
         Left            =   90
         MaxLength       =   250
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   15
         Top             =   1200
         Width           =   5625
      End
      Begin VB.TextBox Scampo 
         BackColor       =   &H80000018&
         Height          =   285
         Index           =   2
         Left            =   4470
         MaxLength       =   10
         TabIndex        =   14
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label C�digo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n *"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   195
         Index           =   10
         Left            =   1410
         TabIndex        =   23
         Top             =   420
         Width           =   1320
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "Observaci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Index           =   9
         Left            =   90
         TabIndex        =   22
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Index           =   8
         Left            =   4470
         TabIndex        =   21
         Top             =   420
         Width           =   1215
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo *"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   255
         Index           =   7
         Left            =   90
         TabIndex        =   20
         Top             =   420
         Width           =   1215
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1890
      Top             =   6480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":3B86
            Key             =   "Agrergar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":4862
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":553E
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":621A
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":6EF6
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":7BD2
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":88AE
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":958A
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":A266
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":A582
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   1170
      Top             =   6480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":B25E
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":BF3A
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":C256
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":C572
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":D24E
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":DF2A
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":EC06
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":EF22
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":FBFE
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":FF1A
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_deshabilitado 
      Left            =   465
      Top             =   6480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":10BF6
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":118D2
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":125AE
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":128CA
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":135A6
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":14282
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":14F5E
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":15C3A
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":16916
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Dgs.frx":16C32
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox CoolBar 
      Height          =   855
      Left            =   5310
      ScaleHeight     =   795
      ScaleWidth      =   5730
      TabIndex        =   28
      Top             =   5640
      Width           =   5790
      Begin VB.Frame Frame6 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   30
         TabIndex        =   29
         Top             =   30
         Width           =   5715
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   675
            Left            =   90
            TabIndex        =   30
            Top             =   60
            Width           =   5550
            _ExtentX        =   9790
            _ExtentY        =   1191
            ButtonWidth     =   1482
            ButtonHeight    =   1191
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            ImageList       =   "Icono_Apagado"
            DisabledImageList=   "Icono_deshabilitado"
            HotImageList    =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "&Cancelar"
                  Key             =   "Cancelar"
                  Object.ToolTipText     =   "Cancelar esta Ficha"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "&Grabar"
                  Key             =   "Grabar"
                  Object.ToolTipText     =   "Imprimir Fichero"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir del Fichero"
                  ImageIndex      =   9
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  Object.ToolTipText     =   "Ayuda del Sistema V�rtigo"
                  ImageIndex      =   10
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.TreeView arbol 
      Height          =   6435
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   11351
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   441
      LabelEdit       =   1
      Sorted          =   -1  'True
      Style           =   7
      ImageList       =   "iconos"
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu menu 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu agregar 
         Caption         =   "&Agregar"
      End
      Begin VB.Menu modificar 
         Caption         =   "&Modificar"
      End
      Begin VB.Menu GUION 
         Caption         =   "-"
      End
      Begin VB.Menu eliminar 
         Caption         =   "&Eliminar"
      End
   End
End
Attribute VB_Name = "ficha_dgs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub agregar_Click()
On Error GoTo error_agregar
    Select Case Mid(lckey, 1, 1)
        Case Is = "R"
            FDPTO.Enabled = True
            Call ACTIVAR_BOTON(True)
            For i = 0 To 3
                DCAMPO(i) = ""
                DCAMPO(i).Enabled = True
            Next
            DCAMPO(0).SetFocus
            
        Case Is = "D"
            FGRUPO.Enabled = True
            Call ACTIVAR_BOTON(True)
            For i = 0 To 3
                Gcampo(i) = ""
                Gcampo(i).Enabled = True
            Next
            Gcampo(0).SetFocus

        Case Is = "G"
            FSUBGRUPO.Enabled = True
            Call ACTIVAR_BOTON(True)
            For i = 0 To 3
                Scampo(i) = ""
                Scampo(i).Enabled = True
            Next
            Scampo(0).SetFocus
    
    End Select
Exit Sub
error_agregar:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub arbol_Click()
    lckey = arbol.SelectedItem.Key
    lctag = arbol.SelectedItem.TAG
    lcnode = arbol.SelectedItem.Index
    lcindex = arbol.SelectedItem.Index
'    Set GLNODO = Node

End Sub

Private Sub arbol_Expand(ByVal Node As MSComctlLib.Node)
On Error GoTo error_arbolexp
    Select Case Mid(Node.Key, 1, 1)
        Case Is = "R"
            Node.ExpandedImage = 2
        Case Is = "D"
            Node.ExpandedImage = 4
        Case Is = "G"
            Node.ExpandedImage = 6
    End Select
Exit Sub
error_arbolexp:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub arbol_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo error_arbolkeydown
    lckey = arbol.SelectedItem.Key
    lctag = arbol.SelectedItem.TAG
    lcnode = arbol.SelectedItem.Index
    lcindex = arbol.SelectedItem.Index
'    Set GLNODO = Node
    Select Case KeyCode
        Case Is = 93
            Call arbol_MouseDown(vbRightButton, 0, 0, 0)
    End Select
Exit Sub
error_arbolkeydown:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub


Private Sub arbol_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
On Error GoTo error_arbolmouse
    Select Case Button
        Case Is = 1
        Case Is = 2
            Select Case Mid(lckey, 1, 1)
                Case Is = "R"
                    Modificar.Enabled = False
                    Eliminar.Enabled = False
            
                Case Is = "S"
                    Agregar.Enabled = False
                    Modificar.Enabled = True
                    Eliminar.Enabled = True
                            
                Case Else
                    Agregar.Enabled = True
                    Modificar.Enabled = True
                    Eliminar.Enabled = True
                    
            End Select
            PopupMenu menu
    End Select
Exit Sub
error_arbolmouse:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub



Private Sub arbol_NodeClick(ByVal Node As MSComctlLib.Node)
On Error GoTo error_arbolnode
    espera = False
    lckey = arbol.SelectedItem.Key
    lctag = arbol.SelectedItem.TAG
    lcnode = arbol.SelectedItem.Index
    lcindex = arbol.SelectedItem.Index
    Set GLNODO = Node
    Select Case Left(UCase(lckey), 1)
        Case Is = "R"
            For i = 0 To 3
                DCAMPO(i).Text = ""
                Gcampo(i).Text = ""
                Scampo(i).Text = ""
            Next
        
        Case "D"
            Call BUSCAR_DPTO(False, lctag)
            For i = 0 To 3
                Gcampo(i).Text = ""
                Scampo(i).Text = ""
            Next
        
        Case Is = "G"
            Call BUSCAR_DPTO(False, arbol.Nodes.Item(lcnode).Parent.TAG)
            Call buscar_grupo(False, lctag, arbol.Nodes.Item(lcnode).Parent.TAG)
            For i = 0 To 3
                Scampo(i).Text = ""
            Next
        
        Case Is = "S"
            Dim dpto As String
            Dim grupo As String
            grupo = arbol.Nodes.Item(lcnode).Parent.TAG
            dpto = Mid(lckey, 2, (Len(lckey) - 1) - (Len(grupo) + Len(lctag)))
            Call BUSCAR_DPTO(False, dpto)
            Call buscar_grupo(False, grupo, dpto)
            Call buscar_subgrupo(False, lctag, grupo, dpto)
    
    End Select
    Call cerrar_recordset(rseureka)
Exit Sub
error_arbolnode:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub dcampo_LostFocus(Index As Integer)
On Error GoTo error_dcampolost
    Select Case Index
        Case Is = 0
            Call Apertura_recordset(rseureka)
            rseureka.Open "select * from ma_departamentos where c_codigo = '" & DCAMPO(0).Text & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rseureka.EOF Then
                Call cerrar_recordset(rseureka)
                Call mensaje(True, "Existe un departamento con este c�digo...!")
                DCAMPO(0).Text = ""
                DCAMPO(0).SetFocus
            End If
        
    End Select
Exit Sub
error_dcampolost:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub Form_Activate()
    Screen.MousePointer = 0
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo error_formkeydown
    If Shift = vbAltMask Then
        Select Case KeyCode
            Case Is = vbKeyC
                Call Form_KeyDown(vbKeyF7, 0)
                
            Case Is = vbKeyG
                Call Form_KeyDown(vbKeyF4, 0)
                
            Case Is = vbKeyS
                Call Form_KeyDown(vbKeyF12, 0)
            
            Case Is = vbKeyA
            
        End Select
    Else
    
        Select Case KeyCode
                
            Case Is = vbKeyF7
                FDPTO.Enabled = False
                FGRUPO.Enabled = False
                FSUBGRUPO.Enabled = False
                Call ACTIVAR_BOTON(False)
                Call cerrar_recordset(rseureka)
                Call arbol_NodeClick(GLNODO)
                arbol.SetFocus
            
            Case Is = vbKeyF4
                '********************************************
                '*** DEPARTAMENTOS
                '********************************************
                If FDPTO.Enabled = True Then
                    For i = 0 To 1
                        If DCAMPO(i).Text = "" Then
                            DCAMPO(i).SetFocus
                            Exit Sub
                        End If
                    Next
                    If DCAMPO(0).Enabled = True Then
                        rseureka.AddNew
                            rseureka!C_CODIGO = DCAMPO(o).Text
                    Else
                        rseureka.update
                    End If
                    rseureka!c_descripcio = DCAMPO(1).Text
                    rseureka!C_GRUPO = DCAMPO(2).Text
                    rseureka!c_observacio = IIf(DCAMPO(3).Text = "", " ", DCAMPO(3).Text)
                    rseureka.UpdateBatch
                    
                    lckey = "D" & DCAMPO(0).Text
                    lctexto = DCAMPO(1).Text
                    If DCAMPO(0).Enabled = True Then
                        Relacion = "RAIZ"
                        Imagen = "dpto"
                        COMENTARIO = DCAMPO(0)
                '        '' se genera el arbol a trav�s de los valores
                        crear_menu "lcnode", COMENTARIO, "RAIZ", lckey, lctexto, Imagen, "dpto"
                    Else
                        arbol.Nodes(lcnode).Text = lctexto
                    End If
                    FDPTO.Enabled = False
                    Call ACTIVAR_BOTON(False)
                    arbol.Refresh
                    arbol.SetFocus
                    Exit Sub
                End If
                
                '********************************************
                '*** GRUPO
                '********************************************
                If FGRUPO.Enabled = True Then
                    For i = 0 To 1
                        If Gcampo(i).Text = "" Then
                            Gcampo(i).SetFocus
                            Exit Sub
                        End If
                    Next
                    If Gcampo(0).Enabled = True Then
                        rseureka.AddNew
                            rseureka!C_CODIGO = Gcampo(0).Text
                    Else
                        rseureka.update
                    End If
                    lctexto = Gcampo(1).Text
                    dpto = DCAMPO(0)
                    lckey = "G" & dpto & Gcampo(0)
                    
                    rseureka!c_descripcio = Gcampo(1).Text
                    rseureka!C_GRUPO = Gcampo(2).Text
                    rseureka!c_observacio = Gcampo(3).Text
                    rseureka!C_DEPARTAMENTO = dpto
                    rseureka.UpdateBatch
                    If Gcampo(0).Enabled = True Then
                        Imagen = "grupo"
                        COMENTARIO = Gcampo(0)
                '        '' se genera el arbol a trav�s de los valores
                        crear_menu "lcnode", COMENTARIO, "D" & DCAMPO(0), lckey, lctexto, Imagen, "grupo1"
                    Else
                        arbol.Nodes(lcnode).Text = lctexto
                    End If
                    FGRUPO.Enabled = False
                    Call ACTIVAR_BOTON(False)
                    arbol.Refresh
                    arbol.SetFocus
                    Exit Sub
                End If
                
                '********************************************
                '*** SUBGRUPO
                '********************************************
                If FSUBGRUPO.Enabled = True Then
                    For i = 0 To 1
                        If Scampo(i).Text = "" Then
                            Scampo(i).SetFocus
                            Exit Sub
                        End If
                    Next
                    If Scampo(0).Enabled = True Then
                        rseureka.AddNew
                            rseureka!C_CODIGO = Scampo(0).Text
                    Else
                        rseureka.update
                    End If
                    lctexto = Scampo(1).Text
                    lckey = "S" & DCAMPO(0) & Gcampo(0) & Scampo(0)
                    lctexto = Scampo(1).Text
                    rseureka!c_descripcio = Scampo(1).Text
                    rseureka!C_GRUPO = Scampo(2).Text
                    rseureka!c_observacio = Scampo(3).Text
                    rseureka!c_in_departamento = DCAMPO(0)
                    rseureka!c_in_grupo = Gcampo(0)
                    rseureka.UpdateBatch
                    If Scampo(0).Enabled = True Then
                        Imagen = "subgrupo"
                        COMENTARIO = Scampo(0)
                '        '' se genera el arbol a trav�s de los valores
                        crear_menu "lcnode", COMENTARIO, "G" & DCAMPO(0) & Gcampo(0), lckey, lctexto, Imagen, "subgrupo1"
                    Else
                        arbol.Nodes(lcnode).Text = lctexto
                    End If
                    FSUBGRUPO.Enabled = False
                    Call ACTIVAR_BOTON(False)
                    arbol.Refresh
                    arbol.SetFocus
                    Exit Sub
                End If
            
            Case Is = vbKeyF12
                Unload Me
        End Select
    End If
Exit Sub
error_formkeydown:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            KeyAscii = 0
           ' oTeclado.Key_Tab
           oTeclado.Key_Tab
    End Select
End Sub

Private Sub Form_Load()
    CANT = Me.Width / (Len(Titulo) + Len(STELLAR))
    CANT = CANT / 4
    Me.Caption = STELLAR & Space(CANT) & Titulo
    Call actualizar_arbol
End Sub


Private Sub crear_menu(campo0, Campo1, campo2, campo3, campo4, campo5, campo6)
'    On Error Resume Next
    If Campo1 <> campo6 Then
        Set campo0 = arbol.Nodes.add(campo2, tvwChild, campo3, campo4, campo5)
    Else
        Set campo0 = arbol.Nodes.add(, , campo3, campo4, campo5)
    End If
    campo0.TAG = Campo1

'    campo0.EnsureVisible   ' Muestra todos los nodos.
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 Then Call Form_KeyDown(vbKeyF12, 0)

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set rseureka = Nothing
    Set ficha_dgs = Nothing
End Sub


Private Sub Gcampo_LostFocus(Index As Integer)
On Error GoTo error_gcampolost
    Select Case Index
        Case Is = 0
            Call Apertura_recordset(rseureka)
            dpto = DCAMPO(0)
            rseureka.Open "select * from ma_grupos where c_codigo = '" & Gcampo(0).Text & "' and c_departamento = '" & dpto & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rseureka.EOF Then
                Call cerrar_recordset(rseureka)
                Call mensaje(True, "Existe un grupo en el Departamento " & DCAMPO(1) & " con este c�digo...!")
                Gcampo(0).Text = ""
                Gcampo(0).SetFocus
            End If
        
    End Select
Exit Sub
error_gcampolost:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub modificar_Click()
On Error GoTo error_modificar
    Select Case Mid(lckey, 1, 1)
        Case "D"
            Call BUSCAR_DPTO(True, lctag)
            
        Case Is = "G"
            Call buscar_grupo(True, lctag, arbol.Nodes.Item(lcnode).Parent.TAG)
            
        Case Is = "S"
            Call buscar_subgrupo(True, lckey, arbol.Nodes.Item(lcnode).Parent.TAG, Mid(lckey, 2, (Len(lckey) - 1) - (Len(arbol.Nodes.Item(lcnode).Parent.TAG) + Len(lctag))))
    
    End Select

Exit Sub
error_modificar:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub eliminar_Click()
On Error GoTo error_eliminar
    Dim SQL As String
    Select Case Mid(lckey, 1, 1)
        Case "R"
                    
        Case Is = "D"
            If arbol.SelectedItem.Children Then
                Call mensaje(True, "El departamento no puede ser eliminado porque posee grupos...!")
                Exit Sub
            End If
                
            Call Apertura_recordset(rseureka)
            
            rseureka.Open "SELECT TOP 1 C_departamento FROM MA_PRODUCTOS WHERE c_departamento = '" & lctag & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rseureka.EOF Then
                Call mensaje(True, "El departamento no puede ser eliminado, ya que existen productos con esta categoria...")
            Else
                Call Apertura_recordset(rseureka)
                rseureka.Open "select * from ma_departamentos where c_codigo = '" & lctag & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                If Not rseureka.EOF Then
                    SQL = "delete from ma_clientes_convenios where tipo=0 and clave=" & rseureka!ID
                    Ent.BDD.Execute SQL
                    rseureka.delete
                    rseureka.UpdateBatch
                End If
                arbol.Nodes.Remove (lcindex)
                arbol.Refresh
                For i = 0 To 3
                    DCAMPO(i) = ""
                Next
            End If
        
        Case Is = "G"
            If arbol.SelectedItem.Children Then
                Call mensaje(True, "El grupo no puede ser eliminado porque posee subgrupos...!")
                Exit Sub
            End If
            Call Apertura_recordset(rseureka)
            
            rseureka.Open "SELECT TOP 1 C_GRUPO FROM MA_PRODUCTOS WHERE C_GRUPO = '" & lctag & "' and c_departamento = '" & DCAMPO(0) & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rseureka.EOF Then
                Call mensaje(True, "El grupo no puede ser eliminado, ya que existen productos con esta categoria...")
            Else
                Call Apertura_recordset(rseureka)
                rseureka.Open "select * from ma_grupos where c_codigo = '" & lctag & "' and  c_departamento = '" & DCAMPO(0).Text & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                If Not rseureka.EOF Then
                    SQL = "delete from ma_clientes_convenios where tipo=1 and clave=" & rseureka!ID
                    Ent.BDD.Execute SQL
                    rseureka.delete
                    rseureka.UpdateBatch
                End If
                arbol.Nodes.Remove (lcindex)
                arbol.Refresh
                For i = 0 To 3
                    Gcampo(i) = ""
                Next
            End If
        
        Case Is = "S"
            Call Apertura_recordset(rseureka)
            
            rseureka.Open "SELECT TOP 1 C_SUBGRUPO FROM MA_PRODUCTOS WHERE C_SUBGRUPO = '" & lctag & "' and c_grupo = '" & Gcampo(0).Text & "' and c_departamento = '" & DCAMPO(0) & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rseureka.EOF Then
                Call mensaje(True, "El subgrupo no puede ser eliminado, ya que existen productos con esta categoria...")
            Else
                Call Apertura_recordset(rseureka)
                rseureka.Open "select * from ma_subgrupoS where c_codigo = '" & lctag & "' and c_in_grupo = '" & Gcampo(0).Text & "' AND c_in_departamento = '" & DCAMPO(0).Text & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                If Not rseureka.EOF Then
                    SQL = "delete from ma_clientes_convenios where tipo=2 and clave=" & rseureka!ID
                    Ent.BDD.Execute SQL
                    rseureka.delete
                    rseureka.UpdateBatch
                End If
                arbol.Nodes.Remove (lcindex)
                arbol.Refresh
                For i = 0 To 3
                    Scampo(i) = ""
                Next
            End If
    End Select
    Call cerrar_recordset(rseureka)
    Call Form_KeyDown(vbKeyF7, 0)

Exit Sub
error_eliminar:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub


Private Sub Scampo_LostFocus(Index As Integer)
On Error GoTo error_scampolost
    Select Case Index
        Case Is = 0
            Call Apertura_recordset(rseureka)
            dpto = DCAMPO(0)
            rseureka.Open "select * from ma_subgrupos where c_codigo = '" & Scampo(0).Text & "' and c_in_departamento = '" & dpto & "' and c_in_grupo = '" & Gcampo(0) & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
            If Not rseureka.EOF Then
                Call cerrar_recordset(rseureka)
                Call mensaje(True, "Existe en el  Grupo " & Gcampo(1) & " un subgrupo con este c�digo...!")
                Scampo(0).Text = ""
                Scampo(0).SetFocus
            End If
        
    End Select
Exit Sub
error_scampolost:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
            
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
            
        Case Is = "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
    End Select
End Sub


Private Sub ACTIVAR_BOTON(Valor As Boolean)
    Toolbar1.Buttons(1).Enabled = Valor
    Toolbar1.Buttons(3).Enabled = Valor
    arbol.Enabled = IIf(Valor = False, True, False)
End Sub


Private Sub BUSCAR_DPTO(Modificar As Boolean, codigo_buscar As String)
On Error GoTo error_buscardpto
    If Modificar Then
        Call Apertura_recordset(rseureka)
        rseureka.Open "select * from ma_departamentos where c_codigo = '" & codigo_buscar & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    Else
        Call apertura_recordsetc(rseureka)
        rseureka.Open "select * from ma_departamentos where c_codigo = '" & codigo_buscar & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        Set rseureka.ActiveConnection = Nothing
    End If
    If Not rseureka.EOF Then
        DCAMPO(0).Text = rseureka!C_CODIGO
        DCAMPO(1).Text = rseureka!c_descripcio
        DCAMPO(2).Text = IIf(IsNull(rseureka!C_GRUPO), "", rseureka!C_GRUPO)
        DCAMPO(3).Text = IIf(IsNull(rseureka!c_observacio), "", rseureka!c_observacio)
        If Modificar Then
            DCAMPO(0).Enabled = False
            FDPTO.Enabled = True
            Call ACTIVAR_BOTON(True)
            DCAMPO(1).SetFocus
        End If
    Else
        arbol.Nodes.Remove (lcindex)
        Call ACTIVAR_BOTON(False)
        Call mensaje(True, "Este Departamento ya fue eliminado por otro usuario...!")
    End If
Exit Sub
error_buscardpto:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub buscar_grupo(Modificar As Boolean, codigo_buscar As String, dpto As String)
On Error GoTo error_buscargrupo
    If Modificar Then
        Call Apertura_recordset(rseureka)
        rseureka.Open "select * from ma_grupos where C_CODIGO = '" & codigo_buscar & "' AND c_DEPARTAMENTO = '" & dpto & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    Else
        Call apertura_recordsetc(rseureka)
        rseureka.Open "select * from ma_grupos where C_CODIGO = '" & codigo_buscar & "' AND c_DEPARTAMENTO = '" & dpto & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        Set rseureka.ActiveConnection = Nothing
    End If
    If Not rseureka.EOF Then
        Gcampo(0).Text = rseureka!C_CODIGO
        Gcampo(1).Text = rseureka!c_descripcio
        Gcampo(2).Text = IIf(IsNull(rseureka!C_GRUPO), "", rseureka!C_GRUPO)
        Gcampo(3).Text = IIf(IsNull(rseureka!c_observacio), "", rseureka!c_observacio)
        If Modificar Then
            Gcampo(0).Enabled = False
            FGRUPO.Enabled = True
            Call ACTIVAR_BOTON(True)
            Gcampo(1).SetFocus
        End If
    Else
        arbol.Nodes.Remove (lcindex)
        Call ACTIVAR_BOTON(False)
        Call mensaje(True, "Este Grupo ya fue eliminado por otro usuario...!")
    End If
Exit Sub
error_buscargrupo:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub buscar_subgrupo(Modificar As Boolean, subgrupo As String, grupo As String, dpto As String)
On Error GoTo error_buscarsub
    If Modificar Then
        Call Apertura_recordset(rseureka)
        rseureka.Open "select * from ma_subgrupos where C_CODIGO = '" & lctag & "' AND c_in_DEPARTAMENTO = '" & dpto & "' and c_in_grupo = '" & grupo & "'", Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    Else
        Call apertura_recordsetc(rseureka)
        rseureka.Open "select * from ma_subgrupos where C_CODIGO = '" & subgrupo & "' AND c_in_DEPARTAMENTO = '" & dpto & "' and c_in_grupo = '" & grupo & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        Set rseureka.ActiveConnection = Nothing
    End If
    If Not rseureka.EOF Then
        Scampo(0).Text = rseureka!C_CODIGO
        Scampo(1).Text = rseureka!c_descripcio
        Scampo(2).Text = IIf(IsNull(rseureka!C_GRUPO), "", rseureka!C_GRUPO)
        Scampo(3).Text = IIf(IsNull(rseureka!c_observacio), "", rseureka!c_observacio)
        If Modificar Then
            Scampo(0).Enabled = False
            FSUBGRUPO.Enabled = True
            Call ACTIVAR_BOTON(True)
            Scampo(1).SetFocus
        End If
    Else
        arbol.Nodes.Remove (lcindex)
        Call ACTIVAR_BOTON(False)
        Call mensaje(True, "Este Grupo ya fue eliminado por otro usuario...!")
        
    End If

Exit Sub
error_buscarsub:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub

Private Sub actualizar_arbol()
'On Error GoTo error_actualizar
    crear_menu "lcnode", "org", "", "RAIZ", "CLASIFICACION", "raiz", "org"
    '*************************************
    '*** DEPARTAMENTOS
    '*************************************
    Call apertura_recordsetc(rseureka)
    rseureka.Open "select * from ma_departamentos order by c_descripcio", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    Set rseureka.ActiveConnection = Nothing
    Do Until rseureka.EOF()
'        '' obtiene los valores
        Relacion = "RAIZ"
        Clave = "D" & Trim(rseureka!C_CODIGO)
        Texto = rseureka!c_descripcio
        Imagen = "dpto"
        COMENTARIO = Trim(rseureka!C_CODIGO)
'        '' se genera el arbol a trav�s de los valores
        crear_menu "lcnode", COMENTARIO, Relacion, Clave, Texto, Imagen, "dpto"
        rseureka.MoveNext
    Loop
    '*************************************
    '*** GRUPOS
    '*************************************
    Call apertura_recordsetc(rseureka)
    rseureka.Open "select * from ma_grupos order by c_descripcio", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    Set rseureka.ActiveConnection = Nothing
    Do Until rseureka.EOF()
'        '' obtiene los valores
        Relacion = "D" & Trim(rseureka!C_DEPARTAMENTO)
        Clave = "G" & Trim(rseureka!C_DEPARTAMENTO) & Trim(rseureka!C_CODIGO)
        Texto = rseureka!c_descripcio
        Imagen = "grupo"
        COMENTARIO = Trim(rseureka!C_CODIGO)
'        '' se genera el arbol a trav�s de los valores
        crear_menu "lcnode", COMENTARIO, Relacion, Clave, Texto, Imagen, "grupo1"
        rseureka.MoveNext
    Loop

    '*************************************
    '*** SUBGRUPOS
    '*************************************
    Call apertura_recordsetc(rseureka)
    rseureka.Open "select * from ma_subgrupos order by c_descripcio", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    Set rseureka.ActiveConnection = Nothing
    Do Until rseureka.EOF()
'        '' obtiene los valores
        Relacion = "G" & Trim(rseureka!c_in_departamento) & Trim(rseureka!c_in_grupo)
        Clave = "S" & Trim(rseureka!c_in_departamento) & Trim(rseureka!c_in_grupo) & Trim(rseureka!C_CODIGO)
        Texto = rseureka!c_descripcio
        Imagen = "subgrupo"
        COMENTARIO = Trim(rseureka!C_CODIGO)
'        '' se genera el arbol a trav�s de los valores
        crear_menu "lcnode", COMENTARIO, Relacion, Clave, Texto, Imagen, "subgrupo1"
        rseureka.MoveNext
    Loop

Exit Sub
error_actualizar:
    Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me

End Sub
