VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form FrmImpresoras 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4335
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   8460
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmImpresoras.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   8460
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuración de Impresoras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6195
         TabIndex        =   11
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   8625
      TabIndex        =   7
      Top             =   421
      Width           =   8655
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Left            =   120
         TabIndex        =   8
         Top             =   0
         Width           =   8000
         Begin MSComctlLib.Toolbar tlb_Config 
            Height          =   810
            Left            =   195
            TabIndex        =   9
            Top             =   120
            Width           =   7500
            _ExtentX        =   13229
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            ToolTips        =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "Cancelar la configuración actual [F7]"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "Grabar esta configuración [F4]"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "salir"
                  Object.ToolTipText     =   "Salir del configurador [F12]"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "Ayuda del Configurador [F1]"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2160
      Left            =   300
      TabIndex        =   0
      Top             =   1815
      Width           =   7755
      Begin VB.ComboBox TipoPuerto 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4140
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   600
         Width           =   3135
      End
      Begin VB.CommandButton cmd_imprimir 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6930
         MaskColor       =   &H00585A58&
         TabIndex        =   5
         Top             =   1470
         Width           =   360
      End
      Begin VB.TextBox TxtUbicacion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         MaxLength       =   255
         TabIndex        =   2
         Top             =   1470
         Width           =   6420
      End
      Begin VB.TextBox Txtcodigo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   375
         Locked          =   -1  'True
         MaxLength       =   20
         TabIndex        =   1
         Top             =   600
         Width           =   3570
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Direccion de la impresora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   360
         TabIndex        =   4
         Top             =   1215
         Width           =   3750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Código"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   375
         TabIndex        =   3
         Top             =   345
         Width           =   1905
      End
   End
   Begin MSComctlLib.ImageList Treeimagen 
      Left            =   2055
      Top             =   90
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmImpresoras.frx":628A
            Key             =   "DPTO CERRADO"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmImpresoras.frx":66DC
            Key             =   "DPTO ABIERTO"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   3810
      Top             =   1290
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmImpresoras.frx":6B2E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmImpresoras.frx":88C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmImpresoras.frx":A652
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmImpresoras.frx":C3E4
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmImpresoras.frx":E176
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog Impresoras 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "FrmImpresoras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private FormaCargada As Boolean

Private Sub cmd_imprimir_Click()
    
    Dim PTest As Boolean, DefPrinter As String
    
    Impresoras.FileName = ""
    Impresoras.Flags = cdlPDDisablePrintToFile Or cdlPDNoPageNums
    Impresoras.ShowPrinter
    
    If TipoPuerto.ListIndex = 0 Then
        DefPrinter = Printer.DeviceName
    Else
        DefPrinter = Printer.Port
    End If
    
    If DefPrinter <> "" Then
        If Not ExisteImpresora(NodoTextoKey, DefPrinter) Then
            'Mensajes.PressBoton = True
            
            'PTest = Mensajes.Mensaje("¿Probar la conexión con la impresora?", True)
            Mensaje True, StellarMensaje(10235), True
            
            If Retorno Then
                Printer.Print Replace(Replace(StellarMensaje(10262), "$(Line)", vbNewLine), "$(Tab)", vbTab) '"Esta es una prueba del Stellar isFood." & vbNewLine & vbTab & "Esta es una prueba que la conexión esta bien." & vbNewLine & vbNewLine & vbTab & "Stellar isFood, la solución acertiva."
                Printer.EndDoc
            End If
            
            TxtUbicacion.Text = DefPrinter
        Else
            'Mensajes.Mensaje "La impresora ya existe en este nivel de configuración.", False
            Mensaje True, "La impresora ya existe en este nivel de configuración."
        End If
    End If
    
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        Call Cancelar
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case vbShiftMask
        Case Else
            Select Case KeyCode
                Case vbKeyF4
                    Call Grabar
                Case vbKeyF7
                    Call Cancelar
                Case vbKeyF12
                    Unload Me
                    Exit Sub
                Case vbKeyReturn
                    Send_Keys Chr(vbKeyTab), True
                Case vbKeyEscape
            End Select
    End Select
End Sub

Public Sub Grabar()
    
    On Error GoTo ErrorAlGrabar
    
    Dim rsGrabar As New ADODB.Recordset, Cont As Integer, Cuantos As Integer, Consecutivos As Object, NwCode As String
    
    Set Consecutivos = CreateObject("recsun.cls_datos")
    
    Ent.POS.BeginTrans
    
    If Mid(Me.Txtcodigo.Text, 1, 1) = "N" Then
        NwCode = Consecutivos.NO_CONSECUTIVO(ProducID, Ent.POS, "cs_impresoras", True)
    Else
        NwCode = Me.Txtcodigo.Text
    End If
    
    Call Apertura_Recordset(True, rsGrabar)
    
    rsGrabar.Open "SELECT * FROM MA_IMPRESORAS WHERE cs_Numero = '" & NwCode & "'", _
    Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
    
    If rsGrabar.EOF Then
        rsGrabar.AddNew
    End If
    
    rsGrabar!cs_Numero = NwCode
    rsGrabar!cs_Relacion = NodoTextoKey
    rsGrabar!cs_Nombre = TxtUbicacion.Text
    rsGrabar!bu_Por_Puerto = TipoPuerto.ListIndex
    rsGrabar.UpdateBatch
    
    'Ent.POS.CommitTrans
    
    lcKey = "|I|" & NodoTextoKey
    Clave = "|P|" & rsGrabar!cs_Numero
    lcTexto = rsGrabar!cs_Nombre
    AddNode = True
    rsGrabar.Close
    
    'Jesus
    'DEPARTAMENTOS
    
    SQL = "SELECT * FROM MA_IMPRESORA_DEPARTAMENTO WHERE cs_Relacion = '" & NwCode & "'"
    rsGrabar.Open SQL, Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText

    While Not rsGrabar.EOF
        rsGrabar!cu_impresora = TxtUbicacion.Text
        rsGrabar!bu_Por_Puerto = TipoPuerto.ListIndex
        rsGrabar.MoveNext
    Wend

    rsGrabar.UpdateBatch
    rsGrabar.Close

    'JESUS
    'GRUPOS

    SQL = "SELECT * FROM MA_IMPRESORA_GRUPO WHERE cs_Relacion = '" & NwCode & "'"
    rsGrabar.Open SQL, Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText

    While Not rsGrabar.EOF
        rsGrabar!cu_impresora = TxtUbicacion.Text
        rsGrabar!bu_Por_Puerto = TipoPuerto.ListIndex
        rsGrabar.MoveNext
    Wend

    rsGrabar.UpdateBatch
    rsGrabar.Close

    'JESUS
    'SUBGRUPOS

    SQL = "SELECT * FROM MA_IMPRESORA_SUBGRUPO WHERE cs_Relacion = '" & NwCode & "'"
    rsGrabar.Open SQL, Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText

    While Not rsGrabar.EOF
        rsGrabar!cu_impresora = TxtUbicacion.Text
        rsGrabar!bu_Por_Puerto = TipoPuerto.ListIndex
        rsGrabar.MoveNext
    Wend

    rsGrabar.UpdateBatch
    rsGrabar.Close
    
    Ent.POS.CommitTrans
    
    Unload Me
    
    Exit Sub
    
ErrorAlGrabar:
    
    Call Mensaje(False, ERR.Description)
    
End Sub

Public Sub Cancelar()
    
    Dim rsGrabar As New ADODB.Recordset, Consecutivos As Object
    
    Set Consecutivos = CreateObject("recsun.cls_datos")
    
    Call Apertura_Recordset(True, rsGrabar)
    
    AddNode = False
    
    If (TipoPuerto.ListCount <= 0) Then
        TipoPuerto.AddItem "Por Nombre"
        TipoPuerto.AddItem "Por Puerto"
        TipoPuerto.ListIndex = 0
    End If
    
    If Trim(Me.Txtcodigo.Text) <> "" Then
        rsGrabar.Open "SELECT * FROM MA_IMPRESORAS WHERE cs_Numero = '" & Trim(Me.Txtcodigo.Text) & "'", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not rsGrabar.EOF Then
            NodoTextoKey = rsGrabar!cs_Relacion
            Txtcodigo.Text = rsGrabar!cs_Numero
            TxtUbicacion.Text = rsGrabar!cs_Nombre
            TipoPuerto.ListIndex = IIf(rsGrabar!bu_Por_Puerto, 1, 0)
        Else
            TxtUbicacion.Text = ""
            Txtcodigo.Text = "N" & Consecutivos.NO_CONSECUTIVO(ProducID, Ent.POS, "cs_impresoras", False)
        End If
        
        rsGrabar.Close
    Else
        TxtUbicacion.Text = ""
        Txtcodigo.Text = "N" & Consecutivos.NO_CONSECUTIVO(ProducID, Ent.POS, "cs_impresoras", False)
    End If
    
    'Me.Caption = "Configuración para el Nivel [" & NodoTexto & "]"
    
End Sub

Private Sub Form_Load()
    
    'TipoPuerto.AddItem "Por Nombre"
    'TipoPuerto.AddItem "Por Puerto"
    
    TipoPuerto.AddItem Stellar_Mensaje(10131, True)
    TipoPuerto.AddItem Stellar_Mensaje(10132, True)
    TipoPuerto.ListIndex = 0
    
    Me.tlb_Config.Buttons(1).Caption = Stellar_Mensaje(6, True) 'cancelar
    Me.tlb_Config.Buttons(2).Caption = Stellar_Mensaje(103, True) 'grabar
    Me.tlb_Config.Buttons(3).Caption = Stellar_Mensaje(54, True) 'salir
    Me.tlb_Config.Buttons(4).Caption = Stellar_Mensaje(7, True) 'ayuda
    Me.tlb_Config.Buttons("Teclado").Caption = Stellar_Mensaje(10080, True) ' Teclado
    
    Me.lbl_Organizacion.Caption = StellarMensaje(10263) '"" 'Configuración de Impresoras
    
    Me.Label2.Caption = Stellar_Mensaje(142, True) 'codigo
    Me.Label1.Caption = Stellar_Mensaje(10130, True) 'direccion de la impresora
    
End Sub

Private Sub tlb_Config_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case LCase(Button.Key)
        Case "grabar"
            Send_Keys Chr(vbKeyF4), True
        Case "cancelar"
            Send_Keys Chr(vbKeyF7), True
        Case "salir"
            Send_Keys Chr(vbKeyF12), True
        Case "ayuda"
            Send_Keys Chr(vbKeyF1)
        Case LCase("Teclado")
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = Txtcodigo
            
            TecladoWindows mCtl
                        
    End Select
End Sub

Private Function ExisteImpresora(Relacion As String, PathPrinter As String) As Boolean
    
    Dim rsImpresora As New ADODB.Recordset
    
    Call Apertura_Recordset(True, rsImpresora)
    
    rsImpresora.Open "SELECT * FROM MA_IMPRESORAS WHERE cs_Nombre = '" & _
    PathPrinter & "' AND cs_Relacion = '" & Relacion & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    ExisteImpresora = Not rsImpresora.EOF
    rsImpresora.Close
    
End Function
