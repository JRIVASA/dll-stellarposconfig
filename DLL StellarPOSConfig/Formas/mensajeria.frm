VERSION 5.00
Begin VB.Form frm_Mensajeria 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3135
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   7965
   ControlBox      =   0   'False
   Icon            =   "mensajeria.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3135
   ScaleWidth      =   7965
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame6 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_Website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   5890
         TabIndex        =   6
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.CommandButton btnCopy 
      BackColor       =   &H80000004&
      Caption         =   "F3"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   1110
      Picture         =   "mensajeria.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "F3"
      Top             =   2040
      Width           =   1095
   End
   Begin VB.CommandButton Cancelar 
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   6645
      Picture         =   "mensajeria.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2025
      Width           =   1095
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   5355
      Picture         =   "mensajeria.frx":9D8E
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2025
      Width           =   1095
   End
   Begin VB.TextBox Mensaje 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1200
      Left            =   1230
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   635
      Width           =   6525
   End
   Begin VB.Label lblMarginTop 
      BackColor       =   &H80000005&
      Height          =   1400
      Left            =   1130
      TabIndex        =   8
      Top             =   535
      Width           =   6625
   End
   Begin VB.Label lbl_Progress 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   480
      TabIndex        =   3
      Top             =   1800
      Visible         =   0   'False
      Width           =   60
   End
   Begin VB.Image Icono 
      Appearance      =   0  'Flat
      Height          =   720
      Left            =   210
      Picture         =   "mensajeria.frx":BB10
      Top             =   900
      Width           =   720
   End
End
Attribute VB_Name = "frm_Mensajeria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub aceptar_Click()
    Tecla_pulsada = False
        Retorno = True
    Unload Me
End Sub

Private Sub btnCopy_Click()
    
    On Error GoTo ErrC
    
    Clipboard.Clear
    Clipboard.SetText Mensaje.Text
    
    btnCopy.Caption = StellarMensaje(10074) '"Copiado"
    
    Exit Sub
    
ErrC:
    
    btnCopy.Caption = StellarMensaje(10075) '"Fallido"
    
End Sub

Private Sub cancelar_Click()
    Retorno = False
    Unload Me
End Sub

Private Sub Form_Activate()
    ModalDisponible = False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF3
            btnCopy_Click
    End Select
End Sub

Private Sub Form_Load()
    
    Screen.MousePointer = vbDefault
    Me.lbl_Organizacion.Caption = "Información Stellar"
    
    'If Uno = True Then
        'cancelar.Enabled = False
        'cancelar.Visible = False
        'frm_MENSAJERIA.Width = 7605
    'Else
        'cancelar.Enabled = True
        
        'If txtMensaje1 <> "" Then aceptar.Caption = txtMensaje1
        'If txtMensaje2 <> "" Then cancelar.Caption = txtMensaje2
        
        'cancelar.Visible = True
        'frm_MENSAJERIA.Width = 8730
    'End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frm_Mensajeria = Nothing
    ModalDisponible = True
End Sub

Private Sub mensaje_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF3
            btnCopy_Click
    End Select
End Sub
