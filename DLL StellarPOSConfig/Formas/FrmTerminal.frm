VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "comct232.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form FrmTerminal 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   11430
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   10320
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmTerminal.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11430
   ScaleWidth      =   10320
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   44
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8115
         TabIndex        =   46
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Datos Generales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   45
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   10665
      TabIndex        =   41
      Top             =   421
      Width           =   10695
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Left            =   0
         TabIndex        =   42
         Top             =   0
         Width           =   10000
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   300
            TabIndex        =   43
            Top             =   120
            Width           =   9500
            _ExtentX        =   16748
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "F4 Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "F7 Cancelar"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "F1 Ayuda del SIstema"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   4
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame fdescripcion 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4365
      Left            =   195
      TabIndex        =   12
      Top             =   1650
      Width           =   9885
      Begin VB.CheckBox fondo_pos 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   8460
         TabIndex        =   62
         Top             =   1140
         Width           =   675
      End
      Begin VB.CheckBox Autodeclaracion 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   6525
         TabIndex        =   60
         Top             =   645
         Width           =   675
      End
      Begin VB.TextBox coddeposito 
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4245
         TabIndex        =   39
         Top             =   2025
         Width           =   1305
      End
      Begin VB.CommandButton view 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5700
         Picture         =   "FrmTerminal.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   2020
         Width           =   360
      End
      Begin VB.TextBox lbl_deposito 
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   6195
         Locked          =   -1  'True
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   2020
         Width           =   3015
      End
      Begin VB.TextBox TxtConsumo 
         Alignment       =   1  'Right Justify
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4200
         Locked          =   -1  'True
         MaxLength       =   255
         TabIndex        =   35
         Text            =   "0"
         Top             =   3855
         Width           =   915
      End
      Begin VB.TextBox TxtActualizar 
         Alignment       =   1  'Right Justify
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4200
         Locked          =   -1  'True
         MaxLength       =   255
         TabIndex        =   32
         Text            =   "0"
         Top             =   3405
         Width           =   915
      End
      Begin VB.CheckBox chk_AdminClientes 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4350
         TabIndex        =   27
         Top             =   2475
         Width           =   750
      End
      Begin VB.ComboBox TipoPrecio 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4200
         Style           =   2  'Dropdown List
         TabIndex        =   26
         Top             =   2940
         Width           =   2460
      End
      Begin VB.ComboBox CampoBusqueda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   6825
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   2480
         Width           =   2340
      End
      Begin VB.TextBox Lbl_Moneda 
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   6195
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   1560
         Width           =   3015
      End
      Begin VB.CommandButton cmd_moneda 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5700
         Picture         =   "FrmTerminal.frx":6A8C
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   1560
         Width           =   360
      End
      Begin VB.TextBox TxtMoneda 
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4245
         TabIndex        =   21
         Top             =   1560
         Width           =   1305
      End
      Begin VB.CheckBox opos 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3885
         TabIndex        =   16
         Top             =   645
         Width           =   795
      End
      Begin VB.TextBox codigo 
         Alignment       =   1  'Right Justify
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1770
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   15
         Text            =   "1"
         Top             =   180
         Width           =   765
      End
      Begin VB.TextBox descri 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4230
         MaxLength       =   50
         TabIndex        =   14
         Top             =   180
         Width           =   5025
      End
      Begin VB.CheckBox dato_numerico 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3885
         TabIndex        =   13
         Top             =   1095
         Width           =   795
      End
      Begin MSComCtl2.UpDown Actualizar 
         Height          =   360
         Left            =   5130
         TabIndex        =   31
         Top             =   3405
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   635
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "TxtActualizar"
         BuddyDispid     =   196619
         OrigLeft        =   3945
         OrigTop         =   2970
         OrigRight       =   4200
         OrigBottom      =   3255
         Max             =   100
         Min             =   1
         SyncBuddy       =   -1  'True
         Wrap            =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown Consumo 
         Height          =   360
         Left            =   5130
         TabIndex        =   34
         Top             =   3855
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   635
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "TxtConsumo"
         BuddyDispid     =   196618
         OrigLeft        =   3930
         OrigTop         =   2370
         OrigRight       =   4185
         OrigBottom      =   2655
         Max             =   100
         Min             =   1
         SyncBuddy       =   -1  'True
         Wrap            =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label Label34 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Solicitar Fondo al Abrir Punto de Venta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4920
         TabIndex        =   63
         Top             =   1140
         Width           =   3330
      End
      Begin VB.Label lblAutoDeclaracion 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Autodeclaraci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4920
         TabIndex        =   61
         Top             =   645
         Width           =   1365
      End
      Begin VB.Label Label59 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito para Descargar Ventas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   40
         Top             =   2025
         Width           =   3120
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "N� Maximo de impresiones de consumo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   480
         TabIndex        =   36
         Top             =   3855
         Width           =   3390
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Transacciones para actualizaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   480
         TabIndex        =   33
         Top             =   3405
         Width           =   3195
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Administra Clientes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   495
         TabIndex        =   30
         Top             =   2475
         Width           =   2970
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo de Precio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   495
         TabIndex        =   29
         Top             =   2940
         Width           =   2550
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Buscar por"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5370
         TabIndex        =   28
         Top             =   2500
         Width           =   915
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Moneda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   24
         Top             =   1560
         Width           =   1995
      End
      Begin VB.Label Label61 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Standard OLE POS "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   20
         Top             =   645
         Width           =   2370
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   19
         Top             =   180
         Width           =   1065
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   2880
         TabIndex        =   18
         Top             =   180
         Width           =   1335
      End
      Begin VB.Label Label56 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo del Producto Alfan�merico"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   17
         Top             =   1095
         Width           =   3375
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Configuraci�n del Tickets de Impresi�n "
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   4965
      Left            =   210
      TabIndex        =   0
      Top             =   6210
      Width           =   9885
      Begin VB.TextBox TxtUbicacion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2520
         MaxLength       =   255
         TabIndex        =   58
         Top             =   1375
         Width           =   4050
      End
      Begin VB.CommandButton cmd_imprimir 
         Height          =   360
         Left            =   6690
         Picture         =   "FrmTerminal.frx":728E
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   1375
         Width           =   465
      End
      Begin VB.ComboBox TipoPuerto 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   7275
         Style           =   2  'Dropdown List
         TabIndex        =   56
         Top             =   1375
         Width           =   1905
      End
      Begin VB.Frame FrameRemotas 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Caption         =   " Comandas Remotas  "
         ForeColor       =   &H80000008&
         Height          =   970
         Left            =   240
         TabIndex        =   52
         Top             =   1920
         Width           =   3900
         Begin VB.CheckBox RComandas 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Caption         =   "Imprimir Comandas"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   240
            TabIndex        =   54
            Top             =   280
            Width           =   3165
         End
         Begin VB.CheckBox RLlevar 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Caption         =   "Imprimir Comandas Llevar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   195
            Left            =   240
            TabIndex        =   53
            Top             =   615
            Width           =   3165
         End
         Begin VB.Label Label17 
            BackStyle       =   0  'Transparent
            Caption         =   "    Comandas Remotas  "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   0
            TabIndex        =   55
            Top             =   0
            Width           =   3135
         End
      End
      Begin VB.Frame FrameLocales 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Caption         =   " Comandas Locales  "
         ForeColor       =   &H80000008&
         Height          =   970
         Left            =   5490
         TabIndex        =   48
         Top             =   1920
         Width           =   3780
         Begin VB.CheckBox LComandas 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Caption         =   "Imprimir Comandas"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   195
            Left            =   195
            TabIndex        =   50
            Top             =   280
            Width           =   3165
         End
         Begin VB.CheckBox LLLevar 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Caption         =   "Imprimir Comandas Llevar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   195
            Left            =   195
            TabIndex        =   49
            Top             =   615
            Width           =   3165
         End
         Begin VB.Label Label16 
            BackStyle       =   0  'Transparent
            Caption         =   " Comandas Locales  "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   120
            TabIndex        =   51
            Top             =   0
            Width           =   2655
         End
      End
      Begin VB.CheckBox cordenar 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   8670
         TabIndex        =   5
         Top             =   390
         Width           =   735
      End
      Begin VB.CheckBox cagrupar 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   8670
         TabIndex        =   4
         Top             =   900
         Width           =   855
      End
      Begin VB.CheckBox forma_continua 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Utiliza Papel de Forma Continua"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   390
         TabIndex        =   3
         Top             =   390
         Width           =   3105
      End
      Begin VB.TextBox no_lineas 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3270
         Locked          =   -1  'True
         TabIndex        =   2
         Text            =   "33"
         Top             =   810
         Width           =   405
      End
      Begin VB.TextBox Observaciones 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   1485
         Left            =   300
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   3330
         Width           =   9300
      End
      Begin ComCtl2.UpDown uplineas 
         Height          =   360
         Left            =   3675
         TabIndex        =   6
         Top             =   810
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   635
         _Version        =   327681
         Value           =   10
         OrigLeft        =   3540
         OrigTop         =   690
         OrigRight       =   3780
         OrigBottom      =   1035
         Max             =   100
         Min             =   10
         SyncBuddy       =   -1  'True
         Wrap            =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   0   'False
      End
      Begin MSComDlg.CommonDialog Impresoras 
         Left            =   4560
         Top             =   600
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Impresora Facturaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   360
         TabIndex        =   59
         Top             =   1375
         Width           =   2010
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   3960
         X2              =   9600
         Y1              =   150
         Y2              =   150
      End
      Begin VB.Label Label4 
         BackColor       =   &H00AE5B00&
         BackStyle       =   0  'Transparent
         Caption         =   " Configuraci�n del Ticket de Impresi�n "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   240
         TabIndex        =   47
         Top             =   0
         Width           =   4095
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ordenar Productos al Imprimir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5640
         TabIndex        =   11
         Top             =   390
         Width           =   2970
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Agrupar Productos al Imprimir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5640
         TabIndex        =   10
         Top             =   900
         Width           =   2955
      End
      Begin VB.Label Label58 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "N�mero de L�neas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   390
         TabIndex        =   9
         Top             =   900
         Width           =   2145
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Comentario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   0
         Left            =   390
         TabIndex        =   8
         Top             =   3015
         Width           =   1590
      End
      Begin VB.Label Label57 
         AutoSize        =   -1  'True
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3780
         TabIndex        =   7
         Top             =   435
         Width           =   525
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   3780
      Top             =   1230
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTerminal.frx":7A90
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTerminal.frx":9822
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTerminal.frx":B5B4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmTerminal.frx":D346
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmTerminal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd_moneda_Click()
    TxtMoneda.SetFocus
    Call TxtMoneda_KeyDown(vbKeyF2, 0)
End Sub

Private Sub coddeposito_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        Tecla_pulsada = True
        'Call Make_View("ma_deposito", "c_coddeposito", "c_descripcion", "D E P O S I T O S", Me, "GENERICO")
        Call Make_View("ma_deposito", "c_coddeposito", "c_descripcion", UCase(Stellar_Mensaje(65, True)), Me, "GENERICO")
        Tecla_pulsada = False
    End If
End Sub

Private Sub Form_Load()
    
    'Me.Caption = Me.Caption & " de la Caja No. " & nCaja
    Toolbar1.Buttons(1).Caption = Stellar_Mensaje(103, True) 'grabar
    Toolbar1.Buttons(2).Caption = Stellar_Mensaje(105, True) 'cancelar
    Toolbar1.Buttons(4).Caption = Stellar_Mensaje(7, True) 'ayuda
    Toolbar1.Buttons("Teclado").Caption = Stellar_Mensaje(10080, True) ' Teclado
    
    Me.lbl_Organizacion.Caption = Stellar_Mensaje(10162, True) 'Datos generales
    Label1.Caption = Stellar_Mensaje(142, True) 'codigo
    Label3.Caption = Stellar_Mensaje(143, True) 'Decripcion
    Label61.Caption = Stellar_Mensaje(10211, True) 'Standar OLE POS
    lblAutoDeclaracion.Caption = Stellar_Mensaje(10212) ' Autodeclaracion
    opos.Caption = Stellar_Mensaje(10144, True) 'si
    Me.Label34.Caption = Stellar_Mensaje(10215) 'solicitar fondo al abrir punto de venta
    Me.fondo_pos.Caption = Stellar_Mensaje(10144) 'si
    dato_numerico.Caption = Stellar_Mensaje(10144, True) 'si
    Label56.Caption = Stellar_Mensaje(10164, True) 'codigo del producto alfanumerico
    Label6.Caption = Replace(Stellar_Mensaje(134, True), ":", "") 'moneda
    Label59.Caption = Stellar_Mensaje(10165, True) 'deposito para descargar ventas
    Label9.Caption = Stellar_Mensaje(10166, True) 'Adminsitra clientes
    chk_AdminClientes.Caption = Stellar_Mensaje(10144, True) 'si
    Autodeclaracion.Caption = Stellar_Mensaje(10144) 'si
    Label11.Caption = Replace(Stellar_Mensaje(55, True), ":", "") 'buscar por
    Label10.Caption = Stellar_Mensaje(10167, True) 'tipo de precio
    Label13.Caption = Stellar_Mensaje(10221, True) 'transacciones para actualizar
    Label14.Caption = Stellar_Mensaje(10174, True) 'Maximo de impresi (Numero maximo de imprecion de comandas)
    
    Me.Label7.Caption = Stellar_Mensaje(10219) 'ubicacion impresora facturacion
    
    Label4.Caption = Stellar_Mensaje(10168, True) ' Configuraci�n del Ticket de Impresi�n
    forma_continua.Caption = Stellar_Mensaje(10169, True) 'Utiliza papel continuo
    Label57.Caption = Stellar_Mensaje(10144, True) 'si
    Label58.Caption = Stellar_Mensaje(10170, True) 'Numero de lineas
    Label29.Caption = Stellar_Mensaje(10171, True) 'Ordenar Productos al imprimir
    cordenar.Caption = Stellar_Mensaje(10144, True) 'si
    Label27.Caption = Stellar_Mensaje(10172, True) 'Agrupar Productos al imprimir
    Label2(0).Caption = Stellar_Mensaje(10173, True) ' Comentario.
    
    Me.Label16.Caption = Stellar_Mensaje(10226) 'comandas locales
    Me.Label17.Caption = Stellar_Mensaje(10223) 'commandas remotas
    
    Me.RComandas.Caption = Stellar_Mensaje(10224) 'imprimir comandas
    Me.RLlevar.Caption = Stellar_Mensaje(10225) 'imprimir comandas llevar
    Me.LComandas.Caption = Stellar_Mensaje(10224) 'imprimir comandas
    Me.LLLevar.Caption = Stellar_Mensaje(10225) 'imprimir comandas llevar
    
    Call Apertura_Recordset(True, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set rsCaja.ActiveConnection = Nothing
    
'    TipoPrecio.AddItem "Del Usuario"
'    TipoPrecio.AddItem "Precio 1"
'    TipoPrecio.AddItem "Precio 2"
'    TipoPrecio.AddItem "Precio 3"
    TipoPrecio.AddItem Stellar_Mensaje(10230)
    TipoPrecio.AddItem Stellar_Mensaje(10175)
    TipoPrecio.AddItem Stellar_Mensaje(10176)
    TipoPrecio.AddItem Stellar_Mensaje(10177)
    TipoPrecio.ListIndex = 0
    
'    CampoBusqueda.AddItem "Codigo"
'    CampoBusqueda.AddItem "Rif"
'    CampoBusqueda.AddItem "Nit"
'    CampoBusqueda.AddItem "Descripci�n"
    CampoBusqueda.AddItem Stellar_Mensaje(10096)
    CampoBusqueda.AddItem Stellar_Mensaje(10231)
    CampoBusqueda.AddItem Stellar_Mensaje(10232)
    CampoBusqueda.AddItem Stellar_Mensaje(143)
    
    CampoBusqueda.ListIndex = 0
    
    'TipoPuerto.AddItem "Por Nombre"
    'TipoPuerto.AddItem "Por Puerto"
    TipoPuerto.AddItem Stellar_Mensaje(10131)
    TipoPuerto.AddItem Stellar_Mensaje(10132)
    TipoPuerto.ListIndex = 0
    
    If Not rsCaja.EOF Then
        Call Llenar_Caja
    End If
    
    Call Cerrar_Recordset(rsCaja)
    
    descri.SelStart = 0
    descri.SelLength = Len(descri.Text)
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF4
            Call Grabar_Caja
            Unload Me
            
        Case vbKeyF12, vbKeyF7
            Unload Me
    End Select
End Sub

Private Sub Llenar_Caja()
    
    codigo.Text = rsCaja!c_codigo 'Format(rsCaja!c_codigo, "000")
    descri.Text = rsCaja!c_Desc_Caja
    
    dato_numerico.Value = IIf(rsCaja!Dato_String = True, 1, 0)
    
    opos.Value = IIf(rsCaja!b_Opos = True, 1, 0)
    cordenar.Value = IIf(rsCaja!Imp_Ordenada = True, 1, 0)
    cagrupar.Value = IIf(rsCaja!Imp_Agrupada = True, 1, 0)
    
    forma_continua.Value = IIf(rsCaja!forma_continua = True, 1, 0)
    no_lineas.Text = rsCaja!no_lineas
    
    'linea(0).Text = IIf(IsNull(RsCaja!MENSAJE1), "", RsCaja!MENSAJE1)
    'linea(1).Text = IIf(IsNull(RsCaja!MENSAJE2), "", RsCaja!MENSAJE2)
    'linea(2).Text = IIf(IsNull(RsCaja!MENSAJE3), "", RsCaja!MENSAJE3)
    'linea(3).Text = IIf(IsNull(RsCaja!MENSAJE4), "", RsCaja!MENSAJE4)
    'linea(4).Text = IIf(IsNull(RsCaja!MENSAJE5), "", RsCaja!MENSAJE5)
    
    fondo_pos.Value = IIf(rsCaja!b_Solicitar_Fondo = True, 1, 0)
    TxtMoneda.Text = IIf(IsNull(rsCaja!c_CodMoneda), "", rsCaja!c_CodMoneda)
    Observaciones.Text = IIf(IsNull(rsCaja!Observaciones), "", rsCaja!Observaciones)
    TxtActualizar.Text = CInt(rsCaja!n_Transacciones)
    Actualizar.Value = CInt(rsCaja!n_Transacciones)
    TxtConsumo.Text = CInt(rsCaja!nu_Maximo_Consumo_imprimir)
    Consumo.Value = CInt(rsCaja!nu_Maximo_Consumo_imprimir)
    coddeposito.Text = IIf(IsNull(rsCaja!c_CodDeposito), "", rsCaja!c_CodDeposito)
    Me.chk_AdminClientes.Value = IIf(rsCaja!bu_Administra_Clientes, 1, 0)
    Autodeclaracion.Value = IIf(rsCaja!bu_AutoDeclaracion, 1, 0)
    Me.TipoPrecio.ListIndex = rsCaja!nu_Tipo_Precio
    Me.CampoBusqueda.ListIndex = rsCaja!nu_Campo_Busqueda
    'Me.chk_shell.Value = IIf(RsCaja!bu_esshell = 1, 1, 0)
    
    EsDigital = IIf(rsCaja!B_POS_Digital = False, 0, 1)
    EsPedidos = IIf(rsCaja!b_POS_Pedido = False, 0, 1)
    
    If EsDigital And Not EsPedidos Then
        Me.Caption = Me.Caption & " POS FOOD"
    ElseIf EsDigital And EsPedidos Then
        'Me.Caption = Me.Caption & " TERMINAL DE PEDIDOS"
        Me.lbl_Organizacion.Caption = Stellar_Mensaje(10178, True)
    Else
        'Me.Caption = Me.Caption & " POS RETAIL"
        Me.lbl_Organizacion.Caption = Stellar_Mensaje(10179, True)
    End If
    
    Call Buscar_Moneda
    Call Buscar_Deposito
    
    TxtUbicacion.Text = rsCaja!cu_Impresora_Facturar
    Me.TipoPuerto.ListIndex = IIf(rsCaja!bu_Por_Puerto, 1, 0)
    Me.RComandas.Value = IIf(rsCaja!bu_Comanda_Remota, 1, 0)
    Me.RLlevar.Value = IIf(rsCaja!bu_Llevar_Remota, 1, 0)
    Me.LComandas.Value = IIf(rsCaja!bu_Comanda_Local, 1, 0)
    Me.LLLevar.Value = IIf(rsCaja!bu_Llevar_Local, 1, 0)
    
End Sub

Private Sub Buscar_Deposito()
    
    Call Apertura_Recordset(False, rsEureka)
    
    rsEureka.Open "SELECT * FROM MA_DEPOSITO WHERE c_CodDeposito= '" & coddeposito.Text & "' and c_CodLocalidad = '" & Sucursal & "'", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEureka.EOF Then
        lbl_deposito.Text = rsEureka!c_Descripcion
    Else
        'Call mensaje(True, "El c�digo del dep�sito no existe.")
        'Call Mensaje(True, stellar_mensaje(10180, True))
        lbl_deposito.Text = Stellar_Mensaje(10233)
    End If
    
    Call Cerrar_Recordset(rsEureka)

End Sub

Private Sub Grabar_Caja()
    
    Dim RsBotonesConfig As New ADODB.Recordset, rsBotones As New ADODB.Recordset
    
    Dim EraDigital As Boolean
    
    Ent.POS.BeginTrans
    
        Call Apertura_Recordset(False, rsCaja)
        
        rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
        Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not rsCaja.EOF Then
            
            EraDigital = rsCaja!B_POS_Digital
            EsPedidos = rsCaja!b_POS_Pedido
            
            rsCaja!nCierre = 1
            
            Clave = "|C|" & Format(codigo.Text, "000")
            
            rsCaja!B_POS_Digital = EsDigital
            rsCaja!b_POS_Pedido = EsPedidos
            rsCaja!b_Opos = opos.Value
            rsCaja!c_Desc_Caja = UCase(descri.Text)
            rsCaja!Imp_Ordenada = IIf(cordenar.Value = 1, 1, 0)
            rsCaja!Imp_Agrupada = IIf(cagrupar.Value = 1, 1, 0)
            rsCaja!Dato_String = dato_numerico.Value = 1
            rsCaja!c_CodMoneda = TxtMoneda.Text
            rsCaja!c_CodDeposito = coddeposito.Text
            rsCaja!forma_continua = IIf(forma_continua.Value = 1, 1, 0)
            rsCaja!no_lineas = Int(no_lineas.Text)
            rsCaja!Observaciones = IIf(IsNull(Observaciones.Text), "", Observaciones.Text)
            rsCaja!b_Solicitar_Fondo = IIf(fondo_pos.Value = 1, 1, 0)
            rsCaja!n_Transacciones = Actualizar.Value
            rsCaja!nu_Maximo_Consumo_imprimir = Consumo.Value

            'RsCaja!MENSAJE1 = IIf(IsNull(linea(0).Text), "", linea(0).Text)
            'RsCaja!MENSAJE2 = IIf(IsNull(linea(1).Text), "", linea(1).Text)
            'RsCaja!MENSAJE3 = IIf(IsNull(linea(2).Text), "", linea(2).Text)
            'RsCaja!MENSAJE4 = IIf(IsNull(linea(3).Text), "", linea(3).Text)
            'RsCaja!MENSAJE5 = IIf(IsNull(linea(4).Text), "", linea(4).Text)

            rsCaja!bu_Administra_Clientes = (Me.chk_AdminClientes.Value = 1)
            rsCaja!bu_AutoDeclaracion = (Autodeclaracion.Value = 1)
            rsCaja!nu_Tipo_Precio = Me.TipoPrecio.ListIndex
            rsCaja!nu_Campo_Busqueda = Me.CampoBusqueda.ListIndex
            'RsCaja!bu_esshell = IIf(Me.chk_shell.Value = 1, 1, 0)
            
            rsCaja!bu_Comanda_Remota = (Me.RComandas.Value = 1)
            rsCaja!bu_Llevar_Remota = (Me.RLlevar.Value = 1)
            rsCaja!bu_Comanda_Local = (Me.LComandas.Value = 1)
            rsCaja!bu_Llevar_Local = (Me.LLLevar.Value = 1)
            rsCaja!bu_Por_Puerto = (Me.TipoPuerto.ListIndex = 1)
            rsCaja!cu_Impresora_Facturar = Trim(TxtUbicacion.Text)
            
            rsCaja.UpdateBatch
            
            ' Chequear Botones
            
            RsBotonesConfig.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "'", _
            Ent.POS, adOpenForwardOnly, adLockPessimistic, adCmdText
            
            If RsBotonesConfig.EOF Then
            
                rsBotones.Open "SELECT * FROM MA_CAJA_BOTONES WHERE TipoPOS = " & IIf(EsDigital And EsPedidos, 2, 1), _
                Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
            
                While Not rsBotones.EOF
                    RsBotonesConfig.AddNew
                    RsBotonesConfig!Caja = nCaja
                    RsBotonesConfig!Posicion = rsBotones!Posicion
                    RsBotonesConfig!Nombre_Boton = rsBotones!Nombre
                    RsBotonesConfig!Tecla = rsBotones!Tecla
                    RsBotonesConfig!Shift = rsBotones!Shift
                    RsBotonesConfig!Activo = True
                    RsBotonesConfig!Llave = False
                    RsBotonesConfig!BLLAVE = rsBotones!BLLAVE
                    RsBotonesConfig!BNIVEL = rsBotones!BNIVEL
                    RsBotonesConfig!bActivo = rsBotones!bActivo
                    RsBotonesConfig.UpdateBatch
                    rsBotones.MoveNext
                Wend
                
                RsBotonesConfig.Close
                rsBotones.Close
            
            End If
            
        End If
            
        'If glgrabar = False Then
            'ARBOL.Nodes.Item(lcindex).Text = UCase(descri.Text)
        'Else
            'Call crear_menu(lckey, clave, UCase(descri.Text), "caja")
        'End If

    Ent.POS.CommitTrans
    
    Call Cerrar_Recordset(rsCaja)
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 Then
        Unload Me
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set general = Nothing
End Sub

Private Sub forma_continua_Click()
    
    If forma_continua.Value = 1 Then
        no_lineas.Enabled = True
        uplineas.Enabled = True
    Else
        no_lineas.Enabled = False
        uplineas.Enabled = False
    End If
    
'    Label57.Caption = IIf(forma_continua.Value = 0, "No", "Si")

End Sub

Private Sub lineal_digital_Click()
    'lineal_digital.Caption = IIf(lineal_digital.Value = 0, "No", "Si")
End Sub

Private Sub opos_Click()
    'opos.Caption = IIf(opos.Value = 0, "No", "Si")
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF12, 0)
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
        Case Is = "AYUDA"
            SendKeys "{F1}"
        Case Is = UCase("Teclado")
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = descri
            
            TecladoWindows mCtl
            
    End Select
End Sub

Private Sub TxtMoneda_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        Tecla_pulsada = True
        'Call Make_View("ma_monedas", "c_codmoneda", "c_descripcion", "M O N E D A S", Me, "GENERICO")
        Call Make_View("ma_monedas", "c_codmoneda", "c_descripcion", Stellar_Mensaje(10051, True), Me, "GENERICO")
        Tecla_pulsada = False
    End If
End Sub

Private Sub Buscar_Moneda()

    Call Apertura_Recordset(False, rsEureka)
    
    rsEureka.Open "SELECT * FROM MA_MONEDAS WHERE c_CodMoneda= '" & TxtMoneda.Text & "'", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEureka.EOF Then
        Lbl_Moneda.Text = rsEureka!c_Descripcion
    Else
        'Call Mensaje(True, "El c�digo de la Moneda no existe.")
        Lbl_Moneda.Text = Stellar_Mensaje(10234)
    End If
    
    Call Cerrar_Recordset(rsEureka)

End Sub

Private Sub view_Click()
    coddeposito.SetFocus
    Call coddeposito_KeyDown(vbKeyF2, 0)
End Sub

Private Sub cmd_imprimir_Click()
    
    Dim PTest As Boolean, DefPrinter As String
    
    On Error GoTo Falla_Local
    
    Impresoras.FileName = ""
    Impresoras.Flags = cdlPDDisablePrintToFile Or cdlPDNoPageNums
    Impresoras.ShowPrinter
    
    If TipoPuerto.ListIndex = 1 Then
        DefPrinter = Printer.Port
    Else
        DefPrinter = Printer.DeviceName
    End If
    
    If DefPrinter <> "" Then
        'Mensajes.PressBoton = True
        'PTest = Mensajes.Mensaje(stellar_mensaje(10235), True)
        'If Mensajes.PressBoton Then
        
        Mensaje True, StellarMensaje(10235), True
        
        If Retorno Then
            Printer.Print Replace(Replace(StellarMensaje(10262), "$(Line)", vbNewLine), "$(Tab)", vbTab) '"Esta es una prueba del Stellar isFood." & vbNewLine & vbTab & "Esta es una prueba que la conexi�n esta bien." & vbNewLine & vbNewLine & vbTab & "Stellar isFood, la soluci�n acertiva."
            Printer.EndDoc
        End If
        
        TxtUbicacion.Text = DefPrinter
    End If
    
    Exit Sub
    
Falla_Local:

    'Call Mensajes.Mensaje(stellar_mensaje(10236), False)
    Mensaje True, StellarMensaje(10236)
    TxtUbicacion.Text = ""
    
End Sub

Private Sub fondo_pos_Click()
    'fondo_pos.Caption = IIf(fondo_pos.Value = 0, "No", "Si")
End Sub
