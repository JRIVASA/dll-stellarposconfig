VERSION 5.00
Begin VB.Form FrmDatosVendedor 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3480
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   5385
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   3480
   ScaleWidth      =   5385
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton CmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   3960
      Picture         =   "FrmDatosVendedor.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2160
      Width           =   1185
   End
   Begin VB.CommandButton Cmd_Aceptar 
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   2520
      Picture         =   "FrmDatosVendedor.frx":1D82
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2160
      Width           =   1185
   End
   Begin VB.CheckBox AmbitoGlobal 
      Appearance      =   0  'Flat
      Caption         =   "Maneja cualquier cuenta en el �mbito"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   975
      Left            =   360
      TabIndex        =   0
      Top             =   850
      Width           =   4575
   End
   Begin VB.Label lblVendedoresZona 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ambito Global"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   345
      Left            =   1680
      TabIndex        =   1
      Top             =   240
      Width           =   1740
   End
End
Attribute VB_Name = "FrmDatosVendedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public OpcAmbitoGlobal As Boolean

Private Sub AmbitoGlobal_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then cmd_aceptar.SetFocus
End Sub

Private Sub Cmd_Aceptar_Click()
    OpcAmbitoGlobal = CBool(AmbitoGlobal.Value)
    Me.Hide
End Sub

Private Sub Cmd_Aceptar_KeyDown(KeyCode As Integer, Shift As Integer)
    If (Shift = 0 And KeyCode = vbKeyReturn) Or (Shift = vbAltMask And KeyCode = vbKeyA) Then
        Cmd_Aceptar_Click
    End If
End Sub

Private Sub CmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    AmbitoGlobal.SetFocus
End Sub

Private Sub Form_Load()
    AmbitoGlobal.Value = IIf(OpcAmbitoGlobal, vbChecked, vbUnchecked)
    Me.cmd_aceptar.Caption = stellar_mensaje(5, True)
    Me.CmdCancelar.Caption = stellar_mensaje(6, True)
    Me.AmbitoGlobal.Caption = stellar_mensaje(10139, True)
    Me.lblVendedoresZona = stellar_mensaje(10138, True)
End Sub
