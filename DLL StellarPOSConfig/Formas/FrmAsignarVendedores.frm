VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmAsignarVendedores 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5100
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   8955
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   8955
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6795
         TabIndex        =   10
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuraci�n de la Estructura de POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.CommandButton Remover 
      CausesValidation=   0   'False
      Height          =   555
      Left            =   4120
      Picture         =   "FrmAsignarVendedores.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Eliminar de la lista de seleccionados"
      Top             =   2280
      Width           =   720
   End
   Begin VB.CommandButton A�adir 
      CausesValidation=   0   'False
      Height          =   555
      Left            =   4120
      Picture         =   "FrmAsignarVendedores.frx":0802
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "A�adir a la lista de seleccionados"
      Top             =   1560
      Width           =   720
   End
   Begin VB.CommandButton cmd_Salir 
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   990
      Left            =   7395
      Picture         =   "FrmAsignarVendedores.frx":1004
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   3855
      Width           =   1305
   End
   Begin VB.CommandButton cmd_aceptar 
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   990
      Left            =   5820
      Picture         =   "FrmAsignarVendedores.frx":2D86
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   3855
      Width           =   1305
   End
   Begin MSComctlLib.ListView VendedoresAsignar 
      CausesValidation=   0   'False
      Height          =   2790
      Left            =   225
      TabIndex        =   2
      Top             =   885
      Width           =   3780
      _ExtentX        =   6668
      _ExtentY        =   4921
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      OLEDragMode     =   1
      OLEDropMode     =   1
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   5790296
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDragMode     =   1
      OLEDropMode     =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView VendedoresZona 
      CausesValidation=   0   'False
      Height          =   2790
      Left            =   4950
      TabIndex        =   3
      Top             =   885
      Width           =   3780
      _ExtentX        =   6668
      _ExtentY        =   4921
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      OLEDragMode     =   1
      OLEDropMode     =   1
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   5790296
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDragMode     =   1
      OLEDropMode     =   1
      NumItems        =   0
   End
   Begin VB.Label lblVendedoresZona 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Vendedores de la Zona"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   4830
      TabIndex        =   5
      Top             =   555
      Width           =   1980
   End
   Begin VB.Label lblVendedoresAsignar 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Vendedores no Asignados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   225
      TabIndex        =   4
      Top             =   555
      Width           =   2220
   End
End
Attribute VB_Name = "FrmAsignarVendedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Relacion As String
Private EvitarActivate As Boolean
Public ConexionTemp As ADODB.Connection
Public SucursalTemp As String

Private Sub IgnorarActivate()
    EvitarActivate = True
End Sub

Private Sub A�adir_Click()
    
    If Not VendedoresAsignar.SelectedItem Is Nothing Then
        
        Dim TmpItem As ListItem
        Dim SelectedItem As ListItem
        
        Set SelectedItem = VendedoresAsignar.SelectedItem
        
        Set TmpItem = VendedoresZona.ListItems.add(, , SelectedItem.Text)
        TmpItem.SubItems(1) = SelectedItem.SubItems(1)
        
        'Dim Resp
        'Resp = InputBox("Ingrese 1 si maneja �mbito global, sino, ingrese 0", "StellarPOSConfig")
        
        IgnorarActivate
        
        FrmDatosVendedor.OpcAmbitoGlobal = False
        FrmDatosVendedor.Show vbModal, Me
        
        TmpItem.SubItems(2) = IIf(FrmDatosVendedor.OpcAmbitoGlobal, "Si", "No") 'IIf(CBool(Val(Resp)), "Si", "No")
        
        Unload FrmDatosVendedor
        Set FrmDatosVendedor = Nothing
        
        TmpItem.SubItems(3) = "1"
        
        VendedoresAsignar.ListItems.Remove SelectedItem.Index
        
    End If
    
End Sub

Private Sub Cmd_Aceptar_Click()
    
    On Error GoTo ERR
    
    Dim rsVendedores As New ADODB.Recordset
    Dim SQL As String
    Dim TmpItem As ListItem
    
    For Each TmpItem In VendedoresZona.ListItems
    
        Apertura_Recordset False, rsVendedores
        SQL = "SELECT * FROM MA_VENDEDORES WHERE cu_Vendedor_Cod = '" & TmpItem.Text & "' AND cs_Localidad = '" & SucursalTemp & "' AND cs_Tipo = 'VEN'"
        
        rsVendedores.Open SQL, ConexionTemp, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not rsVendedores.EOF Then
            rsVendedores!cs_Relacion = Relacion
            rsVendedores!bu_Cualquier_Cuenta = IIf(UCase(TmpItem.SubItems(1)) = UCase("Si"), 1, 0)
            rsVendedores.UpdateBatch
        End If
        
    Next
    
    For Each TmpItem In VendedoresAsignar.ListItems
    
        Apertura_Recordset False, rsVendedores
        SQL = "SELECT * FROM MA_VENDEDORES WHERE cu_Vendedor_Cod = '" & TmpItem.Text & "' AND cs_Localidad = '" & SucursalTemp & "' AND cs_Tipo = 'VEN'"
        
        rsVendedores.Open SQL, ConexionTemp, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not rsVendedores.EOF Then
            rsVendedores!cs_Relacion = ""
            rsVendedores!bu_Cualquier_Cuenta = False
            rsVendedores.UpdateBatch
        End If
        
    Next
    
    Apertura_Recordset False, rsVendedores
    
    Unload Me
    
    Exit Sub
    
ERR:
    
    IgnorarActivate
    Mensaje True, "Ha ocurrido un error. Los cambios no se han guardado"
    
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    If Not EvitarActivate Then
        ConfigReportView
    End If
End Sub

Private Sub Form_Load()
    lblVendedoresAsignar.Caption = Stellar_Mensaje(10136, True) ' Meseros no asignados
    lblVendedoresZona.Caption = Stellar_Mensaje(10135, True) ' Meseros de la Zona
    Me.lbl_Organizacion.Caption = Stellar_Mensaje(10137, True) ' Asignar Meseros
    cmd_aceptar.Caption = Stellar_Mensaje(5, True)
    cmd_Salir.Caption = Stellar_Mensaje(6, True)
End Sub

Private Sub ConfigReportView()

    Dim rsVendedores As New ADODB.Recordset
    Dim SQL As String
    Dim TmpItem As ListItem
    
    VendedoresAsignar.ListItems.Clear
    VendedoresAsignar.ColumnHeaders.Clear
    
    VendedoresAsignar.ColumnHeaders.add 1, , "CODIGO", 0
    VendedoresAsignar.ColumnHeaders.add 2, , "VENDEDOR", 4500
    
    VendedoresAsignar.HideColumnHeaders = True
    
    SQL = "SELECT * FROM MA_VENDEDORES WHERE cs_Relacion = '' AND cs_Localidad = '" & SucursalTemp & "' AND cs_Tipo = 'VEN' ORDER BY cu_Vendedor_Des"
    
    rsVendedores.Open SQL, ConexionTemp, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not rsVendedores.EOF
        Set TmpItem = VendedoresAsignar.ListItems.add(, , rsVendedores!cu_Vendedor_Cod)
        TmpItem.SubItems(1) = rsVendedores!cu_Vendedor_Des
        rsVendedores.MoveNext
    Wend
    
    rsVendedores.Close
    
    VendedoresZona.ListItems.Clear
    VendedoresZona.ColumnHeaders.Clear
    
    VendedoresZona.ColumnHeaders.add 1, , "CODIGO", 0
    VendedoresZona.ColumnHeaders.add 2, , "VENDEDOR", 4500
    VendedoresZona.ColumnHeaders.add 3, , "AMBITO GLOBAL", 0
    VendedoresZona.ColumnHeaders.add 4, , "Cambio", 0
    
    VendedoresZona.HideColumnHeaders = True
    
    SQL = "SELECT * FROM MA_VENDEDORES WHERE cs_Relacion = '" & Relacion & "' AND cs_Localidad = '" & SucursalTemp & "' AND cs_Tipo = 'VEN' ORDER BY cu_Vendedor_Des"
    
    rsVendedores.Open SQL, ConexionTemp, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not rsVendedores.EOF
        Set TmpItem = VendedoresZona.ListItems.add(, , rsVendedores!cu_Vendedor_Cod)
        TmpItem.SubItems(1) = rsVendedores!cu_Vendedor_Des
        TmpItem.SubItems(2) = IIf(rsVendedores!bu_Cualquier_Cuenta, "Si", "No")
        TmpItem.SubItems(3) = "0"
        rsVendedores.MoveNext
    Wend
    
    rsVendedores.Close
    
End Sub

Private Sub Remover_Click()
    If Not VendedoresZona.SelectedItem Is Nothing Then
        
        Dim TmpItem As ListItem
        Dim SelectedItem As ListItem
        
        Set SelectedItem = VendedoresZona.SelectedItem
        
        Set TmpItem = VendedoresAsignar.ListItems.add(, , SelectedItem.Text)
        TmpItem.SubItems(1) = SelectedItem.SubItems(1)
        
        VendedoresZona.ListItems.Remove SelectedItem.Index
        
    End If
End Sub

Private Sub VendedoresAsignar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        A�adir_Click
    End If
End Sub

Private Sub VendedoresZona_DblClick()
    If Not VendedoresZona.SelectedItem Is Nothing Then
        
        Dim SelectedItem As ListItem
        
        Set SelectedItem = VendedoresZona.SelectedItem
        
        IgnorarActivate
        
        FrmDatosVendedor.OpcAmbitoGlobal = (UCase(SelectedItem.SubItems(2)) = UCase("Si"))
        FrmDatosVendedor.Show vbModal, Me
        
        SelectedItem.SubItems(2) = IIf(FrmDatosVendedor.OpcAmbitoGlobal, "Si", "No") 'IIf(CBool(Val(Resp)), "Si", "No")
        
        Unload FrmDatosVendedor
        Set FrmDatosVendedor = Nothing
    
    End If
End Sub

Private Sub VendedoresZona_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Remover_Click
    End If
End Sub
