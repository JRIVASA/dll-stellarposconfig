VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Newcaja 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3900
   ClientLeft      =   120
   ClientTop       =   -195
   ClientWidth     =   8895
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "newcaja.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   3900
   ScaleWidth      =   8895
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   1695
      Left            =   240
      TabIndex        =   6
      Top             =   1920
      Width           =   8295
      Begin VB.TextBox txt_descripcion 
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2100
         TabIndex        =   8
         Top             =   990
         Width           =   5025
      End
      Begin VB.TextBox txt_caja 
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2100
         MaxLength       =   3
         TabIndex        =   7
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Index           =   3
         Left            =   420
         TabIndex        =   10
         Top             =   990
         Width           =   1305
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Caja N�"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Index           =   0
         Left            =   420
         TabIndex        =   9
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6555
         TabIndex        =   5
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Creando una Caja"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   -120
      ScaleHeight     =   1185
      ScaleWidth      =   9105
      TabIndex        =   0
      Top             =   421
      Width           =   9135
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   8500
         Begin MSComctlLib.Toolbar barra_menu 
            Height          =   810
            Left            =   420
            TabIndex        =   2
            Top             =   180
            Width           =   8000
            _ExtentX        =   14102
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "image_menu_prin(0)"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "F7 Borra el campo Descripci�n"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "F4 Graba la informaci�n"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "salir"
                  Object.ToolTipText     =   "F12 Salir "
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "F1 Ayuda del Sistema"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList image_menu_prin 
      Index           =   0
      Left            =   3960
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newcaja.frx":628A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newcaja.frx":801C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newcaja.frx":9DAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newcaja.frx":BB40
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newcaja.frx":D8D2
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
   Begin VB.Menu menu 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu eliminar 
         Caption         =   "Eliminar"
      End
      Begin VB.Menu cambiar 
         Caption         =   "Cambiar Nombre"
      End
   End
End
Attribute VB_Name = "Newcaja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Quien As String
Public ObjArrastre As Object

Dim FormaCargada As Boolean

Private Sub barra_menu_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        
        Case Is = "grabar" ''boton salvar
            Call Grabar
            
        Case Is = "cancelar"
            Call Form_KeyDown(vbKeyF7, 0)
        
        Case Is = "salir"
            AddNode = False
            Unload Me
            
        Case Is = "Teclado"
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = txt_caja
            
            TecladoWindows mCtl
            
    End Select
End Sub

Private Sub Grabar()
    
    Dim rsBotones As New ADODB.Recordset, RsBotonesConfig As New ADODB.Recordset
    
    If txt_descripcion.Enabled Then
        
        Dim CantCajas As Double
        
        CantCajas = Round(Val(Principal.BuscarValorBD("Cant", "SELECT isNULL(COUNT(*), 0) AS Cant " & _
        "FROM MA_CAJA", "0", Ent.POS)), 0)
        
        Call Apertura_Recordset(True, rsCaja)
        
        rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & txt_caja & "'", _
        Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not rsCaja.EOF Then
            'Call mensaje(True, "No es posible crear la caja porque ya existe en el sistema.")
            Call Mensaje(True, Stellar_Mensaje(10128, True))
            Call Cerrar_Recordset(rsCaja)
            Exit Sub
        Else
            
            If IsNumeric(gLC_NumPOS) Then
                If (CantCajas + 1) > Val(gLC_NumPOS) Then
                    'Mensaje True, "Su licencia actual no le permite agregar mas puntos de venta. En caso tal de que requiera poder agregar mas por favor contacte al departamento de administraci�n y / o finanzas."
                    Mensaje True, Replace(StellarMensaje(10362), "$(NumPOS)", gLC_NumPOS)
                    Call Cerrar_Recordset(rsCaja)
                    Exit Sub
                End If
            End If
            
            Call Apertura_Recordset(True, rsBingo)
            
            'NCAJA = "001"
            'rsbingo.Open "select * from ma_caja where c_codigo = '" & NCAJA & "'", Ent.Pos, adOpenForwardOnly, adLockReadOnly, adCmdText
            'If Not rsbingo.EOF Then
            
            nCaja = Trim(txt_caja.Text)
                
                rsCaja.AddNew
                    
                    rsCaja!c_codigo = Trim(txt_caja.Text)
                    rsCaja!c_Desc_Caja = Trim(txt_descripcion)
                    rsCaja!c_CodLocalidad = Sucursal
                    rsCaja!B_POS_Digital = EsDigital
                    rsCaja!b_POS_Pedido = EsPedidos
                    
                    Copiar = True
                    
                    If Copiar Then
                        'rscaja!c_relacion = frm_config_pos1.ARBOL.SelectedItem.Parent.Key
                        rsCaja!c_Relacion = Mid(frm_Config_POS1.ARBOL.SelectedItem.Key, _
                        LPA + 1, Len(frm_Config_POS1.ARBOL.SelectedItem.Key))
                        For i = 3 To rsBingo.Fields.Count - 1
                            Debug.Print i & " "; rsCaja.Fields(i).Name
                            If i = 95 Or (i >= 99 And i <= 106) Or (i >= 108 And i <= 110) Then
                                rsCaja.Fields(i).Value = Trim(rsBingo.Fields(i).Value)
                            ElseIf i = 94 Then
                                rsCaja.Fields(i).Value = rsBingo.Fields(i).Value
                            Else
                                rsCaja.Fields(i).Value = rsBingo.Fields(i).Value
                            End If
                        Next
                    Else
                        rsCaja!c_Relacion = Mid(lcKey, LPA + 1, Len(lcKey))
                    End If
                    
                    If EsDigital And EsPedidos Then
                        rsCaja!bu_Comanda_Remota = True
                        rsCaja!bu_Llevar_Remota = True
                        rsCaja!bu_Comanda_Local = False
                        rsCaja!bu_Llevar_Local = False
                        rsCaja!bu_Por_Puerto = False
                        rsCaja!cu_Impresora_Facturar = vbNullString
                    ElseIf EsDigital Then
                        rsCaja!bu_Por_Puerto = False
                    Else
                        
                        rsCaja!bAyuda = 1
                        rsCaja!nAyuda = 1
                        rsCaja!lAyuda = 0
                        
                        rsCaja!bProductos = 1
                        rsCaja!nProductos = 1
                        rsCaja!lProductos = 0
                        
                        rsCaja!bLimite = 1
                        rsCaja!nLimite = 1
                        rsCaja!lLimite = 0
                        
                        rsCaja!bCantidad = 1
                        rsCaja!nCantidad = 1
                        rsCaja!lCantidad = 0
                        
                        rsCaja!bReintegro = 1
                        rsCaja!nReintegro = 1
                        rsCaja!lReintegro = 0
                        
                        rsCaja!bParcial = 1
                        rsCaja!nParcial = 1
                        rsCaja!lParcial = 0
                        
                        rsCaja!bEspera = 1
                        rsCaja!nEspera = 1
                        rsCaja!lEspera = 0
                        
                        rsCaja!bSuspender = 1
                        rsCaja!nSuspender = 1
                        rsCaja!lSuspender = 0
                        
                        rsCaja!bDevolucion = 1
                        rsCaja!nDevolucion = 1
                        rsCaja!lDevolucion = 0
                        
                        rsCaja!bPrecios = 1
                        rsCaja!nPrecios = 1
                        rsCaja!lPrecios = 0
                        
                        rsCaja!bCliente = 1
                        rsCaja!nCliente = 1
                        rsCaja!lCliente = 0
                        
                        rsCaja!nOpenPOS = 1
                        rsCaja!nClosePOS = 1
                        rsCaja!lCerrar = 0
                        
                        rsCaja!bBalanza = 1
                        rsCaja!nBalanza = 1
                        rsCaja!lBalanza = 0
                        
                    End If
                    
                rsCaja.UpdateBatch
                
                Call Cerrar_Recordset(rsCaja)
                Call Cerrar_Recordset(rsBingo)
                
                lcTexto = txt_descripcion
                Clave = "|C|" & Trim(txt_caja)
                AddNode = True
                
                If EsDigital Then
                    Call Apertura_Recordset(False, rsBotones)
                    
                    If EsPedidos Then
                        rsBotones.Open "SELECT * FROM MA_CAJA_BOTONES WHERE TipoPOS = 2", _
                        Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
                    Else
                        rsBotones.Open "SELECT * FROM MA_CAJA_BOTONES WHERE TipoPOS = 1", _
                        Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
                    End If
                Else
                    Call Apertura_Recordset(False, rsBotones)
                    rsBotones.Open "SELECT * FROM MA_CAJA_BOTONES WHERE TipoPOS = 0", _
                    Ent.POS, adOpenForwardOnly, adLockOptimistic, adCmdText
                End If
                
                If Not rsBotones.EOF Then
                
                    Call Apertura_Recordset(False, RsBotonesConfig)
                    
                    RsBotonesConfig.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "'", _
                    Ent.POS, adOpenForwardOnly, adLockPessimistic, adCmdText
                    
                    While Not rsBotones.EOF
                        RsBotonesConfig.AddNew
                        RsBotonesConfig!Caja = nCaja
                        RsBotonesConfig!Posicion = rsBotones!Posicion
                        RsBotonesConfig!Nombre_Boton = rsBotones!Nombre
                        RsBotonesConfig!Tecla = rsBotones!Tecla
                        RsBotonesConfig!Shift = rsBotones!Shift
                        
                        RsBotonesConfig!Llave = False
                        RsBotonesConfig!BLLAVE = rsBotones!BLLAVE
                        RsBotonesConfig!BNIVEL = rsBotones!BNIVEL
                        RsBotonesConfig!bActivo = rsBotones!bActivo
                        RsBotonesConfig!Activo = rsBotones!bDefault
                        RsBotonesConfig!ResourceID = rsBotones!ResourceID
                        RsBotonesConfig!Nivel = 1
                        RsBotonesConfig.UpdateBatch
                        rsBotones.MoveNext
                    Wend
                    
                    RsBotonesConfig.Close
                    
                End If
                
                rsBotones.Close
                
                Unload Me
            'Else
            '    Call cerrar_recordset(rsbingo)
            '    Call cerrar_recordset(rscaja)
            '    Call mensaje(True, "No se pudo realizar la copia por que la caja ya no existe en el sistema...!")
            '    addnode = False
            '    Unload Me
            'End If
        End If
    Else
        'Call mensaje(True, "No es posible crear la caja.")
        Call Mensaje(True, Stellar_Mensaje(10129, True))
    End If
    
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        If PuedeObtenerFoco(txt_caja) Then txt_caja.SetFocus
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF7
            txt_caja = ""
            txt_descripcion = ""
            txt_descripcion.Enabled = False
            txt_caja.SetFocus
            
        Case Is = vbKeyF4
            Call Grabar
            
        Case Is = vbKeyF12
            AddNode = False
            Unload Me
    End Select
End Sub

Private Sub Form_Load()
    
    Me.barra_menu.Buttons(1).Caption = Stellar_Mensaje(6, True) 'cancelar
    Me.barra_menu.Buttons(2).Caption = Stellar_Mensaje(103, True) 'grabar
    Me.barra_menu.Buttons(4).Caption = Stellar_Mensaje(54, True) 'salir
    Me.barra_menu.Buttons(5).Caption = Stellar_Mensaje(7, True) 'ayuda
    
    barra_menu.Buttons("Teclado").Caption = Stellar_Mensaje(10080, True) ' Teclado
    
    Me.Label1(3).Caption = Stellar_Mensaje(143, True) 'descripcion
    Me.Label1(0).Caption = Stellar_Mensaje(2017, True) 'caja n�
    
    If Copiar Then
        'Me.Caption = "Creando una copia de la Caja N� " & nCaja & "..."
        Me.lbl_Organizacion.Caption = Stellar_Mensaje(10127, True)
    Else
        Me.lbl_Organizacion.Caption = Stellar_Mensaje(10126, True)
        'Me.Caption = "Creando una Caja..."
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    FormaCargada = False
    Set NewDpto = Nothing
End Sub

Private Sub txt_caja_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call Apertura_Recordset(True, rsCaja)
        
        rsCaja.Open "SELECT c_Desc_Caja FROM MA_CAJA WHERE c_Codigo= '" & txt_caja & "'", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not rsCaja.EOF Then
            txt_descripcion.Text = ""
            txt_descripcion.Enabled = False
        Else
            txt_descripcion.Enabled = True
            txt_descripcion.SetFocus
        End If
    End If
End Sub
