VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmDispositivos 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8355
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   10275
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmDispositivos.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8355
   ScaleWidth      =   10275
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   10425
      TabIndex        =   33
      Top             =   421
      Width           =   10455
      Begin VB.Frame Frame2 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1080
         Left            =   240
         TabIndex        =   34
         Top             =   0
         Width           =   9500
         Begin MSComctlLib.Toolbar tlb_Config 
            Height          =   810
            Left            =   75
            TabIndex        =   35
            Top             =   120
            Width           =   9250
            _ExtentX        =   16325
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            ToolTips        =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "Cancelar la configuración actual [F7]"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "Grabar esta configuración [F4]"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "salir"
                  Object.ToolTipText     =   "Salir del configurador [F12]"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "Ayuda del Configurador [F1]"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configurador de Dispositivos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7995
         TabIndex        =   31
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   6495
      Left            =   150
      TabIndex        =   8
      Top             =   1680
      Width           =   9870
      Begin MSComctlLib.ListView LDispositivos 
         Height          =   5550
         Left            =   400
         TabIndex        =   0
         Top             =   550
         Width           =   3585
         _ExtentX        =   6324
         _ExtentY        =   9790
         View            =   2
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FlatScrollBar   =   -1  'True
         _Version        =   393217
         ForeColor       =   5790296
         BackColor       =   16777215
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   0
      End
      Begin VB.Frame FrmDisp 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Caption         =   "  Características   "
         ClipControls    =   0   'False
         ForeColor       =   &H80000002&
         Height          =   5850
         Left            =   4290
         TabIndex        =   10
         Top             =   420
         Visible         =   0   'False
         Width           =   5385
         Begin VB.CheckBox ChkPredeterminada 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            Caption         =   "Utilizar Predeterminada"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   435
            TabIndex        =   45
            Top             =   240
            Width           =   4000
         End
         Begin VB.Frame FrameGavetaxImpresora 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   0  'None
            ClipControls    =   0   'False
            ForeColor       =   &H80000008&
            Height          =   600
            Left            =   0
            TabIndex        =   42
            Top             =   5160
            Visible         =   0   'False
            Width           =   5145
            Begin VB.CheckBox ChkGavetaxImpresora 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00E0E0E0&
               Caption         =   "Abrir Gaveta"
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   240
               Left            =   360
               TabIndex        =   43
               Top             =   360
               Width           =   4000
            End
            Begin VB.Label lblGavetaxImpresora 
               BackStyle       =   0  'Transparent
               Caption         =   " Gaveta Conectada a la Impresora"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00AE5B00&
               Height          =   255
               Left            =   120
               TabIndex        =   44
               Top             =   0
               Width           =   3500
            End
            Begin VB.Line LnGavetaxImpresora 
               BorderColor     =   &H00AE5B00&
               X1              =   3700
               X2              =   5060
               Y1              =   120
               Y2              =   120
            End
         End
         Begin VB.Frame FrmEspeciales 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   0  'None
            Caption         =   "  Características Especiales  "
            ClipControls    =   0   'False
            ForeColor       =   &H80000008&
            Height          =   1450
            Left            =   -30
            TabIndex        =   18
            Top             =   3735
            Width           =   5145
            Begin VB.CheckBox ChkCortarPapel 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00E0E0E0&
               Caption         =   "Cortar Papel al Finalizar Impresión"
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   240
               Left            =   360
               TabIndex        =   41
               Top             =   360
               Width           =   4000
            End
            Begin VB.TextBox IControl 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   360
               Left            =   3780
               MaxLength       =   60
               TabIndex        =   25
               Top             =   150
               Visible         =   0   'False
               Width           =   2220
            End
            Begin VB.ComboBox Rotar 
               Appearance      =   0  'Flat
               CausesValidation=   0   'False
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   360
               ItemData        =   "FrmDispositivos.frx":628A
               Left            =   2580
               List            =   "FrmDispositivos.frx":629A
               Style           =   2  'Dropdown List
               TabIndex        =   22
               Top             =   975
               Width           =   1005
            End
            Begin VB.CheckBox ChkCheques 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00E0E0E0&
               Caption         =   "Imprime Cheques"
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   240
               Left            =   375
               TabIndex        =   21
               Top             =   720
               Width           =   4000
            End
            Begin VB.TextBox iCaracterCorte 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               CausesValidation=   0   'False
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   360
               Left            =   3780
               MaxLength       =   255
               TabIndex        =   19
               Top             =   435
               Visible         =   0   'False
               Width           =   2220
            End
            Begin VB.Line Line4 
               BorderColor     =   &H00AE5B00&
               X1              =   2640
               X2              =   5060
               Y1              =   120
               Y2              =   120
            End
            Begin VB.Label Label7 
               BackStyle       =   0  'Transparent
               Caption         =   " Características Especiales  "
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00AE5B00&
               Height          =   255
               Left            =   120
               TabIndex        =   39
               Top             =   0
               Width           =   2775
            End
            Begin VB.Label TxtCtrl 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Caracter de Gaveta"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   240
               Left            =   3150
               TabIndex        =   26
               Top             =   180
               Visible         =   0   'False
               Width           =   1650
            End
            Begin VB.Label Label65 
               BackStyle       =   0  'Transparent
               Caption         =   "Grados"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   315
               Index           =   3
               Left            =   3960
               TabIndex        =   24
               Top             =   1035
               Width           =   885
            End
            Begin VB.Label Label65 
               BackStyle       =   0  'Transparent
               Caption         =   "Rotación"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   195
               Index           =   2
               Left            =   405
               TabIndex        =   23
               Top             =   1035
               Width           =   735
            End
            Begin VB.Label lblCortarPapel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Caracter de Corte"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   240
               Left            =   3150
               TabIndex        =   20
               Top             =   495
               Visible         =   0   'False
               Width           =   1530
            End
         End
         Begin VB.TextBox NOPOS 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   2565
            MaxLength       =   60
            TabIndex        =   7
            Top             =   3165
            Width           =   2220
         End
         Begin VB.TextBox IComando 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   2565
            MaxLength       =   255
            TabIndex        =   6
            Top             =   2745
            Width           =   2220
         End
         Begin VB.ComboBox IParada 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "FrmDispositivos.frx":62AF
            Left            =   2565
            List            =   "FrmDispositivos.frx":62C2
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   2295
            Width           =   2220
         End
         Begin VB.ComboBox IDato 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "FrmDispositivos.frx":62D8
            Left            =   2565
            List            =   "FrmDispositivos.frx":62EB
            Style           =   2  'Dropdown List
            TabIndex        =   4
            Top             =   1845
            Width           =   2220
         End
         Begin VB.ComboBox IParidad 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "FrmDispositivos.frx":62FE
            Left            =   2565
            List            =   "FrmDispositivos.frx":630B
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   1410
            Width           =   2220
         End
         Begin VB.ComboBox IBaudios 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "FrmDispositivos.frx":6324
            Left            =   2565
            List            =   "FrmDispositivos.frx":633A
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   980
            Width           =   2220
         End
         Begin VB.ComboBox IPuerto 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "FrmDispositivos.frx":6365
            Left            =   2565
            List            =   "FrmDispositivos.frx":63C0
            TabIndex        =   1
            Text            =   "IPuerto"
            Top             =   550
            Width           =   2220
         End
         Begin VB.Label Label8 
            BackColor       =   &H00E7E8E8&
            Height          =   135
            Left            =   0
            TabIndex        =   40
            Top             =   3600
            Width           =   5415
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00AE5B00&
            X1              =   1800
            X2              =   5060
            Y1              =   120
            Y2              =   120
         End
         Begin VB.Label Label6 
            BackStyle       =   0  'Transparent
            Caption         =   "Características   "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00AE5B00&
            Height          =   375
            Left            =   120
            TabIndex        =   38
            Top             =   0
            Width           =   2175
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Nombre OPOS"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   1
            Left            =   435
            TabIndex        =   17
            Top             =   3165
            Width           =   1230
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Enviar Caracter"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   0
            Left            =   435
            TabIndex        =   16
            Top             =   2745
            Width           =   1320
         End
         Begin VB.Label Label55 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Parada"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   435
            TabIndex        =   15
            Top             =   2295
            Width           =   1230
         End
         Begin VB.Label Label54 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Dato"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   435
            TabIndex        =   14
            Top             =   1845
            Width           =   1020
         End
         Begin VB.Label Label53 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Paridad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   435
            TabIndex        =   13
            Top             =   1410
            Width           =   645
         End
         Begin VB.Label Label52 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Baudios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   435
            TabIndex        =   12
            Top             =   980
            Width           =   660
         End
         Begin VB.Label Label51 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Puerto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   435
            TabIndex        =   11
            Top             =   550
            Width           =   555
         End
      End
      Begin VB.Frame FrmLlaves 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Caption         =   "  Característica de las llaves  "
         ForeColor       =   &H80000008&
         Height          =   5850
         Left            =   4290
         TabIndex        =   27
         Top             =   420
         Visible         =   0   'False
         Width           =   5370
         Begin VB.TextBox TxtLlaves 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1875
            TabIndex        =   29
            Top             =   450
            Width           =   2235
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00AE5B00&
            X1              =   2640
            X2              =   4400
            Y1              =   120
            Y2              =   120
         End
         Begin VB.Label Label4 
            BackStyle       =   0  'Transparent
            Caption         =   "Característica de las llaves  "
            ForeColor       =   &H00AE5B00&
            Height          =   255
            Left            =   240
            TabIndex        =   36
            Top             =   0
            Width           =   2775
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Llave OPOS"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   210
            Left            =   315
            TabIndex        =   28
            Top             =   525
            Width           =   945
         End
      End
      Begin VB.Label Label5 
         BackColor       =   &H00FFFFFF&
         Height          =   5850
         Left            =   240
         TabIndex        =   37
         Top             =   420
         Width           =   3855
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Lista de dispositvos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Left            =   210
         TabIndex        =   9
         Top             =   120
         Width           =   1650
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   4380
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":6481
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":8213
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":9FA5
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":BD37
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":DAC9
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList iconos 
      Left            =   120
      Top             =   7440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   50
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":F85B
            Key             =   "org"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":FCAF
            Key             =   "caja"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1198B
            Key             =   "reportes"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":11CA7
            Key             =   "pos"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":12CFB
            Key             =   "solicitud"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":13977
            Key             =   "subnivel"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":13DCB
            Key             =   "user"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":13EDF
            Key             =   "compras"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":14543
            Key             =   "inventario"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":14B17
            Key             =   "printer3"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":14E33
            Key             =   "produccion"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":15473
            Key             =   "ventas"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":15B37
            Key             =   "tesoreria"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1616B
            Key             =   "cxp"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":167BF
            Key             =   "cxc"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":16E43
            Key             =   "compra"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1801F
            Key             =   "orden"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":188B3
            Key             =   "fichero"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":19793
            Key             =   "estadistic"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1A04B
            Key             =   "recepcion"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1ACE7
            Key             =   "traslado"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1B983
            Key             =   "ajuste"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1C35F
            Key             =   "invfisico"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1CFDB
            Key             =   "transferen"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1D2F7
            Key             =   "manufactur"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1D613
            Key             =   "presupuest"
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1D92F
            Key             =   "pedidos"
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1DC4B
            Key             =   "notentrega"
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1DF67
            Key             =   "facturacio"
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1E283
            Key             =   "promocione"
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1E59F
            Key             =   "ctlcaja"
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1E8BB
            Key             =   "movimiento"
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1EBD7
            Key             =   "hablador"
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1EEF3
            Key             =   "listado"
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1F20F
            Key             =   "listado1"
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1F52B
            Key             =   "listado11"
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1F847
            Key             =   "listado10"
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1FB63
            Key             =   "listado2"
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":1FE7F
            Key             =   "listado3"
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":2019B
            Key             =   "listado4"
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":204B7
            Key             =   "listado5"
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":207D3
            Key             =   "listado6"
         EndProperty
         BeginProperty ListImage43 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":20AEF
            Key             =   "listado7"
         EndProperty
         BeginProperty ListImage44 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":20E0B
            Key             =   "listado8"
         EndProperty
         BeginProperty ListImage45 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":21127
            Key             =   "listado9"
         EndProperty
         BeginProperty ListImage46 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":21443
            Key             =   "Balanzas"
         EndProperty
         BeginProperty ListImage47 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":2175F
            Key             =   "printer1"
         EndProperty
         BeginProperty ListImage48 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":21A7B
            Key             =   "printer2"
         EndProperty
         BeginProperty ListImage49 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":21D97
            Key             =   "ubicacion"
            Object.Tag             =   "ubicacion"
         EndProperty
         BeginProperty ListImage50 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDispositivos.frx":221E9
            Key             =   "scanner"
            Object.Tag             =   "scanner"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmDispositivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub chk_Corte_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    
    If chk_Corte.Value = 1 Then
        iCaracterCorte.Enabled = True
    Else
        iCaracterCorte.Enabled = False
    End If
    
    ADispositivo(PosEdit).UsaHojilla = chk_Corte.Value = 1
    
End Sub

Private Sub ChkCheques_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    
    If ChkCheques.Value = 1 Then
        Rotar.Enabled = True
    Else
        Rotar.Enabled = False
    End If
    
    ADispositivo(PosEdit).ImpCheque = ChkCheques.Value = 1
    
End Sub

Private Sub ChkCortarPapel_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    
    ADispositivo(PosEdit).UsaHojilla = ChkCortarPapel.Value = vbChecked
    
    If ADispositivo(PosEdit).UsaHojilla Then
        ADispositivo(PosEdit).CarCorte = "27,109"
    End If
    
End Sub

Private Sub ChkGavetaxImpresora_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    
    If ChkGavetaxImpresora.Value = vbChecked Then
        ADispositivo(PosEdit).CarControl = "27,p,0,62,250"
    Else
        ADispositivo(PosEdit).CarControl = vbNullString
    End If
    
End Sub

Private Sub ChkPredeterminada_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    
    If ChkPredeterminada.Value = vbChecked Then
        IPuerto.Text = "N/A"
        NOPOS.Text = vbNullString
    End If
    
    IPuerto.Enabled = Not (ChkPredeterminada.Value = vbChecked)
    IBaudios.Enabled = Not (ChkPredeterminada.Value = vbChecked)
    IParidad.Enabled = Not (ChkPredeterminada.Value = vbChecked)
    IDato.Enabled = Not (ChkPredeterminada.Value = vbChecked)
    IParada.Enabled = Not (ChkPredeterminada.Value = vbChecked)
    NOPOS.Enabled = Not (ChkPredeterminada.Value = vbChecked)
    IComando.Enabled = Not (ChkPredeterminada.Value = vbChecked)
    
End Sub

Private Sub Form_Activate()
    Call Cancelar
    Call LDispositivos_Click
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case vbShiftMask
        Case Else
            Select Case KeyCode
                Case vbKeyF4
                    Call Grabar
                Case vbKeyF7
                    Call Cancelar
                Case vbKeyF12
                    Unload Me
                    Exit Sub
                Case vbKeyReturn
                    Send_Keys Chr(vbKeyTab), True
                Case vbKeyEscape
            End Select
    End Select
End Sub

Private Sub Form_Load()

    Me.lbl_Organizacion.Caption = Stellar_Mensaje(10185, True) 'Configurador de Dispositivos
    Me.tlb_Config.Buttons(1).Caption = Stellar_Mensaje(6, True)
    Me.tlb_Config.Buttons(2).Caption = Stellar_Mensaje(103, True)
    Me.tlb_Config.Buttons(3).Caption = Stellar_Mensaje(107, True)
    Me.tlb_Config.Buttons(4).Caption = Stellar_Mensaje(7, True)
    Me.tlb_Config.Buttons("Teclado").Caption = Stellar_Mensaje(10080, True) ' Teclado
    
    Me.IParada.List(0) = "" 'ninguno
    Me.IParada.List(0) = "" 'impar
    Me.IParada.List(0) = ""  'par
    
    Label1.Caption = Stellar_Mensaje(10184, True) 'Lista de dispositivos
    Me.Label3.Caption = Stellar_Mensaje(10186, True) 'Llave OPOS
    Me.Label4.Caption = Stellar_Mensaje(10187, True) 'Característica de las llaves
    Label51.Caption = Stellar_Mensaje(10188, True) 'Puerto
    Label52.Caption = Stellar_Mensaje(10189, True) 'Baudios
    Label53.Caption = Stellar_Mensaje(10190, True) ''paridad
    Label54.Caption = Stellar_Mensaje(10191, True) 'bit de dato
    Label55.Caption = Stellar_Mensaje(10192, True) 'bits de para
    Label2(0).Caption = Stellar_Mensaje(10193, True) 'Enviar caracter
    Label2(1).Caption = Stellar_Mensaje(10194, True) 'Nombre OPOS
    
    Label7.Caption = Stellar_Mensaje(10195, True) 'Características Especiales
    TxtCtrl.Caption = Stellar_Mensaje(10196, True) 'Caracter de Gaveta
    'lblCortarPapel.Caption = stellar_mensaje(10197, True) 'Caracter de Corte
    ChkCheques.Caption = Stellar_Mensaje(10198, True) 'Imprime Cheques
    Label65(2).Caption = Stellar_Mensaje(10199, True) 'Rotación
    Label65(3).Caption = Stellar_Mensaje(10200, True) 'Grados
    
End Sub

Private Sub IBaudios_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    ADispositivo(PosEdit).Baudios = Me.IBaudios.Text
    
End Sub

Private Sub iCaracterCorte_Change()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    ADispositivo(PosEdit).CarCorte = Me.iCaracterCorte.Text
    
End Sub

Private Sub IComando_Change()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    ADispositivo(PosEdit).ComandoStr = Me.IComando.Text
    
End Sub

Private Sub IComando_GotFocus()
    IComando.SelStart = 0
    IComando.SelLength = Len(IComando)
End Sub

Private Sub IControl_Change()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    ADispositivo(PosEdit).CarControl = Me.IControl.Text
    
End Sub

Private Sub IControl_GotFocus()
    IControl.SelStart = 0
    IControl.SelLength = Len(IControl)
End Sub

Private Sub IDato_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    ADispositivo(PosEdit).BitDatos = Me.IDato.Text
    
End Sub

Private Sub IParada_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    ADispositivo(PosEdit).Parada = Me.IParada.Text
    
End Sub

Private Sub IParidad_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    
    Select Case LCase(Mid(Me.IParidad.Text, 1, 1))
        Case "p"
            ADispositivo(PosEdit).Paridad = "e"
        Case "i"
            ADispositivo(PosEdit).Paridad = "o"
        Case "n"
            ADispositivo(PosEdit).Paridad = "n"
    End Select

End Sub

Private Sub IPuerto_Change()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    
    If InStr(1, UCase(IPuerto.Text), "LPT") > 0 Then
        IBaudios.Enabled = False
        IParidad.Enabled = False
        IDato.Enabled = False
        IParada.Enabled = False
    Else
        IBaudios.Enabled = True
        IParidad.Enabled = True
        IDato.Enabled = True
        IParada.Enabled = True
    End If
    
    ADispositivo(PosEdit).Puerto = Me.IPuerto.Text

End Sub

Private Sub IPuerto_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    
    If InStr(1, UCase(IPuerto.Text), "LPT") > 0 Then
        IBaudios.Enabled = False
        IParidad.Enabled = False
        IDato.Enabled = False
        IParada.Enabled = False
    Else
        IBaudios.Enabled = True
        IParidad.Enabled = True
        IDato.Enabled = True
        IParada.Enabled = True
    End If
    
    ADispositivo(PosEdit).Puerto = Me.IPuerto.Text
    
End Sub

Private Sub LDispositivos_Click()
    Call LDispositivos_ItemClick(LDispositivos.SelectedItem)
End Sub

Private Sub LDispositivos_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    ADispositivo(Item.Index - 1).Activo = Item.Checked
End Sub

Private Sub LDispositivos_ItemClick(ByVal Item As MSComctlLib.ListItem)
    
    Dim PosEdit As Integer
    
    PosEdit = Item.Index - 1
    
    Me.IBaudios.ListIndex = BuscarEnCombo(IBaudios, ADispositivo(PosEdit).Baudios)
    Me.IComando.Text = ADispositivo(PosEdit).ComandoStr
    Me.IControl.Text = ADispositivo(PosEdit).CarControl
    Me.IDato.ListIndex = BuscarEnCombo(IDato, CStr(ADispositivo(PosEdit).BitDatos))
    Me.IParada.ListIndex = BuscarEnCombo(IParada, ADispositivo(PosEdit).Parada)
    
    Select Case LCase(Mid(ADispositivo(PosEdit).Paridad, 1, 1))
        Case "e"
            Me.IParidad.ListIndex = BuscarEnCombo(IParidad, "Par")
        Case "o"
            Me.IParidad.ListIndex = BuscarEnCombo(IParidad, "Impar")
        Case "n"
            Me.IParidad.ListIndex = BuscarEnCombo(IParidad, "Ninguna")
    End Select
    
    Me.IPuerto.Text = ADispositivo(PosEdit).Puerto
    Me.NOPOS.Text = ADispositivo(PosEdit).NombreOPOS
    Me.iCaracterCorte = ADispositivo(PosEdit).CarCorte
    Me.Rotar.ListIndex = BuscarEnCombo(Rotar, CStr(ADispositivo(PosEdit).Rotacion))
    Me.ChkCheques.Value = IIf(ADispositivo(PosEdit).ImpCheque, 1, 0)
    Me.TxtLlaves.Text = ADispositivo(PosEdit).NombreOPOS
    
    If ADispositivo(PosEdit).Buffer = "DSP0000001" Then
        FrmEspeciales.Visible = True
        FrameGavetaxImpresora.Visible = True
        TxtCtrl.Caption = "Caracter Control"
        Me.ChkCortarPapel.Value = IIf(ADispositivo(PosEdit).CarCorte <> vbNullString, vbChecked, vbUnchecked)
        Me.ChkGavetaxImpresora.Value = IIf(ADispositivo(PosEdit).CarControl <> vbNullString, vbChecked, vbUnchecked)
        ChkPredeterminada.Visible = True
        If ADispositivo(PosEdit).Puerto = "N/A" Then
            ChkPredeterminada.Value = vbChecked
        Else
            ChkPredeterminada.Value = vbUnchecked
        End If
    Else
        TxtCtrl.Caption = "Caracter Control"
        FrmEspeciales.Visible = False
        FrameGavetaxImpresora.Visible = False
        Me.ChkCortarPapel.Value = vbUnchecked
        Me.ChkGavetaxImpresora.Value = vbUnchecked
        ChkPredeterminada.Value = vbUnchecked
        ChkPredeterminada.Visible = False
    End If
    
    If ADispositivo(PosEdit).Buffer = "DSP0000008" Then
        Me.FrmDisp.Visible = False
        Me.FrmLlaves.Visible = True
    Else
        Me.FrmDisp.Visible = True
        Me.FrmLlaves.Visible = False
    End If
        
End Sub

Private Sub NOPOS_Change()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    ADispositivo(PosEdit).NombreOPOS = Me.NOPOS.Text
    
End Sub

Private Sub NOPOS_GotFocus()
    NOPOS.SelStart = 0
    NOPOS.SelLength = Len(NOPOS)
End Sub

Private Sub Rotar_Click()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    ADispositivo(PosEdit).Rotacion = Me.Rotar.Text
    
End Sub

Private Sub tlb_Config_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case LCase(Button.Key)
        Case "grabar"
            Send_Keys Chr(vbKeyF4), True
        Case "cancelar"
            Send_Keys Chr(vbKeyF7), True
        Case "salir"
            Send_Keys Chr(vbKeyF12), True
        Case "ayuda"
            Send_Keys Chr(vbKeyF1)
        Case LCase("Teclado")
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = NOPOS
            
            TecladoWindows mCtl
                        
    End Select
End Sub

Private Sub Grabar()
    'HASTA QUE SE PASE EL PUNTO DE VENTA RETAIL A LAS NUEVAS BASES DE DATOS
    If CheckCaja Then
        If EsDigital Then
            Call GrabarPOSFood
        Else
            Call GrabarPOSRetail
        End If
    End If
End Sub

Private Sub GrabarPOSFood()
    
    Dim rsCajaDispositivos As New ADODB.Recordset, Cont As Integer
    
    Ent.POS.BeginTrans
    
    For Cont = 1 To frm_Config_POS1.ARBOL.SelectedItem.Children
        frm_Config_POS1.ARBOL.Nodes.Remove frm_Config_POS1.ARBOL.SelectedItem.Child.Index
    Next Cont
    
    For Cont = LBound(ADispositivo) To UBound(ADispositivo) - 1
    
        If LDispositivos.ListItems(Cont + 1).Checked Then
            Call Apertura_Recordset(True, rsCajaDispositivos)
            
            rsCajaDispositivos.Open "SELECT * FROM MA_DISPOSITIVOS_CAJA " & _
            "WHERE CAJA = '" & nCaja & "' " & _
            "AND cs_CODIGODISPOSITIVO = '" & Trim(ADispositivo(Cont).Buffer) & "'", _
            Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
            
            If rsCajaDispositivos.EOF Then
                rsCajaDispositivos.AddNew
            End If
            
            rsCajaDispositivos!cs_CodigoDispositivo = Trim(ADispositivo(Cont).Buffer)
            rsCajaDispositivos!Caja = nCaja
            rsCajaDispositivos!cu_Baudios = ADispositivo(Cont).Baudios
            rsCajaDispositivos!cu_BITDATOS = ADispositivo(Cont).BitDatos
            rsCajaDispositivos!cu_CARCONTROL = ADispositivo(Cont).CarControl
            rsCajaDispositivos!cu_COMANDOSTR = ADispositivo(Cont).ComandoStr
            rsCajaDispositivos!cu_carcorte = ADispositivo(PosAdd).CarCorte
            rsCajaDispositivos!cu_NOMBREOPOS = ADispositivo(Cont).NombreOPOS
            rsCajaDispositivos!cu_opos = (ADispositivo(Cont).NombreOPOS <> "")
            rsCajaDispositivos!cu_PARADA = CInt(ADispositivo(Cont).Parada)
            
'            Select Case LCase(Mid(ADispositivo(Cont).Paridad, 1, 1))
'                Case "p"
'                    rsCajaDispositivos!cu_PARIDAD = "e"
'                Case "i"
'                    rsCajaDispositivos!cu_PARIDAD = "o"
'                Case "n"
'                    rsCajaDispositivos!cu_PARIDAD = "n"
'            End Select
            
            rsCajaDispositivos!cu_PARIDAD = ADispositivo(Cont).Paridad ' SE COLOCO ESTO DEBIDO A QUE _
            ANTERIORMENTE YA SE COLOCA EN LA VARIABLE .PARIDAD EL CARACTER A GUARDAR EN LA _
            BASE DE DATOS Y CON ESE ACSE NO IBA A GUARDAR NADA.
            
            rsCajaDispositivos!cu_PUERTO = ADispositivo(Cont).Puerto
            rsCajaDispositivos!cu_imp_cheques = ADispositivo(Cont).ImpCheque
            rsCajaDispositivos!cu_rotacion = ADispositivo(Cont).Rotacion
            rsCajaDispositivos!cu_ihojilla = ADispositivo(Cont).CarCorte <> ""
            rsCajaDispositivos!cu_carcorte = ADispositivo(Cont).CarCorte
            rsCajaDispositivos.UpdateBatch
            
            frm_Config_POS1.ARBOL.Nodes.add frm_Config_POS1.ARBOL.SelectedItem, tvwChild, _
            "|X|" & nCaja & ADispositivo(Cont).Buffer, ADispositivo(Cont).Descripcion, "manufactur"
            
        Else
            Ent.POS.Execute "DELETE FROM MA_DISPOSITIVOS_CAJA " & _
            "WHERE CAJA = '" & nCaja & "' " & _
            "AND cs_CodigoDispositivo = '" & Trim(ADispositivo(Cont).Buffer) & "'"
        End If
        
    Next Cont
    
    Ent.POS.CommitTrans
    
    Unload Me
    
End Sub

Private Sub GrabarPOSRetail()
    
    'On Error GoTo Error
    
    Dim rsCaja As New ADODB.Recordset, MiDato As Dispositivos, TmpDisp As Dispositivos
    
    Call Apertura_Recordset(True, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
    Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If Not rsCaja.EOF Then
        
        rsCaja!b_Opos = True '¿POR QUÉ?, PORQUE esta variable se configura en la tabla MA_CAJA donde le indica si utilizará opos y se configura en la forma general
        'además esta variable se reestablece segun los dispositivos instalados.
        
        MiDato = BuscarItemArreglo("DSP0000001", ADispositivo) 'IMPRESORA
        
        If MiDato.Activo Then
            If Trim(MiDato.NombreOPOS) <> "" Then
                rsCaja!Impresora = 1
            Else
                rsCaja!Impresora = 2
            End If
        Else
            rsCaja!Impresora = 0
        End If
        
        rsCaja!IPuerto = MiDato.Puerto
        rsCaja!IBaudios = MiDato.Baudios
        rsCaja!IParidad = MiDato.Paridad
        rsCaja!IBit = MiDato.BitDatos
        rsCaja!IParada = Val(MiDato.Parada)
        rsCaja!Ihojilla = MiDato.UsaHojilla
        rsCaja!hojilla_caracter = MiDato.CarCorte
        rsCaja!Imp_Cheques = MiDato.ImpCheque
        rsCaja!Rotacion = MiDato.Rotacion
        rsCaja!C_OPOSI = MiDato.NombreOPOS
        rsCaja!b_Opos = MiDato.NombreOPOS <> "" Or rsCaja!b_Opos
        
        ' Debug.Print ADispositivo(3).Buffer
        'ADispositivo(3).Activo = True
        'ADispositivo(3).ComandoStr = MiDato.CarControl
        'ADispositivo(3).Puerto = "LPT1"
        
        MiDato = BuscarItemArreglo("DSP0000002", ADispositivo) 'DISPLAY
        
        If MiDato.Activo Then
            If Trim(MiDato.NombreOPOS) <> "" Then
                rsCaja!Display = 1
            Else
                rsCaja!Display = 2
            End If
        Else
            rsCaja!Display = 0
        End If
        
        rsCaja!DPuerto = MiDato.Puerto
        rsCaja!DBaudios = MiDato.Baudios
        rsCaja!DParidad = MiDato.Paridad
        rsCaja!dBit = MiDato.BitDatos
        rsCaja!DParada = MiDato.Parada
        rsCaja!C_OPOSD = MiDato.NombreOPOS
        rsCaja!b_Opos = MiDato.NombreOPOS <> "" Or rsCaja!b_Opos
        
        MiDato = BuscarItemArreglo("DSP0000003", ADispositivo) 'SCANNER
        
        If MiDato.Activo Then
            If Trim(MiDato.NombreOPOS) <> "" Then
                rsCaja!Scanner = 1
            Else
                rsCaja!Scanner = 2
            End If
        Else
            rsCaja!Scanner = 0
        End If
        
        rsCaja!sPuerto = MiDato.Puerto
        rsCaja!SBaudios = MiDato.Baudios
        rsCaja!SParidad = MiDato.Paridad
        rsCaja!Sbit = MiDato.BitDatos
        rsCaja!SParada = CDbl(Replace(MiDato.Parada, ",", SDecimal()))
        rsCaja!SCONTROL = MiDato.ComandoStr
        rsCaja!c_OposS = MiDato.NombreOPOS
        rsCaja!b_Opos = MiDato.NombreOPOS <> "" Or rsCaja!b_Opos
        
        MiDato = BuscarItemArreglo("DSP0000004", ADispositivo) 'GAVETA
        
        If MiDato.Activo Then
            If Trim(MiDato.NombreOPOS) <> "" Then
                rsCaja!Gabeta = 1
            Else
                rsCaja!Gabeta = 2
            End If
        Else
            rsCaja!Gabeta = 0
        End If
        
        If InStr(1, MiDato.Puerto, "LPT") > 0 Then
            rsCaja!GPuerto = "LPT1"
            rsCaja!Gserial = False
        Else
            rsCaja!GPuerto = MiDato.Puerto
            rsCaja!Gserial = True
        End If
        
        rsCaja!GBaudios = MiDato.Baudios
        rsCaja!GParidad = MiDato.Paridad
        rsCaja!gBit = MiDato.BitDatos
        rsCaja!GParada = MiDato.Parada
        rsCaja!Gcontrol = MiDato.ComandoStr
        rsCaja!C_OPOSG = MiDato.NombreOPOS
        rsCaja!b_Opos = MiDato.NombreOPOS <> "" Or rsCaja!b_Opos
        
        MiDato = BuscarItemArreglo("DSP0000005", ADispositivo) 'BALANZA
        
        If MiDato.Activo Then
            If Trim(MiDato.NombreOPOS) <> "" Then
                rsCaja!Balanza = 1
            Else
                rsCaja!Balanza = 2
            End If
        Else
            rsCaja!Balanza = 0
        End If
        
        rsCaja!bPuerto = MiDato.Puerto
        rsCaja!bBaudios = MiDato.Baudios
        rsCaja!bParidad = MiDato.Paridad
        rsCaja!bBit = MiDato.BitDatos
        rsCaja!bParada = MiDato.Parada
        rsCaja!bControl = MiDato.ComandoStr
        rsCaja!c_OposBA = MiDato.NombreOPOS
        rsCaja!b_Opos = MiDato.NombreOPOS <> "" Or rsCaja!b_Opos
        
        MiDato = BuscarItemArreglo("DSP0000006", ADispositivo) 'LECTOR DE BANDA
        
        If MiDato.Activo Then
            If Trim(MiDato.NombreOPOS) <> "" Then
                rsCaja!Banda = 1
            Else
                rsCaja!Banda = 2
            End If
        Else
            rsCaja!Banda = 0
        End If
        
        rsCaja!bmpuerto = MiDato.Puerto
        rsCaja!bmbaudios = MiDato.Baudios
        rsCaja!bmparidad = MiDato.Paridad
        rsCaja!bmbit = MiDato.BitDatos
        rsCaja!bmparada = MiDato.Parada
        rsCaja!c_OposB = MiDato.NombreOPOS
        rsCaja!b_Opos = MiDato.NombreOPOS <> "" Or rsCaja!b_Opos
        
        MiDato = BuscarItemArreglo("DSP0000007", ADispositivo) 'LECTOR DE CHEQUES
        rsCaja!b_Opos = MiDato.NombreOPOS <> "" Or rsCaja!b_Opos
        
        MiDato = BuscarItemArreglo("DSP0000008", ADispositivo) 'LLAVES POS
        
        rsCaja!Llaves = MiDato.NombreOPOS <> ""
        
        rsCaja!c_OposL = MiDato.NombreOPOS
        rsCaja!b_Opos = MiDato.NombreOPOS <> "" Or rsCaja!b_Opos
        
        rsCaja.UpdateBatch
        
    End If
    
    rsCaja.Close
    
    '----------------------------------------------------------------------------------
    
    Dim rsCajaDispositivos As New ADODB.Recordset, Cont As Integer
    
    Ent.POS.BeginTrans
    
    For Cont = 1 To frm_Config_POS1.ARBOL.SelectedItem.Children
        frm_Config_POS1.ARBOL.Nodes.Remove frm_Config_POS1.ARBOL.SelectedItem.Child.Index
    Next Cont
    
    For Cont = LBound(ADispositivo) To UBound(ADispositivo) - 1
    
        If LDispositivos.ListItems(Cont + 1).Checked Then
            Call Apertura_Recordset(True, rsCajaDispositivos)
            
            rsCajaDispositivos.Open "SELECT * FROM MA_DISPOSITIVOS_CAJA " & _
            "WHERE CAJA = '" & nCaja & "' " & _
            "AND cs_CodigoDispositivo = '" & Trim(ADispositivo(Cont).Buffer) & "'", _
            Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
            
            If rsCajaDispositivos.EOF Then
                rsCajaDispositivos.AddNew
            End If
            
            rsCajaDispositivos!cs_CodigoDispositivo = Trim(ADispositivo(Cont).Buffer)
            rsCajaDispositivos!Caja = nCaja
            rsCajaDispositivos!cu_Baudios = ADispositivo(Cont).Baudios
            rsCajaDispositivos!cu_BITDATOS = ADispositivo(Cont).BitDatos
            rsCajaDispositivos!cu_CARCONTROL = ADispositivo(Cont).CarControl
            rsCajaDispositivos!cu_COMANDOSTR = ADispositivo(Cont).ComandoStr
            rsCajaDispositivos!cu_carcorte = ADispositivo(PosAdd).CarCorte
            rsCajaDispositivos!cu_NOMBREOPOS = ADispositivo(Cont).NombreOPOS
            rsCajaDispositivos!cu_opos = (ADispositivo(Cont).NombreOPOS <> "")
            rsCajaDispositivos!cu_PARADA = Val(ADispositivo(Cont).Parada)
            
            Select Case LCase(Mid(ADispositivo(Cont).Paridad, 1, 1))
                Case "p"
                    rsCajaDispositivos!cu_PARIDAD = "e"
                Case "i"
                    rsCajaDispositivos!cu_PARIDAD = "o"
                Case "n"
                    rsCajaDispositivos!cu_PARIDAD = "n"
            End Select
            
            rsCajaDispositivos!cu_PUERTO = ADispositivo(Cont).Puerto
            rsCajaDispositivos!cu_imp_cheques = ADispositivo(Cont).ImpCheque
            rsCajaDispositivos!cu_rotacion = ADispositivo(Cont).Rotacion
            rsCajaDispositivos!cu_ihojilla = ADispositivo(Cont).CarCorte <> ""
            rsCajaDispositivos!cu_carcorte = ADispositivo(Cont).CarCorte
            rsCajaDispositivos.UpdateBatch
            
            frm_Config_POS1.ARBOL.Nodes.add frm_Config_POS1.ARBOL.SelectedItem, tvwChild, _
            "|X|" & nCaja & ADispositivo(Cont).Buffer, ADispositivo(Cont).Descripcion, "manufactur"
            
        Else
            Ent.POS.Execute "DELETE FROM MA_DISPOSITIVOS_CAJA " & _
            "WHERE CAJA = '" & nCaja & "' " & _
            "AND cs_CodigoDispositivo = '" & Trim(ADispositivo(Cont).Buffer) & "'"
        End If
        
    Next Cont
    
    Ent.POS.CommitTrans
    
    Unload Me
    
    Exit Sub
    
Error:
    
    Debug.Print ERR.Description
    'Resume
    
End Sub

Public Sub Cancelar()
    
    ReDim ADispositivo(0) As Dispositivos
    
    LDispositivos.ListItems.Clear
    
    If CheckCaja Then
        If EsDigital Then
            Call CargarDispositivos(LDispositivos)
        Else
            Call CargarPOSRetail(LDispositivos)
        End If
    End If
    
    Call LDispositivos_ItemClick(LDispositivos.SelectedItem)
    
End Sub

Private Sub CargarDispositivos(LDsp As Object)
    
    Dim RsDispositivos As New ADODB.Recordset, rsCajaDispositivos As New ADODB.Recordset, PosAdd As Integer
    
    Call Apertura_Recordset(True, RsDispositivos)
    
    RsDispositivos.Open "SELECT * FROM MA_DISPOSITIVOS", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsDispositivos.EOF Then
        While Not RsDispositivos.EOF
            Call Apertura_Recordset(True, rsCajaDispositivos)
            
            PosAdd = UBound(ADispositivo)
            
            ReDim Preserve ADispositivo(UBound(ADispositivo) + 1)
            
            rsCajaDispositivos.Open "SELECT * FROM MA_DISPOSITIVOS_CAJA WHERE CAJA = '" & nCaja & "' AND cs_CodigoDispositivo = '" & RsDispositivos!cs_Codigo & "'", _
            Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not rsCajaDispositivos.EOF Then
                LDsp.ListItems.add , RsDispositivos!cs_Codigo, RsDispositivos!cs_Descripcion
                LDsp.ListItems(LDsp.ListItems.Count).Checked = True
                
                ADispositivo(PosAdd).Activo = True
                ADispositivo(PosAdd).Buffer = RsDispositivos!cs_Codigo
                ADispositivo(PosAdd).Descripcion = RsDispositivos!cs_Descripcion
                ADispositivo(PosAdd).Baudios = rsCajaDispositivos!cu_Baudios
                ADispositivo(PosAdd).BitDatos = rsCajaDispositivos!cu_BITDATOS
                ADispositivo(PosAdd).CarControl = rsCajaDispositivos!cu_CARCONTROL
                ADispositivo(PosAdd).CarCorte = rsCajaDispositivos!cu_carcorte
                ADispositivo(PosAdd).ComandoStr = rsCajaDispositivos!cu_COMANDOSTR
                ADispositivo(PosAdd).NombreOPOS = rsCajaDispositivos!cu_NOMBREOPOS
                ADispositivo(PosAdd).opos = (rsCajaDispositivos!cu_NOMBREOPOS <> "")
                ADispositivo(PosAdd).Parada = rsCajaDispositivos!cu_PARADA
                ADispositivo(PosAdd).Paridad = rsCajaDispositivos!cu_PARIDAD
                ADispositivo(PosAdd).pNemotecnico = RsDispositivos!cs_PNEMOTECNICO
                ADispositivo(PosAdd).Puerto = rsCajaDispositivos!cu_PUERTO
                ADispositivo(PosAdd).CarCorte = rsCajaDispositivos!cu_carcorte
                ADispositivo(PosAdd).UsaHojilla = rsCajaDispositivos!cu_carcorte <> ""
                ADispositivo(PosAdd).Rotacion = rsCajaDispositivos!cu_rotacion
                ADispositivo(PosAdd).ImpCheque = rsCajaDispositivos!cu_imp_cheques
            Else
                ADispositivo(PosAdd).Activo = False
                ADispositivo(PosAdd).Buffer = RsDispositivos!cs_Codigo
                ADispositivo(PosAdd).Descripcion = RsDispositivos!cs_Descripcion
                ADispositivo(PosAdd).Baudios = 9600
                ADispositivo(PosAdd).BitDatos = 4
                ADispositivo(PosAdd).CarControl = ""
                ADispositivo(PosAdd).CarCorte = ""
                ADispositivo(PosAdd).ComandoStr = ""
                ADispositivo(PosAdd).NombreOPOS = ""
                ADispositivo(PosAdd).opos = False
                ADispositivo(PosAdd).Parada = "1"
                ADispositivo(PosAdd).Paridad = "e"
                ADispositivo(PosAdd).pNemotecnico = RsDispositivos!cs_PNEMOTECNICO
                ADispositivo(PosAdd).Puerto = "COM1"
                ADispositivo(PosAdd).CarCorte = ""
                ADispositivo(PosAdd).UsaHojilla = False
                ADispositivo(PosAdd).Rotacion = 0
                ADispositivo(PosAdd).ImpCheque = False
                
                LDsp.ListItems.add , RsDispositivos!cs_Codigo, RsDispositivos!cs_Descripcion
                LDsp.ListItems(LDsp.ListItems.Count).Checked = False
            End If
            
            RsDispositivos.MoveNext
        Wend
    Else
        Mensajes.Mensaje "No se encuentra la lista de dispositivos en el sistema.", False
        Unload Me
    End If
End Sub

Private Sub TxtLlaves_Change()
    
    Dim PosEdit As Integer
    
    PosEdit = LDispositivos.SelectedItem.Index - 1
    ADispositivo(PosEdit).NombreOPOS = Me.TxtLlaves.Text
    
End Sub

Private Function BuscarItemArreglo(Campo As String, ByRef ADsp() As Dispositivos) As Dispositivos
    
    'BuscarItemArreglo.Buffer
    
    Dim Cont As Integer
    
    For Cont = LBound(ADsp) To UBound(ADsp)
        If ADsp(Cont).Buffer = Campo Then
            BuscarItemArreglo = ADsp(Cont)
            Exit Function
        End If
    Next Cont
    
End Function

Private Sub CargarPOSRetail(LDsp As Object)
    
    Dim rsCaja As New ADODB.Recordset, MiDato As Dispositivos
    
    Call Apertura_Recordset(True, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
    Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If Not rsCaja.EOF Then
        
        ReDim Preserve ADispositivo(UBound(ADispositivo) + 1) As Dispositivos
        MiDato = ADispositivo(UBound(ADispositivo) - 1)
    
        MiDato.Buffer = "DSP0000001" 'IMPRESORA
        MiDato.Descripcion = "IMPRESORA"
        MiDato.pNemotecnico = "IMPRESORA"
        
        MiDato.Activo = rsCaja!Impresora <> 0
        MiDato.Puerto = rsCaja!IPuerto
        MiDato.Baudios = rsCaja!IBaudios
        MiDato.Paridad = rsCaja!IParidad
        MiDato.BitDatos = rsCaja!IBit
        MiDato.Parada = rsCaja!IParada
        MiDato.UsaHojilla = rsCaja!Ihojilla
        MiDato.CarCorte = rsCaja!hojilla_caracter
        MiDato.CarControl = rsCaja!Gcontrol ' Caracter de Control de la Gaveta
        MiDato.ComandoStr = MiDato.CarControl
        MiDato.ImpCheque = rsCaja!Imp_Cheques
        MiDato.Rotacion = rsCaja!Rotacion
        MiDato.NombreOPOS = rsCaja!C_OPOSI
        
        ADispositivo(UBound(ADispositivo) - 1) = MiDato
        LDsp.ListItems.add , "DSP0000001", "IMPRESORA"
        LDsp.ListItems(LDsp.ListItems.Count).Checked = MiDato.Activo

        ReDim Preserve ADispositivo(UBound(ADispositivo) + 1) As Dispositivos
        MiDato = ADispositivo(UBound(ADispositivo) - 1)
        
        MiDato.Buffer = "DSP0000002" 'DISPLAY
        MiDato.Descripcion = "DISPLAY"
        MiDato.pNemotecnico = "DISPLAY"
        
        MiDato.Activo = rsCaja!Display <> 0
        MiDato.Puerto = rsCaja!DPuerto
        MiDato.Baudios = rsCaja!DBaudios
        MiDato.Paridad = rsCaja!DParidad
        MiDato.BitDatos = rsCaja!dBit
        MiDato.Parada = rsCaja!DParada
        MiDato.NombreOPOS = rsCaja!C_OPOSD
        
        ADispositivo(UBound(ADispositivo) - 1) = MiDato
        LDsp.ListItems.add , "DSP0000002", "DISPLAY"
        LDsp.ListItems(LDsp.ListItems.Count).Checked = MiDato.Activo
        
        ReDim Preserve ADispositivo(UBound(ADispositivo) + 1) As Dispositivos
        MiDato = ADispositivo(UBound(ADispositivo) - 1)
        MiDato.Buffer = "DSP0000003" 'SCANNER
        MiDato.Descripcion = "SCANNER"
        MiDato.pNemotecnico = "SCANNER"
        
        MiDato.Activo = rsCaja!Scanner <> 0
        MiDato.Puerto = rsCaja!sPuerto
        MiDato.Baudios = rsCaja!SBaudios
        MiDato.Paridad = rsCaja!SParidad
        MiDato.BitDatos = rsCaja!Sbit
        MiDato.Parada = rsCaja!SParada
        MiDato.CarControl = rsCaja!SCONTROL
        MiDato.ComandoStr = MiDato.CarControl
        MiDato.NombreOPOS = rsCaja!c_OposS
        
        ADispositivo(UBound(ADispositivo) - 1) = MiDato
        LDsp.ListItems.add , "DSP0000003", "SCANNER"
        LDsp.ListItems(LDsp.ListItems.Count).Checked = MiDato.Activo
    
        ReDim Preserve ADispositivo(UBound(ADispositivo) + 1) As Dispositivos
        MiDato = ADispositivo(UBound(ADispositivo) - 1)
        MiDato.Buffer = "DSP0000004" 'GAVETA
        MiDato.Descripcion = "GAVETA"
        MiDato.pNemotecnico = "GAVETA"
        
        MiDato.Activo = rsCaja!Gabeta <> 0
        MiDato.Puerto = rsCaja!GPuerto
        MiDato.Baudios = rsCaja!GBaudios
        MiDato.Paridad = rsCaja!GParidad
        MiDato.BitDatos = rsCaja!gBit
        MiDato.Parada = rsCaja!GParada
        MiDato.CarControl = rsCaja!Gcontrol
        MiDato.ComandoStr = MiDato.CarControl
        MiDato.NombreOPOS = rsCaja!C_OPOSG
        
        ADispositivo(UBound(ADispositivo) - 1) = MiDato
        LDsp.ListItems.add , "DSP0000004", "GAVETA"
        LDsp.ListItems(LDsp.ListItems.Count).Checked = MiDato.Activo
        
        ReDim Preserve ADispositivo(UBound(ADispositivo) + 1) As Dispositivos
        MiDato = ADispositivo(UBound(ADispositivo) - 1)
        MiDato.Buffer = "DSP0000005" 'BALANZA
        MiDato.Descripcion = "BALANZA"
        MiDato.pNemotecnico = "BALANZA"
        
        MiDato.Activo = rsCaja!Balanza <> 0
        MiDato.Puerto = rsCaja!bPuerto
        MiDato.Baudios = rsCaja!bBaudios
        MiDato.Paridad = rsCaja!bParidad
        MiDato.BitDatos = rsCaja!bBit
        MiDato.Parada = rsCaja!bParada
        MiDato.CarControl = rsCaja!bControl
        MiDato.ComandoStr = MiDato.CarControl
        MiDato.NombreOPOS = rsCaja!c_OposB
        
        ADispositivo(UBound(ADispositivo) - 1) = MiDato
        LDsp.ListItems.add , "DSP0000005", "BALANZA"
        LDsp.ListItems(LDsp.ListItems.Count).Checked = MiDato.Activo
        
        ReDim Preserve ADispositivo(UBound(ADispositivo) + 1) As Dispositivos
        MiDato = ADispositivo(UBound(ADispositivo) - 1)
        MiDato.Buffer = "DSP0000006" 'LECTOR DE BANDA
        MiDato.Descripcion = "LECTOR DE BANDA MAGNÉTICA"
        MiDato.pNemotecnico = "MRS"
        
        MiDato.Activo = rsCaja!Banda <> 0
        MiDato.Puerto = rsCaja!bmpuerto
        MiDato.Baudios = rsCaja!bmbaudios
        MiDato.Paridad = rsCaja!bmparidad
        MiDato.BitDatos = rsCaja!bmbit
        MiDato.Parada = rsCaja!bmparada
        MiDato.NombreOPOS = rsCaja!c_OposBA
        
        ADispositivo(UBound(ADispositivo) - 1) = MiDato
        LDsp.ListItems.add , "DSP0000006", "LECTOR DE BANDA"
        LDsp.ListItems(LDsp.ListItems.Count).Checked = MiDato.Activo
                
        ReDim Preserve ADispositivo(UBound(ADispositivo) + 1) As Dispositivos
        MiDato = ADispositivo(UBound(ADispositivo) - 1)
        MiDato.Buffer = "DSP0000007" 'LECTOR DE CHEQUES
        MiDato.Descripcion = "LECTOR DE CHEQUES"
        MiDato.pNemotecnico = "MIR"
        
        ADispositivo(UBound(ADispositivo) - 1) = MiDato
        LDsp.ListItems.add , "DSP0000007", "LECTOR DE CHEQUES"
        LDsp.ListItems(LDsp.ListItems.Count).Checked = MiDato.Activo
        
        ReDim Preserve ADispositivo(UBound(ADispositivo) + 1) As Dispositivos
        MiDato = ADispositivo(UBound(ADispositivo) - 1)
        MiDato.Buffer = "DSP0000008" 'LLAVES POS
        MiDato.Descripcion = "LLAVES DEL POS"
        MiDato.pNemotecnico = "LLAVES"
        MiDato.Activo = (rsCaja!Llaves And rsCaja!b_Opos _
        And (Trim(rsCaja!c_OposL) <> Empty))
        MiDato.NombreOPOS = rsCaja!c_OposL
        
        ADispositivo(UBound(ADispositivo) - 1) = MiDato
        LDsp.ListItems.add , "DSP0000008", "LLAVES POS"
        LDsp.ListItems(LDsp.ListItems.Count).Checked = MiDato.Activo
        
    End If
    
    rsCaja.Close
    
End Sub
