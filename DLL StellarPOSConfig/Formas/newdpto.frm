VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form NewDpto 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3630
   ClientLeft      =   120
   ClientTop       =   -210
   ClientWidth     =   8445
   ControlBox      =   0   'False
   Icon            =   "newdpto.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   8445
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   1695
      Left            =   240
      TabIndex        =   6
      Top             =   1680
      Width           =   7935
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Index           =   0
         Left            =   480
         TabIndex        =   7
         Top             =   720
         Width           =   6735
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Descripción"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Index           =   3
         Left            =   480
         TabIndex        =   8
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuración de la Estructura de POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6195
         TabIndex        =   4
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   8625
      TabIndex        =   0
      Top             =   421
      Width           =   8655
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   1080
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   8000
         Begin MSComctlLib.Toolbar barra_menu 
            Height          =   810
            Left            =   180
            TabIndex        =   2
            Top             =   120
            Width           =   7500
            _ExtentX        =   13229
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            ToolTips        =   0   'False
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "image_menu_prin(0)"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "F7 Borra el campo Descripción"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "F4 Grabar"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "salir"
                  Object.ToolTipText     =   "F12 Salir "
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "F1 Ayuda del Sistema"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList image_menu_prin 
      Index           =   0
      Left            =   3840
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newdpto.frx":628A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newdpto.frx":801C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newdpto.frx":9DAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newdpto.frx":BB40
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newdpto.frx":D8D2
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
   Begin VB.Menu menu 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu eliminar 
         Caption         =   "Eliminar"
      End
      Begin VB.Menu cambiar 
         Caption         =   "Cambiar Nombre"
      End
   End
End
Attribute VB_Name = "NewDpto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Quien As String
Public ObjArrastre As Object

Private Sub barra_menu_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        
        Case Is = "grabar" ''boton salvar
            Call Grabar
            
        Case Is = "cancelar"
            Text1(0) = ""
        
        Case Is = "salir"
            AddNode = False
            Unload Me
            
        Case Is = "Teclado"
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = Text1(0)
            
            TecladoWindows mCtl
            
    End Select
End Sub

Private Sub Grabar()
    If Text1(0).Text <> "" Then
        lcTexto = Text1(0)
        AddNode = True
        Unload Me
    Else
        Text1(0).SetFocus
        Text1(0).SetFocus
    End If
End Sub

Private Sub Form_Activate()
    If PuedeObtenerFoco(Text1(0)) Then Text1(0).SetFocus
    SeleccionarTexto Text1(0)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF7
            Text1(0) = ""
            
        Case Is = vbKeyF4
            Call Grabar
            
        Case Is = vbKeyF12
            AddNode = False
            Unload Me
    End Select
End Sub

Private Sub Form_Load()
    
    Me.lbl_Organizacion = stellar_mensaje(5510, True) 'Configuración de la Estructura de POS
    Me.barra_menu.Buttons(1).Caption = stellar_mensaje(6, True) 'cancelar
    Me.barra_menu.Buttons(2).Caption = stellar_mensaje(103, True) 'grabar
    Me.barra_menu.Buttons(4).Caption = stellar_mensaje(54, True) 'salir
    Me.barra_menu.Buttons(5).Caption = stellar_mensaje(7, True) 'ayuda
    Me.barra_menu.Buttons("Teclado").Caption = stellar_mensaje(10080, True) ' Teclado
    Me.Label1(3).Caption = stellar_mensaje(143, True) 'descripcion
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set NewDpto = Nothing
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
    End If
End Sub
