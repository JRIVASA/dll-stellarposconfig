VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "comct332.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FrmConfigBotonesAcceso 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n de Botones de Acceso"
   ClientHeight    =   7560
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7350
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmConfigBotonesAcceso.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   7350
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox Combo_Cell 
      CausesValidation=   0   'False
      Height          =   315
      ItemData        =   "FrmConfigBotonesAcceso.frx":628A
      Left            =   2355
      List            =   "FrmConfigBotonesAcceso.frx":628C
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   2430
      Visible         =   0   'False
      Width           =   1725
   End
   Begin ComCtl3.CoolBar CoolBar1 
      Align           =   1  'Align Top
      Height          =   855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7350
      _ExtentX        =   12965
      _ExtentY        =   1508
      BandCount       =   1
      _CBWidth        =   7350
      _CBHeight       =   855
      _Version        =   "6.7.8862"
      Child1          =   "Frame2"
      MinHeight1      =   795
      Width1          =   8370
      NewRow1         =   0   'False
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   795
         Left            =   30
         TabIndex        =   1
         Top             =   30
         Width           =   7230
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   705
            Left            =   60
            TabIndex        =   2
            Top             =   60
            Width           =   7155
            _ExtentX        =   12621
            _ExtentY        =   1244
            ButtonWidth     =   1402
            ButtonHeight    =   1244
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            ImageList       =   "Icono_Apagado"
            DisabledImageList=   "Icono_deshabilitado"
            HotImageList    =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "F4 Grabar"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "F7 Cancelar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "F1 Ayuda"
                  ImageIndex      =   3
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1500
      Top             =   630
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":628E
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":6F6A
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":7C46
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":8922
            Key             =   "inter"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   780
      Top             =   630
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":8B6E
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":984A
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":A526
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":B202
            Key             =   "inter"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_deshabilitado 
      Left            =   60
      Top             =   630
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":B44E
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":C12A
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":CE06
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":DAE2
            Key             =   "inter"
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid Botones 
      Height          =   6675
      Left            =   30
      TabIndex        =   3
      Top             =   870
      Width           =   7275
      _ExtentX        =   12832
      _ExtentY        =   11774
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FrmConfigBotonesAcceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim PosGridCol As Integer, PosGridRow As Integer, KeyFlags As Integer

Public Sub LlenarGrid()
    
    Dim rsBotones As New ADODB.Recordset
    
    If EsDigital Then
        
        Call Apertura_Recordset(False, rsBotones)
        
        rsBotones.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "' ORDER BY Posicion", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        Botones.Row = Botones.Rows - 1
        
        If Not rsBotones.EOF Then
            While Not rsBotones.EOF
                With Botones
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = rsBotones!Nombre_Boton
                    .Col = 1
                    .Text = IIf(rsBotones!Activo, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!Nivel
                    .Col = 3
                    .Text = rsBotones!Llave
                    .Col = 4
                    .Text = IIf(rsBotones!bActivo, "1", "0")
                    .Col = 5
                    .Text = IIf(rsBotones!BNIVEL, "1", "0")
                    .Col = 6
                    .Text = IIf(rsBotones!BLLAVE, "1", "0")
                End With
                rsBotones.MoveNext
            Wend
            
            Botones.Row = 1
            Botones.Col = 1
            
            rsBotones.Close
            
        End If
        
    Else
        
        Call Apertura_Recordset(False, rsBotones)
        
        rsBotones.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "' ORDER BY Posicion", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        Botones.Row = Botones.Rows - 1
        
        If Not rsBotones.EOF Then
            
            While Not rsBotones.EOF
                With Botones
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = rsBotones!Nombre_Boton
                    .Col = 1
                    .Text = IIf(rsBotones!Activo, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!Nivel
                    .Col = 3
                    .Text = rsBotones!Llave
                    .Col = 4
                    .Text = IIf(rsBotones!bActivo, "1", "0")
                    .Col = 5
                    .Text = IIf(rsBotones!BNIVEL, "1", "0")
                    .Col = 6
                    .Text = IIf(rsBotones!BLLAVE, "1", "0")
                End With
                rsBotones.MoveNext
            Wend
            
            Botones.Row = 1
            Botones.Col = 1
            
            rsBotones.Close
            
        Else
            
            rsBotones.Close
            
            rsBotones.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
            Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not rsBotones.EOF Then
                With Botones
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F1 Ayuda"
                    .Col = 1
                    .Text = IIf(rsBotones!bAyuda, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nAyuda
                    .Col = 3
                    .Text = rsBotones!lAyuda
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F2 Buscar"
                    .Col = 1
                    .Text = IIf(rsBotones!bProductos, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nProductos
                    .Col = 3
                    .Text = rsBotones!lProductos
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F3 Limite"
                    .Col = 1
                    .Text = IIf(rsBotones!bLimite, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nLimite
                    .Col = 3
                    .Text = rsBotones!lLimite
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F4 Cantidad"
                    .Col = 1
                    .Text = IIf(rsBotones!bCantidad, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nCantidad
                    .Col = 3
                    .Text = rsBotones!lCantidad
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F5 Balanza"
                    .Col = 1
                    .Text = IIf(rsBotones!bBalanza, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nBalanza
                    .Col = 3
                    .Text = rsBotones!lBalanza
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F6 Reintegro Total"
                    .Col = 1
                    .Text = IIf(rsBotones!bReintegro, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nReintegro
                    .Col = 3
                    .Text = rsBotones!lReintegro
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F6 Reintegro Parcial"
                    .Col = 1
                    .Text = IIf(rsBotones!bParcial, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nParcial
                    .Col = 3
                    .Text = rsBotones!lParcial
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F7 Espera"
                    .Col = 1
                    .Text = IIf(rsBotones!bEspera, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nEspera
                    .Col = 3
                    .Text = rsBotones!lEspera
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F8 Totalizar"
                    .Col = 1
                    .Text = "Si"
                    .Col = 2
                    .Text = "0"
                    .Col = 3
                    .Text = "0"
                    .Col = 4
                    .Text = "1"
                    .Col = 5
                    .Text = "1"
                    .Col = 6
                    .Text = "1"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F9 Suspender"
                    .Col = 1
                    .Text = IIf(rsBotones!bSuspender, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nSuspender
                    .Col = 3
                    .Text = rsBotones!lSuspender
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F10 Devolver"
                    .Col = 1
                    .Text = IIf(rsBotones!bDevolucion, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nDevolucion
                    .Col = 3
                    .Text = rsBotones!lDevolucion
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F11 Precios"
                    .Col = 1
                    .Text = IIf(rsBotones!bPrecios, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nPrecios
                    .Col = 3
                    .Text = rsBotones!lPrecios
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F12 Clientes"
                    .Col = 1
                    .Text = IIf(rsBotones!bCliente, "Si", "No")
                    .Col = 2
                    .Text = rsBotones!nCliente
                    .Col = 3
                    .Text = rsBotones!lCliente
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "Apertura de Caja"
                    .Col = 1
                    .Text = "Si"
                    .Col = 2
                    .Text = rsBotones!nOpenPOS
                    .Col = 3
                    .Text = "0"
                    .Col = 4
                    .Text = "1"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "1"
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "Cierre de Caja"
                    .Col = 1
                    .Text = "Si"
                    .Col = 2
                    .Text = rsBotones!nClosePOS
                    .Col = 3
                    .Text = rsBotones!lCerrar
                    .Col = 4
                    .Text = "1"
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    
                End With
                
                rsBotones.Close
            End If
        
        End If
    
    End If
    
End Sub

Public Sub InicializarGrid(LGrid As MSFlexGrid)
    LGrid.Clear
    LGrid.Rows = 1
    LGrid.Row = 0
    LGrid.Cols = 7
    LGrid.Col = 0
    LGrid.Text = "Bot�n"
    LGrid.ColWidth(0) = 3850
    LGrid.Col = 1
    LGrid.Text = "Activo"
    LGrid.ColWidth(1) = 1000
    LGrid.ColAlignment(LGrid.Col) = flexAlignCenterCenter
    LGrid.Col = 2
    LGrid.Text = "Nivel"
    LGrid.ColWidth(2) = 1000
    LGrid.ColAlignment(LGrid.Col) = flexAlignCenterCenter
    LGrid.Col = 3
    LGrid.Text = "Llave"
    LGrid.ColWidth(3) = 1000
    LGrid.ColAlignment(LGrid.Col) = flexAlignCenterCenter
    LGrid.Row = 0
    LGrid.Col = 1
    LGrid.Col = 4
    LGrid.Text = "BACTIVO"
    LGrid.ColWidth(4) = 0
    LGrid.Row = 0
    LGrid.Col = 1
    LGrid.Col = 5
    LGrid.Text = "BNIVEL"
    LGrid.ColWidth(5) = 0
    LGrid.Row = 0
    LGrid.Col = 1
    LGrid.Col = 6
    LGrid.Text = "BLLAVE"
    LGrid.ColWidth(6) = 0
    LGrid.Row = 0
    LGrid.Col = 1
End Sub

Private Sub Botones_Click()
    Select Case Botones.Col
        Case 1 'Activo
            If ConsultarCelda(Botones.Row, 4, Botones) = "0" Then
                Botones.Text = IIf(UCase(Botones.Text) = "SI", "No", "Si")
            End If
        Case 2 'Nivel
            If ConsultarCelda(Botones.Row, 5, Botones) = "0" Then
                If UCase(ConsultarCelda(Botones.Row, 1, Botones)) = "SI" Then
                    KeyFlags = vbKeyDown
                    Call LlenarCombo(Combo_Cell, 0, 9)
                    Combo_Cell.ListIndex = CInt(ConsultarCelda(Botones.Row, Botones.Col, Botones))
                    Call ObjectDsp(Combo_Cell, Botones)
                End If
            End If
        Case 3 'Llave
            If UsaLlave And ConsultarCelda(Botones.Row, 6, Botones) = "0" Then
                KeyFlags = vbKeyDown
                Call LlenarCombo(Combo_Cell, 1, 5)
                Combo_Cell.ListIndex = CInt(ConsultarCelda(Botones.Row, Botones.Col, Botones))
                Call ObjectDsp(Combo_Cell, Botones)
            End If
    End Select
End Sub

Private Sub Botones_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            Select Case KeyCode
                Case vbKeyReturn, vbKeySpace
                    Call Botones_Click
            End Select
    End Select
    KeyFlags = KeyCode
End Sub

Private Sub Combo_Cell_Click()
    If KeyFlags <> vbKeyUp And KeyFlags <> vbKeyDown Then
        Call Combo_Cell_LostFocus
    End If
End Sub

Private Sub Combo_Cell_GotFocus()
    KeyFlags = 0
    PosGridCol = Botones.Col
    PosGridRow = Botones.Row
End Sub

Private Sub Combo_Cell_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            Select Case KeyCode
                Case vbKeyReturn
                    Call Combo_Cell_LostFocus
                Case vbKeyEscape
                    Combo_Cell.Visible = False
            End Select
    End Select
    KeyFlags = KeyCode
End Sub

Private Sub Combo_Cell_LostFocus()
    Combo_Cell.Visible = False
    If Combo_Cell.Text <> "" Then Call EscribirCelda(PosGridRow, PosGridCol, Botones, Combo_Cell.Text)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbCtrlMask
        Case vbAltMask
        Case Else
            Select Case KeyCode
                Case vbKeyF7
                    Call cancelar
                Case vbKeyF12
                    Unload Me
            End Select
    End Select
End Sub

Private Sub Form_Load()
    Call cancelar
End Sub

Public Sub cancelar()
    Call InicializarGrid(Botones)
    Call LlenarGrid
End Sub

Public Sub ObjectDsp(Objeto As Object, LGrid As MSFlexGrid)
    Objeto.Left = LGrid.Left + LGrid.CellLeft
    Objeto.Top = LGrid.Top + LGrid.CellTop
    Objeto.Width = LGrid.ColWidth(LGrid.Col)
    If UCase(TypeName(Objeto)) = "TEXTBOX" Then Objeto.Height = LGrid.RowHeight(LGrid.Row)
    Objeto.Visible = True
    Objeto.SetFocus
End Sub

Public Function ConsultarCelda(linea As Integer, Columna As Integer, LGrid As MSFlexGrid) As Variant
    
    Dim ACol As Integer, ALinea As Integer
    
    ACol = LGrid.Col
    ALinea = LGrid.Row
    
    LGrid.Col = Columna
    LGrid.Row = linea
    ConsultarCelda = LGrid.Text
    LGrid.Col = ACol
    LGrid.Row = ALinea
    
End Function

Public Function EscribirCelda(linea As Integer, Columna As Integer, LGrid As MSFlexGrid, Valor As Variant) As Variant
    
    Dim ACol As Integer, ALinea As Integer
    
    ACol = LGrid.Col
    ALinea = LGrid.Row
    
    LGrid.Col = Columna
    LGrid.Row = linea
    LGrid.Text = Valor
    LGrid.Col = ACol
    LGrid.Row = ALinea
    
End Function

Public Sub LlenarCombo(ByRef LCombo As ComboBox, Optional Min As Integer = 0, Optional Max As Integer = 9)
    
    Dim LCont As Integer
    
    LCombo.Clear
    
    For LCont = Min To Max
        LCombo.AddItem LCont
    Next LCont
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case "GRABAR"
            Call Grabar
        Case "CANCELAR"
            Call cancelar
        Case "AYUDA"
    End Select
End Sub

Private Function UsaLlave() As Boolean

    Dim rsCaja As New ADODB.Recordset
    
    UsaLlave = False
    
    Call Apertura_Recordset(False, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
    Ent.POS, adOpenDynamic, adLockReadOnly, adCmdText
    
    If Not rsCaja.EOF Then
       UsaLlave = rsCaja!Llaves And rsCaja!b_Opos
    End If
    
End Function

Private Sub Grabar()

    Dim rsCajaConfig As New ADODB.Recordset, rsCaja As New ADODB.Recordset, LCont As Integer
    
    If EsDigital Then
        
        Ent.POS.BeginTrans
        Botones.Enabled = False
        
        For LCont = 1 To Botones.Rows - 1
            Call Apertura_Recordset(False, rsCajaConfig)
            
            rsCajaConfig.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "' AND Nombre_Boton = '" & ConsultarCelda(LCont, 0, Botones) & "'", _
            Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
            
            If rsCajaConfig.EOF Then
                Call mensaje(True, "Ha ocurrido un error, la forma se cerrar�.")
                Ent.POS.RollbackTrans
                Unload Me
            End If
            
            rsCajaConfig!Caja = nCaja
            rsCajaConfig!Nombre_Boton = ConsultarCelda(LCont, 0, Botones)
            rsCajaConfig!Activo = UCase(ConsultarCelda(LCont, 1, Botones)) = "SI"
            rsCajaConfig!Nivel = CInt(ConsultarCelda(LCont, 2, Botones))
            rsCajaConfig!Llave = CInt(ConsultarCelda(LCont, 3, Botones))
            rsCajaConfig.UpdateBatch
        Next LCont
        
        Ent.POS.CommitTrans
        Botones.Enabled = True
        
        Call InicializarGrid(Botones)
        
    Else
        
        Ent.POS.BeginTrans
        Botones.Enabled = False
          
        rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
        Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
        
        If rsCaja.EOF Then
            Mensajes.mensaje "Ha ocurrido un error, la forma se cerrar�."
            Ent.POS.RollbackTrans
            Unload Me
            Exit Sub
        End If
        
        For LCont = 1 To Botones.Rows - 1
            
            Select Case UCase(ConsultarCelda(LCont, 0, Botones))
            
                Case UCase("F1 Ayuda")
                    rsCaja!bAyuda = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nAyuda = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lAyuda = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cAyuda = IIf(rsCaja!nAyuda <> 0, 1, 0)
                                                        
                Case UCase("F2 Buscar")
                    rsCaja!bProductos = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nProductos = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lProductos = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cProductos = IIf(rsCaja!nProductos <> 0, 1, 0)
                    
                Case UCase("F3 Limite")
                    rsCaja!bLimite = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nLimite = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lLimite = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cLimite = IIf(rsCaja!nProductos <> 0, 1, 0)
                    
                Case UCase("F4 Cantidad")
                    rsCaja!bCantidad = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nCantidad = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lCantidad = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cCantidad = IIf(rsCaja!nCantidad <> 0, 1, 0)
                    
                Case UCase("F5 Balanza")
                    rsCaja!bBalanza = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nBalanza = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lBalanza = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cBalanza = IIf(rsCaja!nBalanza <> 0, 1, 0)
                    
                Case UCase("F6 Reintegro Total")
                    rsCaja!bReintegro = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nReintegro = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lReintegro = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cReintegro = IIf(rsCaja!nReintegro <> 0, 1, 0)
                    
                Case UCase("F6 Reintegro Parcial")
                    rsCaja!bParcial = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nParcial = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lParcial = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cParcial = IIf(rsCaja!nParcial <> 0, 1, 0)
                    
                Case UCase("F7 Espera")
                    rsCaja!bEspera = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nEspera = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lEspera = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cEspera = IIf(rsCaja!nEspera <> 0, 1, 0)
                    
                Case UCase("F9 Suspender")
                    rsCaja!bSuspender = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nSuspender = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lSuspender = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cSuspender = IIf(rsCaja!nSuspender <> 0, 1, 0)
                    
                Case UCase("F10 Devolver")
                    rsCaja!bDevolucion = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nDevolucion = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lDevolucion = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cDevolucion = IIf(rsCaja!nDevolucion <> 0, 1, 0)
                    
                Case UCase("F11 Precios")
                    rsCaja!bPrecios = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nPrecios = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lPrecios = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cPrecios = IIf(rsCaja!nPrecios <> 0, 1, 0)
                    
                Case UCase("F12 Clientes")
                    rsCaja!bCliente = UCase(ConsultarCelda(LCont, 1, Botones)) = UCase("Si")
                    rsCaja!nCliente = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lCliente = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cCliente = IIf(rsCaja!nCliente <> 0, 1, 0)
                    
                Case UCase("Apertura de Caja")
                    rsCaja!nOpenPOS = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!cClosePOS = IIf(rsCaja!nProductos <> 0, 1, 0)
                    
                Case UCase("Cierre de Caja")
                    rsCaja!nClosePOS = CInt(ConsultarCelda(LCont, 2, Botones))
                    rsCaja!lCerrar = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cClosePOS = IIf(rsCaja!nClosePOS <> 0, 1, 0)
                    
            End Select
            
        Next LCont
        
        rsCaja.UpdateBatch
        
        For LCont = 1 To Botones.Rows - 1
            
            Call Apertura_Recordset(False, rsCajaConfig)
            
            rsCajaConfig.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "' AND Nombre_Boton = '" & ConsultarCelda(LCont, 0, Botones) & "'", _
            Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
            
            If Not rsCajaConfig.EOF Then

                rsCajaConfig!Caja = nCaja
                rsCajaConfig!Nombre_Boton = ConsultarCelda(LCont, 0, Botones)
                rsCajaConfig!Activo = UCase(ConsultarCelda(LCont, 1, Botones)) = "SI"
                rsCajaConfig!Nivel = CInt(ConsultarCelda(LCont, 2, Botones))
                rsCajaConfig!Llave = CInt(ConsultarCelda(LCont, 3, Botones))
                rsCajaConfig.UpdateBatch

            End If
            
        Next LCont
        
        Ent.POS.CommitTrans
        Botones.Enabled = True
        
        Call InicializarGrid(Botones)
        
    End If
    
    Unload Me
    
End Sub


