VERSION 5.00
Begin VB.Form frm_Login_Configuracion 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6270
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   8970
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   HelpContextID   =   1000
   Icon            =   "frm_Login_Configuracion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6270
   ScaleWidth      =   8970
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H009E5300&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1335
      Left            =   -120
      TabIndex        =   7
      Top             =   -120
      Width           =   9255
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H00BC7200&
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   5280
         TabIndex        =   8
         Top             =   -120
         Width           =   3375
      End
      Begin VB.Image Image1 
         Height          =   900
         Left            =   240
         Picture         =   "frm_Login_Configuracion.frx":628A
         Top             =   240
         Width           =   2700
      End
   End
   Begin VB.CommandButton cmd_Cancelar 
      Caption         =   "Ca&ncelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   7290
      Picture         =   "frm_Login_Configuracion.frx":90FC
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Cancela y Sale de la Aplicaci�n"
      Top             =   4875
      Width           =   1215
   End
   Begin VB.CommandButton cmd_Cambiar 
      Caption         =   "&Cambiar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   4620
      Picture         =   "frm_Login_Configuracion.frx":E395
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Cambia Contrase�a del Usuario"
      Top             =   4875
      Width           =   1215
   End
   Begin VB.CommandButton cmd_Aceptar 
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   5955
      Picture         =   "frm_Login_Configuracion.frx":135F4
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Acepta Nombre de usuario y Contrase�a"
      Top             =   4875
      Width           =   1215
   End
   Begin VB.TextBox txt_contrase�a 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   405
      IMEMode         =   3  'DISABLE
      Left            =   4320
      PasswordChar    =   "�"
      TabIndex        =   1
      ToolTipText     =   "Ingrese su Contrase�a de Seguridad"
      Top             =   3330
      Width           =   3150
   End
   Begin VB.TextBox txt_usuario 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   405
      Left            =   4320
      TabIndex        =   0
      Text            =   "Supervisor"
      ToolTipText     =   "Ingrese su Nombre de Usuario Registrado"
      Top             =   2490
      Width           =   3150
   End
   Begin VB.Label lblTeclado 
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Height          =   1005
      Left            =   3240
      TabIndex        =   9
      Top             =   4800
      Width           =   1095
   End
   Begin VB.Image CmdTeclado 
      Height          =   600
      Left            =   3480
      MouseIcon       =   "frm_Login_Configuracion.frx":18673
      MousePointer    =   99  'Custom
      Picture         =   "frm_Login_Configuracion.frx":1897D
      Stretch         =   -1  'True
      Top             =   5040
      Width           =   600
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Contrase�a              :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   1530
      TabIndex        =   6
      Top             =   3330
      Width           =   2145
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre de Usuario  :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   1530
      TabIndex        =   5
      Top             =   2490
      Width           =   2775
   End
End
Attribute VB_Name = "frm_Login_Configuracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private FormaCargada As Boolean

Private Sub Cmd_Aceptar_Click()
    
    Call Apertura_Recordset(False, rsUsuarios)
    
    rsUsuarios.Open "select * from MA_usuarios where login_name = '" & txt_usuario & "'", _
    Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not rsUsuarios.EOF Then
        'And rsUsuarios!Nivel >= 10 Then ' Est� ya no aplica... ya no es por nivel.
        ' Es simplemente si el usuario tuvo acceso al m�dulo ya entra directo...
        If txt_contrase�a = rsUsuarios!Password Then
            fCancelar = False
            Inicio = False
            Unload Me
        Else
            If AutoLogin Then
                txt_usuario.Text = ""
                txt_contrase�a.Text = ""
                txt_usuario.Visible = True
                txt_contrase�a.Visible = True
                AutoLogin = False
                If PuedeObtenerFoco(txt_usuario) Then txt_usuario.SetFocus
                Exit Sub
            End If
            
            Call Mensaje(True, "No posee los derechos necesarios para acceder.")
            txt_contrase�a.SetFocus
        End If
    Else
        If AutoLogin Then
            txt_usuario.Text = ""
            txt_contrase�a.Text = ""
            txt_usuario.Visible = True
            txt_contrase�a.Visible = True
            AutoLogin = False
            If PuedeObtenerFoco(txt_usuario) Then txt_usuario.SetFocus
            Exit Sub
        End If
        
        Call Mensaje(True, "Verifique el nombre o la clave del Usuario.")
        txt_contrase�a.SetFocus
    End If
    
End Sub

Private Sub cmd_Cambiar_Click()
    Cambio_Password.Show vbModal
End Sub

Private Sub cmd_cancelar_Click()
    Set frm_Login_Configuracion = Nothing ''destruye el objeto
    fCancelar = True
    Unload Me
End Sub

Private Sub cmd_ayuda_Click()
    SendKeys "{f1}"
End Sub

Private Sub CmdTeclado_Click()
    
    Dim mCtl As Object
    
    If Not Me.ActiveControl Is Nothing Then
        If TypeOf Me.ActiveControl Is TextBox Then
            Set mCtl = Me.ActiveControl
        End If
    End If
    
    If mCtl Is Nothing Then Set mCtl = txt_usuario
    
    TecladoWindows mCtl
    
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        
        If AutoLogin Then
            Cmd_Aceptar_Click
            Exit Sub
        End If
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
        Select Case KeyCode
            Case Is = vbKeyF4
                Call cmd_cancelar_Click
        End Select
    End If
End Sub

Private Sub Form_Load()
    
    Me.Label1.Caption = stellar_mensaje(2, True)
    Me.Label2.Caption = stellar_mensaje(3, True)
    Me.cmd_Cambiar.Caption = stellar_mensaje(4, True)
    Me.Cmd_Aceptar.Caption = stellar_mensaje(5, True)
    Me.cmd_cancelar.Caption = stellar_mensaje(6, True)
    
    If AutoLogin Then
        txt_usuario.Text = Inicio_Usuario_Login
        txt_contrase�a.Text = inicio_Usuario_Contrase�a
        txt_usuario.Visible = False
        txt_contrase�a.Visible = False
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frm_Login_Configuracion = Nothing
    Set rsUsuarios = Nothing
End Sub

Private Sub lblTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub txt_contrase�a_GotFocus()
    txt_contrase�a.SelStart = 0
    txt_contrase�a.SelLength = Len(txt_usuario)
End Sub

'Private Sub txt_contrase�a_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyReturn Then cmd_aceptar.SetFocus
'End Sub

Private Sub txt_contrase�a_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Cmd_Aceptar_Click
    End If
End Sub

Private Sub txt_usuario_GotFocus()
    txt_usuario.SelStart = 0
    txt_usuario.SelLength = Len(txt_usuario)
End Sub

'Private Sub txt_usuario_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyReturn Then txt_contrase�a.SetFocus
'End Sub

Private Sub txt_usuario_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        txt_contrase�a.SetFocus
    End If
End Sub
