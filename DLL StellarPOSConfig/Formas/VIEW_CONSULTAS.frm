VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form VIEW_CONSULTAS 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7320
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   10635
   ControlBox      =   0   'False
   ForeColor       =   &H00808080&
   Icon            =   "VIEW_CONSULTAS.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7320
   ScaleWidth      =   10635
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_salir 
      Caption         =   "&Salir"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1140
      Left            =   9000
      Picture         =   "VIEW_CONSULTAS.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   5760
      Width           =   1245
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8355
         TabIndex        =   10
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   75
         Width           =   5295
      End
   End
   Begin MSComctlLib.ListView GRID 
      CausesValidation=   0   'False
      Height          =   4635
      Left            =   240
      TabIndex        =   7
      Top             =   840
      Width           =   10035
      _ExtentX        =   17701
      _ExtentY        =   8176
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FlatScrollBar   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   5790296
      BackColor       =   16777215
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "C�DIGO"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "DESCRIPCI�N"
         Object.Width           =   10583
      EndProperty
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Ordenar y Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1170
      Left            =   240
      TabIndex        =   4
      Top             =   5760
      Width           =   2955
      Begin VB.OptionButton opt_cod 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Por &C�digo"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   270
         TabIndex        =   6
         Top             =   360
         Width           =   1335
      End
      Begin VB.OptionButton opt_des 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Por &Descripci�n"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   270
         TabIndex        =   5
         Top             =   720
         Value           =   -1  'True
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Datos a Buscar:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1170
      Left            =   3120
      TabIndex        =   1
      Top             =   5760
      Width           =   5750
      Begin VB.TextBox txt_dato 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   135
         TabIndex        =   0
         Text            =   "%"
         Top             =   330
         Width           =   4250
      End
      Begin MSComctlLib.ProgressBar barra_prg 
         Height          =   195
         Left            =   135
         TabIndex        =   2
         Top             =   750
         Width           =   4250
         _ExtentX        =   7488
         _ExtentY        =   344
         _Version        =   393216
         Appearance      =   0
         Max             =   1000
         Scrolling       =   1
      End
      Begin VB.Label lblTeclado 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Height          =   850
         Left            =   4560
         TabIndex        =   12
         Top             =   200
         Width           =   1095
      End
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   4800
         MouseIcon       =   "VIEW_CONSULTAS.frx":76D4
         MousePointer    =   99  'Custom
         Picture         =   "VIEW_CONSULTAS.frx":79DE
         Stretch         =   -1  'True
         Top             =   360
         Width           =   600
      End
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00AE5B00&
      Index           =   0
      X1              =   3400
      X2              =   10160
      Y1              =   650
      Y2              =   650
   End
   Begin VB.Label ltitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Top             =   540
      Width           =   2955
   End
End
Attribute VB_Name = "VIEW_CONSULTAS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CmdTeclado_Click()
    TecladoWindows txt_dato
End Sub

Private Sub Form_Load()
    ltitulo.Caption = Titulo
    Me.lbl_Organizacion.Caption = stellar_mensaje(51)
    Me.opt_cod.Caption = stellar_mensaje(61) 'por codigo
    Me.opt_des.Caption = stellar_mensaje(62) 'por descripcion
    Me.cmd_salir.Caption = stellar_mensaje(54) 'salir
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Cerrar_Recordset(rsEureka)
    Set VIEW_CONSULTAS = Nothing
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub grid_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    If GRID.SortOrder = lvwAscending Then
        GRID.SortOrder = lvwDescending
    Else
        GRID.SortOrder = lvwAscending
    End If
    GRID.SortKey = ColumnHeader.Index - 1
    GRID.Sorted = True
End Sub

Private Sub grid_DblClick()
    With GRID
        If GRID.ListItems.Count <> 0 Then
            Select Case UCase(Forma.Tag)
        
                Case Is = "GENERICO"
                    Select Case UCase(Tabla)
                        Case Is = "MA_DEPOSITO"
                            Forma.coddeposito.Text = GRID.ListItems(GRID.SelectedItem.Index)
                            Forma.lbl_deposito.Text = GRID.ListItems(GRID.SelectedItem.Index).SubItems(1)
                            SendKeys "{enter}"
                        Case Is = "MA_MONEDAS"
                            Forma.TxtMoneda.Text = GRID.ListItems(GRID.SelectedItem.Index)
                            Forma.Lbl_Moneda.Text = GRID.ListItems(GRID.SelectedItem.Index).SubItems(1)
                            SendKeys "{enter}"
                    End Select
            End Select
            Unload Me
        Else
            Call Mensaje(True, "No se ha realizado ning�na consulta.")
        End If
    End With
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Private Sub lblTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub opt_cod_Click()
    txt_dato.SetFocus
End Sub

Private Sub opt_des_Click()
    txt_dato.SetFocus
End Sub

Private Sub txt_dato_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Then
        Unload Me
    End If
End Sub

Private Sub txt_dato_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = 39
            KeyAscii = 0
            
        Case Is = vbKeyReturn
            
            Call Limpiar_Grid
            
            i = 1
            
            If txt_dato = "" Then
                txt_dato.SetFocus
                Exit Sub
            End If
            
            Screen.MousePointer = 11
            
            If opt_cod.Value = True Then
                Orden = Campo_Op1
            Else
                Orden = Campo_Op2
            End If
            
            Criterio1 = "SELECT COUNT(" & Campo_Op1 & ") AS CANT FROM " & Tabla & " WHERE " & Orden & " LIKE '"
            Criterio = "SELECT " & Campo_Op1 & "," & Campo_Op2 & " FROM " & Tabla & " WHERE " & Orden & " LIKE '"
            
            Criterio1 = Criterio1 & txt_dato
            
            If Trim(txt_dato) <> "%" Then
                Criterio1 = Criterio1 & "%'"
            Else
                Criterio1 = Criterio1 & "'"
            End If
            
            Criterio = Criterio & txt_dato
            
            If Trim(txt_dato) <> "%" Then
                Criterio = Criterio & "%'"
            Else
                Criterio = Criterio & "'"
            
            End If
            
            If UCase(Tabla) = "MA_DEPOSITO" Then
                Criterio = Criterio & " and c_codlocalidad = '" & Trim(Sucursal) & "' ORDER BY " & Orden
            End If
            
            Call Apertura_Recordset(False, rsEureka)
            
            'MsgBox criterio

            rsEureka.Open Criterio, Ent.BDD, adOpenForwardOnly, adLockReadOnly
            
            If Not rsEureka.EOF Then
                Call Apertura_Recordset(False, Rec_Monitor)
                
                Rec_Monitor.Open Criterio1, Ent.BDD, adOpenForwardOnly, adLockReadOnly
                
                barra_prg.Max = Rec_Monitor!CANT
                
                Rec_Monitor.Close
                
                Set Rec_Monitor = Nothing
                
                GRID.ListItems.Clear
                
                rsEureka.MoveFirst
                
                i = 1
                
                GRID.Enabled = False
                
                Do Until rsEureka.EOF
                     Set itmx = GRID.ListItems.add(, , rsEureka.Fields(0))
                     itmx.SubItems(1) = rsEureka.Fields(1)
                     rsEureka.MoveNext
                     i = i + 1
                     barra_prg.Value = barra_prg.Value + 1
                Loop
                
                GRID.Enabled = True
                barra_prg.Value = 0
                GRID.SetFocus
            Else
                Call Mensaje(True, "B�squeda sin Exito.")
                barra_prg.Value = 0
                txt_dato.SetFocus
            End If
            
            Call Cerrar_Recordset(rsEureka)
            Screen.MousePointer = 0
            
    End Select
End Sub

Private Sub Limpiar_Grid()
    With GRID
        .ListItems.Clear
    End With
End Sub

Private Sub Cargar_Productos(Campo As String, Boton1 As Boolean, Boton2 As Boolean, Boton3 As Boolean, Boton4 As Boolean, Boton5 As Boolean, Boton6 As Boolean, Ver As Boolean)
    
    Call Apertura_Recordset(False, rsProductos)
    
    rsProductos.Open "select * from ma_productos where c_codigo = '" & GRID.ListItems(GRID.SelectedItem.Index) & "'", _
    Ent.BDD, adOpenForwardOnly, adLockBatchOptimistic
    
    Call Ficha_Productos.Cargar_Campos
    Call Ficha_Productos.Habilitar_Datos(False)
    Call Apertura_Recordset(False, rsTrCostosInt)
    
    SQLS = "SELECT * FROM TR_COSTOS_INT WHERE c_Codigo = '" & Trim(Campo) & "'"
    
    rsTrCostosInt.Open SQLS, Ent.BDD, adOpenForwardOnly, adLockReadOnly
    
    Ficha_Productos.Toolbar1.Buttons(1).Enabled = Boton1
    Ficha_Productos.Toolbar1.Buttons(2).Enabled = Boton2
    Ficha_Productos.Toolbar1.Buttons(6).Enabled = Boton3
    Ficha_Productos.Toolbar1.Buttons(3).Enabled = Boton4
    Ficha_Productos.Toolbar1.Buttons(10).Enabled = Boton6
    
    If Ver = True Then Ficha_Productos.Show vbModal
    
End Sub
