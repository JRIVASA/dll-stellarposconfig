VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frm_Config_POS1 
   BackColor       =   &H00FAFAFA&
   BorderStyle     =   0  'None
   ClientHeight    =   9585
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   13740
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   1300
   Icon            =   "frm_config_pos1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   639
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   916
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmb_salir 
      Appearance      =   0  'Flat
      BackColor       =   &H00FAFAFA&
      Caption         =   "cmb_salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   11880
      Picture         =   "frm_config_pos1.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   8280
      Width           =   975
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H009E5300&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1500
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   15375
      Begin VB.Image Image1 
         Height          =   495
         Left            =   14160
         Top             =   600
         Width           =   495
      End
      Begin VB.Image img_atras 
         Height          =   495
         Left            =   13440
         Top             =   600
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Image img_acercade 
         Height          =   495
         Left            =   13440
         Top             =   600
         Width           =   495
      End
      Begin VB.Image Image2 
         Height          =   900
         Left            =   480
         Picture         =   "frm_config_pos1.frx":800C
         Top             =   360
         Width           =   2700
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H00BC7200&
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   6960
         TabIndex        =   7
         Top             =   -120
         Width           =   5295
      End
   End
   Begin VB.PictureBox Picture2 
      BackColor       =   &H00FAFAFA&
      BorderStyle     =   0  'None
      Height          =   1425
      Left            =   360
      Picture         =   "frm_config_pos1.frx":AE7E
      ScaleHeight     =   1425
      ScaleWidth      =   3135
      TabIndex        =   5
      Top             =   0
      Width           =   3135
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   9600
      Picture         =   "frm_config_pos1.frx":C8CE
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   3
      Top             =   8400
      Visible         =   0   'False
      Width           =   495
   End
   Begin MSComctlLib.TreeView ARBOL 
      CausesValidation=   0   'False
      Height          =   6555
      HelpContextID   =   1300
      Left            =   210
      TabIndex        =   0
      Top             =   1620
      Width           =   6585
      _ExtentX        =   11615
      _ExtentY        =   11562
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   176
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "Iconos2017"
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDropMode     =   1
   End
   Begin MSComctlLib.ImageList iconos 
      Left            =   5580
      Top             =   8385
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   50
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":117E8
            Key             =   "org"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":11C3C
            Key             =   "caja"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":13918
            Key             =   "reportes"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":13C34
            Key             =   "pos"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":14C88
            Key             =   "solicitud"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":15904
            Key             =   "subnivel"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":15D58
            Key             =   "user"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":15E6C
            Key             =   "compras"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":164D0
            Key             =   "inventario"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":16AA4
            Key             =   "printer3"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":16DC0
            Key             =   "produccion"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":17400
            Key             =   "ventas"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":17AC4
            Key             =   "tesoreria"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":180F8
            Key             =   "cxp"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1874C
            Key             =   "cxc"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":18DD0
            Key             =   "compra"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":19FAC
            Key             =   "orden"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1A840
            Key             =   "fichero"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1B720
            Key             =   "estadistic"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1BFD8
            Key             =   "recepcion"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1CC74
            Key             =   "traslado"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1D910
            Key             =   "ajuste"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1E2EC
            Key             =   "invfisico"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1EF68
            Key             =   "transferen"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1F284
            Key             =   "manufactur"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1F5A0
            Key             =   "presupuest"
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1F8BC
            Key             =   "pedidos"
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1FBD8
            Key             =   "notentrega"
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":1FEF4
            Key             =   "facturacio"
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":20210
            Key             =   "promocione"
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":2052C
            Key             =   "ctlcaja"
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":20848
            Key             =   "movimiento"
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":20B64
            Key             =   "hablador"
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":20E80
            Key             =   "listado"
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":2119C
            Key             =   "listado1"
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":214B8
            Key             =   "listado11"
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":217D4
            Key             =   "listado10"
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":21AF0
            Key             =   "listado2"
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":21E0C
            Key             =   "listado3"
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":22128
            Key             =   "listado4"
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":22444
            Key             =   "listado5"
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":22760
            Key             =   "listado6"
         EndProperty
         BeginProperty ListImage43 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":22A7C
            Key             =   "listado7"
         EndProperty
         BeginProperty ListImage44 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":22D98
            Key             =   "listado8"
         EndProperty
         BeginProperty ListImage45 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":230B4
            Key             =   "listado9"
         EndProperty
         BeginProperty ListImage46 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":233D0
            Key             =   "Balanzas"
         EndProperty
         BeginProperty ListImage47 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":236EC
            Key             =   "printer1"
         EndProperty
         BeginProperty ListImage48 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":23A08
            Key             =   "printer2"
         EndProperty
         BeginProperty ListImage49 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":23D24
            Key             =   "ubicacion"
            Object.Tag             =   "ubicacion"
         EndProperty
         BeginProperty ListImage50 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":24176
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView menu 
      CausesValidation=   0   'False
      Height          =   6435
      Left            =   7170
      TabIndex        =   2
      Top             =   1620
      Width           =   6180
      _ExtentX        =   10901
      _ExtentY        =   11351
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FlatScrollBar   =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      Icons           =   "image_G(2)"
      ForeColor       =   -2147483640
      BackColor       =   16448250
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin MSComctlLib.ImageList image_G 
      Index           =   1
      Left            =   4680
      Top             =   8400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":247F3
            Key             =   "general"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":267C7
            Key             =   "salida"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":2879B
            Key             =   "entrada"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":28AB7
            Key             =   "botones"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":29793
            Key             =   "digital"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList image_G 
      Index           =   0
      Left            =   4080
      Top             =   8400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":29AB7
            Key             =   "general"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":29DD3
            Key             =   "salida"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":2BDA7
            Key             =   "entrada"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":2C0C3
            Key             =   "botones"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":2CD9F
            Key             =   "digital"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList image_G 
      Index           =   2
      Left            =   7560
      Top             =   8400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   96
      ImageHeight     =   96
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":2D0C3
            Key             =   "digital"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":3D655
            Key             =   "botones"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":4DBE7
            Key             =   "general"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Iconos2017 
      Left            =   240
      Top             =   8400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   18
      ImageHeight     =   18
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":5E179
            Key             =   "org"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":5E783
            Key             =   "manufactur"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":5EC15
            Key             =   "user"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":5F0A7
            Key             =   "caja"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":5F539
            Key             =   "printer1"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":5F9CB
            Key             =   "fichero"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":5FE5D
            Key             =   "subnivel"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":602EF
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":60781
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_config_pos1.frx":60C13
            Key             =   "ubicacion"
            Object.Tag             =   "ubicacion"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackColor       =   &H00FAFAFA&
      Caption         =   "CONFIGURACIÓN"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   495
      Left            =   7680
      TabIndex        =   4
      Top             =   360
      Width           =   4095
   End
   Begin VB.Label lbl_salir 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   9660
      MouseIcon       =   "frm_config_pos1.frx":610A5
      MousePointer    =   99  'Custom
      TabIndex        =   1
      ToolTipText     =   "Salir del Sistema"
      Top             =   8880
      UseMnemonic     =   0   'False
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Menu opcion 
      Caption         =   "opcion"
      Visible         =   0   'False
      Begin VB.Menu Agregar 
         Caption         =   "&Agregar"
         Begin VB.Menu add 
            Caption         =   "&Zona"
            Index           =   0
         End
         Begin VB.Menu add 
            Caption         =   "POS &Retail"
            Index           =   1
         End
         Begin VB.Menu add 
            Caption         =   "POS &Food"
            Index           =   2
         End
         Begin VB.Menu add 
            Caption         =   "PO&E Food"
            Index           =   3
         End
         Begin VB.Menu add 
            Caption         =   "&Impresoras"
            Index           =   4
         End
         Begin VB.Menu add 
            Caption         =   "&KDS"
            Index           =   5
         End
         Begin VB.Menu add 
            Caption         =   "&Mesas"
            Index           =   6
         End
         Begin VB.Menu add 
            Caption         =   "&Dispositivos"
            Index           =   7
         End
         Begin VB.Menu add 
            Caption         =   "&Vendedores"
            Index           =   8
         End
      End
      Begin VB.Menu modificar 
         Caption         =   "&Modificar"
      End
      Begin VB.Menu GUION 
         Caption         =   "-"
      End
      Begin VB.Menu eliminar 
         Caption         =   "&Eliminar"
      End
      Begin VB.Menu config 
         Caption         =   "&Copiar"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu boton 
      Caption         =   "Boton"
      Visible         =   0   'False
      Begin VB.Menu configurar_boton 
         Caption         =   "Configurar"
      End
      Begin VB.Menu eliminar_boton 
         Caption         =   "Eliminar"
      End
   End
   Begin VB.Menu subboton 
      Caption         =   "Subboton"
      Visible         =   0   'False
      Begin VB.Menu configurar_subboton 
         Caption         =   "&Configurar"
      End
      Begin VB.Menu eliminar_subboton 
         Caption         =   "&Eliminar"
      End
   End
   Begin VB.Menu Niveles 
      Caption         =   "Niveles"
      Visible         =   0   'False
      Begin VB.Menu AgregarClas 
         Caption         =   "&Agregar"
         Index           =   0
         Begin VB.Menu Clasificacion 
            Caption         =   "&Departamento"
            Index           =   0
         End
         Begin VB.Menu Clasificacion 
            Caption         =   "&Grupo"
            Index           =   1
         End
         Begin VB.Menu Clasificacion 
            Caption         =   "&Subgrupo"
            Index           =   2
         End
         Begin VB.Menu Clasificacion 
            Caption         =   "&Propiedades"
            Index           =   3
         End
      End
      Begin VB.Menu AgregarClas 
         Caption         =   "&Eliminar"
         Index           =   1
      End
   End
   Begin VB.Menu NivelesKDS 
      Caption         =   "Niveles"
      Visible         =   0   'False
      Begin VB.Menu AgregarKDS 
         Caption         =   "&Agregar"
         Index           =   0
         Begin VB.Menu ClasificacionKDS 
            Caption         =   "&Departamento"
            Index           =   0
         End
         Begin VB.Menu ClasificacionKDS 
            Caption         =   "&Grupo"
            Index           =   1
         End
         Begin VB.Menu ClasificacionKDS 
            Caption         =   "&Subgrupo"
            Index           =   2
         End
      End
      Begin VB.Menu AgregarKDS 
         Caption         =   "&Modificar"
         Index           =   1
      End
      Begin VB.Menu AgregarKDS 
         Caption         =   "&Eliminar"
         Index           =   2
      End
   End
End
Attribute VB_Name = "frm_Config_POS1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ObjMove As Node

' Longitud del Prefijo de los Nodos en el Arbol.
'Private Const LPA = 3 ' Mejor Definirla Publicamente ya que se usa en varios lugares
' La creare dentro de un modulo

Private Sub add_Click(Index As Integer)
    
    Select Case Index
        
        Case Is = 0 'SUB - NIVEL
            
            If Mid(ARBOL.SelectedItem.Key, 1, LPA) = "|N|" _
            Or Mid(ARBOL.SelectedItem.Key, 1, LPA) = "|R|" _
            Or left(ARBOL.SelectedItem.Key, 1) = "N" _
            Or ARBOL.SelectedItem.Key = "ROOT" Then
                
                NewDpto.Show vbModal
                
                If AddNode Then ''verifica que se deba ingresar un node subnivel
                    '************************************
                    Call Apertura_Recordset(False, rsMenu)
                    
                    rsMenu.Open "SELECT Org_Caja FROM MA_CONSECUTIVOS", _
                    Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
                    
                    Clave = Format(rsMenu!Org_Caja, "00")
                    rsMenu.Update
                        rsMenu!Org_Caja = rsMenu!Org_Caja + 1
                    rsMenu.UpdateBatch
                    '*******************************************
                    Call Apertura_Recordset(False, rsMenu)
                    rsMenu.Open "MA_ESTRUC_CAJA", Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdTable
                    rsMenu.AddNew
                        rsMenu!Texto = lcTexto
                        rsMenu!Clave = "|N|" & Clave
                        rsMenu!Relacion = lcKey
                    rsMenu.UpdateBatch
                    '***************************************************
                    Set lcNode = ARBOL.Nodes.add(lcKey, tvwChild, _
                    Key:="|N|" & Clave, Text:=lcTexto, Image:="subnivel")
                    lcNode.Tag = "|N|" & Clave
                    Call AnadirEstructuras(lcNode)
                End If
                
            End If
            
        Case Is = 1, 2, 3 '1 = CAJA 2 = FOOD 3 = POS PEDIDOS
            
            If Index = 1 Then 'CAJA
                EsDigital = False
                EsPedidos = False
                Cadena = "POS Retail (isPOS)  | "
            ElseIf Index = 2 Then 'FOOD
                EsDigital = True
                EsPedidos = False
                Cadena = "POS Food (isFOOD) | "
            ElseIf Index = 3 Then
                EsDigital = True
                EsPedidos = True
                Cadena = "POE Food (isFOOD) | "
            End If
            
            ARBOL.Enabled = False
            Copiar = False
            lcKey = ARBOL.SelectedItem.Key
            
            Newcaja.Show vbModal
            
            If AddNode Then Call Crear_Menu(lcKey, Clave, Cadena & lcTexto, "caja")
            
            ARBOL.Enabled = True
            
        Case Is = 4 '4 = IMPRESORAS
            
            FrmImpresoras.Show vbModal
            
            Set FrmImpresoras = Nothing
            
            If AddNode Then Call Crear_Menu(lcKey, Clave, lcTexto, "printer1")
            
            ARBOL.Enabled = True
            
        Case Is = 5 '5 = KDS
            
            FrmKDS.EsModificar = False
            FrmKDS.Show vbModal
            
            If AddNode Then Call Crear_Menu(lcKey, Clave, lcTexto, "fichero")
            
            ARBOL.Enabled = True
            
        Case Is = 6 '6 = UBICACION
            
            lcKey = Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key))
            
            NodoTextoKey = lcKey
            
            FrmUbicacion.Show vbModal
            
            If AddNode Then Call Crear_Menu(lcKey, Clave, lcTexto, "ubicacion")
            
            ARBOL.Enabled = True
            
        Case Is = 7 '7 = DISPOSITIVO
            
            nCaja = Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key))
            FrmDispositivos.Show vbModal
            
            ARBOL.Enabled = True
            
        Case Is = 8 '8 = Mesero
            
            lcKey = Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key))
            
            Call ClearNode(ARBOL.SelectedItem)
            
            FrmAsignarVendedores.Relacion = lcKey
            Set FrmAsignarVendedores.ConexionTemp = Ent.BDD
            FrmAsignarVendedores.SucursalTemp = Sucursal
            FrmAsignarVendedores.Show vbModal
            Set FrmAsignarVendedores = Nothing
            
            Call BuscarVendedores(lcKey, ARBOL.SelectedItem.Key)
            
        End Select
        
    If PuedeObtenerFoco(ARBOL) Then ARBOL.SetFocus
    
End Sub

Private Sub AgregarClas_Click(Index As Integer)
    
    Dim Dpto As String, Grpo As String, Sbgrp As String, Cadena() As String, CodigoImpresora As String
    Cadena = Split(ARBOL.SelectedItem.Key, "/")
    
    Select Case Index
        
        Case 0
        
        Case 1
            
            'ELIMINAMOS
            If ARBOL.SelectedItem.Children > 0 Then Exit Sub
            
            Select Case Mid(ARBOL.SelectedItem.Key, 1, LPA) ' _
            Posee 1 caracter mas que los otros nodos.
                
                Case "|D|"
                    
                    Dpto = Cadena(UBound(Cadena))
                    CodigoImpresora = Mid(Cadena(LBound(Cadena)), (LPA + 1))
                    
                    Ent.POS.Execute "DELETE FROM MA_IMPRESORA_DEPARTAMENTO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    Ent.POS.Execute "DELETE FROM MA_IMPRESORA_GRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    Ent.POS.Execute "DELETE FROM MA_IMPRESORA_SUBGRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    
                    ARBOL.Nodes.Remove ARBOL.SelectedItem.Index
                    
                Case "|G|"
                    
                    Dpto = Cadena(UBound(Cadena))
                    Grpo = Cadena(UBound(Cadena) - 1)
                    CodigoImpresora = Mid(Cadena(LBound(Cadena)), (LPA + 1))
                    
                    Ent.POS.Execute "DELETE FROM MA_IMPRESORA_GRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Grupo = '" & Grpo & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    Ent.POS.Execute "DELETE FROM MA_IMPRESORA_SUBGRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Grupo = '" & Grpo & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    
                    ARBOL.Nodes.Remove ARBOL.SelectedItem.Index
                    
                Case "|B|"
                    
                    Dpto = Cadena(UBound(Cadena))
                    Grpo = Cadena(UBound(Cadena) - 1)
                    SGrpo = Cadena(UBound(Cadena) - 2)
                    CodigoImpresora = Mid(Cadena(LBound(Cadena)), (LPA + 1))
                    
                    Ent.POS.Execute "DELETE FROM MA_IMPRESORA_SUBGRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Grupo = '" & Grpo & "' AND cs_Subgrupo = '" & SGrpo & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    
                    ARBOL.Nodes.Remove ARBOL.SelectedItem.Index
                    
                Case Else
                    
                    If Mid(ARBOL.SelectedItem.Key, 1, 1) = "|P|" Then
                        Call EliminarImpresora(Mid(ARBOL.SelectedItem.Key, _
                        LPA + 1, Len(ARBOL.SelectedItem.Key)))
                        ARBOL.Nodes.Remove ARBOL.SelectedItem.Index
                    End If
                    
            End Select
    End Select
    
End Sub

Private Sub AgregarKDS_Click(Index As Integer)
    
    On Error GoTo Error
    
    Dim Dpto As String, Grpo As String, Sbgrp As String, Cadena() As String, CodigoImpresora As String
    Cadena = Split(ARBOL.SelectedItem.Key, "/")
    
    Select Case Index
        
        Case 0
            
        Case 1 ' modificar
            
            FrmKDS.EsModificar = True
            FrmKDS.TmpCodigo = Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key))
            FrmKDS.Txtcodigo.Enabled = False
            FrmKDS.Show vbModal
            If AddNode Then ARBOL.SelectedItem.Text = lcTexto
            
        Case 2 ' eliminar
            
            'ELIMINAMOS
            If ARBOL.SelectedItem.Children > 0 Then Exit Sub
            'Debug.Print Mid(ARBOL.SelectedItem.Key, 1, 1)
            
            Select Case Mid(ARBOL.SelectedItem.Key, 1, LPA)
                
                Case "|T|" ' departamento
                    
                    Dpto = Cadena(UBound(Cadena))
                    CodigoImpresora = Mid(Cadena(LBound(Cadena)), (LPA + 1))
                    
                    Ent.POS.Execute "DELETE FROM MA_KDS_DEPARTAMENTO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    Ent.POS.Execute "DELETE FROM MA_KDS_GRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    Ent.POS.Execute "DELETE FROM MA_KDS_SUBGRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    
                    ARBOL.Nodes.Remove ARBOL.SelectedItem.Index
                    
                Case "|L|" 'grupo
                    
                    Dpto = Cadena(UBound(Cadena))
                    Grpo = Cadena(UBound(Cadena) - 1)
                    CodigoImpresora = Mid(Cadena(LBound(Cadena)), (LPA + 1))
                    
                    Ent.POS.Execute "DELETE FROM MA_KDS_GRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Grupo = '" & Grpo & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    Ent.POS.Execute "DELETE FROM MA_KDS_SUBGRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Grupo = '" & Grpo & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    
                    ARBOL.Nodes.Remove ARBOL.SelectedItem.Index
                    
                Case "|W|" 'subgrupo
                    
                    Dpto = Cadena(UBound(Cadena))
                    Grpo = Cadena(UBound(Cadena) - 1)
                    Sbgrp = Cadena(UBound(Cadena) - 2)
                    CodigoImpresora = Mid(Cadena(LBound(Cadena)), (LPA + 1))
                    
                    Ent.POS.Execute "DELETE FROM MA_KDS_SUBGRUPO WHERE cs_Departamento = '" & _
                    Dpto & "' AND cs_Grupo = '" & Grpo & "' AND cs_Subgrupo = '" & _
                    Sbgrp & "' AND cs_Relacion = '" & CodigoImpresora & "'"
                    
                    ARBOL.Nodes.Remove ARBOL.SelectedItem.Index
                    
                Case Else
                    
                    If Mid(ARBOL.SelectedItem.Key, 1, LPA) = "|A|" Then  ' KDS
                        Call EliminarKDS(Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key)))
                        ARBOL.Nodes.Remove ARBOL.SelectedItem.Index
                    End If
                    
            End Select
            
    End Select
    
    Exit Sub
    
Error:
    
    'Debug.Print ERR.Description
    
End Sub

Private Sub arbol_Click()
    
    NodoTexto = ARBOL.SelectedItem.Text
    
    If Not ARBOL.SelectedItem.Parent Is Nothing Then
        NodoTextoKey = ARBOL.SelectedItem.Parent.Key
    Else
        NodoTextoKey = ""
    End If
    
End Sub

'Private Sub ARBOL_DragOver(Source As Control, x As Single, y As Single, State As Integer)
    'MsgBox "hola"
'End Sub

Private Sub arbol_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = 93 'BOTON DE PROPIEDADES
            Call arbol_MouseDown(vbRightButton, 0, 0, 0)
    End Select
End Sub

Private Sub arbol_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    lcKey = ARBOL.SelectedItem.Key
    
    Select Case Button
        
        Case Is = 1
        
        Case Is = 2
            
            If UCase(lcKey) = "ROOT" Then
                
                Agregar.Enabled = True
                add(0).Enabled = True
                add(1).Enabled = False
                add(2).Enabled = False
                add(3).Enabled = False
                add(4).Enabled = False
                add(5).Enabled = False
                add(6).Enabled = False
                add(7).Enabled = False
                add(8).Enabled = False
                eliminar.Enabled = False
                modificar.Enabled = False
                config.Enabled = False
                cambiar = False
                config.Enabled = False
                
            ElseIf Mid(lcKey, 1, LPA) = "|N|" _
            Or left(lcKey, 1) = "N" Then 'Nivel
                
                Agregar.Enabled = True
                add(0).Enabled = True
                add(1).Enabled = False
                add(2).Enabled = False
                add(3).Enabled = False
                add(4).Enabled = False
                add(5).Enabled = False
                add(6).Enabled = False
                add(7).Enabled = False
                add(8).Enabled = False
                config.Enabled = False
                modificar.Enabled = False
                eliminar.Enabled = ARBOL.SelectedItem.Child.Children < 1 _
                And ARBOL.SelectedItem.Child.Next.Children < 1 _
                And ARBOL.SelectedItem.Child.Next.Next.Children < 1 _
                And ARBOL.SelectedItem.Children < 5
                cambiar = True
                
            ElseIf Mid(lcKey, 1, LPA) = "|C|" Then 'Caja
                
                Agregar.Enabled = True
                add(0).Enabled = False
                add(1).Enabled = False
                add(2).Enabled = False
                add(3).Enabled = False
                add(4).Enabled = False
                add(5).Enabled = False
                add(6).Enabled = False
                add(7).Enabled = True
                add(8).Enabled = False
                modificar.Enabled = True
                eliminar.Enabled = True
                cambiar = False
                config = True
            
            ElseIf Mid(lcKey, 1, LPA) = "|P|" Then 'Printer->DEPARTAMENTO
                
                AgregarClas(1).Enabled = ARBOL.SelectedItem.Children < 1
                Clasificacion(0).Visible = True
                Clasificacion(1).Visible = False
                Clasificacion(2).Visible = False
                PopupMenu Niveles
                Exit Sub
                
            ElseIf Mid(lcKey, 1, LPA) = "|D|" Then 'Printer->GRUPO
                
                AgregarClas(1).Enabled = ARBOL.SelectedItem.Children < 1
                Clasificacion(1).Visible = True
                Clasificacion(2).Visible = False
                Clasificacion(0).Visible = False
                PopupMenu Niveles
                Exit Sub
            
            ElseIf Mid(lcKey, 1, LPA) = "|G|" Then 'Printer->SUBGRUPO
                
                AgregarClas(1).Enabled = ARBOL.SelectedItem.Children < 1
                Clasificacion(2).Enabled = True
                Clasificacion(2).Visible = True
                Clasificacion(0).Visible = False
                Clasificacion(1).Visible = False
                PopupMenu Niveles
                Exit Sub
                
            ElseIf Mid(lcKey, 1, LPA) = "|B|" Then 'Printer->SUBGRUPO AL FINAL DEL ARBOL
                
                AgregarClas(1).Enabled = True
                Clasificacion(2).Enabled = False
                Clasificacion(2).Visible = True
                Clasificacion(0).Visible = False
                Clasificacion(1).Visible = False
                PopupMenu Niveles
                Exit Sub
                
            ElseIf Mid(lcKey, 1, LPA) = "|I|" Then 'Impresora
                
                Agregar.Enabled = True
                add(0).Enabled = False
                add(1).Enabled = False
                add(2).Enabled = False
                add(3).Enabled = False
                add(4).Enabled = True
                add(5).Enabled = False
                add(6).Enabled = False
                add(7).Enabled = False
                add(8).Enabled = False
                modificar.Enabled = False
                eliminar.Enabled = False
                cambiar = False
                config = True
                
            ElseIf Mid(lcKey, 1, LPA) = "|K|" Then 'KDS
                
                Agregar.Enabled = True
                add(0).Enabled = False
                add(1).Enabled = False
                add(2).Enabled = False
                add(3).Enabled = False
                add(4).Enabled = False
                add(5).Enabled = True
                add(6).Enabled = False
                add(7).Enabled = False
                add(8).Enabled = False
                modificar.Enabled = False
                eliminar.Enabled = False
                cambiar = False
                config = True
                
            ElseIf Mid(lcKey, 1, LPA) = "|A|" Then 'KDS->DEPARTAMENTO
                
                AgregarKDS(0).Enabled = True
                AgregarKDS(1).Visible = True
                
                ClasificacionKDS(0).Visible = True
                ClasificacionKDS(0).Enabled = True
                
                ClasificacionKDS(2).Visible = False
                ClasificacionKDS(1).Visible = False
                
                PopupMenu NivelesKDS
                
                Exit Sub
                
            ElseIf Mid(lcKey, 1, LPA) = "|T|" Then 'KDS->GRUPO
                
                AgregarKDS(0).Enabled = True
                AgregarKDS(1).Visible = False
                                
                ClasificacionKDS(1).Visible = True
                ClasificacionKDS(1).Enabled = True
                
                ClasificacionKDS(2).Visible = False
                ClasificacionKDS(0).Visible = False
                
                PopupMenu NivelesKDS
                
                Exit Sub
            
            ElseIf Mid(lcKey, 1, LPA) = "|L|" Then 'KDS->SUBGRUPO
                
                AgregarKDS(0).Enabled = True
                AgregarKDS(1).Visible = False
                
                ClasificacionKDS(2).Enabled = True
                ClasificacionKDS(2).Visible = True
                
                ClasificacionKDS(1).Visible = False
                ClasificacionKDS(0).Visible = False
                
                PopupMenu NivelesKDS
                
                Exit Sub
                
            ElseIf Mid(lcKey, 1, LPA) = "|W|" Then 'KDS->SUBGRUPO AL FINAL DEL ARBOL
                
                AgregarKDS(0).Enabled = True
                AgregarKDS(1).Visible = False
                
                ClasificacionKDS(2).Visible = True
                ClasificacionKDS(2).Enabled = False
                
                ClasificacionKDS(1).Visible = False
                ClasificacionKDS(0).Visible = False
                
                PopupMenu NivelesKDS
                
                Exit Sub
            
            ElseIf Mid(lcKey, 1, LPA) = "|S|" Then 'Sitios
                
                Agregar.Enabled = True
                add(0).Enabled = False
                add(1).Enabled = False
                add(2).Enabled = False
                add(3).Enabled = False
                add(4).Enabled = False
                add(5).Enabled = False
                add(6).Enabled = True
                add(7).Enabled = False
                add(8).Enabled = False
                modificar.Enabled = False
                eliminar.Enabled = False
                cambiar = False
                config = True
            
            ElseIf Mid(lcKey, 1, LPA) = "|H|" Then 'casH
                
                Agregar.Enabled = True
                add(0).Enabled = False
                add(1).Enabled = True
                add(2).Enabled = True
                add(3).Enabled = True
                add(4).Enabled = False
                add(5).Enabled = False
                add(6).Enabled = False
                add(7).Enabled = False
                add(8).Enabled = False
                modificar.Enabled = False
                eliminar.Enabled = False
                cambiar = False
                config = True
            
            ElseIf Mid(lcKey, 1, LPA) = "|U|" Then 'UBICACION
                
                Agregar.Enabled = True
                add(0).Enabled = False
                add(1).Enabled = False
                add(2).Enabled = False
                add(3).Enabled = False
                add(4).Enabled = False
                add(5).Enabled = False
                add(6).Enabled = False
                add(7).Enabled = False
                add(8).Enabled = False
                modificar.Enabled = True
                eliminar.Enabled = True
                cambiar = False
                config = True
                
            ElseIf Mid(lcKey, 1, LPA) = "|V|" Then 'NIVEL VENDEDOR
                
                Agregar.Enabled = True
                add(0).Enabled = False
                add(1).Enabled = False
                add(2).Enabled = False
                add(3).Enabled = False
                add(4).Enabled = False
                add(5).Enabled = False
                add(6).Enabled = False
                add(7).Enabled = False
                add(8).Enabled = True
                modificar.Enabled = False
                eliminar.Enabled = False
                cambiar = False
                config = True
            
            ElseIf Mid(lcKey, 1, LPA) = "|R|" Then 'VENDEDOR
                
                Agregar.Enabled = True
                add(0).Enabled = False
                add(1).Enabled = False
                add(2).Enabled = False
                add(3).Enabled = False
                add(4).Enabled = False
                add(5).Enabled = False
                add(6).Enabled = False
                add(7).Enabled = False
                add(8).Enabled = False
                modificar.Enabled = False
                eliminar.Enabled = False
                cambiar = False
                config = True
            
            ElseIf Mid(lcKey, 1, LPA) = "|X|" Then 'Nivel
                Exit Sub
            End If
            
            PopupMenu opcion
            
    End Select

End Sub

Private Sub arbol_NodeClick(ByVal Node As MSComctlLib.Node)
    
    lcKey = Node.Key
    lcTag = Node.Tag
    lcNodo = Node.Index
    lcCount = Node.Children
    lcIndex = Node.Index
        
    If Mid(Node.Key, 1, LPA) = "|R|" _
    Or Mid(Node.Key, 1, LPA) = "|N|" _
    Or left(Node.Key, 1) = "N" Then
        
        menu.ListItems.Clear
        
        If ExistenDigitales(ARBOL.SelectedItem.Key) Then ' Si tiene cajas FOOD.
            Call Cargar_Menu(Siempre)
        End If
        
    ElseIf UCase(Node.Key) = "ROOT" Then
        
        menu.ListItems.Clear
        
        If ExistenDigitales(ARBOL.SelectedItem.Key) Then ' Si tiene cajas FOOD.
            Call Cargar_Menu(Siempre)
        End If
        
    ElseIf Mid(Node.Key, 1, LPA) = "|C|" Then 'CAJA
        
        Caja = Mid(lcKey, LPA + 1, Len(lcKey) - 1)
        
        Call Apertura_Recordset(True, rsCaja)
        
        nCaja = Caja
        
        rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & Caja & "'", _
        Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        Set rsCaja.ActiveConnection = Nothing
        
        If Not rsCaja.EOF Then
            If rsCaja!B_POS_Digital = True Then
                Call Cargar_Menu(PosDigital)
            Else
                Call Cargar_Menu(PosStellar)
            End If
        End If

        lcKey = Node.Key
        
        Call Cerrar_Recordset(rsCaja)
        
    Else
        menu.ListItems.Clear
    End If
    
End Sub

Private Sub ayuda_Click()
    Select Case ayuda.Value
        Case Is = 0
            'Call ACTIVA(ANIVEL, False)
            ayuda.Caption = Replace(ayuda.Caption, "Si", "No", 1)
        Case Is = 1
            'Call ACTIVA(ANIVEL, True)
            ayuda.Caption = Replace(ayuda.Caption, "No", "Si", 1)
    End Select
End Sub

Private Sub Activa(Control As Object, Activar As Boolean)
    Control.Enabled = Activar
End Sub

Private Sub Clasificacion_Click(Index As Integer)
    
    Dim Dpto As Variant, Grpo As Variant, SGrpo As Variant, CDpto As String, CGrpo As String, CSgrpo As String, Relacion As String
    
    Select Case Index
        
        Case 0      'DEPARTAMENTO
            
            TecladoWindows
            Dpto = DepGruSub.Buscar_DepartamentoInterfaz(Ent.BDD.ConnectionString)
            
            If Not IsEmpty(Dpto) Then
                Call GrabarDepto(CStr(Dpto(0)), ARBOL.SelectedItem.Text, _
                Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key)))
                
                If AddNode Then ARBOL.Nodes.add ARBOL.SelectedItem.Key, tvwChild, _
                "|D|" & Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key)) & "/" & Dpto(0), Dpto(1), "subnivel"
            End If
            
        Case 1      'GRUPO
            
            TecladoWindows
            
            CDpto = Mid(ARBOL.SelectedItem.Key, InStr(1, ARBOL.SelectedItem.Key, "/") + 1, _
            Len(ARBOL.SelectedItem.Key))
            
            Grpo = DepGruSub.Buscar_GrupoInterfaz(Ent.BDD.ConnectionString, , CDpto)
            
            If Not IsEmpty(Grpo) Then
                CGrpo = Grpo(0)
                
                Relacion = Mid(ARBOL.SelectedItem.Key, 1, InStr(1, ARBOL.SelectedItem.Key, "/") - 1)
                Relacion = Mid(Relacion, (LPA + 1), Len(Relacion))
                
                Call GrabarGrupo(CDpto, CGrpo, ARBOL.SelectedItem.Parent.Text, Relacion)
                
                If AddNode Then ARBOL.Nodes.add ARBOL.SelectedItem.Key, tvwChild, _
                "|G|" & Relacion & "/" & Grpo(0) & "/" & CDpto, Grpo(1), "subnivel"
            End If
            
        Case 2      'SUBGRUPO
            
            TecladoWindows
            
            CGrpo = Mid(ARBOL.SelectedItem.Key, InStr(1, ARBOL.SelectedItem.Key, "/") + 1, LPA)
            CDpto = Mid(ARBOL.SelectedItem.Parent.Key, InStr(1, ARBOL.SelectedItem.Parent.Key, "/") + 1, _
            Len(ARBOL.SelectedItem.Parent.Key) - 1)
            SGrpo = DepGruSub.Buscar_SubGrupoInterfaz(Ent.BDD.ConnectionString, , CDpto, CGrpo)
            
            If Not IsEmpty(SGrpo) Then
                
                CSgrpo = SGrpo(0)
                
                Relacion = Mid(ARBOL.SelectedItem.Key, 1, InStr(1, ARBOL.SelectedItem.Key, "/") - 1)
                Relacion = Mid(Relacion, (LPA) + 1, Len(Relacion))
                
                Call GrabarSubGrupo(CDpto, CGrpo, CSgrpo, ARBOL.SelectedItem.Parent.Parent.Text, Relacion)
                
                If AddNode Then ARBOL.Nodes.add ARBOL.SelectedItem.Key, tvwChild, _
                "|B|" & Relacion & "/" & SGrpo(0) & "/" & CGrpo & "/" & CDpto, SGrpo(1), "subnivel"
                
            End If
             
        Case 3      'PROPIEDADES
            
            Dim Particiones() As String
            
            CDpto = ""
            CGrpo = ""
            SGrpo = ""
            
            Select Case Mid(ARBOL.SelectedItem.Key, 1, LPA)
                
                Case "|P|"
                    
                    FrmImpresoras.Txtcodigo = Mid(ARBOL.SelectedItem.Key, _
                    LPA + 1, Len(ARBOL.SelectedItem.Key))
                    FrmImpresoras.Show vbModal
                    Set FrmImpresoras = Nothing
                    
                    If AddNode Then ARBOL.SelectedItem.Text = lcTexto
                    ARBOL.Enabled = True
                    
                Case "|D|"
                    
                    Particiones = Split(ARBOL.SelectedItem.Key, "/")
                    'Cdpto = Mid(ARBOL.SelectedItem.Key, InStr(1, ARBOL.SelectedItem.Key, "/") + 1, Len(ARBOL.SelectedItem.Parent.Key))
                    CDpto = Particiones(1)
                    
                    FrmPropiedades.Departamento = CDpto
                    FrmPropiedades.Show vbModal
                    
                Case "|G|"
                    
                    Particiones = Split(ARBOL.SelectedItem.Key, "/")
                    CGrpo = Particiones(1)
                    CDpto = Particiones(2)
                    
                    FrmPropiedades.Departamento = CDpto
                    FrmPropiedades.Grupo = CGrpo
                    FrmPropiedades.Show vbModal
                    
                Case "|B|"
                    
                    Particiones = Split(ARBOL.SelectedItem.Key, "/")
                    
                    CGrpo = Particiones(2)
                    CDpto = Particiones(3)
                    SGrpo = Particiones(1)
                    
                    FrmPropiedades.Departamento = CDpto
                    FrmPropiedades.Grupo = CGrpo
                    FrmPropiedades.SubGrupo = SGrpo
                    FrmPropiedades.Show vbModal
                    
            End Select
    End Select
    
End Sub

Private Sub CmdSalir_Click()
    Unload Me
    Set frm_Config_POS1 = Nothing
End Sub

Private Sub ClasificacionKDS_Click(Index As Integer)
    
    On Error GoTo Error
    
    Dim Dpto As Variant, Grpo As Variant, SGrpo As Variant, CDpto As String, CGrpo As String, CSgrpo As String, Relacion As String
    Dim Rs As New ADODB.Recordset

    Select Case Index
        
        Case 0      'DEPARTAMENTO
            
            TecladoWindows
            Dpto = DepGruSub.Buscar_DepartamentoInterfaz(Ent.BDD.ConnectionString)
            
            If Not IsEmpty(Dpto) Then
                
                Rs.Open "SELECT * FROM MA_KDS WHERE cs_Codigo = '" & _
                Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key)) & "'", _
                Ent.POS, adOpenDynamic, adLockBatchOptimistic
                
                Call GrabarDeptoKDS(CStr(Dpto(0)), Rs!cs_Descripcion, _
                Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key)))
                If AddNode Then ARBOL.Nodes.add ARBOL.SelectedItem.Key, tvwChild, _
                "|T|" & Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key)) & "/" & Dpto(0), Dpto(1), "subnivel"
                
            End If
            
        Case 1      'GRUPO
            
            TecladoWindows
            
            CDpto = Mid(ARBOL.SelectedItem.Key, InStr(1, ARBOL.SelectedItem.Key, "/") + 1, _
            Len(ARBOL.SelectedItem.Key))
            
            Grpo = DepGruSub.Buscar_GrupoInterfaz(Ent.BDD.ConnectionString, , CDpto)
            
            If Not IsEmpty(Grpo) Then
                
                CGrpo = Grpo(0)
                
                Relacion = Mid(ARBOL.SelectedItem.Key, 1, InStr(1, ARBOL.SelectedItem.Key, "/") - 1)
                Relacion = Mid(Relacion, LPA + 1)
                
                Rs.Open "SELECT * FROM MA_KDS WHERE cs_Codigo = '" & _
                Relacion & "'", _
                Ent.POS, adOpenDynamic, adLockBatchOptimistic
                
                Call GrabarGrupoKDS(CDpto, CGrpo, Rs!cs_Descripcion, Relacion)
                
                If AddNode Then ARBOL.Nodes.add ARBOL.SelectedItem.Key, tvwChild, _
                "|L|" & Relacion & "/" & Grpo(0) & "/" & CDpto, Grpo(1), "subnivel"
                
            End If
            
        Case 2      'SUBGRUPO
            
            TecladoWindows
            
            CGrpo = Mid(ARBOL.SelectedItem.Key, InStr(1, ARBOL.SelectedItem.Key, "/") + 1, LPA)
            CDpto = Mid(ARBOL.SelectedItem.Parent.Key, InStr(1, ARBOL.SelectedItem.Parent.Key, "/") + 1, _
            Len(ARBOL.SelectedItem.Parent.Key) - 1)
            
            SGrpo = DepGruSub.Buscar_SubGrupoInterfaz(Ent.BDD.ConnectionString, , CDpto, CGrpo)
            
            If Not IsEmpty(SGrpo) Then
                
                CSgrpo = SGrpo(0)
                
                Relacion = Mid(ARBOL.SelectedItem.Key, 1, InStr(1, ARBOL.SelectedItem.Key, "/") - 1)
                Relacion = Mid(Relacion, LPA + 1)
                
                Rs.Open "SELECT * FROM MA_KDS WHERE cs_Codigo = '" & _
                Relacion & "'", _
                Ent.POS, adOpenDynamic, adLockBatchOptimistic
                
                Call GrabarSubGrupoKDS(CDpto, CGrpo, CSgrpo, Rs!cs_Descripcion, Relacion)
                
                If AddNode Then ARBOL.Nodes.add ARBOL.SelectedItem.Key, tvwChild, _
                "|W|" & Relacion & "/" & SGrpo(0) & "/" & CGrpo & "/" & CDpto, SGrpo(1), "subnivel"
                
            End If
            
    End Select
    
    If Rs.State = 1 Then Rs.Close
    
    Exit Sub
    
Error:
    
    Debug.Print ERR.Description
    
End Sub

Private Sub cmb_salir_Click()
    Picture1_Click
End Sub

Private Sub config_Click()
    ARBOL.Enabled = False
    Copiar = True
    Newcaja.Show vbModal
    If AddNode Then Call Crear_Menu(ARBOL.SelectedItem.Parent.Key, Clave, lcTexto, "caja")
    Copiar = False
    ARBOL.Enabled = True
End Sub

Private Sub Dispositivo_Click(Index As Integer)
    Select Case Index
        Case 0                      'DISPOSITIVOS
            nCaja = Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key))
            AddNode = False
            
            FrmDispositivos.Show vbModal
            
            If AddNode Then
                ARBOL.Nodes.add ARBOL.SelectedItem.Parent.Key, tvwChild, lcKey, lcTexto, "manufactur"
            End If
        Case 1                      'Meseros
            Ubicacion = Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key))
    End Select
End Sub

Private Sub eliminar_Click()
    
    Select Case Mid(lcKey, 1, LPA)
        
        Case Is = "|N|"
            
            'If ARBOL.SelectedItem.Children Then Exit Sub
            Call Apertura_Recordset(False, rsMenu)
            
            rsMenu.Open "SELECT * FROM MA_ESTRUC_CAJA WHERE Clave = '" & lcKey & "'", _
            Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
            
            If Not rsMenu.EOF Then
                rsMenu.Delete
                rsMenu.UpdateBatch
            End If
            
            ARBOL.Nodes.Remove (lcIndex)
            
        Case Is = "|C|"
            
            Call Apertura_Recordset(True, rsCaja)
            
            If Not EsDigital And Not EsPedidos Then
                rsCaja.Open "SELECT TOP 1 * FROM MA_PAGOS WHERE c_Caja = '" & nCaja & "'", _
                Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            Else
                rsCaja.Open "SELECT TOP 1 * FROM TR_VENTAS_POS_PAGOS WHERE cs_Caja = '" & nCaja & "'", _
                Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            End If
        
            If Not rsCaja.EOF Then
                Call Mensaje(True, "La caja no puede ser eliminada, debido a que posee transacciones en el sistema.")
                
                Call Cerrar_Recordset(rsCaja)
                
                Exit Sub
            Else
                Call Apertura_Recordset(True, rsCaja)
                
                rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
                Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
                
                If Not rsCaja.EOF Then
                    rsCaja.Delete

                    Ent.POS.Execute "DELETE FROM TR_CAJA_BOTONES_DIGITALES WHERE Caja = '" & nCaja & "'"
                    Ent.POS.Execute "DELETE FROM MA_CAJA_BOTONES_DIGITALES WHERE Caja = '" & nCaja & "'"
                    Ent.POS.Execute "DELETE FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "'"
                    Ent.POS.Execute "DELETE FROM MA_DISPOSITIVOS_CAJA WHERE Caja = '" & nCaja & "'"

                    rsCaja.UpdateBatch
                    
                    Call Cerrar_Recordset(rsCaja)
                    
                    ARBOL.Nodes.Remove (lcIndex)
                Else
                    Call Mensaje(True, "La caja ya fue eliminada.")
                    Call Cerrar_Recordset(rsCaja)
                End If
            End If
            
        Case Is = "|U|"
            
            Ent.POS.BeginTrans
            
            Ent.POS.Execute "DELETE FROM MA_SITIO WHERE cs_CodigoRelacion = '" & _
            ARBOL.SelectedItem.Parent.Parent.Key & "' AND cs_CodigoSitio = '" & _
            Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key)) & "'"
            
            ARBOL.Nodes.Remove (ARBOL.SelectedItem.Index)
            
            Ent.POS.CommitTrans
            
        Case Is = "|P|"
            
            Ent.POS.BeginTrans
            
            Ent.POS.Execute "DELETE FROM MA_IMPRESORAS WHERE cs_Relacion = '" & _
            ARBOL.SelectedItem.Parent.Parent.Key & "' " & _
            "AND cs_Nombre = '" & ARBOL.SelectedItem.Text & "'"
            
            ARBOL.Nodes.Remove (ARBOL.SelectedItem.Index)
            
            Ent.POS.CommitTrans
            
        Case Is = "|A|"
            
            Ent.POS.BeginTrans
            
            Ent.POS.Execute "DELETE FROM MA_KDS WHERE cs_Relacion = '" & _
            ARBOL.SelectedItem.Parent.Parent.Key & "' " & _
            "AND cs_CODIGO = '" & ARBOL.SelectedItem.Text & "'"
            
            ARBOL.Nodes.Remove (ARBOL.SelectedItem.Index)
            
            Ent.POS.CommitTrans
            
    End Select
    
End Sub

Private Sub Form_Activate()
    If Inicio = True Then
        frm_Login_Configuracion.Show vbModal
        
        If fCancelar = True Then
            Unload Me
            Exit Sub
        End If
        
        Call arbol_NodeClick(ARBOL.SelectedItem)
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbAltMask Then
        Select Case KeyCode
            Case Is = vbKeyS
                Call Form_KeyDown(vbKeyF12, 0)
            Case Is = vbKeyF4
                Call Form_KeyDown(vbKeyF12, 0)
        End Select
    Else
        Select Case KeyCode
            Case Is = vbKeyF12
                If cmb_salir.Enabled Then cmb_salir_Click
        End Select
    End If
End Sub

Private Sub Form_Load()
    
    Call ConstruirArbol
    
    Me.cmb_salir.Caption = Stellar_Mensaje(54) 'salir
    Agregar.Caption = Stellar_Mensaje(10120) 'agregar
    add(0).Caption = Stellar_Mensaje(10254) 'Zona
    
    'add(1).Enabled = False 'POS Retail
    'add(2).Enabled = False 'POS FOOD
    'add(3).Enabled = False 'POE FOOD
    
    add(4).Caption = Stellar_Mensaje(10255) 'impresora
    add(6).Caption = Stellar_Mensaje(10258) 'Mesas
    add(7).Caption = Stellar_Mensaje(10256) 'Dispositivos
    add(8).Caption = Stellar_Mensaje(10257) ' Mesero
    
    eliminar.Caption = Stellar_Mensaje(208) 'eliminar
    
    modificar.Caption = Stellar_Mensaje(207) ' modificar
    
    AgregarClas(0).Caption = Stellar_Mensaje(10120) 'agregar
    AgregarClas(1).Caption = Stellar_Mensaje(208) 'eliminar
    AgregarKDS(0).Caption = Stellar_Mensaje(10120) 'agregar
    AgregarKDS(1).Caption = Stellar_Mensaje(207) ' modificar
    AgregarKDS(2).Caption = Stellar_Mensaje(208) 'eliminar
    Clasificacion(0).Caption = Stellar_Mensaje(3028) 'departemento
    Clasificacion(1).Caption = Stellar_Mensaje(161) 'Grupo
    Clasificacion(2).Caption = Stellar_Mensaje(3029) 'subgrupo
    ClasificacionKDS(0).Caption = Stellar_Mensaje(3028) 'departemento
    ClasificacionKDS(1).Caption = Stellar_Mensaje(161) 'Grupo
    ClasificacionKDS(2).Caption = Stellar_Mensaje(3029) 'subgrupo
    
    'Clasificacion(2).Enabled = False
    'Clasificacion(2).Visible = True
    'Clasificacion(0).Visible = False
    'Clasificacion(1).Visible = False
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frm_Config_POS1 = Nothing
End Sub

Private Sub Crear_Menu(Campo1, Campo2, Campo3, Campo4, Optional Sorted As Boolean = False)
    On Error GoTo Error
            Set lcNode = ARBOL.Nodes.add(Campo1, tvwChild, Campo2, Campo3, Campo4)
            If Sorted Then lcNode.Sorted = True
            lcNode.Tag = Campo2
    Exit Sub
Error:
    Debug.Print ERR.Description
End Sub

Private Sub lbl_salir_Click()
    Unload Me
End Sub

Private Sub Cargar_Menu(Numero As TipoPos)
    
    Dim Estruc(0 To 9) As menu
    'ESTA VARIABLE SE UTILIZA PARA ALMCENAR LAS OPCIONES QUE SE PRESENTAN EN EL MENU DE OPCIONES DEL CONFIGURADOR DEL POS
    'EL TIPO DE DATOS MENU, ESTA DEFINIDO EN EL MÓDULO PRINCIPAL.BAS
    
    Estruc(0).Clave = "Gen"
    Estruc(0).Texto = Stellar_Mensaje(10209) '"General"
    Estruc(0).Imagen = "general"
    Estruc(0).Forma = "gen"
    Estruc(0).Tipo = PosAmbos
    
    Estruc(1).Clave = "Entrada"
    Estruc(1).Texto = Stellar_Mensaje(10208) '"Dispositivos de Entrada"
    Estruc(1).Imagen = "entrada"
    Estruc(1).Forma = "entrada"
    Estruc(1).Tipo = Nunca
    
    Estruc(2).Clave = "Salida"
    Estruc(2).Texto = Stellar_Mensaje(10207) '"Dispositivos de Salida"
    Estruc(2).Imagen = "salida"
    Estruc(2).Forma = "salida"
    Estruc(2).Tipo = Nunca
    
    Estruc(3).Clave = "Digital"
    Estruc(3).Texto = Stellar_Mensaje(10206) '"Configuración de Botones de Comandas"
    Estruc(3).Imagen = "digital"
    Estruc(3).Forma = "digital"
    Estruc(3).Tipo = Nunca 'PosDigital ' -> Ya no existe. La funcionalidad se mudo a Stellar BUSINESS -> Configuración.
    
    Estruc(4).Clave = "BotonesAcceso"
    Estruc(4).Texto = Stellar_Mensaje(10140) '"Configuracion de Botones de Acceso"
    Estruc(4).Imagen = "botones"
    Estruc(4).Forma = "BotonesDeAcceso"
    Estruc(4).Tipo = PosDigital
    
    Estruc(5).Clave = "Perfiles"
    Estruc(5).Texto = Stellar_Mensaje(10205) '"Configuracion de Perfiles de Botones Digitales"
    Estruc(5).Imagen = "digital"
    Estruc(5).Forma = "Perfiles"
    Estruc(5).Tipo = Nunca 'Siempre ' -> Ya no existe. La funcionalidad se mudo a Stellar BUSINESS -> Configuración.
    
    Estruc(6).Clave = "Botones"
    Estruc(6).Texto = Stellar_Mensaje(10140) '"Configuracion de Botones de Acceso"
    Estruc(6).Imagen = "botones"
    Estruc(6).Forma = "Botones"
    Estruc(6).Tipo = PosStellar
    
    Estruc(7).Clave = "Diagrama"
    Estruc(7).Texto = Stellar_Mensaje(5524) '"Diagrama de Mesas"
    Estruc(7).Imagen = "botones"
    Estruc(7).Forma = "Diagrama"
    Estruc(7).Tipo = Siempre
    
    'LA OPCION TIPO DE LA ESTRUCTURA ES TIPO TipoPos que está definido en el MÓDULO: PRINCIPAL.BAS
    
    Estruc(8).Clave = "ProdXDen"
    Estruc(8).Texto = Stellar_Mensaje(5525) '"Productos por Denominación"
    Estruc(8).Imagen = "digital"
    Estruc(8).Forma = "ProdXDen"
    Estruc(8).Tipo = PosStellar
    
    Estruc(9).Clave = "ConfigSetup"
    Estruc(9).Texto = Stellar_Mensaje(5526) '"Parámetros Setup POS"
    Estruc(9).Imagen = "general"
    Estruc(9).Forma = "ConfigSetup"
    Estruc(9).Tipo = PosStellar
    
    i = 0
    
    menu.ListItems.Clear
    
    For i = 0 To UBound(Estruc)
        'POSAMBOS = CUANDO DEBE CARGAR LA OPCION DEL LISTVIEW TANTO PARA EL POS DIGITAL COMO PARA EL POS STELLAR
        'POSIGITAL = SOLO CARGA OPCIONES DEL POS DIGITAL
        'POSSTELLAR
        'SIEMPRE = CARGA E CUALQUIER NODO LA OPCION
        If (Estruc(i).Tipo = PosAmbos And Numero <> Siempre) _
        Or Estruc(i).Tipo = Numero Or _
        (Estruc(i).Tipo = Siempre And ExistenDigitales(ARBOL.SelectedItem.Key)) Then
            lcKey = Estruc(i).Clave
            lcTexto = Estruc(i).Texto
            lcImagen = Estruc(i).Imagen
            Set ItmG = menu.ListItems.add(, Key:=lcKey, Text:=lcTexto, Icon:=lcImagen) ', SmallIcon:=lcimagen
            ItmG.Tag = Estruc(i).Forma
        End If
    Next i
    
End Sub

Private Sub menu_DblClick()
    
    Dim ConfigPerfiles As Object
    
    If menu.SelectedItem.Selected = True Then
        
        Select Case UCase(menu.SelectedItem.Tag)
            
            Case Is = "GEN"
                If CheckCaja Then
                    If EsDigital And EsPedidos Then
                        FrmTerminal.Show vbModal
                    ElseIf EsDigital And Not EsPedidos Then
                        general.Show vbModal
                    Else
                        frmCajaRetail.Show vbModal
                    End If
                    
                    Call arbol_NodeClick(ARBOL.SelectedItem)
                End If
                
            Case Is = "ENTRADA"
                Entrada.Show vbModal
                
            Case Is = "SALIDA"
                salida.Show vbModal
                
            Case Is = "DIGITAL"
                'Botones.Show vbModal
                Set ConfigPerfiles = CreateObject("DLLBotonesDigitales.cls_BtDigitales")
                Call ConfigPerfiles.ConfigCajaDigital(nCaja, Ent.POS, Ent.POS.ConnectionString, False)
                Set ConfigPerfiles = Nothing
                
            'Case Is = "BOTONES"
                'Botones.Show vbModal
                
            Case Is = "BOTONES", "BOTONESDEACCESO"
                CheckCaja
                FrmConfigBotonesAcceso.Show vbModal 'AQUI ESCRIBIR LA FORMA DE LA CONFIGURACIÓN DE LOS BOTONES DE ACCESO
                
            Case Is = "IMPRESORAS"
                ficha_dgs.Show vbModal 'AQUI ESCRIBIR LA FORMA DE LA CONFIGURACIÓN DE LAS IMPRESORAS
            
            Case Is = "PERFILES" 'AQUI ESCRIBIR LA FORMA DE LA CONFIGURACIÓN DE PERFILES PARA BOTONES DE POS DIGITAL
                Set ConfigPerfiles = CreateObject("DLLBotonesDigitales.cls_BtDigitales")
                Call ConfigPerfiles.ConfigurarBotonesDigitales(Ent.POS, Ent.POS.ConnectionString, False)
                Set ConfigPerfiles = Nothing
            
            Case Is = "ADMINISTRAR"
                FrmAdministracion.Show vbModal
                
            Case Is = "DIAGRAMA"
                If Frm_OrganizarMesas.CON.State = 1 Then
                    Frm_OrganizarMesas.CON.Close
                End If
                
                Frm_OrganizarMesas.CON = Ent.POS
                Frm_OrganizarMesas.Show vbModal
                
                Set Frm_OrganizarMesas = Nothing
                
                ARBOL.Nodes.Clear
                ConstruirArbol
            
            Case Is = "PRODXDEN"
                
                Set obj = SafeCreateObject("StellarSetupManager.ClsConfig")
                
                If Not obj Is Nothing Then
                    
                    SafePropAssign obj, "CnVAD10", Ent.BDD
                    SafePropAssign obj, "CnVAD20", Ent.POS
                    
                    obj.ConfigurarCategoriasXDenominacion
                    
                Else
                    
                    Mensaje True, "N/A"
                    
                End If
                
            Case Is = "CONFIGSETUP"
                
                Set obj = SafeCreateObject("StellarSetupManager.ClsConfig")
                
                If Not obj Is Nothing Then
                    
                    SafePropAssign obj, "CnVAD10", Ent.BDD
                    SafePropAssign obj, "CnVAD20", Ent.POS
                    
                    obj.ConfigSetupPOSV1
                    
                Else
                    
                    Mensaje True, "N/A"
                    
                End If
                
        End Select
        
    End If
    
End Sub

Private Sub Buscar_Deposito()
    
    Call Apertura_Recordset(False, rsEureka)
    
    rsEureka.Open "select * from ma_deposito where c_coddeposito= '" & coddeposito.Text & "' and c_codlocalidad = '" & Sucursal & "'", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsEureka.EOF Then
        lbl_deposito.Text = rsEureka!c_Descripcion
    Else
        Call Mensaje(True, "El código del depósito no existe.")
        lbl_deposito.Text = ""
    End If
    
    Call Cerrar_Recordset(rsEureka)

End Sub

Private Sub menu_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case Is = vbKeyReturn
            Call menu_DblClick
    End Select
End Sub

Public Sub ConstruirArbol()
    
    Dim rsEstructura As New ADODB.Recordset, RsDispositivos As New ADODB.Recordset, rsConsulta As New ADODB.Recordset
    Dim Clave As String, REL As String, IP As String
    
    Me.cmb_salir.Enabled = False
    
    'Set lcNode = ARBOL.Nodes.add(, , "ROOT", "Raiz", "org")
    Set lcNode = ARBOL.Nodes.add(, , "ROOT", Stellar_Mensaje(10201), "org")
    lcNode.Tag = "ROOT"
    
    Call Apertura_Recordset(False, rsMenu)
    
    rsMenu.Open "SELECT * FROM MA_ESTRUC_CAJA ORDER BY Clave", _
    Ent.POS, adOpenDynamic, adLockReadOnly, adCmdText
    
    If Not rsMenu.EOF Then
        
        While Not rsMenu.EOF
            
            DoEvents
            
            REL = rsMenu!Relacion
            
            Call Crear_Menu(REL, rsMenu!Clave, rsMenu!Texto, "subnivel")
            
            lcNode.Expanded = True
            
            Call Apertura_Recordset(True, rsEstructura)
            
            'BUSCA LAS CAJAS
            
            REL = rsMenu!Clave
            
            'Call Crear_Menu(REL, "H" & rsMenu!Clave, "CAJAS", "caja")
            Call Crear_Menu(REL, "|H|" & rsMenu!Clave, Stellar_Mensaje(10098), "caja")
            
            lcNode.Sorted = True
            
            rsEstructura.Open "SELECT * FROM MA_CAJA WHERE c_Relacion = '" & rsMenu!Clave & "'", _
            Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not rsEstructura.EOF Then
                Do Until rsEstructura.EOF
                    DoEvents
                    Clave = "|C|" & rsEstructura!c_codigo
                    REL = "|H|" & rsEstructura!c_Relacion
                    
                    If Not rsEstructura!B_POS_Digital And Not rsEstructura!b_POS_Pedido Then
                        Call Crear_Menu(REL, Clave, "POS Retail (isPOS)  | " & rsEstructura!c_Desc_Caja, "caja") 'Call Crear_Menu(REL, Clave, rsEstructura!c_Desc_Caja & " POS Retail", "caja")
                    ElseIf rsEstructura!B_POS_Digital And Not rsEstructura!b_POS_Pedido Then
                        Call Crear_Menu(REL, Clave, "POS Food (isFOOD) | " & rsEstructura!c_Desc_Caja, "caja")
                    ElseIf rsEstructura!B_POS_Digital And rsEstructura!b_POS_Pedido Then
                        Call Crear_Menu(REL, Clave, "POE Food (isFOOD) | " & rsEstructura!c_Desc_Caja, "caja")
                    End If
                    
                    Call Apertura_Recordset(True, RsDispositivos)
                    
                    RsDispositivos.Open "SELECT * FROM MA_DISPOSITIVOS_CAJA LEFT JOIN MA_DISPOSITIVOS ON MA_DISPOSITIVOS.cs_Codigo = MA_DISPOSITIVOS_CAJA.cs_CodigoDispositivo WHERE Caja = '" & rsEstructura!c_codigo & "'", _
                    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
                    REL = Clave
                    
                    While Not RsDispositivos.EOF
                        DoEvents
                        Clave = "|X|" & rsEstructura!c_codigo & RsDispositivos!cs_CodigoDispositivo
                        Call Crear_Menu(REL, Clave, RsDispositivos!cs_Descripcion, "manufactur")
                        RsDispositivos.MoveNext
                    Wend
                    
                    rsEstructura.MoveNext
                Loop
            End If
            
            'BUSCA LAS IMPRESORAS
            
            Call Apertura_Recordset(True, rsEstructura)
            
            REL = rsMenu!Clave
            
            'Call Crear_Menu(REL, "I" & rsMenu!Clave, "IMPRESORAS DE COMANDAS", "printer1")
            Call Crear_Menu(REL, "|I|" & rsMenu!Clave, Stellar_Mensaje(10202), "printer1")
            
            rsEstructura.Open "SELECT * FROM MA_IMPRESORAS WHERE cs_Relacion = '" & rsMenu!Clave & "'", _
            Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not rsEstructura.EOF Then
                Do Until rsEstructura.EOF
                    DoEvents
                    
                    Clave = "|P|" & rsEstructura!cs_Numero
                    REL = "|I|" & rsEstructura!cs_Relacion
                    
                    Call Crear_Menu(REL, Clave, rsEstructura!cs_Nombre, "printer1")
                    Call BuscarClasificacion("|P|" & rsEstructura!cs_Numero, rsEstructura!cs_Nombre)
                    
                    rsEstructura.MoveNext
                Loop
            End If
            
            'BUSCAR LOS KDS
            
            Call Apertura_Recordset(True, rsEstructura)
            
            REL = rsMenu!Clave
            
            Call Crear_Menu(REL, "|K|" & rsMenu!Clave, "KITCHEN DISPLAY", "fichero")
            
            If ExisteTabla("MA_KDS", Ent.POS) Then
                
                rsEstructura.Open "SELECT * FROM MA_KDS WHERE cs_Relacion = '" & rsMenu!Clave & "'", _
                Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                If Not rsEstructura.EOF Then
                    Do Until rsEstructura.EOF
                        DoEvents
                        
                        Clave = "|A|" & rsEstructura!cs_Codigo
                        REL = "|K|" & rsEstructura!cs_Relacion
                                    
                        Call Crear_Menu(REL, Clave, rsEstructura!cs_Descripcion, "fichero")
                        Call BuscarClasificacionKDS(Clave, vbNullString)
                        
                        rsEstructura.MoveNext
                    Loop
                End If
                
            End If
            
            'BUSCA LOS SITIOS CONFIGURADOS
            
            Call Apertura_Recordset(True, rsEstructura)
            
            REL = rsMenu!Clave
            
            'Call Crear_Menu(REL, "S" & rsMenu!Clave, "MESAS", "ubicacion")
            Call Crear_Menu(REL, "|S|" & rsMenu!Clave, Stellar_Mensaje(10203), "ubicacion")
            
            lcNode.Sorted = True
            
            rsEstructura.Open "SELECT * FROM MA_SITIO WHERE cs_CodigoRelacion = '" & rsMenu!Clave & "'", _
            Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not rsEstructura.EOF Then
                Do Until rsEstructura.EOF
                    DoEvents
                    
                    Clave = "|U|" & rsEstructura!cs_CodigoSitio
                    REL = "|S|" & rsEstructura!cs_CodigoRelacion
                    
                    Call Crear_Menu(REL, Clave, rsEstructura!cu_descripcion, "ubicacion")
                    rsEstructura.MoveNext
                Loop
            End If
            
            'BUSCA LOS VENDEDORES DEL SISTEMA
            
            REL = rsMenu!Clave
            
            'Call Crear_Menu(REL, "V" & rsMenu!Clave, "VENDEDORES", "user")
            Call Crear_Menu(REL, "|V|" & rsMenu!Clave, Stellar_Mensaje(10204), "user") ' MESEROS
            
            lcNode.Sorted = True
            
            Call BuscarVendedores(rsMenu!Clave, "|V|" & rsMenu!Clave)
            
            rsMenu.MoveNext
            
        Wend
        
        'ARBOL.Sorted = True
        
    End If
    
    ARBOL.Nodes(1).Root.Expanded = True
    ARBOL.Nodes(1).Root.Sorted = True
    
    Me.cmb_salir.Enabled = True
    
End Sub

Public Sub AnadirEstructuras(Nodo As Node)
    
    Dim NodoActual As Node
    Set NodoActual = Nodo
    
    REL = NodoActual.Key
    Call Crear_Menu(REL, "|H|" & NodoActual.Key, "CAJAS", "caja")
    
    REL = NodoActual.Key
    Call Crear_Menu(REL, "|I|" & NodoActual.Key, "IMPRESORAS", "printer1")
    
    REL = NodoActual.Key
    Call Crear_Menu(REL, "|S|" & NodoActual.Key, "UBICACION", "ubicacion")
    
    REL = NodoActual.Key
    Call Crear_Menu(REL, "|V|" & NodoActual.Key, "VENDEDORES", "user")
    
    REL = NodoActual.Key
    Call Crear_Menu(REL, "|K|" & NodoActual.Key, "KITCHEN DISPLAY", "fichero")
    
End Sub

Private Sub modificar_Click()
    Select Case Mid(lcKey, 1, LPA)
        Case "|D|", "|G|", "|S|"
        Case "|U|"
            FrmUbicacion.Txtcodigo.Text = Mid(ARBOL.SelectedItem.Key, LPA + 1, Len(ARBOL.SelectedItem.Key))
            FrmUbicacion.Show vbModal
            If AddNode Then ARBOL.SelectedItem.Text = lcTexto
        Case "|C|"
            If CheckCaja Then
                If EsDigital And EsPedidos Then
                    FrmTerminal.Show vbModal
                ElseIf EsDigital And Not EsPedidos Then
                    general.Show vbModal
                Else
                    frmCajaRetail.Show vbModal
                End If
                
                Call arbol_NodeClick(ARBOL.SelectedItem)
            End If
    End Select
End Sub

Private Sub GrabarDepto(Departamento As String, PrinterDesc As String, Clave As String)
    
    Dim rsDepartamento As New ADODB.Recordset
    
    Call Apertura_Recordset(False, rsDepartamento)
    
    rsDepartamento.Open "SELECT * FROM MA_IMPRESORA_DEPARTAMENTO " & _
    "WHERE cs_Relacion = '" & Clave & "' " & _
    "and cs_Departamento = '" & Departamento & "'", _
    Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If rsDepartamento.EOF Then
        Ent.POS.BeginTrans
        rsDepartamento.AddNew
        rsDepartamento!cs_Departamento = Departamento
        rsDepartamento!cu_impresora = PrinterDesc
        rsDepartamento!cs_Relacion = Clave
        rsDepartamento!bu_Por_Puerto = PorPuertoImpresora(Clave) 'Se agrego esta funcion ya que anterior mente no se indicaba un valor para bu_por_puerto
        rsDepartamento.UpdateBatch
        Ent.POS.CommitTrans
        AddNode = True
    Else
        Call Mensaje(True, "Ya existe el departamento asignado a esta impresora.")
        AddNode = False
    End If
    
End Sub

Private Function PorPuertoImpresora(Relacion As String) As Boolean
    
    Dim rsPorPuerto As New ADODB.Recordset
    
    Call Apertura_Recordset(False, rsPorPuerto)
    
    rsPorPuerto.Open "SELECT bu_por_puerto FROM MA_IMPRESORAS " & _
    "WHERE cs_Numero = '" & Relacion & "' ", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsPorPuerto.EOF Then
        PorPuertoImpresora = rsPorPuerto!bu_Por_Puerto
    Else
        PorPuertoImpresora = "0"
    End If
    
End Function

Private Sub GrabarDeptoKDS(Departamento As String, PrinterDesc As String, Clave As String)
    
    Dim rsDepartamento As New ADODB.Recordset
    
    Call Apertura_Recordset(False, rsDepartamento)
    
    rsDepartamento.Open "SELECT * FROM MA_KDS_DEPARTAMENTO " & _
    "WHERE cs_Relacion = '" & Clave & "' AND cs_Departamento = '" & Departamento & "'", _
    Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If rsDepartamento.EOF Then
        Ent.POS.BeginTrans
        rsDepartamento.AddNew
        rsDepartamento!cs_Departamento = Departamento
        rsDepartamento!cu_KDS = PrinterDesc
        rsDepartamento!cs_Relacion = Clave
        rsDepartamento.UpdateBatch
        Ent.POS.CommitTrans
        AddNode = True
    Else
        Call Mensaje(True, "Ya existe el departamento asignado a este KDS.")
        AddNode = False
    End If
    
End Sub

Private Sub GrabarGrupo(Departamento As String, Grupo As String, PrinterDesc As String, Clave As String)
    
    Dim rsGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(False, rsGrupo)
    
    rsGrupo.Open "SELECT * FROM MA_IMPRESORA_GRUPO " & _
    "WHERE cs_Relacion = '" & Clave & "' " & _
    "AND cs_Departamento = '" & Departamento & "' " & _
    "AND cs_Grupo = '" & Grupo & "'", _
    Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If rsGrupo.EOF Then
        Ent.POS.BeginTrans
        rsGrupo.AddNew
        rsGrupo!cs_Departamento = Departamento
        rsGrupo!cs_Grupo = Grupo
        rsGrupo!cu_impresora = PrinterDesc
        rsGrupo!cs_Relacion = Clave
        rsGrupo.UpdateBatch
        Ent.POS.CommitTrans
        AddNode = True
    Else
        Call Mensaje(True, "Ya existe el grupo asignado a esta impresora.")
        AddNode = False
    End If
    
End Sub

Private Sub GrabarGrupoKDS(Departamento As String, Grupo As String, PrinterDesc As String, Clave As String)
    
    Dim rsGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(False, rsGrupo)
    
    rsGrupo.Open "SELECT * FROM MA_KDS_GRUPO " & _
    "WHERE cs_Relacion = '" & Clave & "' " & _
    "AND cs_Departamento = '" & Departamento & "' " & _
    "AND cs_Grupo = '" & Grupo & "'", _
    Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If rsGrupo.EOF Then
        Ent.POS.BeginTrans
        rsGrupo.AddNew
        rsGrupo!cs_Departamento = Departamento
        rsGrupo!cs_Grupo = Grupo
        rsGrupo!cu_KDS = PrinterDesc
        rsGrupo!cs_Relacion = Clave
        rsGrupo.UpdateBatch
        Ent.POS.CommitTrans
        AddNode = True
    Else
        Call Mensaje(True, "Ya existe el grupo asignado a este KDS.")
        AddNode = False
    End If
    
End Sub

Private Sub GrabarSubGrupo(Departamento As String, Grupo As String, SubGrupo As String, PrinterDesc As String, Clave As String)
    
    Dim rsSubGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(False, rsSubGrupo)
    
    rsSubGrupo.Open "SELECT * FROM MA_IMPRESORA_SUBGRUPO " & _
    "WHERE cs_Relacion = '" & Clave & "' " & _
    "AND cs_Departamento = '" & Departamento & "' " & _
    "AND cs_Grupo = '" & Grupo & "' " & _
    "AND cs_Subgrupo = '" & SubGrupo & "'", _
    Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If rsSubGrupo.EOF Then
        Ent.POS.BeginTrans
        rsSubGrupo.AddNew
        rsSubGrupo!cs_Departamento = Departamento
        rsSubGrupo!cs_Grupo = Grupo
        rsSubGrupo!cs_Subgrupo = SubGrupo
        rsSubGrupo!cu_impresora = PrinterDesc
        rsSubGrupo!cs_Relacion = Clave
        rsSubGrupo.UpdateBatch
        Ent.POS.CommitTrans
        AddNode = True
    Else
        Call Mensaje(True, "Ya existe el subgrupo asignado a esta impresora.")
        AddNode = False
    End If
    
End Sub

Private Sub GrabarSubGrupoKDS(Departamento As String, Grupo As String, SubGrupo As String, PrinterDesc As String, Clave As String)
    
    Dim rsSubGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(False, rsSubGrupo)
    
    rsSubGrupo.Open "SELECT * FROM MA_KDS_SUBGRUPO " & _
    "WHERE cs_Relacion = '" & Clave & "' " & _
    "AND cs_Departamento = '" & Departamento & "' " & _
    "AND cs_Grupo = '" & Grupo & "' " & _
    "AND cs_Subgrupo = '" & SubGrupo & "'", _
    Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If rsSubGrupo.EOF Then
        Ent.POS.BeginTrans
        rsSubGrupo.AddNew
        rsSubGrupo!cs_Departamento = Departamento
        rsSubGrupo!cs_Grupo = Grupo
        rsSubGrupo!cs_Subgrupo = SubGrupo
        rsSubGrupo!cu_KDS = PrinterDesc
        rsSubGrupo!cs_Relacion = Clave
        rsSubGrupo.UpdateBatch
        Ent.POS.CommitTrans
        AddNode = True
    Else
        Call Mensaje(True, "Ya existe el subgrupo asignado a este KDS.")
        AddNode = False
    End If
    
End Sub

Public Sub BuscarClasificacion(Clave As String, PrinterDesc As String)
    
    Dim rsDepartamento As New ADODB.Recordset, rsSubGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(True, rsDepartamento)
    Call Apertura_Recordset(True, rsSubGrupo)
    
    rsDepartamento.Open "SELECT * FROM MA_IMPRESORA_DEPARTAMENTO LEFT JOIN VAD10.dbo.MA_DEPARTAMENTOS AS MA_DEPARTAMENTOS ON MA_DEPARTAMENTOS.c_Codigo = MA_IMPRESORA_DEPARTAMENTO.cs_Departamento WHERE cs_Relacion = '" & _
    Mid(Clave, LPA + 1) & "' AND cu_Impresora = '" & PrinterDesc & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsDepartamento.EOF Then
        While Not rsDepartamento.EOF
            ARBOL.Nodes.add Clave, tvwChild, "|D|" & Mid(Clave, LPA + 1) & "/" & rsDepartamento!cs_Departamento, _
            IIf(IsNull(rsDepartamento!c_Descripcio), "", rsDepartamento!c_Descripcio), "subnivel"
            Call BuscarGrupos(rsDepartamento!cs_Departamento, Clave, PrinterDesc)
            rsDepartamento.MoveNext
        Wend
    End If
    
    rsDepartamento.Close
    
End Sub

Public Sub BuscarClasificacionKDS(Clave As String, PrinterDesc As String)
    
    Dim rsDepartamento As New ADODB.Recordset, rsSubGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(True, rsDepartamento)
    Call Apertura_Recordset(True, rsSubGrupo)
    
    rsDepartamento.Open "SELECT * FROM MA_KDS_DEPARTAMENTO LEFT JOIN VAD10.dbo.MA_DEPARTAMENTOS " & _
    "AS MA_DEPARTAMENTOS ON MA_DEPARTAMENTOS.c_Codigo = MA_KDS_DEPARTAMENTO.cs_Departamento " & _
    "WHERE cs_Relacion = '" & Mid(Clave, LPA + 1) & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsDepartamento.EOF Then
        While Not rsDepartamento.EOF
            ARBOL.Nodes.add Clave, tvwChild, "|T|" & Mid(Clave, LPA + 1) & "/" & rsDepartamento!cs_Departamento, _
            IIf(IsNull(rsDepartamento!c_Descripcio), "", rsDepartamento!c_Descripcio), "subnivel"
            Call BuscarGruposKDS(rsDepartamento!cs_Departamento, Clave, PrinterDesc)
            rsDepartamento.MoveNext
        Wend
    End If
    
    rsDepartamento.Close
    
End Sub

Public Sub BuscarGrupos(Departamento As String, Clave As String, PrinterDesc As String)
    
    Dim rsGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(True, rsGrupo)
                 
    rsGrupo.Open "SELECT * FROM MA_IMPRESORA_GRUPO LEFT JOIN VAD10.dbo.MA_GRUPOS AS MA_GRUPOS ON MA_GRUPOS.c_Codigo = MA_IMPRESORA_GRUPO.cs_Grupo AND MA_GRUPOS.c_Departamento = MA_IMPRESORA_GRUPO.cs_Departamento " & _
    " WHERE cs_Relacion = '" & Mid(Clave, LPA + 1) & "' AND cu_Impresora = '" & _
    PrinterDesc & "' AND cs_Departamento = '" & Departamento & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsGrupo.EOF Then
        While Not rsGrupo.EOF
            ARBOL.Nodes.add "|D|" & Mid(Clave, LPA + 1) & "/" & rsGrupo!cs_Departamento, _
            tvwChild, "|G|" & Mid(Clave, LPA + 1) & "/" & rsGrupo!cs_Grupo & "/" & rsGrupo!cs_Departamento, _
            IIf(IsNull(rsGrupo!c_Descripcio), "", rsGrupo!c_Descripcio), "subnivel"
            Call BuscarSubGrupos(Departamento, rsGrupo!cs_Grupo, Clave, PrinterDesc)
            rsGrupo.MoveNext
        Wend
    End If
    
    rsGrupo.Close
    
End Sub

Public Sub BuscarGruposKDS(Departamento As String, Clave As String, PrinterDesc As String)
    
    Dim rsGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(True, rsGrupo)
                 
    rsGrupo.Open "SELECT * FROM MA_KDS_GRUPO LEFT JOIN VAD10.dbo.MA_GRUPOS AS MA_GRUPOS " & _
    "ON MA_GRUPOS.c_Codigo = MA_KDS_GRUPO.cs_Grupo AND MA_GRUPOS.c_Departamento = " & _
    "MA_KDS_GRUPO.cs_Departamento WHERE cs_Relacion = '" & Mid(Clave, LPA + 1) & "' " & _
    "AND cs_Departamento = '" & Departamento & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsGrupo.EOF Then
        While Not rsGrupo.EOF
            ARBOL.Nodes.add "|T|" & Mid(Clave, LPA + 1) & "/" & rsGrupo!cs_Departamento, tvwChild, _
            "|L|" & Mid(Clave, LPA + 1) & "/" & rsGrupo!cs_Grupo & "/" & rsGrupo!cs_Departamento, _
            IIf(IsNull(rsGrupo!c_Descripcio), "", rsGrupo!c_Descripcio), "subnivel"
            Call BuscarSubGruposKDS(Departamento, rsGrupo!cs_Grupo, Clave, PrinterDesc)
            rsGrupo.MoveNext
        Wend
    End If
    
    rsGrupo.Close
    
End Sub

Public Sub BuscarSubGrupos(Departamento As String, Grupo As String, Clave As String, PrinterDesc As String)
    
    Dim rsSubGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(True, rsSubGrupo)
                 
    rsSubGrupo.Open "SELECT * FROM MA_IMPRESORA_SUBGRUPO LEFT JOIN VAD10.dbo.MA_SUBGRUPOS AS MA_SUBGRUPOS ON MA_SUBGRUPOS.c_Codigo = MA_IMPRESORA_SUBGRUPO.cs_Subgrupo AND MA_SUBGRUPOS.c_In_Departamento = MA_IMPRESORA_SUBGRUPO.cs_Departamento and MA_SUBGRUPOS.c_In_Grupo = MA_IMPRESORA_SUBGRUPO.cs_Grupo" & _
    " WHERE cs_Relacion = '" & Mid(Clave, LPA + 1) & "' AND cu_Impresora = '" & PrinterDesc & "' AND cs_Departamento = '" & Departamento & "' AND cs_Grupo = '" & Grupo & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsSubGrupo.EOF Then
        While Not rsSubGrupo.EOF
            ARBOL.Nodes.add "|G|" & Mid(Clave, LPA + 1) & "/" & rsSubGrupo!cs_Grupo & "/" & rsSubGrupo!cs_Departamento, _
            tvwChild, "|B|" & Mid(Clave, LPA + 1) & "/" & rsSubGrupo!cs_Subgrupo & "/" & rsSubGrupo!cs_Grupo & "/" & rsSubGrupo!cs_Departamento, _
            IIf(IsNull(rsSubGrupo!c_Descripcio), "", rsSubGrupo!c_Descripcio), "subnivel"
            rsSubGrupo.MoveNext
        Wend
    End If
    
    rsSubGrupo.Close
    
End Sub

Public Sub BuscarSubGruposKDS(Departamento As String, Grupo As String, Clave As String, PrinterDesc As String)
    
    Dim rsSubGrupo As New ADODB.Recordset
    
    Call Apertura_Recordset(True, rsSubGrupo)
                 
    rsSubGrupo.Open "SELECT * FROM MA_KDS_SUBGRUPO LEFT JOIN VAD10.dbo.MA_SUBGRUPOS AS MA_SUBGRUPOS " & _
    "ON MA_SUBGRUPOS.c_Codigo = MA_KDS_SUBGRUPO.cs_Subgrupo AND MA_SUBGRUPOS.c_In_Departamento " & _
    "= MA_KDS_SUBGRUPO.cs_Departamento and MA_SUBGRUPOS.c_In_Grupo = MA_KDS_SUBGRUPO.cs_Grupo" & _
    " WHERE cs_Relacion = '" & Mid(Clave, LPA + 1) & "' AND cs_Departamento = " & _
    "'" & Departamento & "' AND cs_Grupo = '" & Grupo & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsSubGrupo.EOF Then
        While Not rsSubGrupo.EOF
            ARBOL.Nodes.add "|L|" & Mid(Clave, LPA + 1) & "/" & rsSubGrupo!cs_Grupo & "/" & rsSubGrupo!cs_Departamento, _
            tvwChild, "|W|" & Mid(Clave, LPA + 1) & "/" & rsSubGrupo!cs_Subgrupo & "/" & rsSubGrupo!cs_Grupo & "/" & rsSubGrupo!cs_Departamento, _
            IIf(IsNull(rsSubGrupo!c_Descripcio), "", rsSubGrupo!c_Descripcio), "subnivel"
            rsSubGrupo.MoveNext
        Wend
    End If
    
    rsSubGrupo.Close
    
End Sub

Private Sub EliminarImpresora(Clave As String)
    Ent.POS.Execute "DELETE FROM MA_IMPRESORAS WHERE cs_Numero = '" & Clave & "'"
    Ent.POS.Execute "DELETE FROM MA_IMPRESORA_DEPARTAMENTO WHERE cs_Relacion = '" & Clave & "'"
    Ent.POS.Execute "DELETE FROM MA_IMPRESORA_GRUPO WHERE cs_Relacion = '" & Clave & "'"
    Ent.POS.Execute "DELETE FROM MA_IMPRESORA_SUBGRUPO WHERE cs_Relacion = '" & Clave & "'"
End Sub

Private Sub EliminarKDS(Clave As String)
    Ent.POS.Execute "DELETE FROM MA_KDS WHERE cs_CODIGO = '" & Clave & "'"
    Ent.POS.Execute "DELETE FROM MA_KDS_DEPARTAMENTO WHERE cs_Relacion = '" & Clave & "'"
    Ent.POS.Execute "DELETE FROM MA_KDS_GRUPO WHERE cs_Relacion = '" & Clave & "'"
    Ent.POS.Execute "DELETE FROM MA_KDS_SUBGRUPO WHERE cs_Relacion = '" & Clave & "'"
End Sub

Private Sub BuscarVendedores(Clave As String, Relacion As String)
    
    Dim rsVendedores As Object
    
    Set rsVendedores = RecordSets
    
    Call Apertura_Recordset(True, rsVendedores)
    
    rsVendedores.Open "SELECT * FROM MA_VENDEDORES " & _
    "WHERE cs_Relacion = '" & Clave & "' and cs_Localidad = '" & Sucursal & "'", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not rsVendedores.EOF
        ARBOL.Nodes.add Relacion, tvwChild, _
        "|R|" & rsVendedores!cu_Vendedor_Cod, rsVendedores!cu_Vendedor_Des, "user"
        rsVendedores.MoveNext
    Wend
    
End Sub

Private Sub ClearNode(ActNodo As Node)
    
    Dim Cont As Integer, NodoBorrar As Node, Hasta As Integer
    
    If ActNodo.Children > 0 Then
        Hasta = ActNodo.Children
        For Cont = 1 To Hasta
            Set NodoBorrar = ActNodo.Child
            ARBOL.Nodes.Remove NodoBorrar.Index
        Next Cont
    End If
    
End Sub

Private Sub Picture1_Click()
    Unload Me
End Sub
