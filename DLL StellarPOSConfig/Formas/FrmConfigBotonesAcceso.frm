VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmConfigBotonesAcceso 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8805
   ClientLeft      =   30
   ClientTop       =   90
   ClientWidth     =   8250
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmConfigBotonesAcceso.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8805
   ScaleWidth      =   8250
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6195
         TabIndex        =   7
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuración de Botones de Acceso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   8505
      TabIndex        =   2
      Top             =   420
      Width           =   8535
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   0
         TabIndex        =   3
         Top             =   0
         Width           =   8235
         Begin MSComctlLib.Toolbar tlb_Config 
            Height          =   810
            Left            =   195
            TabIndex        =   4
            Top             =   120
            Width           =   7905
            _ExtentX        =   13944
            _ExtentY        =   1429
            ButtonWidth     =   1667
            ButtonHeight    =   1429
            ToolTips        =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   7
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "Cancelar la configuración actual [F7]"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "Grabar esta configuración [F4]"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "salir"
                  Object.ToolTipText     =   "Salir del configurador [F12]"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "Ayuda del Configurador [F1]"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Restaurar"
                  Key             =   "Reset"
                  Description     =   "Restaura los botones a su configuración original."
                  Object.ToolTipText     =   "Restaura los botones a su configuración original."
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Rest. Todo"
                  Key             =   "ResetAll"
                  Description     =   "Restaura los botones a su configuración original."
                  Object.ToolTipText     =   "Restaura los botones a su configuración original."
                  ImageIndex      =   6
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.ComboBox Combo_Cell 
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "FrmConfigBotonesAcceso.frx":628A
      Left            =   2355
      List            =   "FrmConfigBotonesAcceso.frx":628C
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   2430
      Visible         =   0   'False
      Width           =   1725
   End
   Begin MSFlexGridLib.MSFlexGrid Botones 
      Height          =   6615
      Left            =   480
      TabIndex        =   0
      Top             =   1800
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   11668
      _Version        =   393216
      Cols            =   8
      FixedCols       =   2
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      GridLinesFixed  =   0
      ScrollBars      =   2
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   3720
      Top             =   1320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":628E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":8020
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":9DB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":BB44
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":D8D6
            Key             =   "Reset"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigBotonesAcceso.frx":E5B0
            Key             =   "ResetAll"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmConfigBotonesAcceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim PosGridCol As Integer, PosGridRow As Integer, KeyFlags As Integer, TmpRows As Long

Private Const Proc_Part1 = "-- SCRIPT PARA RESETEAR DE MANERA RÁPIDA LOS BOTONES EN UNA O VARIAS CAJAS." & vbNewLine & _
vbNewLine & _
"-- COMO FUNCIONA: POR MEDIO DE UN PROCEDIMIENTO ALMACENADO TEMPORAL QUE SE ENCARGA DE RESETEAR LOS" & vbNewLine & _
"-- BOTONES DE FUNCION DE UNA SOLA CAJA. POR LO TANTO LA CLAVE ES UTILIZAR EL PROCEDIMIENTO ALMACENADO" & vbNewLine & _
"-- PARA TANTAS CAJAS COMO SE NECESITE. ABAJO ESTA EL PROCEDIMIENTO ALMACENADO EN CUESTION, PARA SABER" & vbNewLine & _
"-- COMO USARLO Y LOS PARÁMETROS QUE REQUIERE, LEER LOS COMENTARIOS DENTRO DE LA DEFINICIÓN DEL MISMO." & vbNewLine & _
 vbNewLine & _
"-- PARA UTILIZAR ESTE SCRIPT, BAJA HACIA LA LÍNEA QUE DICE:" & vbNewLine & _
"-- COMENZAR" & vbNewLine & _
"-- SIGUE EL EJEMPLO, COLOCA LAS CAJAS CUYOS BOTONES SE VAN A RESETEAR Y EJECUTA EL SCRIPT." & vbNewLine & _
 vbNewLine & _
"--IF (OBJECT_ID ('ResetearBotonesFuncionPOSFOOD#', 'P') IS NOT NULL)" & vbNewLine & _
vbTab & "--DROP PROCEDURE ResetearBotonesFuncionPOSFOOD#" & vbNewLine & _
"--GO" & vbNewLine & _
 vbNewLine & _
"CREATE PROCEDURE ResetearBotonesFuncionPOSFOOD#" & vbNewLine & _
vbTab & "@Caja NVARCHAR(MAX)" & vbNewLine & _
"AS" & vbNewLine & _
vbNewLine & _
vbTab & "-- PROCEDIMIENTO PARA RESETEAR DE MANERA RÁPIDA LOS BOTONES EN UNA CAJA." & vbNewLine & _
vbNewLine & _
vbTab & "-- 1) CONECTAR SCRIPT A LA INSTANCIA DONDE ESTA BD VAD20."

Private Const Proc_Part2 = _
vbNewLine & _
vbTab & "-- 2) ASIGNAR LOS VALORES DE LAS VARIABLES:" & vbNewLine & _
vbTab & "-- @Caja = Numero (PK) de la Caja." & vbNewLine & _
vbNewLine & _
vbTab & "-- 3) COMO FUNCIONA: Borra el registro en MA_CAJA_CONFIGURACION para @Caja, y lo vuelve a ingresar con respecto" & vbNewLine & _
vbTab & "-- a la configuración inicial en MA_CAJA_BOTONES." & vbNewLine & _
vbNewLine & _
vbTab & "IF (@Caja = '*')" & vbNewLine & _
vbTab & "BEGIN" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "DECLARE AllRows CURSOR READ_ONLY" & vbNewLine & _
vbTab & vbTab & "FOR SELECT c_Codigo FROM VAD20..MA_CAJA" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "OPEN AllRows" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "FETCH NEXT FROM AllRows INTO @Caja" & vbNewLine & _
vbTab & vbTab & "WHILE @@FETCH_STATUS = 0"

Private Const Proc_Part3 = _
vbNewLine & _
vbTab & vbTab & "BEGIN" & vbNewLine & _
vbTab & vbTab & vbTab & "IF (@Caja = '*') BREAK" & vbNewLine & _
vbTab & vbTab & vbTab & "EXEC ResetearBotonesFuncionPOSFOOD# @Caja" & vbNewLine & _
vbTab & vbTab & vbTab & "FETCH NEXT FROM AllRows INTO @Caja" & vbNewLine & _
vbTab & vbTab & "End" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "Close AllRows" & vbNewLine & _
vbTab & vbTab & "DEALLOCATE AllRows" & vbNewLine & _
vbNewLine & _
vbTab & "END" & vbNewLine & _
vbTab & "ELSE" & vbNewLine & _
vbNewLine & _
vbTab & "BEGIN" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "DECLARE @TmpQuery NVARCHAR(MAX)" & vbNewLine & _
vbTab & vbTab & "DECLARE @TipoPOS INT"

Private Const Proc_Part4 = _
vbNewLine & _
vbTab & vbTab & "SET @TipoPOS = (SELECT CASE" & vbNewLine & _
vbTab & vbTab & "WHEN b_POS_Digital = 0 THEN 0" & vbNewLine & _
vbTab & vbTab & "WHEN b_POS_Digital = 1 AND b_POS_Pedido = 0 THEN 1" & vbNewLine & _
vbTab & vbTab & "WHEN b_POS_Digital = 1 AND b_POS_Pedido = 1 THEN 2" & vbNewLine & _
vbTab & vbTab & "END AS TipoPOS FROM [VAD20]..MA_CAJA WHERE c_Codigo = @Caja)" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "IF (@TipoPOS IS NOT NULL)" & vbNewLine & _
vbTab & vbTab & "BEGIN" & vbNewLine & _
vbNewLine

Private Const Proc_Part5 = _
vbNewLine & _
vbTab & vbTab & vbTab & "DELETE FROM [VAD20]..MA_CAJA_CONFIGURACION WHERE Caja = @Caja" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO [VAD20]..MA_CAJA_CONFIGURACION (Caja, Posicion, Nombre_Boton, Tecla, Shift, Activo, Nivel, Llave, bNivel, bLLave, bActivo, ResourceID)" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT @Caja AS Caja, Posicion, Nombre, Tecla, Shift, bDefault, 1, bLLave, bNivel, bLLave, bActivo, ResourceID     FROM [VAD20]..MA_CAJA_BOTONES WHERE TipoPOS = @TipoPOS" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "PRINT CHAR(13) + 'Los Botones de Funcion para la Caja [' + @Caja + '][' + CAST(@TipoPOS AS NVARCHAR(MAX)) + '] se han restaurado con exito.'" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "END" & vbNewLine & _
vbTab & vbTab & "ELSE" & vbNewLine & _
vbTab & vbTab & "BEGIN" & vbNewLine & _
vbTab & vbTab & vbTab & "PRINT CHAR(13) + 'La Caja [' + @Caja + '] posee una configuración o tipo inválido. No se realizó ninguna acción.'" & vbNewLine & _
vbTab & vbTab & "END" & vbNewLine & _
vbNewLine & _
vbTab & "END" & vbNewLine & _
vbNewLine

Private Const ProcedimientoSQL_Base = Proc_Part1 & Proc_Part2 & Proc_Part3 & Proc_Part4 & Proc_Part5

Private Const ProcemientoSQL_Caja_Base = "EXECUTE ResetearBotonesFuncionPOSFOOD# '$(POSNumberPlaceHolder)'"

Private Const ProcedimientoSQL_All = "EXECUTE ResetearBotonesFuncionPOSFOOD# '*'"

Private Const ProcedimientoSQL_Limpiar = _
"IF (OBJECT_ID ('ResetearBotonesFuncionPOSFOOD#', 'P') IS NOT NULL)" & vbNewLine & _
vbTab & "DROP PROCEDURE ResetearBotonesFuncionPOSFOOD#"

Private ProcedimientoSQL_Caja               As String
Private ProcedimientoSQL                    As String
Private Reseteando                          As Boolean

Public Sub LlenarGrid()
    
    Dim rsBotones As New ADODB.Recordset
    Dim clsConfig As New Cls_POSConfig
    
    If EsDigital Then
        
        Call Apertura_Recordset(False, rsBotones)
        
        rsBotones.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "' ORDER BY Posicion", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        Botones.Row = Botones.Rows - 1
        
        If Not rsBotones.EOF Then
            While Not rsBotones.EOF
                With Botones
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = rsBotones!Nombre_Boton
                    .Col = 1
                    
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = IIf(ExisteCampoTabla("ResourceID", rsBotones), _
                    IIf(rsBotones!ResourceID <> 0, _
                    StellarMensaje(rsBotones!ResourceID, rsBotones!Nombre_Boton), rsBotones!Nombre_Boton) _
                    , rsBotones!Nombre_Boton)
                    
                    .Col = 2
                    .Text = IIf(rsBotones!Activo, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!Nivel
                    .Col = 4
                    .Text = rsBotones!Llave
                    .Col = 5
                    .Text = IIf(rsBotones!bActivo, "1", "0")
                    .Col = 6
                    .Text = IIf(rsBotones!BNIVEL, "1", "0")
                    .Col = 7
                    .Text = IIf(rsBotones!BLLAVE, "1", "0")
                    .Col = 8
                    .Text = rsBotones!Posicion
                End With
                rsBotones.MoveNext
            Wend
            
            Botones.Row = 1
            Botones.Col = 1
            
            rsBotones.Close
            
        End If
        
    Else
        
        Call Apertura_Recordset(False, rsBotones)
        
        rsBotones.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "' ORDER BY Posicion", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        Botones.Row = Botones.Rows - 1
        
        If Not rsBotones.EOF Then
            
            While Not rsBotones.EOF
                With Botones
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = rsBotones!Nombre_Boton
                    .Col = 1
                    
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = IIf(ExisteCampoTabla("ResourceID", rsBotones), _
                    IIf(rsBotones!ResourceID <> 0, _
                    StellarMensaje(rsBotones!ResourceID, rsBotones!Nombre_Boton), rsBotones!Nombre_Boton) _
                    , rsBotones!Nombre_Boton)
                    
                    .Col = 2
                    '.Text = IIf(rsBotones!Activo, "Si", "No")
                    .Text = IIf(rsBotones!Activo, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!Nivel
                    .Col = 4
                    .Text = rsBotones!Llave
                    .Col = 5
                    .Text = IIf(rsBotones!bActivo, "1", "0")
                    .Col = 6
                    .Text = IIf(rsBotones!BNIVEL, "1", "0")
                    .Col = 7
                    .Text = IIf(rsBotones!BLLAVE, "1", "0")
                    .Col = 8
                    .Text = rsBotones!Posicion
                End With
                rsBotones.MoveNext
            Wend
            
            Botones.Row = 1
            Botones.Col = 2
            
            rsBotones.Close
            
        Else
            
            rsBotones.Close
            
            rsBotones.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
            Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not rsBotones.EOF Then
                
                With Botones
                    
                    Dim PosCont As Long: PosCont = 0
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F1 Ayuda"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10161, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bAyuda, "Si", "No")
                    .Text = IIf(rsBotones!bAyuda, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nAyuda
                    .Col = 4
                    .Text = rsBotones!lAyuda
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F2 Buscar"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10146, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bProductos, "Si", "No")
                    .Text = IIf(rsBotones!bProductos, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nProductos
                    .Col = 4
                    .Text = rsBotones!lProductos
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F3 Limite"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10147, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bLimite, "Si", "No")
                    .Text = IIf(rsBotones!bLimite, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nLimite
                    .Col = 4
                    .Text = rsBotones!lLimite
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F4 Cantidad"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10148, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bCantidad, "Si", "No")
                    .Text = IIf(rsBotones!bCantidad, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nCantidad
                    .Col = 4
                    .Text = rsBotones!lCantidad
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F5 Balanza"
                    .Col = 1
                    '.Text = .Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10149, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bBalanza, "Si", "No")
                    .Text = IIf(rsBotones!bBalanza, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nBalanza
                    .Col = 4
                    .Text = rsBotones!lBalanza
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F6 Reintegro Total"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10150, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bReintegro, "Si", "No")
                    .Text = IIf(rsBotones!bReintegro, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nReintegro
                    .Col = 4
                    .Text = rsBotones!lReintegro
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F6 Reintegro Parcial"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10151, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bParcial, "Si", "No")
                    .Text = IIf(rsBotones!bParcial, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nParcial
                    .Col = 4
                    .Text = rsBotones!lParcial
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F7 Espera"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10152, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bEspera, "Si", "No")
                    .Text = IIf(rsBotones!bEspera, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nEspera
                    .Col = 4
                    .Text = rsBotones!lEspera
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F8 Totalizar"
                    .Col = 1
                    .Text = StellarMensaje(10153, .TextMatrix(.Row, 0))
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Col = 2
                    .Text = Stellar_Mensaje(10144, True)
                    .Col = 3
                    .Text = "0"
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "1"
                    .Col = 6
                    .Text = "1"
                    .Col = 7
                    .Text = "1"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F9 Suspender"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10154, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bSuspender, "Si", "No")
                    .Text = IIf(rsBotones!bSuspender, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nSuspender
                    .Col = 4
                    .Text = rsBotones!lSuspender
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F10 Devolver"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10155, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bDevolucion, "Si", "No")
                    .Text = IIf(rsBotones!bDevolucion, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nDevolucion
                    .Col = 4
                    .Text = rsBotones!lDevolucion
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F11 Precios"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10156, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bPrecios, "Si", "No")
                    .Text = IIf(rsBotones!bPrecios, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nPrecios
                    .Col = 4
                    .Text = rsBotones!lPrecios
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "F12 Clientes"
                    .Col = 1
                    '.Text = IIf(rsBotones!ResourceID <> 0, stellar_mensaje(rsBotones!ResourceID), rsBotones!Nombre_Boton)
                    .Text = StellarMensaje(10157, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = IIf(rsBotones!bCliente, "Si", "No")
                    .Text = IIf(rsBotones!bCliente, Stellar_Mensaje(10144, True), Stellar_Mensaje(10145, True))
                    .Col = 3
                    .Text = rsBotones!nCliente
                    .Col = 4
                    .Text = rsBotones!lCliente
                    .Col = 5
                    .Text = "0"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "Apertura de Caja"
                    .Col = 1
                    '.Text = "Apertura de Caja"
                    .Text = StellarMensaje(10158, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = "Si"
                    .Text = Stellar_Mensaje(10144, True)
                    .Col = 3
                    .Text = rsBotones!nOpenPOS
                    .Col = 4
                    .Text = "0"
                    .Col = 5
                    .Text = "1"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "1"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = "Cierre de Caja"
                    .Col = 1
                    .Text = StellarMensaje(10159, .TextMatrix(.Row, 0))
                    .Col = 2
                    '.Text = "Si"
                    .Text = Stellar_Mensaje(10144, True)
                    .Col = 3
                    .Text = rsBotones!nClosePOS
                    .Col = 4
                    .Text = rsBotones!lCerrar
                    .Col = 5
                    .Text = "1"
                    .Col = 6
                    .Text = "0"
                    .Col = 7
                    .Text = "0"
                    .Col = 8
                    PosCont = PosCont + 1
                    .Text = PosCont
                    
                End With
                
                rsBotones.Close
                
            End If
            
        End If
        
    End If
    
    If Not Botones.RowIsVisible(Botones.Rows - 1) Then
        Botones.ColWidth(1) = 3595
    End If
    
    Botones.ScrollTrack = True
    
End Sub

Public Sub InicializarGrid(LGrid As MSFlexGrid)
    
    LGrid.Clear
    LGrid.Rows = 1
    LGrid.Row = 0
    LGrid.Cols = 9
    
    LGrid.Col = 0
    'LGrid.Text = "Botón"
    LGrid.Text = "ID_BOTON"
    LGrid.ColWidth(0) = 0
    LGrid.CellAlignment = flexAlignLeftCenter
    'LGrid.ColAlignment = flexAlignLeftCenter

    LGrid.Col = 1
    'LGrid.Text = "Botón"
    LGrid.Text = Stellar_Mensaje(10141, True)
    LGrid.ColWidth(1) = 3850
    
    LGrid.Col = 2
    'LGrid.Text = "Activo"
    LGrid.Text = Stellar_Mensaje(165, True)
    LGrid.ColWidth(2) = 1125
    LGrid.ColAlignment(LGrid.Col) = flexAlignCenterCenter
    
    LGrid.Col = 3
    'LGrid.Text = "Nivel"
    LGrid.Text = Stellar_Mensaje(10142, True)
    LGrid.ColWidth(3) = 1125
    LGrid.ColAlignment(LGrid.Col) = flexAlignCenterCenter
    
    LGrid.Col = 4
    'LGrid.Text = "Llave"
    LGrid.Text = Stellar_Mensaje(10143, True)
    LGrid.ColWidth(4) = 1125
    LGrid.ColAlignment(LGrid.Col) = flexAlignCenterCenter

    LGrid.Col = 5
    LGrid.Text = "BACTIVO"
    LGrid.ColWidth(5) = 0
    
    LGrid.Col = 6
    LGrid.Text = "BNIVEL"
    LGrid.ColWidth(6) = 0
    
    LGrid.Col = 7
    LGrid.Text = "BLLAVE"
    LGrid.ColWidth(7) = 0
    
    LGrid.Col = 8
    LGrid.Text = "Posicion"
    LGrid.ColWidth(8) = 0
    
End Sub

Private Sub Botones_Click()
    Select Case Botones.Col
        Case 2 'Activo
            If ConsultarCelda(Botones.Row, 5, Botones) = "0" Then
                'Botones.Text = IIf(UCase(Botones.Text) = "SI", "No", "Si")
'                If UCase(Botones.Text) = stellar_mensaje(10144, True) Then
            If UCase(Botones.Text) = UCase(Stellar_Mensaje(10144, True)) Then
                    Botones.Text = Stellar_Mensaje(10145, True)
                Else
                    Botones.Text = Stellar_Mensaje(10144, True)
                End If
            End If
        Case 3 'Nivel
            If ConsultarCelda(Botones.Row, 6, Botones) = "0" Then
                'If UCase(ConsultarCelda(Botones.Row, 1, Botones)) = "SI" Then
                If UCase(ConsultarCelda(Botones.Row, 2, Botones)) = UCase(Stellar_Mensaje(10144, True)) Then
                    KeyFlags = vbKeyDown
                    Call LlenarCombo(Combo_Cell, 0, 9)
                    Combo_Cell.ListIndex = CInt(ConsultarCelda(Botones.Row, Botones.Col, Botones))
                    Call ObjectDsp(Combo_Cell, Botones)
                End If
            End If
        Case 4 'Llave
            If UsaLlave And ConsultarCelda(Botones.Row, 7, Botones) = "0" Then
                KeyFlags = vbKeyDown
                Call LlenarCombo(Combo_Cell, 0, 6)
                Combo_Cell.ListIndex = CInt(ConsultarCelda(Botones.Row, Botones.Col, Botones))
                Call ObjectDsp(Combo_Cell, Botones)
            End If
    End Select
End Sub

Private Sub Botones_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            Select Case KeyCode
                Case vbKeyReturn, vbKeySpace
                    Call Botones_Click
            End Select
    End Select
    KeyFlags = KeyCode
End Sub

Private Sub Combo_Cell_Click()
    If KeyFlags <> vbKeyUp And KeyFlags <> vbKeyDown Then
        Call Combo_Cell_LostFocus
    End If
End Sub

Private Sub Combo_Cell_GotFocus()
    KeyFlags = 0
    PosGridCol = Botones.Col
    PosGridRow = Botones.Row
End Sub

Private Sub Combo_Cell_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            Select Case KeyCode
                Case vbKeyReturn
                    Call Combo_Cell_LostFocus
                Case vbKeyEscape
                    Combo_Cell.Visible = False
            End Select
    End Select
    KeyFlags = KeyCode
End Sub

Private Sub Combo_Cell_LostFocus()
    Combo_Cell.Visible = False
    If Combo_Cell.Text <> "" Then Call EscribirCelda(PosGridRow, PosGridCol, Botones, Combo_Cell.Text)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbCtrlMask
        Case vbAltMask
        Case Else
            Select Case KeyCode
                Case vbKeyF4
                    Grabar
                Case vbKeyF7
                    Call Cancelar
                Case vbKeyF12
                    Unload Me
            End Select
    End Select
End Sub

Private Sub Form_Load()
    Call Cancelar
    Me.lbl_Organizacion.Caption = StellarMensaje(10140)
    Me.tlb_Config.Buttons(1).Caption = StellarMensaje(6)
    Me.tlb_Config.Buttons(2).Caption = StellarMensaje(103)
    Me.tlb_Config.Buttons(3).Caption = StellarMensaje(54)
    Me.tlb_Config.Buttons(4).Caption = StellarMensaje(7)
    Me.tlb_Config.Buttons("Reset").Caption = StellarMensaje(10351)
    Me.tlb_Config.Buttons("Reset").ToolTipText = StellarMensaje(10353)
    Me.tlb_Config.Buttons("ResetAll").Caption = StellarMensaje(10352)
    Me.tlb_Config.Buttons("ResetAll").ToolTipText = StellarMensaje(10353)
End Sub

Public Sub Cancelar()
    Call InicializarGrid(Botones)
    Call LlenarGrid
End Sub

Public Sub ObjectDsp(Objeto As Object, LGrid As MSFlexGrid)
    Objeto.left = LGrid.left + LGrid.CellLeft
    Objeto.top = LGrid.top + LGrid.CellTop
    Objeto.Width = LGrid.ColWidth(LGrid.Col)
    If UCase(TypeName(Objeto)) = "TEXTBOX" Then Objeto.Height = LGrid.RowHeight(LGrid.Row)
    Objeto.Visible = True
    Objeto.SetFocus
End Sub

Public Function ConsultarCelda(linea As Integer, Columna As Integer, LGrid As MSFlexGrid) As Variant
    
    Dim ACol As Integer, ALinea As Integer
    
    ACol = LGrid.Col
    ALinea = LGrid.Row
    
    LGrid.Col = Columna
    LGrid.Row = linea
    ConsultarCelda = LGrid.Text
    LGrid.Col = ACol
    LGrid.Row = ALinea
    
End Function

Public Function EscribirCelda(linea As Integer, Columna As Integer, LGrid As MSFlexGrid, Valor As Variant) As Variant
    
    Dim ACol As Integer, ALinea As Integer
    
    ACol = LGrid.Col
    ALinea = LGrid.Row
    
    LGrid.Col = Columna
    LGrid.Row = linea
    LGrid.Text = Valor
    LGrid.Col = ACol
    LGrid.Row = ALinea
    
End Function

Public Sub LlenarCombo(ByRef LCombo As ComboBox, Optional Min As Integer = 0, Optional Max As Integer = 9)
    
    Dim LCont As Integer
    
    LCombo.Clear
    
    For LCont = Min To Max
        LCombo.AddItem LCont
    Next LCont
    
End Sub

Private Function UsaLlave() As Boolean

    Dim rsCaja As New ADODB.Recordset
    
    UsaLlave = False
    
    Call Apertura_Recordset(False, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
    Ent.POS, adOpenDynamic, adLockReadOnly, adCmdText
    
    If Not rsCaja.EOF Then
        UsaLlave = rsCaja!Llaves And rsCaja!b_Opos
    End If
    
End Function

Private Sub Grabar()
    
    Dim rsCajaConfig As New ADODB.Recordset, rsCaja As New ADODB.Recordset, LCont As Integer
    
    If EsDigital Then
        
        Ent.POS.BeginTrans
        Botones.Enabled = False
        
        For LCont = 1 To Botones.Rows - 1
            
            Call Apertura_Recordset(False, rsCajaConfig)
            
            rsCajaConfig.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "' AND Posicion = '" & ConsultarCelda(LCont, 8, Botones) & "'", _
            Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
            
            If rsCajaConfig.EOF Then
                Call Mensaje(True, Stellar_Mensaje(10160, True))
                Ent.POS.RollbackTrans
                Unload Me
            End If
            
            rsCajaConfig!Caja = nCaja
            rsCajaConfig!Nombre_Boton = ConsultarCelda(LCont, 0, Botones)
            rsCajaConfig!Activo = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True)) 'si
            rsCajaConfig!Nivel = CInt(ConsultarCelda(LCont, 3, Botones))
            rsCajaConfig!Llave = CInt(ConsultarCelda(LCont, 4, Botones))
            rsCajaConfig.UpdateBatch
            
        Next LCont
        
        Ent.POS.CommitTrans
        Botones.Enabled = True
        
        Call InicializarGrid(Botones)
        
    Else
        
        Ent.POS.BeginTrans
        Botones.Enabled = False
          
        rsCaja.Open "SELECT * FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'", _
        Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
        
        If rsCaja.EOF Then
            Mensajes.Mensaje "Ha ocurrido un error, la forma se cerrará."
            Ent.POS.RollbackTrans
            Unload Me
            Exit Sub
        End If
        
        For LCont = 1 To Botones.Rows - 1
            
            Select Case UCase(ConsultarCelda(LCont, 0, Botones))
            
                Case UCase("F1 Ayuda"), UCase(Stellar_Mensaje(10161, True))
                    rsCaja!bAyuda = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nAyuda = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lAyuda = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cAyuda = IIf(rsCaja!nAyuda <> 0, 1, 0)
                                                        
                Case UCase("F2 Buscar"), UCase(Stellar_Mensaje(10146, True))
                    rsCaja!bProductos = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nProductos = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lProductos = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cProductos = IIf(rsCaja!nProductos <> 0, 1, 0)
                    
                Case UCase("F3 Limite"), UCase(Stellar_Mensaje(10147, True))
                    rsCaja!bLimite = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nLimite = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lLimite = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cLimite = IIf(rsCaja!nProductos <> 0, 1, 0)
                    
                Case UCase("F4 Cantidad"), UCase(Stellar_Mensaje(10148, True))
                    rsCaja!bCantidad = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nCantidad = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lCantidad = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cCantidad = IIf(rsCaja!nCantidad <> 0, 1, 0)
                    
                Case UCase("F5 Balanza"), UCase(Stellar_Mensaje(10149, True))
                    rsCaja!bBalanza = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nBalanza = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lBalanza = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cBalanza = IIf(rsCaja!nBalanza <> 0, 1, 0)
                    
                Case UCase("F6 Reintegro Total"), UCase(Stellar_Mensaje(10150, True))
                    rsCaja!bReintegro = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nReintegro = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lReintegro = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cReintegro = IIf(rsCaja!nReintegro <> 0, 1, 0)
                    
                Case UCase("F6 Reintegro Parcial"), UCase(Stellar_Mensaje(10151, True))
                    rsCaja!bParcial = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nParcial = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lParcial = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cParcial = IIf(rsCaja!nParcial <> 0, 1, 0)
                    
                Case UCase("F7 Espera"), UCase(Stellar_Mensaje(10152, True))
                    rsCaja!bEspera = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nEspera = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lEspera = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cEspera = IIf(rsCaja!nEspera <> 0, 1, 0)
                    
                Case UCase("F9 Suspender"), UCase(Stellar_Mensaje(10154, True))
                    rsCaja!bSuspender = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nSuspender = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lSuspender = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cSuspender = IIf(rsCaja!nSuspender <> 0, 1, 0)
                    
                Case UCase("F10 Devolver"), UCase(Stellar_Mensaje(10155, True))
                    rsCaja!bDevolucion = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nDevolucion = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lDevolucion = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cDevolucion = IIf(rsCaja!nDevolucion <> 0, 1, 0)
                    
                Case UCase("F11 Precios"), UCase(Stellar_Mensaje(10156, True))
                    rsCaja!bPrecios = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nPrecios = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lPrecios = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cPrecios = IIf(rsCaja!nPrecios <> 0, 1, 0)
                    
                Case UCase("F12 Clientes"), UCase(Stellar_Mensaje(10157, True))
                    rsCaja!bCliente = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                    rsCaja!nCliente = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lCliente = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cCliente = IIf(rsCaja!nCliente <> 0, 1, 0)
                    
                Case UCase("Apertura de Caja"), UCase(Stellar_Mensaje(10158, True))
                    rsCaja!nOpenPOS = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!cClosePOS = IIf(rsCaja!nProductos <> 0, 1, 0)
                    
                Case UCase("Cierre de Caja"), UCase(Stellar_Mensaje(10159, True))
                    rsCaja!nClosePOS = CInt(ConsultarCelda(LCont, 3, Botones))
                    rsCaja!lCerrar = CInt(ConsultarCelda(LCont, 4, Botones))
                    rsCaja!cClosePOS = IIf(rsCaja!nClosePOS <> 0, 1, 0)
                    
            End Select
            
        Next LCont
        
        rsCaja.UpdateBatch
        
        For LCont = 1 To Botones.Rows - 1
            
            Call Apertura_Recordset(False, rsCajaConfig)
            
            rsCajaConfig.Open "SELECT * FROM MA_CAJA_CONFIGURACION WHERE Caja = '" & nCaja & "' AND Posicion = '" & ConsultarCelda(LCont, 8, Botones) & "'", _
            Ent.POS, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
            
            If Not rsCajaConfig.EOF Then
                
                rsCajaConfig!Caja = nCaja
                rsCajaConfig!Nombre_Boton = ConsultarCelda(LCont, 0, Botones)
                'rsCajaConfig!ResourceID = ConsultarCelda(LCont, 1, Botones)
                rsCajaConfig!Activo = UCase(ConsultarCelda(LCont, 2, Botones)) = UCase(Stellar_Mensaje(10144, True))
                rsCajaConfig!Nivel = CInt(ConsultarCelda(LCont, 3, Botones))
                rsCajaConfig!Llave = CInt(ConsultarCelda(LCont, 4, Botones))
                rsCajaConfig.UpdateBatch
                
            End If
            
        Next LCont
        
        Ent.POS.CommitTrans
        Botones.Enabled = True
        
        Call InicializarGrid(Botones)
        
    End If
    
    If Reseteando Then
        ' Configuración guardada con Exito.
        Mensaje True, StellarMensaje(10350)
        Reseteando = False
        Form_Load
    Else
        Unload Me
    End If
    
End Sub

Private Sub tlb_Config_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        
        Case UCase("Grabar")
            
            'Call Grabar
            Form_KeyDown vbKeyF4, 0
            
        Case UCase("Cancelar")
            
            'Call Cancelar
            Form_KeyDown vbKeyF7, 0
            
        'Case "AYUDA"
        
        Case UCase("Salir")
            
            Form_KeyDown vbKeyF12, 0
            
        Case UCase("Reset")
            
            On Error Resume Next
            
            ' Atención, la configuración de botones para la caja seleccionada [$(POSNumber)][$(POSDescription)] _
            será restaurada a sus valores originales. Si está seguro, presione aceptar para continuar.
            
            Mensaje True, Replace(Replace(StellarMensaje(10354), "$(POSNumber)", nCaja), _
            "$(POSDescription)", Ent.POS.Execute( _
            "SELECT c_Desc_Caja FROM MA_CAJA WHERE c_Codigo = '" & nCaja & "'")!c_Desc_Caja), True
            
            If Retorno Then
                
                ProcedimientoSQL = ProcedimientoSQL_Limpiar
                Ent.POS.Execute ProcedimientoSQL, TmpRows
                
                ProcedimientoSQL = ProcedimientoSQL_Base
                Ent.POS.Execute ProcedimientoSQL, TmpRows
                
                ' Debug
                'Clipboard.Clear
                'Clipboard.SetText ProcedimientoSQL
                
                ProcedimientoSQL_Caja = Replace(ProcemientoSQL_Caja_Base, "$(POSNumberPlaceHolder)", nCaja)
                ProcedimientoSQL = ProcedimientoSQL_Caja
                Ent.POS.Execute ProcedimientoSQL, TmpRows
                                
                If Not EsPedidos And Not EsDigital Then ' POS Retail - MA_CAJA
                    Form_Load
                    Reseteando = True
                    Form_KeyDown vbKeyF4, 0
                Else
                    ' Configuración guardada con Exito.
                    Mensaje True, StellarMensaje(10350)
                    Form_Load
                End If
                
            End If
            
        Case UCase("ResetAll")
            
            On Error Resume Next
            
            ' Atención, la configuración de botones para Todas las Cajas será restaurada a _
            sus valores originales. Si está seguro, presione aceptar para continuar.
            
            Mensaje True, StellarMensaje(10355), True
            
            If Retorno Then
                
                ProcedimientoSQL = ProcedimientoSQL_Limpiar
                Ent.POS.Execute ProcedimientoSQL, TmpRows
                
                ProcedimientoSQL = ProcedimientoSQL_Base
                Ent.POS.Execute ProcedimientoSQL, TmpRows
                
                ProcedimientoSQL = ProcedimientoSQL_All
                Ent.POS.Execute ProcedimientoSQL, TmpRows
                
                If Not EsPedidos And Not EsDigital Then ' POS Retail - MA_CAJA
                    Ent.POS.Execute "UPDATE MA_CAJA SET " & _
                    "bAyuda = 1, nAyuda = 1, lAyuda = 0, " & _
                    "bProductos = 1, nProductos = 1, lProductos = 0, " & _
                    "bLimite = 1, nLimite = 1, lLimite = 0, " & _
                    "bCantidad = 1, nCantidad = 1, lCantidad = 0, " & _
                    "bReintegro = 1, nReintegro = 1, lParcial = 0, " & _
                    "bEspera = 1, nEspera = 1, lEspera = 0, " & _
                    "bSuspender = 1, nSuspender = 1, lSuspender = 0, " & _
                    "bDevolucion = 1, nDevolucion = 1, lDevolucion = 0, " & _
                    "bPrecios = 1, nPrecios = 1, lPrecios = 0, " & _
                    "bCliente = 1, nCliente = 1, lCliente = 0, " & _
                    "bBalanza = 1, nBalanza = 1, lBalanza = 0, " & _
                    "nOpenPOS = 1, nClosePOS = 1, lCerrar = 0 " & _
                    "WHERE b_POS_Digital = 0 AND b_POS_Pedido = 0"
                    Form_Load
                    Reseteando = True
                    Form_KeyDown vbKeyF4, 0
                Else
                    ' Configuración guardada con Exito.
                    Mensaje True, StellarMensaje(10350)
                    Form_Load
                End If
                
            End If
            
    End Select
    
End Sub
