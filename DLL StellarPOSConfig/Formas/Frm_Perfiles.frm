VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Frm_Perfiles 
   Caption         =   "Punto Restaurant"
   ClientHeight    =   2895
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4470
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2895
   ScaleWidth      =   4470
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_buscar 
      Caption         =   "..."
      Height          =   360
      Left            =   2100
      TabIndex        =   1
      Top             =   1305
      Width           =   435
   End
   Begin VB.Frame Frame1 
      Height          =   900
      Left            =   15
      TabIndex        =   5
      Top             =   -45
      Width           =   4425
      Begin MSComctlLib.Toolbar tbr_Perfiles 
         Height          =   675
         Left            =   75
         TabIndex        =   6
         Top             =   165
         Width           =   4260
         _ExtentX        =   7514
         _ExtentY        =   1191
         ButtonWidth     =   1217
         ButtonHeight    =   1191
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         ImageList       =   "Icono_Apagado"
         DisabledImageList=   "Icono_Apagado"
         HotImageList    =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   5
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Nuevo"
               Key             =   "Nuevo"
               Object.ToolTipText     =   "Agregar Nuevo Perfil"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "Buscar"
               Object.ToolTipText     =   "Buscar Perfil"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "Grabar"
               Object.ToolTipText     =   "Graba Perfil"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agregar"
               Key             =   "Agregar"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "Salir"
               ImageIndex      =   8
            EndProperty
         EndProperty
      End
   End
   Begin VB.TextBox txt_Descripcion 
      BackColor       =   &H80000018&
      Height          =   630
      Left            =   330
      TabIndex        =   2
      Top             =   2100
      Width           =   3720
   End
   Begin VB.TextBox txt_CodigoPerfil 
      BackColor       =   &H80000018&
      CausesValidation=   0   'False
      Height          =   330
      Left            =   330
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   1305
      Width           =   1755
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   3825
      Top             =   930
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":0000
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":0CDC
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":19B8
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":2694
            Key             =   "inter"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":28E0
            Key             =   "Nuevo"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":2F5A
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":35D4
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":3A26
            Key             =   "salir"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   3135
      Top             =   930
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":3B30
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":480C
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":54E8
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":61C4
            Key             =   "inter"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":6410
            Key             =   "Nuevo"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":6A8A
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":7104
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":7556
            Key             =   "salir"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Descripcion"
      ForeColor       =   &H80000002&
      Height          =   255
      Left            =   330
      TabIndex        =   4
      Top             =   1770
      Width           =   1665
   End
   Begin VB.Label lbl_Codigo 
      BackStyle       =   0  'Transparent
      Caption         =   "Codigo del Perfil"
      ForeColor       =   &H80000002&
      Height          =   255
      Left            =   330
      TabIndex        =   3
      Top             =   1005
      Width           =   1665
   End
End
Attribute VB_Name = "Frm_Perfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim objmens As Object

Private Sub cmd_buscar_Click()
    Call Buscar
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Call Buscar
        Case Is = vbKeyF4
            Call Grabar
        Case Is = vbKeyF7
            Call cancelar
        Case Is = vbKeyF12
            Unload Me
        End Select
        
End Sub

Private Sub Form_Load()
    Dim conexion As New ADODB.Connection

    conexion.Open CADENA
    Set objmens = CreateObject("recsun.OBJ_MENSAJERIA")
    With tbr_Perfiles
        For i = 1 To .Buttons.Count - 1
            If .Buttons(i).Caption = "" Then
                .Buttons(i).Enabled = False
            End If
        Next i
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim conexion As New ADODB.Connection
    Dim Tabla As String
    
    conexion.Open CADENA
    Tabla = "TR_Config_Botones_Temp"
    If VerificarPerfil(conexion, Tabla) = False Then
        objmens.mensaje "El perfil actual contiene datos,Desea Salir", True
            If objmens.PressBoton = False Then
                Cancel = True
            Else
                BorrarPerfilTemp conexion, "TR_Config_Botones_Temp"
                Set objmens = Nothing
            End If
    End If
    
    conexion.Close
End Sub

Private Sub tbr_Perfiles_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case Is = "Nuevo"
            Call Nuevo
        Case Is = "Buscar"
            Call Buscar
        Case Is = "Grabar"
            Call Grabar
        Case Is = "Agregar"
            Call agregar
        Case Is = "Salir"
            Unload Me
    End Select
    
End Sub
Private Sub Nuevo()
    Dim TablaTemporal As String
    Dim mdatos(1) As String
    Dim misDatosPerfil As Variant
    
    TablaTemporal = "TR_Config_Botones_Temp"
    mdatos(0) = g_CodigoPerfil
    mdatos(1) = Me.txt_Descripcion.Text
    misDatosPerfil = mdatos()
    Call NuevoPerfil(misDatosPerfil, TablaTemporal, CADENA)
    Me.txt_CodigoPerfil.Text = g_CodigoPerfil
    Frm_Configuracion.Show vbModal
    
End Sub

Private Sub Grabar()
    Dim Tabla As String
    Dim m_datos As Variant
    Dim m_Temp(1) As String
    Dim conexion As New ADODB.Connection
    Dim error As Boolean
    
    conexion.Open CADENA
        objmens.mensaje "Desea conservar el mismo perfil", True
            If objmens.PressBoton = True Then
                m_Temp(0) = g_CodigoPerfil
            Else
                g_CodigoPerfil = micorrelativo(conexion, "cs_COD_PERFIL")
                 m_Temp(0) = g_CodigoPerfil
            End If
            m_Temp(1) = Me.txt_Descripcion.Text
            m_datos = m_Temp
            Tabla = "TR_Config_Botones_Temp"
            Call GrabarPerfil(m_datos, Tabla, CADENA, error)
            
End Sub

Private Sub Buscar()
    Dim mbusqueda As Variant
    Dim m_sql As String
    Dim misdatos As Variant
    Dim perfil(1) As String
        
        m_sql = "TR_Config_Botones_Temp"
        perfil(0) = g_CodigoPerfil
        perfil(1) = Me.txt_Descripcion.Text
        misdatos = perfil()
        
        mbusqueda = BuscarPerfil(m_sql, CADENA, misdatos)
        
        If Not IsEmpty(mbusqueda) Then
            Me.txt_CodigoPerfil = mbusqueda(0)
            Me.txt_Descripcion = mbusqueda(1)
            g_CodigoPerfil = CStr(mbusqueda(0))
            
            Frm_Configuracion.Show vbModal
        End If
    
End Sub

Private Sub agregar()
    Dim conexion As New ADODB.Connection
    
    conexion.Open CADENA
    If VerificarPerfil(conexion, "TR_Config_Botones_Temp") Then
        objmens.mensaje "No hay perfil disponible"
    Else
        Frm_Configuracion.Show
    End If
    conexion.Close
End Sub

Private Sub cancelar()
    Dim CONEX As New ADODB.Connection
    
    CONEX.Open CADENA
    
    objmens.mensaje "Esta seguro, si tiene algun perfil cargado perdera su informacion "
        If objmens.PressBoton = True Then
            BorrarPerfilTemp CONEX, "TR_Config_Botones_Temp"
            Me.txt_CodigoPerfil = ""
            Me.txt_Descripcion.Text = ""
        
        End If
End Sub

