VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form FrmFormatosPos 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4185
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   9390
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmFormatosPos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4185
   ScaleWidth      =   9390
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   2175
      Left            =   240
      TabIndex        =   5
      Top             =   720
      Width           =   8895
      Begin VB.TextBox TxtFacturaRuta 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2835
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   255
         Width           =   2970
      End
      Begin VB.CommandButton cmd_buscar_factura 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5910
         Picture         =   "FrmFormatosPos.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   240
         Width           =   360
      End
      Begin VB.ComboBox cmb_tipopuerto_factura 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "FrmFormatosPos.frx":6A8C
         Left            =   6360
         List            =   "FrmFormatosPos.frx":6A96
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   255
         Width           =   2145
      End
      Begin VB.TextBox TxtDevolucionRuta 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2835
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   705
         Width           =   2970
      End
      Begin VB.CommandButton cmd_buscar_devolucion 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5910
         Picture         =   "FrmFormatosPos.frx":6AB2
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   690
         Width           =   360
      End
      Begin VB.ComboBox cmb_tipopuerto_devolucion 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "FrmFormatosPos.frx":72B4
         Left            =   6360
         List            =   "FrmFormatosPos.frx":72BE
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   705
         Width           =   2145
      End
      Begin VB.TextBox TxtReintegroRuta 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2835
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   1155
         Width           =   2970
      End
      Begin VB.CommandButton cmd_buscar_reintegro 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5910
         Picture         =   "FrmFormatosPos.frx":72DA
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1140
         Width           =   360
      End
      Begin VB.ComboBox cmb_tipopuerto_reintegro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "FrmFormatosPos.frx":7ADC
         Left            =   6360
         List            =   "FrmFormatosPos.frx":7AE6
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1155
         Width           =   2145
      End
      Begin VB.TextBox TxtEsperaRuta 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2835
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   1620
         Width           =   2970
      End
      Begin VB.CommandButton cmd_buscar_espera 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5910
         Picture         =   "FrmFormatosPos.frx":7B02
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1605
         Width           =   360
      End
      Begin VB.ComboBox cmb_tipopuerto_espera 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "FrmFormatosPos.frx":8304
         Left            =   6360
         List            =   "FrmFormatosPos.frx":830E
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1620
         Width           =   2145
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Formato de Factura:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         TabIndex        =   21
         Top             =   270
         Width           =   2310
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Formato de Devolución:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         TabIndex        =   20
         Top             =   720
         Width           =   2670
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Formato de Reintegro:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         TabIndex        =   19
         Top             =   1170
         Width           =   2670
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Formato de Espera:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         TabIndex        =   18
         Top             =   1635
         Width           =   2670
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7035
         TabIndex        =   4
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Formatos de Reportes (RTF)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   75
         Width           =   5415
      End
   End
   Begin VB.CommandButton cmd_aceptar 
      Caption         =   "&Aceptar"
      Height          =   945
      Left            =   6825
      Picture         =   "FrmFormatosPos.frx":832A
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   3015
      Width           =   1080
   End
   Begin VB.CommandButton cmd_cancelar 
      Caption         =   "&Cancelar"
      Height          =   945
      Left            =   8070
      Picture         =   "FrmFormatosPos.frx":A0AC
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   3015
      Width           =   1080
   End
   Begin MSComDlg.CommonDialog DialogoComun 
      Left            =   7575
      Top             =   3030
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "FrmFormatosPos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public RtfFormatoFactura As String
Public RtfFacturaPuerto As Boolean

Public RtfFormatoDevolucion As String
Public RtfDevolucionPuerto As Boolean

Public RtfFormatoReintegro As String
Public RtfReintegroPuerto As Boolean

Public RtfFormatoEspera As String
Public RtfEsperaPuerto As Boolean

Public Aceptar As Boolean

Private Sub cmb_tipopuerto_devolucion_Click()
    RtfDevolucionPuerto = (cmb_tipopuerto_devolucion.ListIndex = 0)
End Sub

Private Sub cmb_tipopuerto_espera_Click()
    RtfEsperaPuerto = (cmb_tipopuerto_espera.ListIndex = 0)
End Sub

Private Sub cmb_tipopuerto_factura_Click()
    RtfFacturaPuerto = (cmb_tipopuerto_factura.ListIndex = 0)
End Sub

Private Sub cmb_tipopuerto_reintegro_Click()
    RtfReintegroPuerto = (cmb_tipopuerto_reintegro.ListIndex = 0)
End Sub

Private Sub Cmd_Aceptar_Click()
    Aceptar = True
    Unload Me
End Sub

Private Sub cmd_buscar_devolucion_Click()
    DialogoComun.ShowOpen
    
    If Trim(DialogoComun.FileName) <> "" Then
        TxtDevolucionRuta.Text = DialogoComun.FileName
        RtfFormatoDevolucion = DialogoComun.FileName
    End If
End Sub

Private Sub cmd_buscar_espera_Click()
    DialogoComun.ShowOpen
    
    If Trim(DialogoComun.FileName) <> "" Then
        TxtEsperaRuta.Text = DialogoComun.FileName
        RtfFormatoEspera = DialogoComun.FileName
    End If
End Sub

Private Sub cmd_buscar_factura_Click()
    DialogoComun.ShowOpen
    
    If Trim(DialogoComun.FileName) <> "" Then
        TxtFacturaRuta.Text = DialogoComun.FileName
        RtfFormatoFactura = DialogoComun.FileName
    End If
End Sub

Private Sub cmd_buscar_reintegro_Click()
    DialogoComun.ShowOpen
    
    If Trim(DialogoComun.FileName) <> "" Then
        TxtReintegroRuta.Text = DialogoComun.FileName
        RtfFormatoReintegro = DialogoComun.FileName
    End If
End Sub

Private Sub cmd_cancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()

    Me.cmb_tipopuerto_devolucion.ListIndex = IIf(RtfDevolucionPuerto, 0, 1)
    Me.cmb_tipopuerto_espera.ListIndex = IIf(RtfEsperaPuerto, 0, 1)
    Me.cmb_tipopuerto_factura.ListIndex = IIf(RtfFacturaPuerto, 0, 1)
    Me.cmb_tipopuerto_reintegro.ListIndex = IIf(RtfReintegroPuerto, 0, 1)
    
    Me.cmb_tipopuerto_devolucion.List(0) = stellar_mensaje(10132) 'puerto
    Me.cmb_tipopuerto_devolucion.List(1) = stellar_mensaje(10131) 'nombre
    Me.cmb_tipopuerto_espera.List(0) = stellar_mensaje(10132) 'puerto
    Me.cmb_tipopuerto_espera.List(1) = stellar_mensaje(10131) 'nombre
    Me.cmb_tipopuerto_reintegro.List(0) = stellar_mensaje(10132) 'puerto
    Me.cmb_tipopuerto_reintegro.List(1) = stellar_mensaje(10131) 'nombre
    Me.cmb_tipopuerto_factura.List(0) = stellar_mensaje(10132) 'puerto
    Me.cmb_tipopuerto_factura.List(1) = stellar_mensaje(10131) 'nombre

    Me.TxtDevolucionRuta.Text = RtfFormatoDevolucion
    Me.TxtEsperaRuta.Text = RtfFormatoEspera
    Me.TxtFacturaRuta.Text = RtfFormatoFactura
    Me.TxtReintegroRuta.Text = RtfFormatoReintegro
    Me.lbl_Organizacion.Caption = stellar_mensaje(10247) 'Formatos de reportes (RTF)
    Me.Label1.Caption = stellar_mensaje(10243) 'Formato de factura
    Me.Label2.Caption = stellar_mensaje(10244) 'formato de devolucion
    Me.Label3.Caption = stellar_mensaje(10245) 'formato de reintegro
    Me.Label5.Caption = stellar_mensaje(10246) 'formato de espera
    
    Me.cmd_aceptar.Caption = stellar_mensaje(5) 'aceptar
    Me.cmd_cancelar.Caption = stellar_mensaje(6) 'cancelar
    
    Aceptar = False
    
End Sub

Private Sub TxtDevolucionRuta_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        TxtDevolucionRuta.Text = ""
    End If
End Sub

Private Sub TxtEsperaRuta_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        TxtEsperaRuta.Text = ""
    End If
End Sub

Private Sub TxtFacturaRuta_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        TxtFacturaRuta.Text = ""
    End If
End Sub

Private Sub TxtReintegroRuta_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        TxtReintegroRuta.Text = ""
    End If
End Sub
