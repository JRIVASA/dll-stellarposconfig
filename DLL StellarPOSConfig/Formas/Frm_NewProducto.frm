VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Frm_NewProducto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "POS Configuracion de Botones"
   ClientHeight    =   6960
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5805
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6960
   ScaleWidth      =   5805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   960
      Left            =   0
      TabIndex        =   46
      Top             =   0
      Width           =   5805
      Begin MSComctlLib.Toolbar tlb_Perfiles 
         Height          =   675
         Left            =   75
         TabIndex        =   47
         Top             =   180
         Width           =   5640
         _ExtentX        =   9948
         _ExtentY        =   1191
         ButtonWidth     =   1058
         ButtonHeight    =   1191
         Appearance      =   1
         Style           =   1
         ImageList       =   "Icono_Apagado"
         DisabledImageList=   "Iconos_Encendidos"
         HotImageList    =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   10
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "Buscar"
               Object.ToolTipText     =   "Buscar Perfil"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "Grabar"
               Object.ToolTipText     =   "Graba Perfil"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "Salir"
               ImageIndex      =   8
            EndProperty
         EndProperty
      End
   End
   Begin VB.TextBox txt_Codigo 
      BackColor       =   &H80000018&
      Height          =   330
      Left            =   165
      TabIndex        =   0
      Top             =   1350
      Width           =   1515
   End
   Begin VB.TextBox txt_Posicion 
      BackColor       =   &H80000018&
      Height          =   330
      Left            =   165
      TabIndex        =   2
      Top             =   2070
      Width           =   855
   End
   Begin VB.TextBox txt_Nombre 
      BackColor       =   &H80000018&
      Height          =   330
      Left            =   2010
      TabIndex        =   1
      Top             =   1350
      Width           =   3630
   End
   Begin VB.Frame Frame2 
      Caption         =   "Posiciones"
      ForeColor       =   &H80000002&
      Height          =   4125
      Left            =   165
      TabIndex        =   45
      Top             =   2775
      Width           =   5490
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   35
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   3345
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   34
         Left            =   3585
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   3345
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   33
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   3345
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   32
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   3345
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   31
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   3345
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   30
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   3345
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   29
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   2730
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   28
         Left            =   3585
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   2730
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   27
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   2730
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   26
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   2730
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   25
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   2730
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   24
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   2730
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   23
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   2115
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   22
         Left            =   3585
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   2115
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   21
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   2115
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   20
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   2115
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   19
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   2115
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   18
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   2115
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   17
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   1500
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   16
         Left            =   3585
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   1500
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   15
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   1500
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   14
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   1500
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   13
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   1500
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   12
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   1500
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   11
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   885
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   10
         Left            =   3585
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   885
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   9
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   885
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   8
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   885
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   7
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   885
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   6
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   885
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   5
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   270
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   4
         Left            =   3585
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   270
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   3
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   270
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   2
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   270
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   1
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   270
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   0
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   270
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Asignacion de Teclas"
      ForeColor       =   &H80000002&
      Height          =   960
      Left            =   2010
      TabIndex        =   41
      Top             =   1770
      Width           =   3645
      Begin VB.ComboBox cbo_tecla 
         BackColor       =   &H80000018&
         Height          =   315
         Left            =   1875
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   540
         Width           =   1545
      End
      Begin VB.ComboBox cbo_Mascara 
         BackColor       =   &H80000018&
         Height          =   315
         Left            =   255
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   555
         Width           =   1545
      End
      Begin VB.Label Label4 
         Caption         =   "Mascara"
         ForeColor       =   &H80000002&
         Height          =   165
         Left            =   255
         TabIndex        =   44
         Top             =   285
         Width           =   750
      End
      Begin VB.Label Label5 
         Caption         =   "Tecla"
         ForeColor       =   &H80000002&
         Height          =   195
         Left            =   1875
         TabIndex        =   43
         Top             =   255
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Tecla"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3705
         TabIndex        =   42
         Top             =   210
         Width           =   540
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   705
      Top             =   6930
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":0000
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":0CDC
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":19B8
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":2694
            Key             =   "inter"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":28E0
            Key             =   "Nuevo"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":2F5A
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":35D4
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":3A26
            Key             =   "salir"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":3B30
            Key             =   "DPTO CERRADO"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":3F82
            Key             =   "DPTO ABIERTO"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   15
      Top             =   6930
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":43D4
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":50B0
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":5D8C
            Key             =   "Ayuda"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":6A68
            Key             =   "inter"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":6CB4
            Key             =   "Nuevo"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":732E
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":79A8
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":7DFA
            Key             =   "salir"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":7F04
            Key             =   "DPTO CERRADO"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_NewProducto.frx":8356
            Key             =   "DPTO ABIERTO"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre"
      ForeColor       =   &H80000002&
      Height          =   255
      Left            =   1980
      TabIndex        =   50
      Top             =   1095
      Width           =   735
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Posicion"
      ForeColor       =   &H80000002&
      Height          =   255
      Left            =   165
      TabIndex        =   49
      Top             =   1770
      Width           =   765
   End
   Begin VB.Label lbl_codigo 
      BackStyle       =   0  'Transparent
      Caption         =   "Codigo"
      ForeColor       =   &H80000002&
      Height          =   255
      Left            =   165
      TabIndex        =   48
      Top             =   1080
      Width           =   615
   End
End
Attribute VB_Name = "Frm_NewProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public objeto As Object

Private Sub barra_menu_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
    
    Case Is = "grabar" ''boton salvar
        Call Grabar
        
    Case Is = "cancelar"
        txt_Nombre = ""
    
    Case Is = "salir"
        Frm_Configuracion.addnode = False
        Unload Me
End Select

End Sub
Private Sub Grabar()
    Dim sql As String
    Dim respuesta As Boolean
    Dim CONEX As New ADODB.Connection
    
    CONEX.Open CADENA
       
    DatosBoton.codigo = micorrelativo(CONEX, "cs_COD_PRODUCTO")
    DatosBoton.CodigoNativo = Me.txt_Codigo.Text
    DatosBoton.Grupo = Frm_Configuracion.fNodoArr_Sel(2)
    DatosBoton.imagen = "DPTO CERRADO"
    DatosBoton.Mascara = Me.cbo_Mascara.Text
    DatosBoton.Nombre = Me.txt_Nombre.Text
    DatosBoton.Posicion = Val(Me.txt_Posicion.Text)
    DatosBoton.Tecla = Me.cbo_tecla
    GrabarBoton CADENA, "TR_Config_Botones_Temp", respuesta
    
    If respuesta = False Then
        Frm_Configuracion.addnode = True
        Unload Me
    End If
    
End Sub
   
Private Sub cmd_Botones_Click(Index As Integer)
    If Me.cmd_Botones(Index).Caption = "" Or Me.cmd_Botones(Index).Tag = Frm_Configuracion.fNodoArr_Sel(2) Then
        Me.txt_Posicion.Text = Index + 1
    Else
        objeto.mensaje "Boton asignado, seleccione otra posicion o modifique el boton actual "
    End If
End Sub
Private Sub BuscarProductos()
    Dim Buscar As Object
    Dim mProducto As Variant
    
    Set Buscar = CreateObject("recsuna.cls_productos")
    
    mProducto = Buscar.Buscar_ProductosInterfaz(CADENA)
    If Not IsEmpty(mProducto) Then
        Me.txt_Codigo.Text = mProducto(0)
        Me.txt_Nombre = mProducto(1)
    End If
    
    Set Buscar = Nothing
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF1
        Case Is = vbKeyF2
            Call BuscarProductos
        Case Is = vbKeyF7
            Call cancelar
        Case Is = vbKeyF4
            Call Grabar
        Case Is = vbKeyF12
            Frm_Configuracion.addnode = False
            Unload Me
    End Select
End Sub
Private Sub Form_Load()
    Dim Botones As New cls_Boton
    Dim min As Long, max As Long
    Dim CONEX As New ADODB.Connection
    Dim m_sql As String
    
    min = 0
    max = Me.cmd_Botones.Count - 1
        
    CONEX.Open CADENA
    Set objeto = CreateObject("recsun.obj_MENSAJERIA")
    Call LlenarCombos(Me.cbo_Mascara, Me.cbo_tecla)
    txt_Codigo.Text = micorrelativo(CONEX, "cs_COD_PRODUCTO", False)
    m_sql = "Select * from TR_Config_Botones_Temp where relacion='" & CStr(Frm_Configuracion.fNodoArr_Sel(2)) + "'"
    Botones.IniciarBotones Me.cmd_Botones, CONEX, m_sql, min, max
           
    CONEX.Close
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set objeto = Nothing
    Set Newdpto = Nothing
End Sub


Private Sub tlb_Perfiles_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case Is = "Buscar"
            Call BuscarProductos
        Case Is = "Grabar"
            Call Grabar
        Case Is = "Salir"
            Unload Me
    End Select
End Sub
Private Sub cancelar()
    Me.txt_Codigo.Text = ""
    Me.txt_Nombre.Text = ""
    Me.txt_Posicion.Text = ""
    Me.cbo_Mascara.ListIndex = 0
    Me.cbo_tecla.ListIndex = 0
End Sub

