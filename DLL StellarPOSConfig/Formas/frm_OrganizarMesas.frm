VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form Frm_OrganizarMesas 
   BackColor       =   &H00404080&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   11490
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11490
   ScaleWidth      =   15330
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog Dialogo 
      Left            =   720
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   0
      TabIndex        =   4
      Top             =   9840
      Width           =   15375
      Begin VB.CommandButton CmdNuevaMesa 
         Caption         =   "Nueva Mesa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   9600
         Picture         =   "frm_OrganizarMesas.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton CargarImagen 
         Caption         =   "Imagen"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   11040
         Picture         =   "frm_OrganizarMesas.frx":0CCA
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   360
         Width           =   1215
      End
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1095
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   9015
         Begin VB.CommandButton Zonas 
            Caption         =   "Command1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Index           =   0
            Left            =   -120
            TabIndex        =   8
            Top             =   60
            Width           =   75
         End
      End
      Begin VB.CommandButton Salir 
         Caption         =   "Salir"
         DisabledPicture =   "frm_OrganizarMesas.frx":2A4C
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   13920
         Picture         =   "frm_OrganizarMesas.frx":47CE
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton Guardar 
         Caption         =   "Guardar"
         DisabledPicture =   "frm_OrganizarMesas.frx":6550
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   12480
         Picture         =   "frm_OrganizarMesas.frx":82D2
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   360
         Width           =   1215
      End
      Begin VB.Image Ocultar 
         Height          =   375
         Left            =   6480
         Top             =   0
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   500
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   15495
      Begin VB.Image Exit 
         Height          =   480
         Left            =   14640
         Picture         =   "frm_OrganizarMesas.frx":A054
         Top             =   0
         Width           =   480
      End
      Begin VB.Label LbWebsite 
         BackColor       =   &H00404040&
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12000
         TabIndex        =   3
         Top             =   90
         Width           =   2295
      End
      Begin VB.Label Lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   960
         TabIndex        =   2
         Top             =   120
         Width           =   6855
      End
   End
   Begin VB.CommandButton mesas 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      Caption         =   "Command4"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1755
      Index           =   0
      Left            =   480
      MaskColor       =   &H00404040&
      Picture         =   "frm_OrganizarMesas.frx":BDD6
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1200
      Visible         =   0   'False
      Width           =   1380
   End
   Begin MSComctlLib.ImageList Flechas 
      Left            =   120
      Top             =   240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   120
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_OrganizarMesas.frx":186B1
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frm_OrganizarMesas.frx":196BB
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Image picfoto 
      Height          =   11520
      Left            =   0
      Stretch         =   -1  'True
      Top             =   0
      Width           =   15360
   End
End
Attribute VB_Name = "Frm_OrganizarMesas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Option Explicit
Public obj As Cls_OrganizarMesas

Const Mesa As Integer = 10
' Cantiadad de PictureBox
Dim cantidad As Byte
Dim P() As Integer
Dim RsZonas As New ADODB.Recordset
Dim ClPOSconfig As New Cls_POSConfig
Public CodZona As String
Public CON As New ADODB.Connection
Public RsMesas As New ADODB.Recordset
Public Imagen_Name As String
Public Imagen_Handle As Double

Private FormaCargada As Boolean

Private Sub CargarImagen_Click()
    
    Dim SQL As String
    
    On Error GoTo Errores
    
    Dialogo.Filter = "Imágenes (*.jpg)|*.jpg"
    Dialogo.ShowOpen
    
    If Dialogo.FileName <> "" Then
        picfoto.Picture = LoadPicture(Dialogo.FileName, 4, 0, 3480, 4054)
        'imagen_Handle = PicFoto.Picture.Handle
        Imagen_Name = Dialogo.FileName
        Label1 = ""
    Else
        Imagen_Name = ""
        Imagen_Handle = 0
        'PicFoto.Picture.Handle = 0
        'Label1 = "Haga Doble Click Para Buscar Foto del Empleado"
    End If
    
    Exit Sub
    
Errores:
    
    MsgBox ERR.Description
    'Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Name & ".Imagen_DblClick", Err.Source)
    'Unload Me
    
End Sub

Private Sub CmdNuevaMesa_Click()
    NodoTextoKey = Me.CodZona
    FrmUbicacion.Show vbModal
    If AddNode Then Form_Load: AddNode = False
End Sub

Private Sub Exit_Click()
    Salir_Click
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        If Zonas.Count > 0 Then
            If PuedeObtenerFoco(Zonas(1)) Then Zonas(1).SetFocus
        End If
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF6 Then
        Ocultar_Click
    ElseIf KeyCode = vbKeyF4 Then
        Guardar_Click
    ElseIf KeyCode = vbKeyEscape Or KeyCode = vbKeyF12 Then
        Salir_Click
    End If
End Sub

Private Sub Form_Load()
    
    Dim SQL As String
    
    Me.Frame2.ZOrder (1)
    Me.Frame1.ZOrder (1)
    
    Me.lbl_Organizacion.Caption = stellar_mensaje(5524) 'Diagrama de Mesas
    Me.CmdNuevaMesa.Caption = StellarMensaje(10303) ' Nueva Mesa
    Me.CargarImagen.Caption = StellarMensaje(10302) ' Fondo
    Me.Guardar.Caption = stellar_mensaje(103) ' Grabar
    Me.Salir.Caption = stellar_mensaje(107) ' Salir
    
    Frame2.top = 9840
    
    For Each TmpCtl In mesas
        If TmpCtl.Index <> 0 Then
            Unload TmpCtl
        End If
    Next
    
    For Each TmpCtl In Zonas
        If TmpCtl.Index <> 0 Then
            Unload TmpCtl
        End If
    Next
    
    SQL = "SELECT * FROM MA_SITIO WHERE CS_CODIGORELACION IN (SELECT TOP(1)CLAVE FROM ma_ESTRUC_CAJA order by cs_CODIGORELACION)"
    
    If CON.State = 0 Then
       CON.Open
    End If
    
    Apertura_Recordset True, RsMesas
    
    RsMesas.Open SQL, CON, adOpenStatic, adLockReadOnly, adCmdText
    
    Me.ScaleMode = vbTwips
    
    Dim i As Integer
    
    If Not RsMesas.EOF Then RsMesas.MoveFirst
    
    Ocultar.Picture = Flechas.ListImages(1).Picture
    i = 0
    
    'Agrega las mesas con sus cordenadas y todo segun lo que este en la tabla ma_sitio y la primera zona del ma_estruc_caja
    
    While Not RsMesas.EOF
        If Not ExisteCampoTabla("AxisX", RsMesas) Then
             If RsMesas.State = 1 Then
                 RsMesas.Close
                 Exit Sub
             End If
        End If
        
        If i > 0 Then
            Load mesas(i)
        End If
        
        mesas(i).Visible = True
        mesas(i).ZOrder (0)
        mesas(i).Caption = RsMesas!cu_descripcion & vbNewLine
        mesas(i).top = RsMesas!AxisX
        mesas(i).left = RsMesas!AxisY
        mesas(i).Height = RsMesas!Height
        mesas(i).Width = RsMesas!Width
        mesas(i).Tag = RsMesas!cs_CodigoSitio
        
        i = i + 1
        
        Me.CodZona = RsMesas!cs_CodigoRelacion
        RsMesas.MoveNext
    Wend
    
    If RsMesas.RecordCount = 0 Then
       mesas(o).Visible = False
       'Call Mensaje(True, "No existen mesas creadas en esta zona", False)
    End If
    
    RsMesas.Close
    
    SQL = "SELECT * FROM MA_ESTRUC_CAJA"
    i = 1
    
    Apertura_Recordset True, RsZonas
    
    RsZonas.Open SQL, CON, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not RsZonas.EOF
       Load Zonas(i)
       Zonas(i).Visible = True
       Zonas(i).Caption = RsZonas!Texto
       If Me.CodZona = vbNullString Then Me.CodZona = RsZonas!Clave
       Zonas(i).Height = 1000
       Zonas(i).Tag = RsZonas!Clave
       Zonas(i).left = 120 + (Zonas(i - 1).left) + (Zonas(i - 1).Width)
       Zonas(i).Width = 1215
       RsZonas.MoveNext
       i = i + 1
    Wend
    
    RsZonas.Close
    
    Call PopImageOfDb(Me.picfoto, "FONDO")
        
    '    For i = 1 To Mesa
    '        Load mesas(i)
    '        mesas(i).Visible = True
    '        mesas(i).Caption = "Mesa " & i
    '    Next
    
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    OrganizarMesas.DisallowResize
End Sub

Private Sub CrearPicture(top As Long, left As Long)
      
    Dim MiMesa As CommandButton
      
    cantidad = cantidad + 1
  
    Set MiMesa = Controls.add("VB.CommandButton", "Mimesa" & CStr(cantidad))
  
    'Le establecemos propiedades al nuevo Picture que se crea
    With MiMesa
        ' Hay que hacer el control visible
        .Visible = True
        'dimensiones y posición
        .top = top
        .left = left
        .Width = 50
        .Height = 50
        'Le cambia el color por un valor aleatorio
        .Caption = "Mesa" & CStr(cantidad)
    End With
  
End Sub

Private Sub Command3_Click()
    'Le pasa los valores Top y Left para el control _
    creado en tiempo dejecución
    CrearPicture CLng(Text1), CLng(Text2)
End Sub

Private Sub Frame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Frame2.ZOrder (0)
End Sub

Private Sub Frame2_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
        Ocultar_Click
    End If
End Sub

Private Sub Frame3_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
        Ocultar_Click
    End If
End Sub

Private Sub Guardar_Click()
    
    Dim Rs As New ADODB.Recordset
    Dim SQL As String
    
    If CON.State = 0 Then
        CON.Open
    End If
    
    For i = 0 To mesas.Count - 1
        ReDim Preserve P(i)
        
        'AxisX = mesas(i).top
        'AxisY = mesas(i).left
        'TableWidth = mesas(i).Width
        'TableHeight = mesas(i).Height

        SQL = "UPDATE MA_SITIO SET AxisX = '" & mesas(i).top & "'" & vbNewLine & _
        ", AxisY = '" & mesas(i).left & "', Height = '" & mesas(i).Height & "'" & vbNewLine & _
        ", Width = '" & mesas(i).Width & "' WHERE cs_CodigoSitio = '" & mesas(i).Tag & "'"
        
        CON.Execute (SQL)
        
    Next i
    
    'rs.Open sql, Me.CON, adOpenDynamic, adLockBatchOptimistic
    resp = Dir(Imagen_Name)
        
    If (picfoto.Picture.Handle <> Imagen_Handle) And (Imagen_Name <> "" And resp <> "") Then
        Call PushImageToDb(Imagen_Name, "FONDO")
    ElseIf picfoto.Picture.Handle = Imagen_Handle And Imagen_Handle = 0 Then
        'mRs("iu_fotografia") = Null
    End If
    
    'mRs.Update
    'rs.Close
    
    Mensaje True, StellarMensaje(10350) 'Configuración guardada con Exito.
    
End Sub

Private Sub mesas_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ReleaseCapture
    SendMessage mesas(Index).hWnd, &HA1, 2, 0&
End Sub

Private Sub mesas_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    OrganizarMesas.AllowResize mesas(Index)
End Sub

Private Sub mesas_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton And Shift = 0 Then
        FrmUbicacion.TxtCodigo.Text = mesas(Index).Tag
        CmdNuevaMesa_Click
    ElseIf Button = vbRightButton And Shift > 0 Then
        
        On Error Resume Next
        
        Dim TmpRs As Object, mSQL As String, TmpWidth As Long, TmpHeight As Long
        
        If Shift = vbShiftMask Then
            mSQL = "SELECT cs_CodigoRelacion, cs_CodigoSitio, cu_Descripcion, CAST(NumericDescription AS FLOAT) AS NumericDescription FROM (" & vbNewLine & _
            "SELECT M.cs_CodigoRelacion, M.cs_CodigoSitio, cu_Descripcion, FinalTable.NumericDescription AS NumericDescription FROM MA_SITIO M INNER JOIN (" & vbNewLine & _
            "SELECT cs_CodigoSitio, STUFF((" & vbNewLine & _
            "SELECT '' + EachChar FROM (" & vbNewLine & _
            "SELECT EachChar, cs_CodigoSitio FROM (" & vbNewLine & _
            "SELECT SUBSTRING((SELECT cu_Descripcion FROM MA_SITIO WHERE cs_CodigoSitio = M.cs_CodigoSitio), TB.n, 1) EachChar, M.cs_CodigoSitio FROM(" & vbNewLine & _
            "SELECT TOP (255) n = ROW_NUMBER() OVER (ORDER BY number)" & vbNewLine & _
            "FROM [master]..spt_values) TB INNER JOIN MA_SITIO M ON 1=1" & vbNewLine & _
            ") TB2 WHERE EachChar <> '' AND EachChar LIKE '[0-9.]'" & vbNewLine & _
            ") TB3 WHERE (cs_CodigoSitio = Results.cs_CodigoSitio) FOR XML PATH(''),TYPE).value('(./text())[1]','NVARCHAR(MAX)')" & vbNewLine & _
            ",1,0,'') AS NumericDescription" & vbNewLine & _
            "FROM (" & vbNewLine & _
            "SELECT EachChar, cs_CodigoSitio FROM (" & vbNewLine & _
            "SELECT SUBSTRING((SELECT cu_Descripcion FROM MA_SITIO WHERE cs_CodigoSitio = M.cs_CodigoSitio), TB.n, 1) EachChar, M.cs_CodigoSitio FROM(" & vbNewLine & _
            "SELECT TOP (255) n = ROW_NUMBER() OVER (ORDER BY number)" & vbNewLine & _
            "FROM [master]..spt_values) TB INNER JOIN MA_SITIO M ON 1=1" & vbNewLine & _
            ") TB2 WHERE EachChar <> '' AND EachChar LIKE '[0-9.]') Results" & vbNewLine & _
            "GROUP BY cs_CodigoSitio" & vbNewLine & _
            ") FinalTable ON M.cs_CodigoSitio = FinalTable.cs_CodigoSitio" & vbNewLine & _
            "WHERE IsNumeric(FinalTable.NumericDescription) = 1" & vbNewLine & _
            ") MA_SITIO WHERE cs_CodigoRelacion = '" & Me.CodZona & "' Order by CAST(NumericDescription AS FLOAT)"
            
        ElseIf Shift = (vbShiftMask + vbCtrlMask) Then
            mSQL = "SELECT * FROM MA_SITIO WHERE cs_CodigoRelacion = '" & Me.CodZona & "' ORDER BY cu_Descripcion"
        End If
        
        Set TmpRs = CON.Execute(mSQL)
        
        If Not TmpRs Is Nothing Then
            If Not TmpRs.EOF Then
                TmpWidth = mesas(Index).Width
                TmpHeight = mesas(Index).Height
                ' TO DO
                
                CurrentLeft = TmpWidth * -1
                CurrentTop = Me.Frame1.Height
                
                While Not TmpRs.EOF
                    
                    TmpIndex = -1
                    
                    For i = 0 To mesas.UBound
                        If mesas(i).Caption = (TmpRs!cu_descripcion & vbNewLine) _
                        And mesas(i).Tag = TmpRs!cs_CodigoSitio Then
                            TmpIndex = i
                            Exit For
                        End If
                    Next i
                    
                    If TmpIndex <> -1 Then
                        
                        If (CurrentLeft + 2 * TmpWidth) >= Me.Width Then
                            CurrentTop = CurrentTop + TmpHeight
                            CurrentLeft = 0
                        Else
                            CurrentLeft = CurrentLeft + TmpWidth
                        End If
                        
                        mesas(i).left = CurrentLeft
                        mesas(i).top = CurrentTop
                        mesas(i).Width = TmpWidth
                        mesas(i).Height = TmpHeight
                        
                    End If
                    
                    TmpRs.MoveNext
                    
                Wend
                
            End If
        End If
    End If
End Sub

Private Sub Ocultar_Click()
    Frame2.ZOrder (0)
    If (Frame2.top = 9840) Then
        Ocultar.Picture = Flechas.ListImages(2).Picture
        Frame2.top = 11160
    Else
        Ocultar.Picture = Flechas.ListImages(1).Picture
        Frame2.top = 9840
    End If
End Sub

Private Sub picfoto_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    OrganizarMesas.DisallowResize
End Sub

Private Sub picfoto_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
        Ocultar_Click
    End If
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub

Private Sub Zonas_Click(Index As Integer)
    
    Dim SQL As String
    Dim i As Double
    
    If CON.State = 0 Then
        CON.Open
    End If
    
    i = 1
    Total = Me.mesas.Count
    
    'Debug.Print Me.mesas.Count
    
    If Me.mesas.Count > 1 Then
        While i <= Total - 1
            Unload mesas(i)
            i = i + 1
        Wend
    End If
    
    SQL = "SELECT * FROM MA_SITIO WHERE CS_CODIGORELACION = '" & Zonas(Index).Tag & "'"
    
    RsMesas.Open SQL, CON, adOpenStatic, adLockReadOnly, adCmdText
    i = 0
    
    While Not RsMesas.EOF
        If i > 0 Then
            Load mesas(i)
        End If
        mesas(i).Visible = True
        mesas(i).ZOrder (0)
        mesas(i).Caption = RsMesas!cu_descripcion & vbNewLine
        mesas(i).top = RsMesas!AxisX
        mesas(i).left = RsMesas!AxisY
        mesas(i).Height = RsMesas!Height
        mesas(i).Width = RsMesas!Width
        mesas(i).Tag = RsMesas!cs_CodigoSitio
        i = i + 1
        RsMesas.MoveNext
    Wend
    
    Me.CodZona = Zonas(Index).Tag
    Call PopImageOfDb(Me.picfoto, "FONDO")
    
    If RsMesas.RecordCount = 0 Then
        mesas(o).Visible = False
        Call Mensaje(True, StellarMensaje(10356)) ' No existen mesas creadas para la zona seleccionada.
        Set mObj = Nothing
    End If
    
    RsMesas.Close
    
End Sub

Function PushImageToDb(PathImage As String, Campo As Variant) As Boolean

    Dim RecSet As New ADODB.Recordset
    Dim LibreArc As Integer, Buffer As Variant
    Dim FS, f, s, counter As Long
    Dim SQL As String
    
    PushImageToDb = False
    
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    sTemp = PathImage
    
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    If Not FS.FileExists(PathImage) Then
        Exit Function
    End If
    
    LibreArc = FreeFile()
    FilTam = FileLen(PathImage)
    
    Open PathImage For Binary Access Read As #LibreArc
    Buffer = Input(LOF(LibreArc), LibreArc)
    
    SQL = "SELECT * FROM MA_ESTRUC_CAJA WHERE Clave = '" & Me.CodZona & "'"
    
    'Debug.Print SQL
    
    RecSet.Open SQL, Me.CON, adOpenStatic, adLockOptimistic, adCmdText
    'Call CON.Execute("UPDATE MA_ESTRUC_CAJA SET FONDO='" & Buffer & "' WHERE CLAVE='" & Me.CodZona & "'")
    
    If Not RecSet.EOF Then
        RecSet.Fields(Campo).AppendChunk (Buffer)
        'RecSet.Fields(Campo).Value = Input(LOF(LibreArc), LibreArc)
    End If
    
    RecSet.Update
    RecSet.Close
    
    Close LibreArc
    PushImageToDb = True
    
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
End Function

Function PopImageOfDb(ByRef ObjImg As Object, Campo As String) As Boolean
    
    Dim LibreArc As Integer
    Dim FS, f, s, counter As Long
    Dim Rs  As New ADODB.Recordset
    Dim SQL As String
    PopImageOfDb = False
    
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    SQL = "SELECT * FROM MA_ESTRUC_CAJA WHERE CLAVE = '" & Me.CodZona & "'"
    
    sTemp = "STELLAR_BK001.TMP"
    
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    If FS.FileExists(sTemp) Then
        Set f = FS.GetFile(sTemp)
        f.Delete
    End If
    
    'temp = Rs.Fields(Campo).ActualSize
    
    LibreArc = FreeFile()
    Open sTemp For Append Access Write As #LibreArc
    Rs.Open SQL, Me.CON, adOpenDynamic, adLockBatchOptimistic
    
    If Not Rs.EOF Then
        If Not ExisteCampoTabla(Campo, Rs) Then
            Rs.Close
            Close LibreArc
            Exit Function
        End If
        If (Rs.Fields(Campo).ActualSize > 0) Then
            Buffer = Rs.Fields(Campo).GetChunk(Rs.Fields(Campo).ActualSize)
        End If
    End If
    
    Print #LibreArc, Buffer
    Close LibreArc
    
    If IsEmpty(Buffer) Then
        ObjImg.Picture = Nothing
    Else
        ObjImg.Picture = LoadPicture(sTemp)
    End If
    
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
    If FS.FileExists(sTemp) Then
        Set f = FS.GetFile(sTemp)
        f.Delete
    End If
    
    PopImageOfDb = True
    Rs.Close
    
End Function
