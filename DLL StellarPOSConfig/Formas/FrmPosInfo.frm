VERSION 5.00
Begin VB.Form FrmPosInfo 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3585
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   6630
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmPosInfo.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3585
   ScaleWidth      =   6630
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1575
      Left            =   240
      TabIndex        =   5
      Top             =   600
      Width           =   6135
      Begin VB.TextBox TxtDevolucionRuta 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2220
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   570
         Width           =   3405
      End
      Begin VB.TextBox TxtFacturaRuta 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2220
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   120
         Width           =   3405
      End
      Begin VB.TextBox TxtTurnoRuta 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2220
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   1035
         Width           =   3405
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Devolución:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   360
         TabIndex        =   11
         Top             =   585
         Width           =   1395
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Factura:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   360
         TabIndex        =   10
         Top             =   135
         Width           =   1395
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Turno:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   360
         TabIndex        =   9
         Top             =   1050
         Width           =   1395
      End
   End
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7035
         TabIndex        =   4
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Información del Punto de Venta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   75
         Width           =   5415
      End
   End
   Begin VB.CommandButton cmd_refrescar 
      Caption         =   "Ac&tualizar"
      Height          =   945
      Left            =   4005
      Picture         =   "FrmPosInfo.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2400
      Width           =   1080
   End
   Begin VB.CommandButton cmd_cancelar 
      Caption         =   "&Salir"
      Height          =   945
      Left            =   5250
      Picture         =   "FrmPosInfo.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   2400
      Width           =   1080
   End
End
Attribute VB_Name = "FrmPosInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public PosNo As String
Dim RsCorrelativos As New ADODB.Recordset

Private Sub cmd_cancelar_Click()
    Unload Me
End Sub

Private Sub cmd_refrescar_Click()

    On Error GoTo Falla_Local
    
    Call Apertura_Recordset(True, RsCorrelativos)
    
    If Not EsDigital Then
    
        RsCorrelativos.Open "SELECT MAX(FACTURAS) AS FACTURAS, MAX(DEVOLUCIONES) AS DEVOLUCIONES, MAX(TURNOS) AS TURNO FROM " & _
                            "( SELECT MAX(C_NUMERO) AS FACTURAS, 0 AS DEVOLUCIONES, 0 AS TURNOS FROM MA_PAGOS WHERE C_CONCEPTO = 'VEN' AND C_CAJA = '" & PosNo & "'" & _
                            " UNION" & _
                            " SELECT 0 AS FACTURAS, MAX(C_NUMERO)AS DEVOLUCIONES, 0 AS TURNOS FROM MA_PAGOS WHERE C_CONCEPTO = 'DEV' AND C_CAJA = '" & PosNo & "'" & _
                            " UNION" & _
                            " SELECT 0 AS FACTURAS, 0 AS DEVOLUCIONES, MAX(TURNO)AS TURNOS FROM MA_PAGOS WHERE C_CONCEPTO = 'DEV' AND C_CAJA = '" & PosNo & "'" & _
                            ") AS CORRELATIVOS" _
        , Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
    Else
        
        RsCorrelativos.Open "SELECT MAX(FACTURAS) AS FACTURAS, MAX(DEVOLUCIONES) AS DEVOLUCIONES, MAX(TURNOS) AS TURNO FROM " & _
                            "( SELECT MAX(RIGHT(cs_DOCUMENTO,10)) AS FACTURAS, 0 AS DEVOLUCIONES, 0 AS TURNOS FROM MA_VENTAS_POS WHERE cs_TIPO_DOCUMENTO = 'VEN' AND cs_CAJA = '" & PosNo & "'" & _
                            " UNION" & _
                            " SELECT 0 AS FACTURAS, MAX(RIGHT(cs_DOCUMENTO,10))AS DEVOLUCIONES, 0 AS TURNOS FROM MA_VENTAS_POS WHERE cs_TIPO_DOCUMENTO = 'DEV' AND cs_CAJA = '" & PosNo & "'" & _
                            " UNION" & _
                            " SELECT 0 AS FACTURAS, 0 AS DEVOLUCIONES, MAX(nu_TURNO)AS TURNOS FROM MA_VENTAS_POS WHERE cs_TIPO_DOCUMENTO = 'DEV' AND cs_CAJA = '" & PosNo & "'" & _
                            ") AS CORRELATIVOS" _
        , Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
    End If
    
    If Not RsCorrelativos.EOF Then
    
        If Not IsNull(RsCorrelativos!facturas) Then
            If Not EsDigital Then
                Me.TxtFacturaRuta.Text = Format(RsCorrelativos!facturas, "000000000")
            Else
                Me.TxtFacturaRuta.Text = PosNo & Format(RsCorrelativos!facturas, "000000000")
            End If
        Else
            Me.TxtFacturaRuta.Text = ""
        End If
        
        If Not IsNull(RsCorrelativos!devoluciones) Then
            If Not EsDigital Then
                Me.TxtDevolucionRuta.Text = Format(RsCorrelativos!devoluciones, "000000000")
            Else
                Me.TxtDevolucionRuta.Text = PosNo & Format(RsCorrelativos!devoluciones, "000000000")
            End If
        Else
            Me.TxtDevolucionRuta.Text = ""
        End If
        
        If Not IsNull(RsCorrelativos!turno) Then
            Me.TxtTurnoRuta.Text = RsCorrelativos!turno
        Else
            Me.TxtTurnoRuta.Text = ""
        End If
        
    Else
    
        Me.TxtFacturaRuta.Text = ""
        Me.TxtDevolucionRuta.Text = ""
        
    End If
    
    Call Apertura_Recordset(True, RsCorrelativos)
    
    Exit Sub
    
Falla_Local:

    'MsgBox Err.Description
    Call Mensaje(True, Err.Description)
    
End Sub

Private Sub Form_Load()
    Call cmd_refrescar_Click
    Me.lbl_Organizacion.Caption = "" 'informacion del punto de venta
    Me.Label1.Caption = stellar_mensaje(2517) 'factura
    Me.Label2.Caption = stellar_mensaje(10238) 'devolucion
    Me.Label3.Caption = stellar_mensaje(2022) 'turno
    Me.cmd_refrescar.Caption = stellar_mensaje(2016) 'actualizar
    Me.cmd_cancelar.Caption = stellar_mensaje(54) 'salir
End Sub
