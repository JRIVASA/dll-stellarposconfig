VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "comct332.ocx"
Begin VB.Form botones 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9435
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   8985
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00585A58&
   Icon            =   "botones.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9435
   ScaleWidth      =   8985
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   63
      Top             =   0
      Width           =   11160
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6795
         TabIndex        =   65
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuraci�n de Botones POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   64
         Top             =   75
         Width           =   5415
      End
   End
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   9585
      TabIndex        =   60
      Top             =   421
      Width           =   9615
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   1155
         Left            =   0
         TabIndex        =   61
         Top             =   0
         Width           =   8235
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   300
            TabIndex        =   62
            Top             =   120
            Width           =   7155
            _ExtentX        =   12621
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "F4 Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "F7 Cancelar"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "F1 Ayuda"
                  ImageIndex      =   3
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame fconfig 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Botones"
      ForeColor       =   &H80000008&
      Height          =   7365
      Left            =   300
      TabIndex        =   34
      Top             =   1770
      Width           =   8385
      Begin VB.ComboBox rllave 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":628A
         Left            =   6780
         List            =   "botones.frx":629D
         TabIndex        =   59
         Text            =   "Combo1"
         Top             =   3600
         Width           =   945
      End
      Begin VB.ComboBox rnivel 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":62B0
         Left            =   5040
         List            =   "botones.frx":62D2
         TabIndex        =   58
         Text            =   "Combo1"
         Top             =   3600
         Width           =   900
      End
      Begin VB.ComboBox rpllave 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":62F4
         Left            =   6780
         List            =   "botones.frx":6307
         TabIndex        =   56
         Text            =   "Combo1"
         Top             =   4050
         Width           =   945
      End
      Begin VB.CheckBox parcial 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   55
         Top             =   4050
         Width           =   615
      End
      Begin VB.ComboBox rpnivel 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":631A
         Left            =   5040
         List            =   "botones.frx":633C
         TabIndex        =   54
         Text            =   "Combo1"
         Top             =   4050
         Width           =   900
      End
      Begin VB.ComboBox nclose 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":635E
         Left            =   5040
         List            =   "botones.frx":6380
         TabIndex        =   32
         Text            =   "Combo1"
         Top             =   6750
         Width           =   900
      End
      Begin VB.ComboBox nopen 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         IntegralHeight  =   0   'False
         ItemData        =   "botones.frx":63A2
         Left            =   5040
         List            =   "botones.frx":63C4
         TabIndex        =   0
         Text            =   "Combo1"
         Top             =   900
         Width           =   900
      End
      Begin VB.ComboBox CLNIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":63E6
         Left            =   5040
         List            =   "botones.frx":6408
         TabIndex        =   30
         Text            =   "Combo1"
         Top             =   6300
         Width           =   900
      End
      Begin VB.ComboBox PRNIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":642A
         Left            =   5040
         List            =   "botones.frx":644C
         TabIndex        =   27
         Text            =   "Combo1"
         Top             =   5850
         Width           =   900
      End
      Begin VB.ComboBox DNIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":646E
         Left            =   5040
         List            =   "botones.frx":6490
         TabIndex        =   24
         Text            =   "Combo1"
         Top             =   5400
         Width           =   900
      End
      Begin VB.ComboBox SNIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":64B2
         Left            =   5040
         List            =   "botones.frx":64D4
         TabIndex        =   21
         Text            =   "Combo1"
         Top             =   4950
         Width           =   900
      End
      Begin VB.ComboBox ENIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":64F6
         Left            =   5040
         List            =   "botones.frx":6518
         TabIndex        =   18
         Text            =   "Combo1"
         Top             =   4485
         Width           =   900
      End
      Begin VB.ComboBox BNIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":653A
         Left            =   5040
         List            =   "botones.frx":655C
         TabIndex        =   14
         Text            =   "Combo1"
         Top             =   3150
         Width           =   900
      End
      Begin VB.ComboBox CNIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":657E
         Left            =   5040
         List            =   "botones.frx":65A0
         TabIndex        =   11
         Text            =   "Combo1"
         Top             =   2700
         Width           =   900
      End
      Begin VB.ComboBox LNIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":65C2
         Left            =   5040
         List            =   "botones.frx":65E4
         TabIndex        =   8
         Text            =   "Combo1"
         Top             =   2250
         Width           =   900
      End
      Begin VB.ComboBox PNIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":6606
         Left            =   5040
         List            =   "botones.frx":6628
         TabIndex        =   5
         Text            =   "Combo1"
         Top             =   1800
         Width           =   900
      End
      Begin VB.ComboBox ANIVEL 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":664A
         Left            =   5040
         List            =   "botones.frx":666C
         TabIndex        =   2
         Text            =   "Combo1"
         Top             =   1350
         Width           =   900
      End
      Begin VB.CheckBox suspender 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   20
         Top             =   4950
         Width           =   615
      End
      Begin VB.CheckBox balanzas 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   13
         Top             =   3150
         Width           =   615
      End
      Begin VB.CheckBox ayuda 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   1
         Top             =   1395
         Width           =   615
      End
      Begin VB.CheckBox cliente 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   29
         Top             =   6300
         Width           =   615
      End
      Begin VB.CheckBox limite 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   7
         Top             =   2250
         Width           =   615
      End
      Begin VB.CheckBox devolucion 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   23
         Top             =   5400
         Width           =   615
      End
      Begin VB.CheckBox reintegro 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   16
         Top             =   3600
         Width           =   615
      End
      Begin VB.CheckBox productos 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   4
         Top             =   1800
         Width           =   615
      End
      Begin VB.CheckBox precios 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   26
         Top             =   5850
         Width           =   615
      End
      Begin VB.CheckBox cantidad 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   10
         Top             =   2700
         Width           =   615
      End
      Begin VB.CheckBox espera 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3630
         TabIndex        =   17
         Top             =   4485
         Width           =   615
      End
      Begin VB.ComboBox ALLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":668E
         Left            =   6780
         List            =   "botones.frx":66A1
         TabIndex        =   3
         Text            =   "Combo1"
         Top             =   1350
         Width           =   945
      End
      Begin VB.ComboBox PLLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":66B4
         Left            =   6780
         List            =   "botones.frx":66C7
         TabIndex        =   6
         Text            =   "Combo1"
         Top             =   1800
         Width           =   945
      End
      Begin VB.ComboBox LLLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":66DA
         Left            =   6780
         List            =   "botones.frx":66ED
         TabIndex        =   9
         Text            =   "Combo1"
         Top             =   2250
         Width           =   945
      End
      Begin VB.ComboBox CLLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":6700
         Left            =   6780
         List            =   "botones.frx":6713
         TabIndex        =   12
         Text            =   "Combo1"
         Top             =   2700
         Width           =   945
      End
      Begin VB.ComboBox BLLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":6726
         Left            =   6780
         List            =   "botones.frx":6739
         TabIndex        =   15
         Text            =   "Combo1"
         Top             =   3150
         Width           =   945
      End
      Begin VB.ComboBox ELLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":674C
         Left            =   6780
         List            =   "botones.frx":675F
         TabIndex        =   19
         Text            =   "Combo1"
         Top             =   4485
         Width           =   945
      End
      Begin VB.ComboBox SLLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":6772
         Left            =   6780
         List            =   "botones.frx":6785
         TabIndex        =   22
         Text            =   "Combo1"
         Top             =   4950
         Width           =   945
      End
      Begin VB.ComboBox DLLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":6798
         Left            =   6780
         List            =   "botones.frx":67AB
         TabIndex        =   25
         Text            =   "Combo1"
         Top             =   5400
         Width           =   945
      End
      Begin VB.ComboBox PRLLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":67BE
         Left            =   6780
         List            =   "botones.frx":67D1
         TabIndex        =   28
         Text            =   "Combo1"
         Top             =   5850
         Width           =   945
      End
      Begin VB.ComboBox CLLLAVE 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":67E4
         Left            =   6780
         List            =   "botones.frx":67F7
         TabIndex        =   31
         Text            =   "Combo1"
         Top             =   6300
         Width           =   945
      End
      Begin VB.ComboBox cellave 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         ItemData        =   "botones.frx":680A
         Left            =   6780
         List            =   "botones.frx":681D
         TabIndex        =   33
         Text            =   "Combo1"
         Top             =   6750
         Width           =   945
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   1150
         X2              =   7920
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Botones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   240
         TabIndex        =   66
         Top             =   0
         Width           =   1095
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Reintegros Parciales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   540
         TabIndex        =   57
         Top             =   4050
         Width           =   1740
      End
      Begin VB.Label Label39 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cierre de Caja"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   53
         Top             =   6750
         Width           =   1245
      End
      Begin VB.Label Label38 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Apertura de Caja"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   52
         Top             =   930
         Width           =   1470
      End
      Begin VB.Label Label37 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ACCION"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   51
         Top             =   390
         Width           =   675
      End
      Begin VB.Label Label36 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "NIVEL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5250
         TabIndex        =   50
         Top             =   540
         Width           =   495
      End
      Begin VB.Label Label35 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CONTRASE�A"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4860
         TabIndex        =   49
         Top             =   390
         Width           =   1200
      End
      Begin VB.Label Label33 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ACTIVAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3420
         TabIndex        =   48
         Top             =   390
         Width           =   780
      End
      Begin VB.Label Label32 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Suspender"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   47
         Top             =   4950
         Width           =   915
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Balanzas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   46
         Top             =   3150
         Width           =   750
      End
      Begin VB.Label Label28 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ayuda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   45
         Top             =   1395
         Width           =   525
      End
      Begin VB.Label Label26 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cliente Frecuente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   44
         Top             =   6300
         Width           =   1500
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Reintegro Total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   540
         TabIndex        =   43
         Top             =   3600
         Width           =   1320
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Consulta de Precios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   42
         Top             =   5850
         Width           =   1680
      End
      Begin VB.Label Label23 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Consultar Productos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   41
         Top             =   1800
         Width           =   1710
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad de Productos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   40
         Top             =   2700
         Width           =   1920
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "L�mite de Compra"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   39
         Top             =   2250
         Width           =   1515
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Devoluciones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   38
         Top             =   5400
         Width           =   1110
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Compras en Espera"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   37
         Top             =   4485
         Width           =   1680
      End
      Begin VB.Label Label66 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "POSICION"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   6780
         TabIndex        =   36
         Top             =   390
         Width           =   855
      End
      Begin VB.Label Label67 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "LLAVE"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   6990
         TabIndex        =   35
         Top             =   540
         Width           =   525
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   780
      Top             =   1230
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "botones.frx":6830
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "botones.frx":85C2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "botones.frx":A354
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "botones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Llaves As Boolean

Private Sub ayuda_Click()
    Call Definir(ayuda, ayuda.Value, ANIVEL, ALLAVE)
End Sub

Private Sub balanzas_Click()
    Call Definir(balanzas, balanzas.Value, BNIVEL, BLLAVE)
End Sub

Private Sub cantidad_Click()
    Call Definir(cantidad, cantidad.Value, CNIVEL, CLLAVE)
End Sub

Private Sub cliente_Click()
    Call Definir(cliente, cliente.Value, CLNIVEL, CLLLAVE)
End Sub

Private Sub devolucion_Click()
    Call Definir(devolucion, devolucion.Value, DNIVEL, DLLAVE)
End Sub

Private Sub espera_Click()
    Call Definir(espera, espera.Value, ENIVEL, ELLAVE)
End Sub

Private Sub limite_Click()
    Call Definir(limite, limite.Value, LNIVEL, LLLAVE)
End Sub

Private Sub parcial_Click()
    Call Definir(parcial, parcial.Value, rpnivel, rpllave)
End Sub

Private Sub precios_Click()
    Call Definir(precios, precios.Value, PRNIVEL, PRLLAVE)
End Sub

Private Sub productos_Click()
    Call Definir(productos, productos.Value, PNIVEL, PLLAVE)
End Sub

Private Sub reintegro_Click()
    Call Definir(reintegro, reintegro.Value, rnivel, rllave)
End Sub

Private Sub suspender_Click()
    Call Definir(suspender, suspender.Value, SNIVEL, SLLAVE)
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF12, 0)
        
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
    End Select
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 Then
        Unload Me
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set Botones = Nothing
End Sub

Private Sub Grabar_Caja()

    Ent.POS.BeginTrans
    
    Call Apertura_Recordset(False, rsCaja)

    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
    Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not rsCaja.EOF Then
        rsCaja.Update
            Call Grabar_Detalle
        rsCaja.UpdateBatch
    End If
        
'            If glgrabar = False Then
'                ARBOL.Nodes.Item(lcindex).Text = UCase(descri.Text)
'            Else
'                Call crear_menu(lckey, clave, UCase(descri.Text), "caja")
'            End If

    Ent.POS.CommitTrans

    Call Cerrar_Recordset(rsCaja)
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF4
            Call Grabar_Caja
            Unload Me
            
        Case vbKeyF12, vbKeyF7
            Unload Me
    End Select
End Sub

Private Sub Form_Load()

    Me.Caption = Me.Caption & " de la Caja No. " & nCaja
    
    Call Apertura_Recordset(True, rsCaja)
        
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
        
    Set rsCaja.ActiveConnection = Nothing
        
    If Not rsCaja.EOF Then
        If rsCaja!Llaves Then
            If rsCaja!b_Opos Then
                Llaves = True
                Call Objetos_OPOS(True)
            Else
                Call Objetos_OPOS(False)
                Llaves = False
            End If
        Else
            Llaves = False
            Call Objetos_OPOS(False)
        End If
        
        Call Llenar_Caja
    End If
    
    Call Cerrar_Recordset(rsCaja)

End Sub

Private Sub Llenar_Caja()
        
    ayuda.Value = IIf(rsCaja!bAyuda = True, 1, 0)
    productos.Value = IIf(rsCaja!bProductos = True, 1, 0)
    limite.Value = IIf(rsCaja!bLimite = True, 1, 0)
    cantidad.Value = IIf(rsCaja!bCantidad = True, 1, 0)
    balanzas.Value = IIf(rsCaja!bBalanza = True, 1, 0)
    reintegro.Value = IIf(rsCaja!bReintegro = True, 1, 0)
    parcial.Value = IIf(rsCaja!bParcial = True, 1, 0)
    espera.Value = IIf(rsCaja!bEspera = True, 1, 0)
    suspender.Value = IIf(rsCaja!bSuspender = True, 1, 0)
    devolucion.Value = IIf(rsCaja!bDevolucion = True, 1, 0)
    precios.Value = IIf(rsCaja!bPrecios = True, 1, 0)
    cliente.Value = IIf(rsCaja!bCliente = True, 1, 0)
    
    ANIVEL.Text = rsCaja!nAyuda
    PNIVEL.Text = rsCaja!nProductos
    LNIVEL.Text = rsCaja!nLimite
    CNIVEL.Text = rsCaja!nCantidad
    BNIVEL.Text = rsCaja!nBalanza
    rnivel.Text = rsCaja!nReintegro
    rpnivel.Text = rsCaja!nParcial
    ENIVEL.Text = rsCaja!nEspera
    SNIVEL.Text = rsCaja!nSuspender
    DNIVEL.Text = rsCaja!nDevolucion
    PRNIVEL.Text = rsCaja!nPrecios
    CLNIVEL.Text = rsCaja!nCliente
    nopen.Text = rsCaja!nOpenPOS
    nclose.Text = rsCaja!nClosePOS
    
    ALLAVE.Text = rsCaja!lAyuda
    PLLAVE.Text = rsCaja!lProductos
    LLLAVE.Text = rsCaja!lLimite
    CLLAVE.Text = rsCaja!lCantidad
    BLLAVE.Text = rsCaja!lBalanza
    rllave.Text = rsCaja!lReintegro
    rpllave.Text = rsCaja!lParcial
    ELLAVE.Text = rsCaja!lEspera
    SLLAVE.Text = rsCaja!lSuspender
    DLLAVE.Text = rsCaja!lDevolucion
    PRLLAVE.Text = rsCaja!lPrecios
    CLLLAVE.Text = rsCaja!lCliente
    cellave.Text = rsCaja!lCerrar
    
End Sub

Private Sub Grabar_Detalle()
        
    rsCaja!bAyuda = IIf(ayuda.Value = 1, 1, 0)
    rsCaja!bProductos = IIf(productos.Value = 1, 1, 0)
    rsCaja!bLimite = IIf(limite.Value = 1, 1, 0)
    rsCaja!bCantidad = IIf(cantidad.Value = 1, 1, 0)
    rsCaja!bBalanza = IIf(balanzas.Value = 1, 1, 0)
    rsCaja!bReintegro = IIf(reintegro.Value = 1, 1, 0)
    rsCaja!bParcial = IIf(parcial.Value = 1, 1, 0)
    rsCaja!bEspera = IIf(espera.Value = 1, 1, 0)
    rsCaja!bSuspender = IIf(suspender.Value = 1, 1, 0)
    rsCaja!bDevolucion = IIf(devolucion.Value = 1, 1, 0)
    rsCaja!bPrecios = IIf(precios.Value = 1, 1, 0)
    rsCaja!bCliente = IIf(cliente.Value = 1, 1, 0)

    rsCaja!cOpenPOS = IIf(CInt(nopen.Text) <> 0, 1, 0)
    rsCaja!cAyuda = IIf(CInt(ANIVEL.Text) <> 0, 1, 0)
    rsCaja!cProductos = IIf(CInt(PNIVEL.Text) <> 0, 1, 0)
    rsCaja!cLimite = IIf(CInt(LNIVEL.Text) <> 0, 1, 0)
    rsCaja!cCantidad = IIf(CInt(CNIVEL.Text) <> 0, 1, 0)
    rsCaja!cBalanza = IIf(CInt(BNIVEL.Text) <> 0, 1, 0)
    rsCaja!cReintegro = IIf(CInt(rnivel.Text) <> 0, 1, 0)
    rsCaja!cParcial = IIf(CInt(rpnivel.Text) <> 0, 1, 0)
    rsCaja!cEspera = IIf(CInt(ENIVEL.Text) <> 0, 1, 0)
    rsCaja!cSuspender = IIf(CInt(SNIVEL.Text) <> 0, 1, 0)
    rsCaja!cDevolucion = IIf(CInt(DNIVEL.Text) <> 0, 1, 0)
    rsCaja!cPrecios = IIf(CInt(PRNIVEL.Text) <> 0, 1, 0)
    rsCaja!cCliente = IIf(CInt(CLNIVEL.Text) <> 0, 1, 0)
    rsCaja!cClosePOS = IIf(CInt(nclose.Text) <> 0, 1, 0)
    
    rsCaja!nOpenPOS = CInt(nopen.Text)
    rsCaja!nAyuda = CInt(ANIVEL.Text)
    rsCaja!nProductos = CInt(PNIVEL.Text)
    rsCaja!nLimite = CInt(LNIVEL.Text)
    rsCaja!nCantidad = CInt(CNIVEL.Text)
    rsCaja!nBalanza = CInt(BNIVEL.Text)
    rsCaja!nReintegro = CInt(rnivel.Text)
    rsCaja!nParcial = CInt(rpnivel.Text)
    rsCaja!nEspera = CInt(ENIVEL.Text)
    rsCaja!nSuspender = CInt(SNIVEL.Text)
    rsCaja!nDevolucion = CInt(DNIVEL.Text)
    rsCaja!nPrecios = CInt(PRNIVEL.Text)
    rsCaja!nCliente = CInt(CLNIVEL.Text)
    rsCaja!nClosePOS = CInt(nclose.Text)
    
    rsCaja!lAyuda = CInt(ALLAVE.Text)
    rsCaja!lProductos = CInt(PLLAVE.Text)
    rsCaja!lLimite = CInt(LLLAVE.Text)
    rsCaja!lCantidad = CInt(CLLAVE.Text)
    rsCaja!lBalanza = CInt(BLLAVE.Text)
    rsCaja!lReintegro = CInt(rllave.Text)
    rsCaja!lParcial = CInt(rpllave.Text)
    rsCaja!lEspera = CInt(ELLAVE.Text)
    rsCaja!lSuspender = CInt(SLLAVE.Text)
    rsCaja!lDevolucion = CInt(DLLAVE.Text)
    rsCaja!lPrecios = CInt(PRLLAVE.Text)
    rsCaja!lCliente = CInt(CLLLAVE.Text)
    rsCaja!lCerrar = CInt(cellave.Text)

End Sub

Private Sub Definir(Cuadro As Object, SN As Integer, Nivel As Object, ComboLlave As Object)
    Select Case SN
        Case Is = 1
            'CUADRO.Caption = "Si"
            Nivel.Enabled = True
            If Llaves Then ComboLlave.Enabled = True
        Case Is = 0
            'CUADRO.Caption = "No"
            Nivel.Enabled = False
            If Llaves Then ComboLlave.Enabled = False
    End Select
End Sub

Private Sub Objetos_OPOS(Valor As Boolean)
    
    ALLAVE.Enabled = Valor
    PLLAVE.Enabled = Valor
    LLLAVE.Enabled = Valor
    CLLAVE.Enabled = Valor
    BLLAVE.Enabled = Valor
    rllave.Enabled = Valor
    rpllave.Enabled = Valor
    ELLAVE.Enabled = Valor
    SLLAVE.Enabled = Valor
    DLLAVE.Enabled = Valor
    PRLLAVE.Enabled = Valor
    CLLLAVE.Enabled = Valor
    cellave.Enabled = Valor

End Sub
