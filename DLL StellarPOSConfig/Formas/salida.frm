VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form salida 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9975
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   12975
   ControlBox      =   0   'False
   Icon            =   "salida.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9975
   ScaleWidth      =   12975
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   71
      Top             =   0
      Width           =   14040
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   10515
         TabIndex        =   73
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Dispositivos de Salida"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   72
         Top             =   75
         Width           =   5415
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   13185
      TabIndex        =   68
      Top             =   421
      Width           =   13215
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   1080
         Left            =   0
         TabIndex        =   69
         Top             =   0
         Width           =   13000
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   300
            TabIndex        =   70
            Top             =   120
            Width           =   12500
            _ExtentX        =   22040
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "F4 Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "F7 Cancelar"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "F1 Ayuda"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   4
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame FGAVETA 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Gaveta para Dinero "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2565
      Left            =   210
      TabIndex        =   44
      Top             =   4965
      Width           =   12495
      Begin VB.Frame fparalelog 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   645
         Left            =   4500
         TabIndex        =   65
         Top             =   780
         Width           =   7875
         Begin VB.TextBox Gcontrol 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3270
            MaxLength       =   60
            TabIndex        =   66
            Top             =   150
            Width           =   4365
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Car�cter de Control de Apertua"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   7
            Left            =   210
            TabIndex        =   67
            Top             =   210
            Width           =   2685
         End
      End
      Begin VB.Frame ftipo 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Tipo"
         ClipControls    =   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   765
         Left            =   210
         TabIndex        =   62
         Top             =   660
         Width           =   2835
         Begin VB.OptionButton tconexion 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Paralela"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   225
            Index           =   1
            Left            =   1500
            TabIndex        =   64
            Top             =   330
            Width           =   1035
         End
         Begin VB.OptionButton tconexion 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Serial"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   225
            Index           =   0
            Left            =   180
            TabIndex        =   63
            Top             =   330
            Value           =   -1  'True
            Width           =   885
         End
      End
      Begin VB.Frame FSERIALg 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   915
         Left            =   180
         TabIndex        =   51
         Top             =   1500
         Width           =   12195
         Begin VB.ComboBox GPuerto 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":628A
            Left            =   210
            List            =   "salida.frx":62D9
            Style           =   2  'Dropdown List
            TabIndex        =   56
            Top             =   420
            Width           =   2205
         End
         Begin VB.ComboBox GBaudios 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":6383
            Left            =   2610
            List            =   "salida.frx":6399
            Style           =   2  'Dropdown List
            TabIndex        =   55
            Top             =   420
            Width           =   2205
         End
         Begin VB.ComboBox GParidad 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":63C4
            Left            =   5010
            List            =   "salida.frx":63D1
            Style           =   2  'Dropdown List
            TabIndex        =   54
            Top             =   420
            Width           =   2205
         End
         Begin VB.ComboBox GDato 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":63EA
            Left            =   7410
            List            =   "salida.frx":63FD
            Style           =   2  'Dropdown List
            TabIndex        =   53
            Top             =   420
            Width           =   2205
         End
         Begin VB.ComboBox GParada 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":6410
            Left            =   9810
            List            =   "salida.frx":641D
            Style           =   2  'Dropdown List
            TabIndex        =   52
            Top             =   420
            Width           =   2205
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Puerto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   210
            TabIndex        =   61
            Top             =   120
            Width           =   555
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Baudios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   2610
            TabIndex        =   60
            Top             =   120
            Width           =   660
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Paridad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   5010
            TabIndex        =   59
            Top             =   120
            Width           =   645
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Dato"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   7410
            TabIndex        =   58
            Top             =   120
            Width           =   1020
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Parada"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   9810
            TabIndex        =   57
            Top             =   120
            Width           =   1230
         End
      End
      Begin VB.Frame foposg 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   675
         Left            =   4530
         TabIndex        =   48
         Top             =   360
         Width           =   7845
         Begin VB.TextBox oposg 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3260
            MaxLength       =   50
            TabIndex        =   49
            Top             =   0
            Width           =   4365
         End
         Begin VB.Label Label65 
            BackStyle       =   0  'Transparent
            Caption         =   "OPOS Gaveta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Index           =   0
            Left            =   180
            TabIndex        =   50
            Top             =   0
            Width           =   1275
         End
      End
      Begin VB.OptionButton oposgaveta 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "OLE POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   1545
         TabIndex        =   47
         Top             =   390
         Width           =   1155
      End
      Begin VB.OptionButton nogaveta 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "No"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   330
         TabIndex        =   46
         Top             =   390
         Width           =   555
      End
      Begin VB.OptionButton serialgaveta 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Si"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   3120
         TabIndex        =   45
         Top             =   390
         Width           =   765
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   2140
         X2              =   12240
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   " Gaveta para Dinero "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   75
         Top             =   0
         Width           =   1815
      End
   End
   Begin VB.Frame FDISPLAY 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Display"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1995
      Left            =   210
      TabIndex        =   26
      Top             =   7725
      Width           =   12495
      Begin VB.Frame FSERIALD 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   915
         Left            =   90
         TabIndex        =   33
         Top             =   1020
         Width           =   12315
         Begin VB.ComboBox DParada 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":642C
            Left            =   9780
            List            =   "salida.frx":6439
            Style           =   2  'Dropdown List
            TabIndex        =   38
            Top             =   420
            Width           =   2205
         End
         Begin VB.ComboBox DDato 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":6448
            Left            =   7380
            List            =   "salida.frx":645B
            Style           =   2  'Dropdown List
            TabIndex        =   37
            Top             =   420
            Width           =   2205
         End
         Begin VB.ComboBox DParidad 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":646E
            Left            =   4980
            List            =   "salida.frx":647B
            Style           =   2  'Dropdown List
            TabIndex        =   36
            Top             =   420
            Width           =   2205
         End
         Begin VB.ComboBox DBaudios 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":6494
            Left            =   2580
            List            =   "salida.frx":64AA
            Style           =   2  'Dropdown List
            TabIndex        =   35
            Top             =   420
            Width           =   2205
         End
         Begin VB.ComboBox DPuerto 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":64D5
            Left            =   180
            List            =   "salida.frx":6524
            Style           =   2  'Dropdown List
            TabIndex        =   34
            Top             =   420
            Width           =   2205
         End
         Begin VB.Label Label43 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Parada"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   9690
            TabIndex        =   43
            Top             =   120
            Width           =   1230
         End
         Begin VB.Label Label42 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Dato"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   7380
            TabIndex        =   42
            Top             =   120
            Width           =   1020
         End
         Begin VB.Label Label41 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Paridad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   4980
            TabIndex        =   41
            Top             =   120
            Width           =   645
         End
         Begin VB.Label Label40 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Baudios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   2580
            TabIndex        =   40
            Top             =   120
            Width           =   660
         End
         Begin VB.Label Label31 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Puerto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   180
            TabIndex        =   39
            Top             =   120
            Width           =   555
         End
      End
      Begin VB.Frame FOPOSD 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   675
         Left            =   4440
         TabIndex        =   30
         Top             =   240
         Width           =   7965
         Begin VB.TextBox OPOSD 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   3300
            MaxLength       =   50
            TabIndex        =   31
            Top             =   180
            Width           =   4335
         End
         Begin VB.Label Label44 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "OPOS Display"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Left            =   300
            TabIndex        =   32
            Top             =   210
            Width           =   1275
         End
      End
      Begin VB.OptionButton SERIALDISPLAY 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Serial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3120
         TabIndex        =   29
         Top             =   480
         Width           =   1125
      End
      Begin VB.OptionButton NODISPLAY 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   330
         TabIndex        =   28
         Top             =   480
         Width           =   795
      End
      Begin VB.OptionButton OPOSDISPLAY 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "OLE POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   1545
         TabIndex        =   27
         Top             =   480
         Width           =   1395
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00AE5B00&
         X1              =   1320
         X2              =   12240
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Display"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   76
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame fimpresora 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Impresora "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2955
      Left            =   210
      TabIndex        =   0
      Top             =   1800
      Width           =   12495
      Begin VB.Frame fhojilla 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   465
         Left            =   90
         TabIndex        =   22
         Top             =   1350
         Width           =   12195
         Begin VB.TextBox hojilla_caracter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   2100
            MaxLength       =   60
            TabIndex        =   24
            Top             =   60
            Width           =   3675
         End
         Begin VB.CheckBox hsi 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Utiliza Hojilla para el Corte del Papel "
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   315
            Left            =   6120
            TabIndex        =   23
            Top             =   90
            Width           =   3705
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Car�cter de Control "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   8
            Left            =   180
            TabIndex        =   25
            Top             =   0
            Width           =   1740
         End
      End
      Begin VB.Frame fserialimpresora 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   915
         Left            =   90
         TabIndex        =   11
         Top             =   1930
         Width           =   12315
         Begin VB.ComboBox IParada 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":65CE
            Left            =   9990
            List            =   "salida.frx":65DB
            Style           =   2  'Dropdown List
            TabIndex        =   16
            Top             =   330
            Width           =   2205
         End
         Begin VB.ComboBox IDato 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":65EA
            Left            =   7590
            List            =   "salida.frx":65FD
            Style           =   2  'Dropdown List
            TabIndex        =   15
            Top             =   330
            Width           =   2205
         End
         Begin VB.ComboBox IParidad 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":6610
            Left            =   5190
            List            =   "salida.frx":661D
            Style           =   2  'Dropdown List
            TabIndex        =   14
            Top             =   330
            Width           =   2205
         End
         Begin VB.ComboBox IBaudios 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":6636
            Left            =   2790
            List            =   "salida.frx":664C
            Style           =   2  'Dropdown List
            TabIndex        =   13
            Top             =   330
            Width           =   2205
         End
         Begin VB.ComboBox IPuerto 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":6677
            Left            =   270
            List            =   "salida.frx":66C6
            Style           =   2  'Dropdown List
            TabIndex        =   12
            Top             =   330
            Width           =   2205
         End
         Begin VB.Label Label55 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Parada"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   9990
            TabIndex        =   21
            Top             =   30
            Width           =   1230
         End
         Begin VB.Label Label54 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Dato"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   7590
            TabIndex        =   20
            Top             =   30
            Width           =   1020
         End
         Begin VB.Label Label53 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Paridad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   5190
            TabIndex        =   19
            Top             =   30
            Width           =   645
         End
         Begin VB.Label Label52 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Baudios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   2790
            TabIndex        =   18
            Top             =   30
            Width           =   660
         End
         Begin VB.Label Label51 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Puerto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   270
            TabIndex        =   17
            Top             =   30
            Width           =   555
         End
      End
      Begin VB.Frame foposi 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   465
         Left            =   180
         TabIndex        =   4
         Top             =   720
         Width           =   12105
         Begin VB.TextBox oposi 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   2040
            MaxLength       =   50
            TabIndex        =   7
            Top             =   30
            Width           =   3615
         End
         Begin VB.CheckBox cheques 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "Imprime Cheques"
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   315
            Left            =   6090
            TabIndex        =   6
            Top             =   120
            Width           =   2145
         End
         Begin VB.ComboBox Rotar 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "salida.frx":6770
            Left            =   10020
            List            =   "salida.frx":6780
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   120
            Width           =   1005
         End
         Begin VB.Label Label65 
            BackStyle       =   0  'Transparent
            Caption         =   "OPOS Impresora"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Index           =   1
            Left            =   60
            TabIndex        =   10
            Top             =   60
            Width           =   1545
         End
         Begin VB.Label Label65 
            BackStyle       =   0  'Transparent
            Caption         =   "Rotaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   195
            Index           =   2
            Left            =   9060
            TabIndex        =   9
            Top             =   180
            Width           =   735
         End
         Begin VB.Label Label65 
            BackStyle       =   0  'Transparent
            Caption         =   "Grados"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   195
            Index           =   3
            Left            =   11190
            TabIndex        =   8
            Top             =   180
            Width           =   645
         End
      End
      Begin VB.OptionButton oposimpresora 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "OLE POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   1620
         TabIndex        =   3
         Top             =   330
         Width           =   1395
      End
      Begin VB.OptionButton noimpresora 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Paralela"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   2
         Top             =   330
         Width           =   1245
      End
      Begin VB.OptionButton serialimpresora 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Serial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3150
         TabIndex        =   1
         Top             =   330
         Width           =   1065
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   1320
         X2              =   12240
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Impresora "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   74
         Top             =   0
         Width           =   975
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   6180
      Top             =   1350
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "salida.frx":6795
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "salida.frx":8527
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "salida.frx":A2B9
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "salida.frx":C04B
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "salida"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub serialdisplay_Click()
    FOPOSD.Enabled = False
    FSERIALD.Enabled = True
End Sub

Private Sub Oposdisplay_Click()
    FOPOSD.Enabled = True
    FSERIALD.Enabled = False
End Sub

Private Sub nodisplay_Click()
    FOPOSD.Enabled = False
    FSERIALD.Enabled = False
End Sub

Private Sub tconexion_Click(Index As Integer)
    Select Case Index
        Case Is = 0
            FSERIALg.Enabled = True
            fparalelog.Enabled = False
        Case Is = 1
            FSERIALg.Enabled = False
            fparalelog.Enabled = True
    End Select
End Sub

Private Sub serialgaveta_Click()
    ftipo.Enabled = True
    foposg.Enabled = False
    FSERIALg.Enabled = False
    fparalelog.Enabled = False
End Sub

Private Sub nogaveta_Click()
    ftipo.Enabled = False
    foposg.Enabled = False
    FSERIALg.Enabled = False
    fparalelog.Enabled = False
End Sub

Private Sub oposgaveta_Click()
    ftipo.Enabled = False
    foposg.Enabled = True
    FSERIALg.Enabled = False
    fparalelog.Enabled = False
End Sub

Private Sub noimpresora_Click()
    fserialimpresora.Enabled = False
    foposi.Enabled = False
    fhojilla.Enabled = True
End Sub

Private Sub oposimpresora_Click()
    fserialimpresora.Enabled = False
    foposi.Enabled = True
    hsi_Click
End Sub

Private Sub serialimpresora_Click()
    fserialimpresora.Enabled = True
    foposi.Enabled = False
    fhojilla.Enabled = True
    hsi_Click
End Sub

Private Sub cheques_Click()
    Select Case cheques.Value
        Case Is = 0
            'resp = Replace(cheques.Caption, "Si", "No", 1)
            Rotar.Enabled = False
            cheques.Caption = resp
        Case Is = 1
            'resp = Replace(cheques.Caption, "No", "Si", 1)
            Rotar.Enabled = True
            cheques.Caption = resp
    End Select
End Sub

Private Sub hsi_Click()
    Select Case hsi.Value
        Case Is = 0
            hojilla_caracter.Enabled = False
            'resp = Replace(hsi.Caption, "Si", "No", 1)
            'hsi.Caption = resp
        Case Is = 1
            If oposimpresora.Value = False Then
                hojilla_caracter.Enabled = True
            Else
                hojilla_caracter.Enabled = False
            End If
            'resp = Replace(hsi.Caption, "No", "Si", 1)
            'hsi.Caption = resp
    End Select
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF12, 0)
        
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
            
        Case Is = UCase("Teclado")
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = oposi
            
            TecladoWindows mCtl
                        
    End Select
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 Then
        Unload Me
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set salida = Nothing
End Sub

Private Sub Grabar_Caja()
    Ent.POS.BeginTrans
    
    Call Apertura_Recordset(False, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
    Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not rsCaja.EOF Then
        rsCaja.Update
            Call Grabar_Detalle
        rsCaja.UpdateBatch
    End If
        
'            If glgrabar = False Then
'                ARBOL.Nodes.Item(lcindex).Text = UCase(descri.Text)
'            Else
'                Call crear_menu(lckey, clave, UCase(descri.Text), "caja")
'            End If

    Ent.POS.CommitTrans
    
    Call Cerrar_Recordset(rsCaja)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF4
            Call Grabar_Caja
            Unload Me
            
        Case vbKeyF12, vbKeyF7
            Unload Me
    End Select
End Sub

Private Sub Form_Load()

    Me.Caption = Me.Caption & " de la Caja No. " & nCaja
    Call Apertura_Recordset(True, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set rsCaja.ActiveConnection = Nothing
    If Not rsCaja.EOF Then
        If rsCaja!b_Opos Then
            Call Objetos_OPOS(True)
        Else
            nogaveta.Value = True
            NODISPLAY.Value = True
            
            Call Objetos_OPOS(False)
        End If
        
        Call Llenar_Caja
    End If
    
    Call Cerrar_Recordset(rsCaja)

End Sub

Private Sub Llenar_Caja()

    Select Case rsCaja!Gabeta
        Case 0
            nogaveta.Value = True
        Case 1
            oposgaveta.Value = True
        Case 2
            serialgaveta.Value = True
    End Select
    
    If rsCaja!Gserial = True Then
        tconexion(0).Value = True
    Else
        tconexion(1).Value = True
    End If
    
    GPuerto.Text = rsCaja!GPuerto
    GBaudios.Text = rsCaja!GBaudios
    
    Select Case LCase(rsCaja!GParidad)
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    
    GParidad.Text = Paridad
    GDato.Text = rsCaja!gBit
    GParada.Text = rsCaja!GParada
    Gcontrol.Text = rsCaja!Gcontrol
    
    Select Case rsCaja!Display
        Case 0
            NODISPLAY.Value = True
        Case 1
            OPOSDISPLAY.Value = True
        Case 2
            SERIALDISPLAY.Value = True

    End Select
    
    DPuerto.Text = rsCaja!DPuerto
    DBaudios.Text = rsCaja!DBaudios
    
    Select Case rsCaja!DParidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    
    DParidad.Text = Paridad
    DDato.Text = rsCaja!dBit
    DParada.Text = rsCaja!DParada
    
    Select Case rsCaja!Impresora
        Case 0
            noimpresora.Value = True
        Case 1
            oposimpresora.Value = True
        Case 2
            serialimpresora.Value = True
        
    End Select
    
    IPuerto.Text = rsCaja!IPuerto
    IBaudios.Text = rsCaja!IBaudios
    
    Select Case rsCaja!IParidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    
    IParidad.Text = Paridad
    IDato.Text = rsCaja!IBit
    IParada.Text = rsCaja!IParada
    hsi.Value = IIf(rsCaja!Ihojilla = True, 1, 0)
    hojilla_caracter = IIf(IsNull(rsCaja!hojilla_caracter), "", rsCaja!hojilla_caracter)
    
    cheques.Value = IIf(rsCaja!Imp_Cheques = True, 1, 0)
    Rotar.Text = rsCaja!Rotacion
    
    OPOSD.Text = IIf(IsNull(rsCaja!C_OPOSD), "", rsCaja!C_OPOSD)
    oposg.Text = IIf(IsNull(rsCaja!C_OPOSG), "", rsCaja!C_OPOSG)
    oposi.Text = IIf(IsNull(rsCaja!C_OPOSI), "", rsCaja!C_OPOSI)
    
End Sub

Private Sub Grabar_Detalle()

    '*********************************
    '*** Gaveta
    '*********************************
    
    If nogaveta.Value = True Then
        Numero = 0
    ElseIf oposgaveta.Value = True Then
        Numero = 1
    Else
        Numero = 2
    End If
    
    rsCaja!Gabeta = Numero
    rsCaja!Gserial = tconexion(0).Value
    rsCaja!GPuerto = GPuerto.Text
    rsCaja!GBaudios = GBaudios.Text
    
    Select Case GParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    
    rsCaja!GParidad = Paridad
    rsCaja!gBit = CInt(GDato.Text)
    rsCaja!GParada = CDbl(GParada.Text)
    rsCaja!Gcontrol = IIf(Len(Trim(Gcontrol.Text)), Gcontrol.Text, "")
    
    '*********************************
    '*** Display
    '*********************************
    
    If NODISPLAY.Value = True Then
        Numero = 0
    ElseIf OPOSDISPLAY.Value = True Then
        Numero = 1
    Else
        Numero = 2
    End If
    
    rsCaja!Display = Numero
    rsCaja!DPuerto = DPuerto.Text
    rsCaja!DBaudios = DBaudios.Text
    
    Select Case DParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    
    rsCaja!DParidad = Paridad
    rsCaja!dBit = CInt(DDato.Text)
    rsCaja!DParada = CDbl(DParada.Text)
    
    '*********************************
    '*** Impresora
    '*********************************
    
    If noimpresora.Value = True Then
        Numero = 0
    ElseIf oposimpresora.Value = True Then
        Numero = 1
    Else
        Numero = 2
    End If
    
    rsCaja!Impresora = Numero
    rsCaja!IPuerto = IPuerto.Text
    rsCaja!IBaudios = IBaudios.Text
    
    Select Case IParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    
    rsCaja!IParidad = Paridad
    rsCaja!IBit = CInt(IDato.Text)
    rsCaja!IParada = CDbl(IParada.Text)
    rsCaja!Ihojilla = hsi.Value
    rsCaja!hojilla_caracter = hojilla_caracter
    rsCaja!Imp_Cheques = cheques.Value
    rsCaja!Rotacion = CInt(Rotar.Text)
    
    
    rsCaja!C_OPOSD = OPOSD.Text
    rsCaja!C_OPOSG = oposg.Text
    rsCaja!C_OPOSI = oposi.Text
'    rscaja!C_OposT = opost.Text

End Sub

Private Sub Objetos_OPOS(Valor As Boolean)
    oposgaveta.Enabled = Valor
    OPOSDISPLAY.Enabled = Valor
    oposimpresora.Enabled = Valor
End Sub
