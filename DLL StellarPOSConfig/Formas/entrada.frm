VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form Entrada 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9540
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   11985
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "entrada.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9540
   ScaleWidth      =   11985
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   65
      Top             =   0
      Width           =   14040
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Dispositivos de Entrada"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   67
         Top             =   75
         Width           =   5415
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   9795
         TabIndex        =   66
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   12585
      TabIndex        =   62
      Top             =   421
      Width           =   12615
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   915
         Left            =   0
         TabIndex        =   63
         Top             =   0
         Width           =   12000
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   180
            TabIndex        =   64
            Top             =   60
            Width           =   11500
            _ExtentX        =   20294
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "F4 Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "F7 Cancelar"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "ayuda"
                  Object.ToolTipText     =   "F1 Ayuda"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   4
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame FBALANZA 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Balanza "
      ClipControls    =   0   'False
      ForeColor       =   &H80000002&
      Height          =   1995
      Left            =   210
      TabIndex        =   52
      Top             =   3825
      Width           =   11655
      Begin VB.Frame fserialba 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   915
         Left            =   120
         TabIndex        =   55
         Top             =   900
         Width           =   11355
         Begin VB.TextBox bprefijo 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   9150
            MaxLength       =   40
            TabIndex        =   18
            Top             =   420
            Width           =   1755
         End
         Begin VB.ComboBox bPuerto 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":628A
            Left            =   180
            List            =   "entrada.frx":62D9
            Style           =   2  'Dropdown List
            TabIndex        =   13
            Top             =   420
            Width           =   1695
         End
         Begin VB.ComboBox bBaudios 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":6383
            Left            =   1980
            List            =   "entrada.frx":6399
            Style           =   2  'Dropdown List
            TabIndex        =   14
            Top             =   420
            Width           =   1695
         End
         Begin VB.ComboBox bParidad 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":63C4
            Left            =   3765
            List            =   "entrada.frx":63D1
            Style           =   2  'Dropdown List
            TabIndex        =   15
            Top             =   420
            Width           =   1695
         End
         Begin VB.ComboBox bDato 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":63EA
            Left            =   5565
            List            =   "entrada.frx":63FD
            Style           =   2  'Dropdown List
            TabIndex        =   16
            Top             =   420
            Width           =   1695
         End
         Begin VB.ComboBox bParada 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":6410
            Left            =   7350
            List            =   "entrada.frx":641D
            Style           =   2  'Dropdown List
            TabIndex        =   17
            Top             =   420
            Width           =   1695
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Prefijo Ctrl"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   5
            Left            =   9150
            TabIndex        =   61
            Top             =   120
            Width           =   915
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Puerto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   180
            TabIndex        =   60
            Top             =   120
            Width           =   555
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Baudios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   1980
            TabIndex        =   59
            Top             =   120
            Width           =   660
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Paridad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   3780
            TabIndex        =   58
            Top             =   120
            Width           =   645
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Dato"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   5565
            TabIndex        =   57
            Top             =   120
            Width           =   1020
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Parada"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   7350
            TabIndex        =   56
            Top             =   120
            Width           =   1230
         End
      End
      Begin VB.Frame foposba 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   555
         Left            =   4110
         TabIndex        =   53
         Top             =   240
         Width           =   7365
         Begin VB.TextBox oposba 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1530
            MaxLength       =   50
            TabIndex        =   12
            Top             =   60
            Width           =   5505
         End
         Begin VB.Label Label63 
            BackStyle       =   0  'Transparent
            Caption         =   "OPOS  Balanza"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   405
            Left            =   90
            TabIndex        =   54
            Top             =   90
            Width           =   1335
         End
      End
      Begin VB.OptionButton serialbalanza 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Serial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   2730
         TabIndex        =   11
         Top             =   360
         Width           =   825
      End
      Begin VB.OptionButton nobalanza 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   300
         TabIndex        =   9
         Top             =   360
         Width           =   555
      End
      Begin VB.OptionButton Oposbalanza 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "OLE POS"
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   1275
         TabIndex        =   10
         Top             =   360
         Width           =   1155
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   1200
         X2              =   11400
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Balanza "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   240
         TabIndex        =   71
         Top             =   0
         Width           =   975
      End
   End
   Begin VB.Frame FSCANNER 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Scanner  "
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2025
      Left            =   210
      TabIndex        =   43
      Top             =   1590
      Width           =   11655
      Begin VB.Frame fserials 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   945
         Left            =   120
         TabIndex        =   45
         Top             =   900
         Width           =   11355
         Begin VB.TextBox sPrefijo 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   9210
            MaxLength       =   40
            TabIndex        =   8
            Top             =   450
            Width           =   1695
         End
         Begin VB.ComboBox SParada 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":642C
            Left            =   7410
            List            =   "entrada.frx":6439
            Style           =   2  'Dropdown List
            TabIndex        =   7
            Top             =   450
            Width           =   1695
         End
         Begin VB.ComboBox SDato 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":6448
            Left            =   5595
            List            =   "entrada.frx":645B
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   450
            Width           =   1695
         End
         Begin VB.ComboBox SParidad 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":646E
            Left            =   3795
            List            =   "entrada.frx":647B
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   450
            Width           =   1695
         End
         Begin VB.ComboBox SBaudios 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":6494
            Left            =   1980
            List            =   "entrada.frx":64AA
            Style           =   2  'Dropdown List
            TabIndex        =   4
            Top             =   450
            Width           =   1695
         End
         Begin VB.ComboBox sPuerto 
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            ItemData        =   "entrada.frx":64D5
            Left            =   180
            List            =   "entrada.frx":6524
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   450
            Width           =   1695
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Prefijo Ctrl"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Index           =   6
            Left            =   9240
            TabIndex        =   51
            Top             =   150
            Width           =   915
         End
         Begin VB.Label Label18 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Parada"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   7410
            TabIndex        =   50
            Top             =   150
            Width           =   1230
         End
         Begin VB.Label Label17 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Dato"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   5610
            TabIndex        =   49
            Top             =   150
            Width           =   1020
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Paridad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   3810
            TabIndex        =   48
            Top             =   150
            Width           =   645
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Baudios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   1980
            TabIndex        =   47
            Top             =   150
            Width           =   660
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Puerto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   180
            TabIndex        =   46
            Top             =   150
            Width           =   555
         End
      End
      Begin VB.Frame foposs 
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         Height          =   555
         Left            =   4050
         TabIndex        =   44
         Top             =   240
         Width           =   7425
         Begin VB.TextBox oposs 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1560
            MaxLength       =   50
            TabIndex        =   68
            Top             =   180
            Width           =   5595
         End
         Begin VB.Label Label62 
            BackStyle       =   0  'Transparent
            Caption         =   "OPOS Scanner"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Left            =   120
            TabIndex        =   69
            Top             =   150
            Width           =   1305
         End
      End
      Begin VB.OptionButton Oposscanner 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "OLE POS"
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   1155
         TabIndex        =   1
         Top             =   380
         Width           =   1155
      End
      Begin VB.OptionButton noscanner 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   210
         TabIndex        =   0
         Top             =   380
         Width           =   555
      End
      Begin VB.OptionButton serialscanner 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Serial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   2700
         TabIndex        =   2
         Top             =   380
         Width           =   795
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   1080
         X2              =   11400
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   " Scanner  "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   70
         Top             =   0
         Width           =   1335
      End
   End
   Begin VB.Frame fbanda 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Lector de Banda Magnética "
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   2145
      Left            =   210
      TabIndex        =   34
      Top             =   5910
      Width           =   11655
      Begin VB.Frame fserialb 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   945
         Left            =   90
         TabIndex        =   37
         Top             =   1020
         Width           =   11475
         Begin VB.ComboBox bmparada 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            ItemData        =   "entrada.frx":65CE
            Left            =   7350
            List            =   "entrada.frx":65DB
            Style           =   2  'Dropdown List
            TabIndex        =   27
            Top             =   450
            Width           =   1725
         End
         Begin VB.ComboBox bmdato 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            ItemData        =   "entrada.frx":65EA
            Left            =   5550
            List            =   "entrada.frx":65FD
            Style           =   2  'Dropdown List
            TabIndex        =   26
            Top             =   450
            Width           =   1725
         End
         Begin VB.ComboBox bmparidad 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            ItemData        =   "entrada.frx":6610
            Left            =   3750
            List            =   "entrada.frx":661D
            Style           =   2  'Dropdown List
            TabIndex        =   25
            Top             =   450
            Width           =   1725
         End
         Begin VB.ComboBox bmbaudios 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            ItemData        =   "entrada.frx":6636
            Left            =   1950
            List            =   "entrada.frx":664C
            Style           =   2  'Dropdown List
            TabIndex        =   24
            Top             =   450
            Width           =   1725
         End
         Begin VB.ComboBox bmpuerto 
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            ItemData        =   "entrada.frx":6677
            Left            =   150
            List            =   "entrada.frx":66C6
            Style           =   2  'Dropdown List
            TabIndex        =   23
            Top             =   450
            Width           =   1725
         End
         Begin VB.Label Label50 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Parada"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   7350
            TabIndex        =   42
            Top             =   150
            Width           =   1230
         End
         Begin VB.Label Label49 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Bits de Dato"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   5580
            TabIndex        =   41
            Top             =   150
            Width           =   1020
         End
         Begin VB.Label Label48 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Paridad"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   3780
            TabIndex        =   40
            Top             =   150
            Width           =   645
         End
         Begin VB.Label Label47 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Baudios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   1980
            TabIndex        =   39
            Top             =   150
            Width           =   660
         End
         Begin VB.Label Label46 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Puerto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   240
            Left            =   150
            TabIndex        =   38
            Top             =   150
            Width           =   555
         End
      End
      Begin VB.Frame foposb 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   675
         Left            =   4200
         TabIndex        =   35
         Top             =   240
         Width           =   7245
         Begin VB.TextBox oposb 
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1440
            MaxLength       =   50
            TabIndex        =   22
            Top             =   180
            Width           =   5595
         End
         Begin VB.Label Label64 
            BackStyle       =   0  'Transparent
            Caption         =   "OPOS Banda"
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   180
            TabIndex        =   36
            Top             =   210
            Width           =   2505
         End
      End
      Begin VB.OptionButton Oposbanda 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "OLE POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   1305
         TabIndex        =   20
         Top             =   450
         Width           =   1155
      End
      Begin VB.OptionButton nobanda 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   240
         TabIndex        =   19
         Top             =   450
         Width           =   555
      End
      Begin VB.OptionButton serialbanda 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Serial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   2730
         TabIndex        =   21
         Top             =   450
         Width           =   795
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00AE5B00&
         X1              =   2640
         X2              =   11400
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   " Lector de Banda Magnética "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   120
         TabIndex        =   72
         Top             =   0
         Width           =   2775
      End
   End
   Begin VB.Frame fllaves 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Llaves de Seguridad"
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   1035
      Left            =   210
      TabIndex        =   31
      Top             =   8265
      Width           =   11655
      Begin VB.Frame foposl 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   675
         Left            =   4200
         TabIndex        =   32
         Top             =   180
         Width           =   7125
         Begin VB.TextBox oposl 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   1440
            MaxLength       =   50
            TabIndex        =   30
            Top             =   180
            Width           =   5565
         End
         Begin VB.Label Label45 
            BackColor       =   &H00E7E8E8&
            BackStyle       =   0  'Transparent
            Caption         =   "OPOS Llaves"
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   180
            TabIndex        =   33
            Top             =   210
            Width           =   2865
         End
      End
      Begin VB.OptionButton nollaves 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   300
         TabIndex        =   28
         Top             =   510
         Width           =   555
      End
      Begin VB.OptionButton oposllaves 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "OLE POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   1245
         TabIndex        =   29
         Top             =   510
         Width           =   1155
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00AE5B00&
         X1              =   2040
         X2              =   11400
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Llaves de Seguridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   120
         TabIndex        =   73
         Top             =   0
         Width           =   2055
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   3780
      Top             =   1230
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "entrada.frx":6770
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "entrada.frx":8502
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "entrada.frx":A294
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "entrada.frx":C026
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Entrada"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Llenar_Caja()

    '*************************************
    '*** Balanzas
    '*************************************
    
    Select Case rsCaja!Balanza
        Case 0
            nobalanza.Value = True
        Case 1
            Oposbalanza.Value = True
        Case 2
            serialbalanza.Value = True
        
    End Select
    
    bPuerto.ListIndex = ItemCombo(bPuerto, rsCaja!bPuerto)
    bBaudios.Text = rsCaja!bBaudios
    
    Select Case LCase(rsCaja!bParidad)
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    
    bParidad.Text = IIf(Not IsNull(Paridad), Paridad, "")
    bDato.Text = rsCaja!bBit
    bParada.Text = rsCaja!bParada
    bprefijo.Text = IIf(IsNull(rsCaja!bControl), "", rsCaja!bControl)
      
    '*************************************
    '*** Banda
    '*************************************
    
    Select Case rsCaja!Banda
        Case 0
            nobanda.Value = True
        Case 1
            Oposbanda.Value = True
        Case 2
            serialbanda.Value = True
        
    End Select
    
    bmpuerto.Text = IIf(IsNull(rsCaja!bmpuerto), "COM1", rsCaja!bmpuerto)
    bmbaudios.Text = IIf(IsNull(rsCaja!bmbaudios), "9600", rsCaja!bmbaudios)
    
    Select Case rsCaja!bmparidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    
    bmparidad.Text = Paridad
    bmdato.Text = rsCaja!bmbit
    
    If bmparada.Enabled Then
        bmparada.Text = rsCaja!bmparada
    End If
    
    '*************************************
    '*** Scanner
    '*************************************
    
    Select Case rsCaja!Scanner
        Case 0
            noscanner.Value = True
        Case 1
            Oposscanner.Value = True
        Case 2
            serialscanner.Value = True
        
    End Select
    
    sPuerto.Text = rsCaja!sPuerto
    SBaudios.Text = rsCaja!SBaudios
    
    Select Case rsCaja!SParidad
        Case Is = "e"
            Paridad = "Par"
        Case Is = "o"
            Paridad = "Impar"
        Case Is = "n"
            Paridad = "Ninguna"
    End Select
    
    SParidad.Text = IIf(Not IsNull(Paridad), Paridad, "")
    SDato.Text = rsCaja!Sbit
    If SParada.Enabled Then SParada.ListIndex = ItemCombo(SParada, rsCaja!SParada)
    'SParada.ListIndex = rscaja!SParada
    sPrefijo.Text = IIf(IsNull(rsCaja!SCONTROL), "", rsCaja!SCONTROL)
    
    Select Case rsCaja!Llaves
        Case 0
            nollaves.Value = True
        Case 1
            oposllaves.Value = True
    End Select
    
    oposs.Text = IIf(IsNull(rsCaja!c_OposS), "", rsCaja!c_OposS)
    oposba.Text = IIf(IsNull(rsCaja!c_OposBA), "", rsCaja!c_OposBA)
    oposl.Text = IIf(IsNull(rsCaja!c_OposL), "", rsCaja!c_OposL)
    oposb.Text = IIf(IsNull(rsCaja!c_OposB), "", rsCaja!c_OposB)

End Sub

Private Sub Grabar_Detalle()
    
    '******************************
    '*** Balanza
    '******************************
    
    If nobalanza.Value = True Then
        Numero = 0
    ElseIf Oposbalanza.Value = True Then
        Numero = 1
    Else
        Numero = 2
    End If
    
    rsCaja!Balanza = Numero
    rsCaja!bPuerto = bPuerto.Text
    rsCaja!bBaudios = bBaudios.Text
    
    Select Case bParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    
    rsCaja!bParidad = Paridad
    rsCaja!bBit = CInt(bDato.Text)
    rsCaja!bParada = CDbl(bParada.Text)
    rsCaja!bControl = IIf(IsNull(bprefijo.Text), "", bprefijo.Text)
        
    '******************************
    '*** Scanner
    '******************************
    
    If noscanner.Value = True Then
        Numero = 0
    ElseIf Oposscanner.Value = True Then
        Numero = 1
    Else
        Numero = 2
    End If
    
    rsCaja!Scanner = Numero
    rsCaja!sPuerto = sPuerto.Text
    rsCaja!SBaudios = SBaudios.Text
    
    Select Case SParidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    
    rsCaja!SParidad = Paridad
    rsCaja!Sbit = CInt(SDato.Text)
    
    If SParada.Text <> "" Then
        rsCaja!SParada = CDbl(SParada.Text)
    End If
    
    rsCaja!SCONTROL = IIf(IsNull(sPrefijo.Text), "", sPrefijo.Text)
    
    '******************************
    '*** Banda
    '******************************
    
    If nobanda.Value = True Then
        Numero = 0
    ElseIf Oposbanda.Value = True Then
        Numero = 1
    Else
        Numero = 2
    End If
    
    rsCaja!Banda = Numero
    rsCaja!bmpuerto = sPuerto.Text
    rsCaja!bmbaudios = SBaudios.Text
    
    Select Case bmparidad.Text
        Case Is = "Par"
            Paridad = "e"
        Case Is = "Impar"
            Paridad = "o"
        Case Is = "Ninguna"
            Paridad = "n"
    End Select
    
    rsCaja!bmparidad = Paridad
    rsCaja!bmbit = CInt(SDato.Text)
    
    If SParada.Text <> "" Then
        rsCaja!bmparada = CDbl(SParada.Text)
    End If
    
    '******************************
    '*** Llaves
    '******************************
    
    Numero = 0
    
    If oposllaves.Value = True Then
        Numero = 1
    End If
    
    rsCaja!Llaves = Numero
    
    rsCaja!c_OposS = oposs.Text
    rsCaja!c_OposB = oposb.Text
    rsCaja!c_OposBA = oposba.Text
    rsCaja!c_OposL = oposl.Text
    
End Sub

Private Sub Grabar_Caja()
    
    Ent.POS.BeginTrans
    
    Call Apertura_Recordset(False, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
    Ent.POS, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not rsCaja.EOF Then
        rsCaja.Update
            Call Grabar_Detalle
        rsCaja.UpdateBatch
    End If
            
    '            If glgrabar = False Then
    '                ARBOL.Nodes.Item(lcindex).Text = UCase(descri.Text)
    '            Else
    '                Call crear_menu(lckey, clave, UCase(descri.Text), "caja")
    '            End If
    
    Ent.POS.CommitTrans
    
    Call Cerrar_Recordset(rsCaja)
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF4
            Call Grabar_Caja
            Unload Me
            
        Case vbKeyF12, vbKeyF7
            Unload Me
    End Select
End Sub

Private Sub Form_Load()
    Me.Caption = Me.Caption & " de la Caja No. " & nCaja
    
    Call Apertura_Recordset(True, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set rsCaja.ActiveConnection = Nothing
    
    If Not rsCaja.EOF Then
        If rsCaja!b_Opos Then
            Call Objetos_OPOS(True)
        Else
            nollaves.Value = True
            noscanner.Value = True
            nobalanza.Value = True
            nobanda.Value = True
            Call Objetos_OPOS(False)
        End If
        
        Call Llenar_Caja
    End If
    
    Call Cerrar_Recordset(rsCaja)
End Sub

Private Sub nobalanza_Click()
    foposba.Enabled = False
    fserialba.Enabled = False
End Sub

Private Sub nobanda_Click()
    foposb.Enabled = False
    fserialb.Enabled = False
End Sub

Private Sub nollaves_Click()
    foposl.Enabled = False
End Sub

Private Sub oposllaves_Click()
    foposl.Enabled = True
End Sub

Private Sub serialbanda_Click()
    foposb.Enabled = False
    fserialb.Enabled = True
End Sub

Private Sub Oposbanda_Click()
    nobanda.Value = False
    fserialb.Enabled = False
    foposb.Enabled = True
End Sub

Private Sub Oposbalanza_Click()
    nobalanza.Value = False
    fserialba.Enabled = False
    foposba.Enabled = True
    serialbalanza.Value = False
End Sub

Private Sub serialbalanza_Click()
    foposba.Enabled = False
    fserialba.Enabled = True
End Sub

Private Sub noscanner_Click()
    foposs.Enabled = False
    fserials.Enabled = False
End Sub

Private Sub Oposscanner_Click()
    noscanner.Value = False
    serialscanner.Value = False
    foposs.Enabled = True
    fserials.Enabled = False
End Sub

Private Sub serialscanner_Click()
    foposs.Enabled = False
    fserials.Enabled = True
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF12, 0)
        
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
            
        Case Is = UCase("Teclado")
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = oposs
            
            TecladoWindows mCtl
            
    End Select
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Cancel = 0 Then
        Unload Me
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set Entrada = Nothing
End Sub

Private Sub Objetos_OPOS(Valor As Boolean)
    Oposscanner.Enabled = Valor
    Oposbalanza.Enabled = Valor
    Oposbanda.Enabled = Valor
    oposllaves.Enabled = Valor
End Sub
