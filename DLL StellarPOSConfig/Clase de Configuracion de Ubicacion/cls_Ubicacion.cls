VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_Ubicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function ConfigUbicacion(Relacion As String, Optional ConexionP As Object, Optional StrConexionP As String, Optional isWeb As Boolean = False) As Boolean
    Dim CerrarAlSalir As Boolean
    On Error GoTo Error_Local
    CerrarAlSalir = False
    If IsMissing(ConexionP) Then
        CerrarAlSalir = True
        Set GConexion = CreateObject("ADODB.Connection")
        GConexion.ConnectionString = StrConexionP
        GConexion.open
    Else
        Set GConexion = ConexionP
    End If
    
    If Not isWeb Then
        FrmUbicacion.Show vbModal
        ConfigUbicacion = ModUbicacion.Configurado
    End If
    If CerrarAlSalir Then
        GConexion.Close
    End If
    Exit Function
Error_Local:
    If CerrarAlSalir Then
        GConexion.Close
    End If
End Function

Private Sub Class_Initialize()
    ModUbicacion.Configurado = False
    Set ModUbicacion.GConexion = Nothing
End Sub
