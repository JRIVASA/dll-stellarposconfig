VERSION 5.00
Begin VB.Form FrmUbicacion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "#"
   ClientHeight    =   8850
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12000
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   590
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   800
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   7920
      Left            =   45
      TabIndex        =   0
      Top             =   930
      Width           =   11925
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   69
         Left            =   10650
         Picture         =   "FrmUbicacion.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   70
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   68
         Left            =   9480
         Picture         =   "FrmUbicacion.frx":0442
         Style           =   1  'Graphical
         TabIndex        =   69
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   67
         Left            =   8310
         Picture         =   "FrmUbicacion.frx":0884
         Style           =   1  'Graphical
         TabIndex        =   68
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   66
         Left            =   7140
         Picture         =   "FrmUbicacion.frx":0CC6
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   65
         Left            =   5970
         Picture         =   "FrmUbicacion.frx":1108
         Style           =   1  'Graphical
         TabIndex        =   66
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   64
         Left            =   4800
         Picture         =   "FrmUbicacion.frx":154A
         Style           =   1  'Graphical
         TabIndex        =   65
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   63
         Left            =   3630
         Picture         =   "FrmUbicacion.frx":198C
         Style           =   1  'Graphical
         TabIndex        =   64
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   62
         Left            =   2460
         Picture         =   "FrmUbicacion.frx":1DCE
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   61
         Left            =   1290
         Picture         =   "FrmUbicacion.frx":2210
         Style           =   1  'Graphical
         TabIndex        =   62
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   60
         Left            =   120
         Picture         =   "FrmUbicacion.frx":2652
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   6780
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   59
         Left            =   10650
         Picture         =   "FrmUbicacion.frx":2A94
         Style           =   1  'Graphical
         TabIndex        =   60
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   58
         Left            =   9480
         Picture         =   "FrmUbicacion.frx":2ED6
         Style           =   1  'Graphical
         TabIndex        =   59
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   57
         Left            =   8310
         Picture         =   "FrmUbicacion.frx":3318
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   56
         Left            =   7140
         Picture         =   "FrmUbicacion.frx":375A
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   55
         Left            =   5970
         Picture         =   "FrmUbicacion.frx":3B9C
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   54
         Left            =   4800
         Picture         =   "FrmUbicacion.frx":3FDE
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   53
         Left            =   3630
         Picture         =   "FrmUbicacion.frx":4420
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   52
         Left            =   2460
         Picture         =   "FrmUbicacion.frx":4862
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   51
         Left            =   1290
         Picture         =   "FrmUbicacion.frx":4CA4
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   50
         Left            =   120
         Picture         =   "FrmUbicacion.frx":50E6
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   5670
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   49
         Left            =   10650
         Picture         =   "FrmUbicacion.frx":5528
         Style           =   1  'Graphical
         TabIndex        =   50
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   48
         Left            =   9480
         Picture         =   "FrmUbicacion.frx":596A
         Style           =   1  'Graphical
         TabIndex        =   49
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   47
         Left            =   8310
         Picture         =   "FrmUbicacion.frx":5DAC
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   46
         Left            =   7140
         Picture         =   "FrmUbicacion.frx":61EE
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   45
         Left            =   5970
         Picture         =   "FrmUbicacion.frx":6630
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   44
         Left            =   4800
         Picture         =   "FrmUbicacion.frx":6A72
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   43
         Left            =   3630
         Picture         =   "FrmUbicacion.frx":6EB4
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   42
         Left            =   2460
         Picture         =   "FrmUbicacion.frx":72F6
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   41
         Left            =   1290
         Picture         =   "FrmUbicacion.frx":7738
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   40
         Left            =   120
         Picture         =   "FrmUbicacion.frx":7B7A
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   4560
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   39
         Left            =   10650
         Picture         =   "FrmUbicacion.frx":7FBC
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   38
         Left            =   9480
         Picture         =   "FrmUbicacion.frx":83FE
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   37
         Left            =   8310
         Picture         =   "FrmUbicacion.frx":8840
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   36
         Left            =   7140
         Picture         =   "FrmUbicacion.frx":8C82
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   35
         Left            =   5970
         Picture         =   "FrmUbicacion.frx":90C4
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   34
         Left            =   4800
         Picture         =   "FrmUbicacion.frx":9506
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   33
         Left            =   3630
         Picture         =   "FrmUbicacion.frx":9948
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   32
         Left            =   2460
         Picture         =   "FrmUbicacion.frx":9D8A
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   31
         Left            =   1290
         Picture         =   "FrmUbicacion.frx":A1CC
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   30
         Left            =   120
         Picture         =   "FrmUbicacion.frx":A60E
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   3450
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   29
         Left            =   10650
         Picture         =   "FrmUbicacion.frx":AA50
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   28
         Left            =   9480
         Picture         =   "FrmUbicacion.frx":AE92
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   27
         Left            =   8310
         Picture         =   "FrmUbicacion.frx":B2D4
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   26
         Left            =   7140
         Picture         =   "FrmUbicacion.frx":B716
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   25
         Left            =   5970
         Picture         =   "FrmUbicacion.frx":BB58
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   24
         Left            =   4800
         Picture         =   "FrmUbicacion.frx":BF9A
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   23
         Left            =   3630
         Picture         =   "FrmUbicacion.frx":C3DC
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   22
         Left            =   2460
         Picture         =   "FrmUbicacion.frx":C81E
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   21
         Left            =   1290
         Picture         =   "FrmUbicacion.frx":CC60
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   20
         Left            =   120
         Picture         =   "FrmUbicacion.frx":D0A2
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   2355
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   19
         Left            =   10650
         Picture         =   "FrmUbicacion.frx":D4E4
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   18
         Left            =   9480
         Picture         =   "FrmUbicacion.frx":D926
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   17
         Left            =   8310
         Picture         =   "FrmUbicacion.frx":DD68
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   16
         Left            =   7140
         Picture         =   "FrmUbicacion.frx":E1AA
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   15
         Left            =   5970
         Picture         =   "FrmUbicacion.frx":E5EC
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   14
         Left            =   4800
         Picture         =   "FrmUbicacion.frx":EA2E
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   13
         Left            =   3630
         Picture         =   "FrmUbicacion.frx":EE70
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   12
         Left            =   2460
         Picture         =   "FrmUbicacion.frx":F2B2
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   11
         Left            =   1290
         Picture         =   "FrmUbicacion.frx":F6F4
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   10
         Left            =   120
         Picture         =   "FrmUbicacion.frx":FB36
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1260
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   9
         Left            =   10650
         Picture         =   "FrmUbicacion.frx":FF78
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   165
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   8
         Left            =   9480
         Picture         =   "FrmUbicacion.frx":103BA
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   165
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   7
         Left            =   8310
         Picture         =   "FrmUbicacion.frx":107FC
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   165
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   6
         Left            =   7140
         Picture         =   "FrmUbicacion.frx":10C3E
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   165
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   5
         Left            =   5970
         Picture         =   "FrmUbicacion.frx":11080
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   165
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   4
         Left            =   4800
         Picture         =   "FrmUbicacion.frx":114C2
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   165
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   3
         Left            =   3630
         Picture         =   "FrmUbicacion.frx":11904
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   165
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   2
         Left            =   2460
         Picture         =   "FrmUbicacion.frx":11D46
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   165
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   1
         Left            =   1290
         Picture         =   "FrmUbicacion.frx":12188
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   165
         Width           =   1140
      End
      Begin VB.CommandButton Ubicacion 
         CausesValidation=   0   'False
         Height          =   1080
         Index           =   0
         Left            =   120
         Picture         =   "FrmUbicacion.frx":125CA
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   165
         Width           =   1140
      End
   End
End
Attribute VB_Name = "FrmUbicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
            Select Case KeyCode
                
            End Select
        Case vbCtrlMask
            Select Case KeyCode
                
            End Select
        Case vbShiftMask
            Select Case KeyCode
                
            End Select
        Case Else
            Select Case KeyCode
                Case vbKeyReturn
                    Send_Keys Chr(vbKeyTab), True
                Case vbKeyF12
                    Unload Me
            End Select
    End Select
End Sub

