VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_Boton"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private m_posicion As Integer
Private m_texto As String
Private m_Codigo As String
Private m_CodigoNativo As String
Private m_Grupo As String
Private m_imagen As String
Private m_Mascara As String
Private m_Tecla As String
Public erroralguardar As Boolean

Dim mensaje As Object

Property Get imagen() As String
    imagen = m_imagen
End Property
Property Let imagen(dato As String)
    If dato = "" Then
        mensaje.mensaje "Debe introducir una imagen"
    Else
        m_imagen = dato
    End If
End Property
Property Get Posicion() As Integer
    'Posicion del boton
    Posicion = m_posicion
End Property
Property Let Posicion(pValor As Integer)
    If CStr(pValor) = "" Then
        mensaje.mensaje "Debe introducir un valor para la posicion", vbInformation
        
    ElseIf pValor < 1 Or pValor > 36 Then
        mensaje.mensaje "Valor fuera del intervalo la posicion debe ser entre 1 y 36", vbInformation
        
    Else
        m_posicion = pValor
    End If
End Property
Property Get texto() As String
    'Texto del nodo y texto del Boton
    texto = m_texto
End Property
Property Let texto(pdato As String)
    If pdato <> "" Then
         m_texto = pdato
    Else
    mensaje.mensaje "Debe ingresar el nombre"
    End If
End Property
Property Get codigo() As String
    'Codigo es el correlativo del grupo o producto
     codigo = m_Codigo
End Property
Property Let codigo(pdato As String)
    If pdato <> "" Then
        m_Codigo = pdato
    End If
End Property
Property Get CodigoNativo() As String
    'Codigo Nativo es el codigo del producto que viene dado por la maestro de productos
    CodigoNativo = m_CodigoNativo
End Property
Property Let CodigoNativo(pdato As String)
    If pdato <> "" Then
        m_CodigoNativo = pdato
    End If
End Property


Property Get Grupo() As String
    ' Grupo es la clave del nodo padre
    Grupo = m_Grupo
End Property
Property Let Grupo(pdato As String)
    If pdato <> "" Then
        m_Grupo = pdato
    End If
End Property
Property Get Mascara() As String
    'Texto del nodo y texto del Boton
    Mascara = m_Mascara
End Property
Property Let Mascara(pdato As String)
    If pdato <> "" And pdato <> "Ninguna" Then
         m_Mascara = pdato
    End If
End Property
Property Get Tecla() As String
    'Texto del nodo y texto del Boton
    Tecla = m_Tecla
End Property
Property Let Tecla(pdato As String)
    If pdato <> "" And pdato <> "Ninguna" Then
         m_Tecla = pdato
    End If
End Property

Public Sub guardar(pconexion As ADODB.Connection, pnomtabla As String, Optional pcodnativo As String, Optional ptipo As String)
    Dim m_rec As New ADODB.Recordset
    Dim mSQL As String
    Dim transaccion As Boolean
    
    transaccion = False
    
    mSQL = "Delete  from " & pnomtabla + " where Clave ='" & codigo + "'"
    mSQL = mSQL + " and relacion='" & Grupo + "'"
    
    pconexion.CursorLocation = adUseServer
    pconexion.BeginTrans
    transaccion = True
    On Error GoTo error
    pconexion.Execute mSQL
    
    
        If ptipo = "PRODUCTO" Then
            If pcodnativo = "" Then
                erroralguardar = True
            End If
        End If
        
        If codigo <> "" And texto <> "" And Grupo <> "" And Posicion >= 1 And Posicion <= 36 And erroralguardar = False Then
            m_rec.Open pnomtabla, pconexion, adOpenDynamic, adLockOptimistic
            m_rec.AddNew
                m_rec.Fields("Clave") = codigo
                m_rec.Fields("TEXTO") = texto
                m_rec.Fields("POSICION") = Posicion
                m_rec.Fields("relacion") = Grupo
                m_rec.Fields("imagen") = imagen
                m_rec.Fields("Mascara") = Mascara
                m_rec.Fields("Tecla") = Tecla
                m_rec.Fields("Tag") = CodigoNativo
            m_rec.Update

                
         Else
           
            mensaje.mensaje "Error al grabar, verifique si todos los campos estan llenos"
            erroralguardar = True
        End If
    pconexion.CommitTrans
    Exit Sub
error:
    If transaccion = True Then
        pconexion.RollbackTrans
    End If
End Sub


Public Sub eliminar(pconexion As ADODB.Connection, pNomTablaTemp As String, ByRef p_resp As String)
    Dim m_con As New ADODB.Connection
    Dim m_rec As New ADODB.Recordset
    Dim m_SQL1, m_SQL2 As String
       
    m_SQL1 = "Select * From " & pNomTablaTemp + " where Relacion='" & codigo + "'"
    m_SQL2 = "Select * from " & pNomTablaTemp + " where Clave='" & codigo + "'"
           
    m_rec.CursorLocation = adUseServer
    m_rec.Open m_SQL1, pconexion, adOpenStatic, adLockReadOnly
    
    If Not m_rec.EOF Then
        mensaje.mensaje "Registro no se puede eliminar, tiene botones asignados"
        p_resp = vbNo
    Else
        m_rec.Close
        m_rec.Open m_SQL2, pconexion, adOpenDynamic, adLockOptimistic
        If m_rec.EOF Then
            mensaje.mensaje "Error: no se enconctro el registro"
            p_resp = vbNo
        Else
            m_rec.Delete
            mensaje.mensaje "Registro eliminado"
            p_resp = vbYes
        End If
    End If
        
End Sub

Public Sub IniciarBotones(ByRef pbotones As Object, pconexion As ADODB.Connection, psql As String, pmin As Long, pmax As Long)
    
    Dim m_rec As New ADODB.Recordset
    Dim txt As String
    Dim indice As Integer
    Dim numcaracteres As Long
    Dim elementos() As String
    Dim str As String
    Dim Teclas_Asignadas As String
    
    
    m_rec.CursorLocation = adUseServer
    m_rec.Open psql, pconexion, adOpenKeyset, adLockReadOnly
    For i = pmin To pmax
        pbotones(i).Caption = ""
        pbotones(i).ToolTipText = ""
        pbotones(i).Tag = ""
    Next i
        Do While Not m_rec.EOF
            indice = m_rec.Fields("Posicion") - 1
            numcaracteres = Len(m_rec.Fields("Texto"))
            str = Mid(m_rec.Fields("Clave"), 1, 1)
            Select Case str
                Case Is = "R"
                    txt = "Grupo:"
                Case Is = "G"
                    txt = "Grupo:"
                Case Else
                    txt = "Producto:"
            End Select
            Select Case numcaracteres
                Case Is > 8
                    elementos() = Split(m_rec.Fields("Texto"))
                    pbotones(indice).Caption = elementos(0)
                    If m_rec.Fields("Mascara") <> "" Then
                        Teclas_Asignadas = "Tecla:" & m_rec.Fields("Mascara") & " + " & m_rec.Fields("Tecla")
                    Else
                        Teclas_Asignadas = "Tecla:" & m_rec.Fields("Tecla")
                    End If
                    pbotones(indice).ToolTipText = txt & m_rec.Fields("Texto") & " " & Teclas_Asignadas
                    pbotones(indice).Tag = m_rec.Fields("Clave")
                    If str <> "R" And str <> "G" Then
                        Call CargarImagenEnBoton(pbotones(indice), m_rec.Fields("TAG"), pconexion)
                    End If
                    m_rec.MoveNext
                Case Else
                    pbotones(indice).Caption = m_rec.Fields("Texto")
                    If m_rec.Fields("Mascara") <> "" Then
                        Teclas_Asignadas = "Tecla:" & m_rec.Fields("Mascara") & " + " & m_rec.Fields("Tecla")
                    Else
                        Teclas_Asignadas = "Tecla:" & m_rec.Fields("Tecla")
                    End If
                    pbotones(indice).ToolTipText = txt & m_rec.Fields("Texto") & " " & Teclas_Asignadas
                    pbotones(indice).Tag = m_rec.Fields("Clave")
                    If str <> "R" And str <> "G" Then
                        Call CargarImagenEnBoton(pbotones(indice), m_rec.Fields("TAG"), pconexion)
                    End If
                    m_rec.MoveNext
            End Select
        Loop
    
    m_rec.Close

End Sub
Public Function BuscarBoton(pconex As ADODB.Connection, psql As String) As Variant
    Dim elementos() As Variant
    Dim rec As New ADODB.Recordset
    Dim ncampos As Integer
    
    rec.Open psql, pconex, adOpenForwardOnly, adLockReadOnly
    ncampos = rec.Fields.Count - 1
    ReDim elementos(ncampos)
    If Not rec.EOF Then
        For i = 0 To rec.Fields.Count - 1
            elementos(i) = rec.Fields(i)
        Next i
    End If
    BuscarBoton = elementos()
    
End Function

Public Sub CargarImagenEnBoton(ByRef VBoton As CommandButton, codigo As String, Conexion As Object)
    Dim LFHand As Long, Buffers As Variant, RsProductos As New ADODB.Recordset
    On Error GoTo Falla_Local
    RsProductos.Open "select * from ma_productos where c_codigo = '" & codigo & "'", Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsProductos.EOF Then
        LFHand = FreeFile()
        If Dir("IMGTMP.TMP") <> "" Then Kill "IMGTMP.TMP"
        Open "IMGTMP.TMP" For Output Access Write As LFHand
        Buffers = RsProductos.Fields("c_fileimagen").GetChunk(RsProductos.Fields("c_fileimagen").ActualSize - 1)
        Print #LFHand, Buffers
        Close LFHand
        Set VBoton.Picture = LoadPicture("IMGTMP.TMP")
        Kill "IMGTMP.TMP"
    End If
    RsProductos.Close
    Exit Sub
Falla_Local:
    If LFHand <> 0 Then Close LFHand
    MsgBox Err.Description
    Exit Sub
End Sub

Public Sub ValidarTeclasBoton(pconex As ADODB.Connection, pnomtabla As String)
    Dim rec As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "Select * from " & pnomtabla + " where relacion='" & Grupo + "'"
    
    
    
    If Mascara <> "" And Tecla = "" Then
        erroralguardar = True
        mensaje.mensaje "Si asigna una tecla mascara,debe asignar una tecla"
    ElseIf Mascara = "Alt" And Tecla = "C" Then
        erroralguardar = True
        mensaje.mensaje "Combinacion no permitida Alt+C"
    Else
        rec.Open mSQL, pconex, adOpenForwardOnly, adLockReadOnly
        Do While Not rec.EOF
            If rec("Clave") <> codigo Then
                If rec.Fields("Mascara") = Mascara And rec.Fields("Mascara") <> "" Then
                    If rec.Fields("Tecla") = Tecla And rec.Fields("Tecla") <> "" Then
                        mensaje.mensaje "Ya existe un boton con esa combinacion de teclas"
                        erroralguardar = True
                        
                        Exit Do
                    End If
                End If
            End If
            rec.MoveNext
        Loop
        rec.Close
    End If
    
End Sub

Private Sub Class_Initialize()
    Set mensaje = CreateObject("recsun.obj_MENSAJERIA")
    
    erroralguardar = False
End Sub

Private Sub Class_Terminate()
    Set mensaje = Nothing
End Sub
