Attribute VB_Name = "OrganizarMesas"
Public Declare Function ReleaseCapture Lib "user32" () As Long
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Const SWP_DRAWFRAME As Long = &H20
Public Const SWP_NOMOVE As Long = &H2
Public Const SWP_NOSIZE As Long = &H1
Public Const SWP_NOZORDER As Long = &H4
Public Const SWP_FLAGS As Long = SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOMOVE Or SWP_DRAWFRAME
Public Const GWL_STYLE As Long = (-16)
Public Const WS_THICKFRAME As Long = &H40000
Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Private mStyle As Long
Public mObj As Control

Private Sub SetControlStyle(Style, X As Control)
    If Style Then
        Call SetWindowLong(X.hwnd, GWL_STYLE, Style)
        Call SetWindowPos(X.hwnd, X.Parent.hwnd, 0, 0, 0, 0, SWP_FLAGS)
    End If
End Sub

Public Sub DisallowResize()
    
    On Error GoTo ErrorDisallowResize
    
    If Not mObj Is Nothing Then
        SetControlStyle mStyle, mObj
        Set mObj = Nothing
    End If
    
    Exit Sub
    
ErrorDisallowResize:

    MsgBox Err & ":Error in call to DisallowResize()." _
    & vbCrLf & vbCrLf & "Error Description: " & Err.Description, vbCritical, "Warning"
    
    Exit Sub
    
End Sub

Public Sub AllowResize(ctl As Object)
    
    On Error GoTo ErrorAllowResize
    
    If Not mObj Is Nothing Then
        If mObj.Name = ctl.Name And mObj.Parent.Name = ctl.Parent.Name Then Exit Sub
        DisallowResize
    Else
        Set mObj = ctl
    End If
    
    Style = GetWindowLong(ctl.hwnd, GWL_STYLE)
    mStyle = Style
    Style = Style Or WS_THICKFRAME
    SetControlStyle Style, ctl
    ctl.ZOrder 0
    
    Exit Sub
    
ErrorAllowResize:
    
    MsgBox Err & ":Error in call to AllowResize()." _
    & vbCrLf & vbCrLf & "Error Description: " & Err.Description, vbCritical, "Warning"
    
    Exit Sub
    
End Sub
