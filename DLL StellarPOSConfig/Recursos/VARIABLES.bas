Attribute VB_Name = "Principal"
Type Dispositivos
    pNemotecnico                    As String
    Descripcion                     As String
    Puerto                          As String
    Baudios                         As String
    Paridad                         As String
    Parada                          As String
    BitDatos                        As Integer
    CarControl                      As String
    ComandoStr                      As String
    CarCorte                        As String
    Buffer                          As String
    EsPOS                           As Boolean
    NombreOPOS                      As String
    Activo                          As Boolean
    opos                            As Boolean
    UsaHojilla                      As Boolean
    ImpCheque                       As Boolean
    Rotacion                        As Integer
    AdministraGaveta                As Boolean
End Type

Public Mensajes As New recsun.OBJ_MENSAJERIA
Public DepGruSub As New recsuna.cls_Departmento_Grupo_Subrupo
Public Busqueda As New recsun.cls_datos

Public Const ProducID = 20389
Global ADispositivo() As Dispositivos

Enum TipoPos
    PosStellar = 0
    PosDigital = 1
    PosAmbos = 2
    Siempre = 3
    Nunca = -1
End Enum

Type menu
    Clave As String
    Texto As String
    Imagen As String
    Forma As String
    Tipo As TipoPos
End Type

Global EsDigital As Boolean
Global EsPedidos As Boolean
Global NodoTexto As String
Global NodoTextoKey As String
Global rsEureka As New ADODB.Recordset
Global rsBingo As New ADODB.Recordset
Global rsSubBtn As New ADODB.Recordset
Global rsMenu As New ADODB.Recordset
Global rsUsuarios As New ADODB.Recordset
Global rsCaja As New ADODB.Recordset
Global rsBotones As New ADODB.Recordset
Global rsBotones_TMP As New ADODB.Recordset
Global rsTrBotones As New ADODB.Recordset
Global rsTrBotones_TMP As New ADODB.Recordset
Global rsTrBotones_TMP1 As New ADODB.Recordset
Global Rec_Monitor As New ADODB.Recordset
Global rsProductos As New ADODB.Recordset
Global rsTrCostosInt As New ADODB.Recordset

Global Forma As Object
' Longitud del Prefijo de los Nodos en el Arbol.
Public Const LPA = 3 ' Mejor Definirla Publicamente ya que se usa en varios lugares

Global Srv_Local As String
Global Srv_Local_Login As String
Global Srv_Local_Password As String

Global fCancelar As Boolean
Global Inicio As Boolean

'Variables para la Organizaci�n

Global Sucursal As String
Global Setup As String
Global Titulo As String
Global Tabla As String
Global Campo_Op1 As String
Global Campo_Op2 As String
Global nCaja As String
Global Ubicacion As String

Global gLC_Edicion As String
Global gLC_NumPOS As String

'Variables para los �rboles **Configuraci�n de Usuarios

Global lcNode As Node
Global lcNodeMenu As Node
Global lcNodo As String
Global lcTag As String
Global lcIndex As Integer
Global lcKey As String
Global lcTexto As String
Global Clave_User As String
Global lcCount As Integer
Global Valor As String
Global SubNivel As Boolean
Global AddNode As Boolean
Global glGrabar As Boolean
Global Config_Group As Boolean
Global Copiar As Boolean
Global Clave As String

Global AutoLogin As Boolean
Global Inicio_Usuario_Login As String
Global inicio_Usuario_Contrase�a As String

'Otras Variables

Global Valores(10) As String

'Frm_mensaje

Global Retorno As Boolean

Public Enum OperatingSystemArchitecture
    [32Bits]
    [64Bits]
    [OperatingSystemArchitecture_Count]
End Enum

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Global WindowsArchitecture          As OperatingSystemArchitecture

'*************************************
'*** FUNCIONES DE LECTURA DE INI
'*************************************

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Public Function sGetIni(sIniFile As String, sSection As String, sKey _
        As String, sDefault As String) As String
    Dim sTemp As String * 256
    Dim nLength As Integer
    
    sTemp = Space$(10000)
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, 10000, sIniFile)
    sGetIni = left$(sTemp, nLength)
End Function

'*************************************
'*** FUNCIONES MAIN
'*************************************

Sub Main()

    On Error GoTo Error_main
    
    Screen.MousePointer = 11
    X = Screen.Width / 15
    Y = Screen.Height / 15
    
    If X < 800 And Y < 600 Then
       Call Mensaje(True, "Su pantalla debe tener una resoluci�n de 800x600. Configure su Monitor en el Panel de Control.")
    End If
    
    If App.PrevInstance = True Then
        Call Mensaje(True, "Una instancia del programa ya se encuentra en ejecuci�n.")
        'End
    End If
    
Reintentar:

    Setup = App.Path & "\setup.ini"
    Ubicar = Dir(Setup)
    
    If LCase(Ubicar) <> "setup.ini" Then
        Dim SetupDLL As Object 'New SB_library.Clases
        Set SetupDLL = CreateObject("SB_library.Clases")
        If SetupDLL.LoadPOSSTDConfigSetup(App.Path) Then
            Set SetupDLL = Nothing
            GoTo Reintentar
        Else
            Set SetupDLL = Nothing
            Call Mensaje(True, "El archivo de configuraci�n no se encontro o esta corrupto" & Chr(13) & "Comuniquese con Soporte T�cnico.")
            'End
        End If
    End If
    
    Srv_Local = sGetIni(Setup, "Server", "Srv_Local", "?")
    Srv_Local_Login = sGetIni(Setup, "Server", "Srv_Local_Login", "SA")
    Srv_Local_Password = sGetIni(Setup, "Server", "Srv_Local_Pwd", "")

    If Srv_Local = "?" Then
        Call Mensaje(True, "El servidor local no se encuentra configurado.")
        'End
    End If

    Sucursal = sGetIni(Setup, "Branch", "branch", "?")
    
    If Sucursal = "?" Then
        Call Mensaje(True, "La Sucursal no se encuentra configurada.")
        'End
    End If

    Ent.BDD.ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & Srv_Local_Login & ";Initial Catalog=VAD10;Data Source=" & Srv_Local & ";Password=" & Srv_Local_Password
    Ent.POS.ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & Srv_Local_Login & ";Initial Catalog=VAD20;Data Source=" & Srv_Local & ";Password=" & Srv_Local_Password
    Ent.BDD.Open
    Ent.POS.Open
    
    Screen.MousePointer = 0
    Inicio = True
    frm_Config_POS1.Show vbModal
    
Exit Sub

Error_main:
    
    Call Mensaje(True, "Se ha producido un error, la aplicacion se cerrara a continuacion.")
    'MsgBox "Se ha producido un error...! La aplicaci�n no continuar�..", vbCritical + vbOKOnly, "Mensaje Stellar"
    'End

End Sub

Public Sub Apertura_Recordset(Rec_Local As Boolean, ByRef Rec As ADODB.Recordset)
    If Rec.State = adStateOpen Then Rec.Close
    If Rec_Local Then
        Rec.CursorLocation = adUseClient
    Else
        Rec.CursorLocation = adUseServer
    End If
End Sub

Public Sub Cerrar_Recordset(Rec As ADODB.Recordset)
    If Rec.State = adStateOpen Then Rec.Close
    Set Rec = Nothing
End Sub

Public Sub Mensaje(Activo As Boolean, CadMen As String, Optional BotonCancelar = False)

    'Uno = Activo
    'frm_MENSAJERIA.mensaje.Text = Texto
    'frm_MENSAJERIA.Show vbModal
    
    On Error GoTo ErrorMensaje
    
    If Not PrimerShow Then
        PrimerShow = True
        inactivo = True
    End If
    
    If inactivo Then
        If BotonCancelar Then
            frm_Mensajeria.Cancelar.Visible = True
        Else
            frm_Mensajeria.Aceptar.left = frm_Mensajeria.Cancelar.left
            frm_Mensajeria.Cancelar.Visible = False
        End If
    
        frm_Mensajeria.Mensaje = CadMen
        frm_Mensajeria.Show vbModal
    End If
    
    Set FrmMensajes = Nothing
    
    Exit Sub
    
ErrorMensaje:
    
    'Unload FrmMensajes
    Retorno = False
    
End Sub

Public Sub Make_View(ByRef nTabla, nCampo1, nCampo2, nTitulo, Name_Form As Form, Comentario As String)
    Titulo = nTitulo
    Tabla = nTabla
    Campo_Op1 = nCampo1
    Campo_Op2 = nCampo2
    Set Forma = Name_Form
    Forma.Tag = Comentario
    VIEW_CONSULTAS.Show vbModal
End Sub

Public Function ItemCombo(Combo As ComboBox, Item As String) As Long
    
    Dim PCont As Long
    
    For PCont = 0 To Combo.ListCount - 1
        If Combo.List(PCont) = Item Then Exit For
    Next PCont
    
    If PCont < Combo.ListCount Then
        ItemCombo = PCont
    Else
        ItemCombo = 0
    End If
    
End Function

Public Function ExistenDigitales(Relacion As String) As Boolean
    
    Dim rsCajas As New ADODB.Recordset
    
    Call Apertura_Recordset(False, rsCajas)
    
    If UCase(Relacion) = "ROOT" Then
        rsCajas.Open "SELECT * FROM MA_CAJA WHERE b_pos_digital = 1", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    Else
        rsCajas.Open "SELECT * FROM MA_CAJA WHERE b_pos_digital = 1 and C_RELACION = '" & Relacion & "'", _
        Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    End If
    
    ExistenDigitales = Not rsCajas.EOF
    
    rsCajas.Close
    
End Function

Public Function CheckCaja() As Boolean
    
    Dim rsCaja As New ADODB.Recordset
    
    CheckCaja = False
    
    Call Apertura_Recordset(True, rsCaja)
    
    rsCaja.Open "SELECT * FROM MA_CAJA WHERE C_CODIGO = '" & nCaja & "'", _
    Ent.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rsCaja.EOF Then
        CheckCaja = True
        EsDigital = rsCaja!B_POS_Digital
        EsPedidos = rsCaja!b_POS_Pedido
    Else
        EsDigital = False
        EsPedidos = False
    End If
    
End Function

Public Function BuscarEnCombo(LCombo As ComboBox, Cadena As String) As Integer
    
    Dim Cont As Integer
    
    BuscarEnCombo = 0
    
    For Cont = 0 To LCombo.ListCount - 1
        If UCase(LCombo.List(Cont)) = UCase(Cadena) Then
            BuscarEnCombo = Cont
            Exit For
        End If
    Next Cont
    
End Function

Public Function Conexion() As Object
    Set Conexion = CreateObject("ADODB.connection")
End Function

Public Function RecordSets() As Object
    Set RecordSets = CreateObject("ADODB.recordset")
End Function

Public Function StellarMensaje(pResourceID As Long, _
Optional ByVal pDefaultNotFound As String = "CHK_RES") As String
    StellarMensaje = Stellar_Mensaje(pResourceID, True, pDefaultNotFound)
End Function

Public Function Stellar_Mensaje(Mensaje As Long, _
Optional pDevolver As Boolean = True, _
Optional ByVal pDefaultNotFound As String = "CHK_RES") As String
    
    On Error GoTo Error
    
    Texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = Texto
    End If
    
    Exit Function
        
Error:
    
    Stellar_Mensaje = pDefaultNotFound
    
End Function

Public Function IsValidIPAddress(ByVal strIPAddress As String) As Boolean
    On Error GoTo Handler
    Dim varAddress As Variant, n As Long, lCount As Long
    varAddress = Split(strIPAddress, ".", , vbTextCompare)
    '//
    If IsArray(varAddress) Then
        For n = LBound(varAddress) To UBound(varAddress)
            lCount = lCount + 1
            varAddress(n) = CByte(varAddress(n))
        Next
        '//
        IsValidIPAddress = (lCount = 4)
    End If
    '//
Handler:
End Function

Public Function ExisteCampoTabla(pCampo As String, Optional pRs As ADODB.Recordset, Optional pSql As String = "", Optional pValidarFecha As Boolean = False, Optional pCn) As Boolean
    Dim mAux As Variant
    Dim mRs As New ADODB.Recordset
    On Error GoTo Error
    If Not IsMissing(pRs) Then
     
        mAux = pRs.Fields(pCampo).Value
        If pValidarFecha Then
            ExisteCampoTabla = mAux <> "01/01/1900"
        Else
            ExisteCampoTabla = True
        End If
    Else
        If IsMissing(pCn) Then
            mRs.Open pSql, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        Else
            mRs.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        End If
        ExisteCampoTabla = True
        mRs.Close
    End If
    Exit Function
    
Error:
    'MsgBox Err.Description
    'Debug.Print Err.Description
    ERR.Clear
    ExisteCampoTabla = False
End Function

Public Function ExisteTabla(pNombreTabla As String, pConexion) As Boolean
    Dim rsDatos As Object
    On Error GoTo Falla_Local
    ExisteTabla = False
    Set rsDatos = CrearAdoRs
    Set rsDatos = pConexion.OpenSchema(adSchemaTables)
    If Not rsDatos.EOF Then
        Do While Not rsDatos.EOF
            
            If UCase(rsDatos!TABLE_NAME) = UCase(pNombreTabla) Then
                ExisteTabla = True
                Exit Do
            End If
            rsDatos.MoveNext
        Loop
    End If
    rsDatos.Close
Falla_Local:
End Function

Public Function CrearAdoRs() As Object
    Set CrearAdoRs = CreateObject("ADODB.recordset")
End Function

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    If OrMode Then
        PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
    Else
        PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
    End If
End Function

Public Function GetEnvironmentVariable(Expression) As String
    On Error Resume Next
    GetEnvironmentVariable = Environ$(Expression)
End Function

Public Sub TecladoWindows(Optional pControl As Object)
    
    On Error Resume Next
    
    ' Abrir el Teclado.
    
    Dim Ruta As String
    
    If WindowsArchitecture = [32Bits] Then
                    
        Ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(Ruta) Then res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
            
    ElseIf WindowsArchitecture = [64Bits] Then
    
        Ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramW6432") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(Ruta) Then
            res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
        Else
            
            Ruta = FindPath("TabTip32.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles(X86)") & "\Common Files\Microsoft Shared\Ink")
            
            If PathExists(Ruta) Then
                res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
            End If
            
        End If
        
    End If
    
    If PuedeObtenerFoco(pControl) Then pControl.SetFocus
    
End Sub

Public Function PathExists(pPath As String) As Boolean
    'On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then PathExists = True
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim POS As Long
    
    POS = InStr(1, pPath, "\")
    
    If POS <> 0 Then
        GetDirectoryRoot = Strings.left(pPath, POS - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim POS As Long
    
    POS = InStrRev(pPath, "\")
    
    If POS <> 0 Then
        GetDirParent = Strings.left(pPath, POS - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    ERR.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Strings.left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For i = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(i)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next i
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function

Public Function Rellenar_SpaceR(intValue, intDigits, car)
    '*** ESPACIOA A LA DER
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), car)
End Function

Public Function GetLines(Optional HowMany As Long = 1)
        
    Dim HowManyLines As Integer
    
    HowManyLines = ValidarNumeroIntervalo(HowMany, , 1)
        
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional HowMany As Long = 1)
        
    Dim HowManyTabs As Integer
    
    HowManyTabs = ValidarNumeroIntervalo(HowMany, , 1)
        
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant

    On Error GoTo ERR

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
ERR:

    ValidarNumeroIntervalo = pValor

End Function

Public Sub SeleccionarTexto(pControl As Object)
    On Error Resume Next
    pControl.SelStart = 0
    pControl.SelLength = Len(pControl.Text)
End Sub

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print ERR.Description
    
    Set SafeCreateObject = Nothing
    
End Function

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If ERR.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If ERR.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function isDBNull(ByVal pValue, _
Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    On Error GoTo Err_Campo
    
    Dim mRs As ADODB.Recordset: Set mRs = New ADODB.Recordset
    
    mRs.Open SQL, IIf(pCn Is Nothing, Ent.BDD, pCn), adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorBD = mRs.Fields(Campo).Value
    Else
        BuscarValorBD = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Err_Campo:
    
    'Debug.Print Err.Description
    
    ERR.Clear
    BuscarValorBD = pDefault
    
End Function
