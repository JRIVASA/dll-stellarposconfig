Attribute VB_Name = "FunClaseBoton"
Type DatosBoton             'Estructura de los datos del Boton
        Codigo As String
        CodigoNativo As String
        Posicion As Integer
        Nombre As String
        Grupo As String
        Mascara As String
        Tecla As String
        imagen As String
End Type
Public DatosBoton As DatosBoton

'Busca los datos de un boton y los asigna a la variable DatosBoton
Public Sub BuscarDatosBoton(sql As String, cadenadeconexion As String)
    Dim boton As cls_Boton
    Dim mibusqueda As Variant
    Dim CONEX As New ADODB.Connection
  
    
    Set boton = New cls_Boton
    CONEX.Open CADENA
    mibusqueda = boton.BuscarBoton(CONEX, sql)
        If Not IsEmpty(mibusqueda) Then
            For i = LBound(mibusqueda) To UBound(mibusqueda)
                If IsNull(mibusqueda(i)) Then
                    mibusqueda(i) = " "
                End If
            Next i
            DatosBoton.Grupo = mibusqueda(0)
            DatosBoton.Codigo = mibusqueda(1)
            DatosBoton.Nombre = mibusqueda(2)
            DatosBoton.imagen = mibusqueda(3)
            DatosBoton.CodigoNativo = mibusqueda(4)
            DatosBoton.Posicion = mibusqueda(5)
            If mibusqueda(8) <> "" Then
                DatosBoton.Mascara = mibusqueda(8)
            Else
                DatosBoton.Mascara = "Ninguna"
            End If
            If mibusqueda(9) <> "" Then
                DatosBoton.Tecla = mibusqueda(9)
            Else
                DatosBoton.Tecla = "Ninguna"
            End If
    End If
    Set boton = Nothing

End Sub

' Graba los datos de la estructura a la clase Boton
Public Sub GrabarBoton(cadenaconexion As String, NomTablaTemp As String, ByRef resp As Boolean)
    Dim boton As New cls_Boton
    Dim conexion As New ADODB.Connection
    Dim m_Codigo As String
    Dim Nomtabla As String
    Dim m_codnativo As String
    
    conexion.ConnectionString = cadenaconexion
    conexion.Open
        
    boton.Codigo = DatosBoton.Codigo
    boton.Grupo = DatosBoton.Grupo
    boton.Posicion = DatosBoton.Posicion
    boton.texto = DatosBoton.Nombre
    boton.Tecla = DatosBoton.Tecla
    boton.Mascara = DatosBoton.Mascara
    boton.imagen = "DPTO CERRADO"
    boton.CodigoNativo = DatosBoton.CodigoNativo
    boton.ValidarTeclasBoton conexion, NomTablaTemp
    boton.guardar conexion, NomTablaTemp
    resp = boton.erroralguardar
    
   
    
    conexion.Close

End Sub
