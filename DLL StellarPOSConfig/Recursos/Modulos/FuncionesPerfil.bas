Attribute VB_Name = "FuncionesPerfil"
Dim ErrorGrabar As Boolean          'Variable utilizada para Saber si ocurrio un error al grabar
Public g_CodigoPerfil As String     'Variable global que almacena el codigo del perfil actual


Public Sub BorrarPerfilTemp(ByRef pconexion As ADODB.Connection, pNomTabTemp As String)
    Dim sql As String
        sql = "Delete from " & pNomTabTemp
        pconexion.Execute sql
        
End Sub

Public Function VerificarPerfil(pconexion As ADODB.Connection, psql As String) As Boolean
    Dim rec As New ADODB.Recordset
    
    rec.Open psql, pconexion
    If rec.EOF Then
        VerificarPerfil = True
    Else
        VerificarPerfil = False
    End If
    rec.Close
    
End Function

' Sub rutina para Buscar un perfil creado previamente, Esta Sub rutina es llamada desde una funcion llamada BuscarPerfil que es publica
Private Sub BuscarP(ByRef pconex As ADODB.Connection, pNomTabTemp As String, ByRef pResultado As Variant)
    Dim rec_Origen As New ADODB.Recordset
    Dim rec_Temp As New ADODB.Recordset
    Dim TextoSql As String
    Dim mensaje As Object
    Dim Buscar As Object
    
    Set mensaje = CreateObject("recsun.OBJ_MENSAJERIA")
    Set Buscar = CreateObject("recsun.obj_busqueda")
        Buscar.Inicializar "Select * From MA_Config_Botones", "Busqueda de Perfil", CADENA
        Buscar.Add_Campos "Codigo", "CodigoPerfil", 2500, 0
        Buscar.Add_Campos "Descripcion", "Descripcion", 6500, 0
        Buscar.Add_CamposBusqueda "Codigo", "CodigoPerfil"
        Buscar.Add_CamposBusqueda "Descripcion", "Descripcion"
        
        pResultado = Buscar.Ejecutar
    
    If Not IsEmpty(pResultado) Then
        TextoSql = "Select * from TR_Config_Botones where CodigoPerfil='" & CStr(pResultado(0)) + "'"
        
        rec_Origen.Open TextoSql, pconex        'Abriendo recorset origen Tabla de transaccion de botones
        
        If rec_Origen.EOF Then
            mensaje.mensaje "No se cargo el perfil,por favor introduzca otra vez el Codigo"
        Else
                rec_Temp.Open pNomTabTemp, pconex, adOpenDynamic, adLockOptimistic  'Abriendo Tabla temporal para vaciado de informacion
                Do While Not rec_Origen.EOF
                    rec_Temp.AddNew
                    rec_Temp!relacion = rec_Origen!relacion
                    rec_Temp!Clave = rec_Origen!Clave
                    rec_Temp!texto = rec_Origen!texto
                    rec_Temp!imagen = rec_Origen!imagen
                    rec_Temp.Fields("tag") = rec_Origen.Fields("tag")
                    rec_Temp!Posicion = rec_Origen!Posicion
                    rec_Temp!CodigoPerfil = rec_Origen!CodigoPerfil
                    rec_Temp!Mascara = rec_Origen!Mascara
                    rec_Temp!Tecla = rec_Origen!Tecla
                    rec_Temp.Update
                    rec_Origen.MoveNext
                Loop
            rec_Temp.Close
          End If
        rec_Origen.Close
    End If
    
    Set mensaje = Nothing
    Set Buscar = Nothing
    
End Sub

' Sub que Guarda el Perfil esta funcion es llamada desde una Sub llamada GrabarPerfil
Private Sub GuardarPerfil(ByRef pconexion As ADODB.Connection, ByRef pdatos As Variant, pNomTablaBotTemp As String)
    Dim m_RecPerfil As New ADODB.Recordset
    Dim m_RecOrigen As New ADODB.Recordset
    Dim sql, SQLOrigen As String
    Dim mCodigoPerfil, mDescripcionPerfil As String
    Dim TransaccionenProceso  As Boolean
    
    TransaccionenProceso = False
    
    mCodigoPerfil = pdatos(0)
    mDescripcionPerfil = pdatos(1)
    If mDescripcionPerfil = "" Then
        mDescripcionPerfil = "Sin Descripcion Fecha:" & Format(Now, "short date")
    End If
'*************************** EMPIEZA TRANSACCION  ***************************************
    pconexion.CursorLocation = adUseServer
    pconexion.BeginTrans
    ErrorGrabar = False
    TransaccionenProceso = True
    On Error GoTo ManejadorError

'**************************** GRABANDO PERFIL EN MAESTRO DE BOTONES********************************
        sql = "Delete from MA_Config_Botones where CodigoPerfil='" & mCodigoPerfil + "'"
        pconexion.Execute sql
        m_RecPerfil.Open "Select * From MA_Config_Botones where CodigoPerfil='" & mCodigoPerfil + "'", pconexion, adOpenDynamic, adLockOptimistic
        m_RecPerfil.AddNew
            m_RecPerfil.Fields("CodigoPerfil") = mCodigoPerfil
            m_RecPerfil.Fields("Descripcion") = mDescripcionPerfil
        m_RecPerfil.Update
        m_RecPerfil.Close
    
        
    
'Abriendo Recorsets para vaciado de informacion
      sql = "Delete from TR_Config_Botones where CodigoPerfil='" & mCodigoPerfil + "'"
        pconexion.Execute sql
      
      SQLOrigen = "Select * from " & pNomTablaBotTemp + " order by id"
        m_RecOrigen.Open SQLOrigen, pconexion, adOpenDynamic, adLockOptimistic
        
      sql = "Select * from TR_Config_Botones where CodigoPerfil='" & mCodigoPerfil + "'"
        m_RecPerfil.Open sql, pconexion, adOpenDynamic, adLockOptimistic, adCmdText
    
    'Vaciando Recordset
            
                Do While Not m_RecOrigen.EOF
                    m_RecPerfil.AddNew
                        m_RecPerfil!relacion = m_RecOrigen!relacion
                        m_RecPerfil!Clave = m_RecOrigen!Clave
                        m_RecPerfil!texto = m_RecOrigen!texto
                        m_RecPerfil!imagen = m_RecOrigen!imagen
                        m_RecPerfil.Fields("tag") = m_RecOrigen.Fields("tag")
                        m_RecPerfil!Posicion = m_RecOrigen!Posicion
                        m_RecPerfil!CodigoPerfil = mCodigoPerfil
                        m_RecPerfil!Mascara = m_RecOrigen!Mascara
                        m_RecPerfil!Tecla = m_RecOrigen!Tecla
                    m_RecPerfil.Update
                    m_RecOrigen.MoveNext
                Loop
            
'******************** Cerrando Recordsets *********************
    pconexion.CommitTrans
    ErrorGrabar = False
    m_RecOrigen.Close
    m_RecPerfil.Close
    Exit Sub
ManejadorError:
    If TransaccionenProceso = True Then
        pconexion.RollbackTrans
        ErrorGrabar = True
    End If
    
End Sub
'*************************** Funciones de la Forma ************************************


'Crea un nuevo perfil, primero revisa si la tabla temporal no tiene datos, si no tiene crea nuevo perfil
' si existen datos en la tabla temporal pregunta si se van a guardar o no, Dependiendo de la respuesta graba,borra y crea o borra y crea

Public Sub NuevoPerfil(pmisdatos As Variant, pNomTabTemp As String, cadenadeconexion As String)
    Dim conexion As New ADODB.Connection
    Dim mDatosPerfil As Variant
    Dim objmensaje As Object
    
    Set objmensaje = CreateObject("recsun.obj_MENSAJERIA")
    conexion.Open cadenadeconexion
    If VerificarPerfil(conexion, pNomTabTemp) Then
        g_CodigoPerfil = micorrelativo(conexion, "cs_COD_PERFIL")
        conexion.Close
    Else
        objmensaje.mensaje "El perfil actual contiene datos desea grabar", True
            If objmensaje.PressBoton = True Then
                mDatosPerfil = pmisdatos()
                GuardarPerfil conexion, mDatosPerfil, "TR_Config_Botones_Temp"
                BorrarPerfilTemp conexion, "TR_Config_Botones_Temp"
                g_CodigoPerfil = micorrelativo(conexion, "cs_COD_PERFIL")
                
            Else
                BorrarPerfilTemp conexion, "TR_Config_Botones_Temp"
                g_CodigoPerfil = micorrelativo(conexion, "cs_COD_PERFIL")
            End If
        conexion.Close
        Set objmensaje = Nothing
    End If

End Sub

'Valida los datos antes de grabar y Utiliza la sub Guardar perfil para grabar
Public Sub GrabarPerfil(pdatos As Variant, NomTablaTemp As String, cadenadeconexion As String, ByRef pErrorGrabar As Boolean)
    Dim conexion As New ADODB.Connection
    Dim objmensaje As Object
        
        pErrorGrabar = ErrorGrabar
        Set objmensaje = CreateObject("recsun.obj_MENSAJERIA")
        conexion.Open cadenadeconexion
        If VerificarPerfil(conexion, NomTablaTemp) = False Then         'Verifica si hay datos en la tabla temporal para grabarlos en la de transaccion
            GuardarPerfil conexion, pdatos, NomTablaTemp
            If ErrorGrabar = False Then
                BorrarPerfilTemp conexion, NomTablaTemp
                objmensaje.mensaje "Perfil Guardado"
            End If
        Else
            objmensaje.mensaje "No hay un perfil para Grabar, o ya grabo el actual"
        End If
        conexion.Close
        Set objmensaje = Nothing
    
End Sub

'Funcion que devuelve los datos del perfil cargado. Utiliza la Subrutina BuscarP para buscar todos los datos del perfil y guardarlos en la tabla de perfil temporal
Public Function BuscarPerfil(pNombreTablaTemp As String, CadenadeConex As String, pdatos As Variant) As Variant
    Dim miconexion As New ADODB.Connection
    Dim mibusqueda As Variant
    Dim objmens As Object
        
        Set objmens = CreateObject("recsun.obj_Mensajeria")
        miconexion.Open CadenadeConex
        
        If VerificarPerfil(miconexion, pNombreTablaTemp) Then
            BuscarP miconexion, pNombreTablaTemp, mibusqueda
        Else
            objmens.mensaje "El perfil actual contiene datos desea grabar", True
                If objmens.PressBoton = True Then
                    GuardarPerfil miconexion, pdatos, pNombreTablaTemp
                    BorrarPerfilTemp miconexion, pNombreTablaTemp
                    BuscarP miconexion, pNombreTablaTemp, mibusqueda
                Else
                    BorrarPerfilTemp miconexion, pNombreTablaTemp
                    BuscarP miconexion, pNombreTablaTemp, mibusqueda
                End If
        End If
        BuscarPerfil = mibusqueda

        Set objmens = Nothing
        miconexion.Close
End Function
