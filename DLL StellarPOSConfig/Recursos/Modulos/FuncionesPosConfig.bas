Attribute VB_Name = "FuncionesPosConfig"

' Funcion que proporciona la cadena de conexion para la conexion
Function CADENA() As String
    CADENA = Ent.POS.ConnectionString
End Function
' Sub que dibuja el arbol toma los datos de la tabla temporal
Public Sub DibujarArbol(ptv As TreeView, pcadenaconexion As String, ptextoSQL As String)
    Dim m_rs As New ADODB.Recordset
    Dim m_conex As New ADODB.Connection
    Dim obj As Object
    Set obj = CreateObject("recsun.obj_treeview")
    
    m_conex.ConnectionString = pcadenaconexion
    m_conex.Open
    m_rs.CursorLocation = adUseServer
    m_rs.Open ptextoSQL, m_conex, adOpenKeyset, adLockReadOnly
    obj.crearRaiztw ptv, "RAIZ", "BOTONES", "DPTO CERRADO"
    
        While Not m_rs.EOF
            obj.AdicionarTw ptv, m_rs!relacion, m_rs!CLAVE, m_rs!texto, m_rs!imagen, m_rs!imagen
            m_rs.MoveNext
        Wend
        
        m_rs.Close
        m_conex.Close
        Set obj = Nothing
End Sub

' Crea un nuevo correlativo para un Grupo, producto o Perfil
Public Function micorrelativo(pconexion As ADODB.Connection, pcampo As String, Optional psumar As Boolean = True) As String
    Dim m_Obj_correlativo As Object
    Dim m_correlativo As String
    
    Set m_Obj_correlativo = CreateObject("recsun.cls_datos")
    m_correlativo = m_Obj_correlativo.NO_CONSECUTIVO(pconexion, pcampo, psumar)
    micorrelativo = m_correlativo
    
End Function

'Llena los combos de Asignacion de Teclas
Public Sub LlenarCombos(ByRef pcbo_mascara As ComboBox, ByRef pcbo_Tecla As ComboBox)
    Dim i As Integer
    
    With pcbo_mascara
        .AddItem "Ninguna"
        .AddItem "Ctrl"
        .AddItem "Alt"
        .AddItem "Shift"
        .ListIndex = 0
    End With
    With pcbo_Tecla
        .AddItem "Ninguna"
        For i = 65 To 90
            .AddItem Chr$(i)
        Next i
        .ListIndex = 0
    End With
    
End Sub
