VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_Boton"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private m_posicion As Integer
Private m_texto As String
Private m_Codigo As String
Private m_CodigoNativo As String
Private m_Grupo As String
Private m_imagen As String
Private m_Mascara As String
Private m_Tecla As String
Private m_Perfil As String
Public erroralguardar As Boolean

Dim mensaje As Object

Property Get Imagen() As String
    Imagen = m_imagen
End Property
Property Let Imagen(dato As String)
    If dato = "" Then
        mensaje.mensaje "Debe introducir una imagen"
    Else
        m_imagen = dato
    End If
End Property
Property Get Posicion() As Integer
    'Posicion del boton
    Posicion = m_posicion
End Property
Property Let Posicion(pValor As Integer)
    If CStr(pValor) = "" Then
        mensaje.mensaje "Debe introducir un valor para la posicion", vbInformation
        
    ElseIf pValor < 1 Or pValor > 36 Then
        mensaje.mensaje "Valor fuera del intervalo la posicion debe ser entre 1 y 36", vbInformation
        
    Else
        m_posicion = pValor
    End If
End Property
Property Get Texto() As String
    'Texto del nodo y texto del Boton
    Texto = m_texto
End Property
Property Let Texto(pdato As String)
    If pdato <> "" Then
         m_texto = pdato
    Else
    mensaje.mensaje "Debe ingresar el nombre"
    End If
End Property
Property Get Codigo() As String
    'Codigo es el correlativo del grupo o producto
     Codigo = m_Codigo
End Property
Property Let Codigo(pdato As String)
    If pdato <> "" Then
        m_Codigo = pdato
    End If
End Property
Property Get CodigoNativo() As String
    'Codigo Nativo es el codigo del producto que viene dado por la maestro de productos
    CodigoNativo = m_CodigoNativo
End Property
Property Let CodigoNativo(pdato As String)
    If pdato <> "" Then
        m_CodigoNativo = pdato
    End If
End Property

Property Get Grupo() As String
    ' Grupo es la clave del nodo padre
    Grupo = m_Grupo
End Property
Property Let Grupo(pdato As String)
    If pdato <> "" Then
        m_Grupo = pdato
    End If
End Property
Property Get Mascara() As String
    'Texto del nodo y texto del Boton
    Mascara = m_Mascara
End Property
Property Let Mascara(pdato As String)
    If pdato <> "" And pdato <> "Ninguna" Then
         m_Mascara = pdato
    End If
End Property
Property Get Tecla() As String
    'Texto del nodo y texto del Boton
    Tecla = m_Tecla
End Property
Property Let Tecla(pdato As String)
    If pdato <> "" And pdato <> "Ninguna" Then
         m_Tecla = pdato
    End If
End Property

Property Get perfil() As String
    'Texto del nodo y texto del Boton
    perfil = m_Perfil
End Property

Property Let perfil(pdato As String)
    If pdato <> "" And pdato <> "Ninguna" Then
         m_Perfil = pdato
    End If
End Property

Public Sub Guardar(PConexion As ADODB.Connection, pNomTabla As String, _
Optional pCodNativo As String, Optional pTipo As String, Optional pModificar = False)
    
    On Error GoTo Error
    
    Dim m_Rec As New ADODB.Recordset
    Dim mSQL As String
    Dim Transaccion As Boolean
    
    Transaccion = False
    
    'mSQL = "Delete  from " & pNomTabla + " where Clave ='" & codigo + "'"
    'mSQL = mSQL + " and relacion='" & Grupo + "'"
    
    PConexion.CursorLocation = adUseServer
    PConexion.BeginTrans
    Transaccion = True

    'pConexion.Execute mSQL, REG
    'Debug.Print mSQL

    If pTipo = "PRODUCTO" Then
        If pCodNativo = "" Then
            erroralguardar = True
        End If
    End If
    
    If Codigo <> "" And Texto <> "" And Grupo <> "" And Posicion >= 1 And Posicion <= 36 And erroralguardar = False Then
        If Not pModificar Then
            m_Rec.Open pNomTabla, PConexion, adOpenDynamic, adLockOptimistic
            m_Rec.AddNew
                m_Rec.Fields("CodigoPerfil") = perfil 'g_CodigoPerfil
                m_Rec.Fields("Clave") = Codigo
                m_Rec.Fields("TEXTO") = Texto
                m_Rec.Fields("POSICION") = Posicion
                m_Rec.Fields("relacion") = Grupo
                m_Rec.Fields("imagen") = Imagen
                m_Rec.Fields("Mascara") = Mascara
                m_Rec.Fields("Tecla") = Tecla
                m_Rec.Fields("Tag") = CodigoNativo
            m_Rec.Update
        Else
            m_Rec.Open "SELECT * FROM " & pNomTabla & " WHERE CodigoPerfil = '" & perfil & "' AND Clave = '" & Codigo & "'", PConexion, adOpenDynamic, adLockBatchOptimistic
            m_Rec.Update
                m_Rec.Fields("CodigoPerfil") = perfil 'g_CodigoPerfil
                m_Rec.Fields("Clave") = Codigo
                m_Rec.Fields("TEXTO") = Texto
                m_Rec.Fields("POSICION") = Posicion
                m_Rec.Fields("relacion") = Grupo
                m_Rec.Fields("imagen") = Imagen
                m_Rec.Fields("Mascara") = Mascara
                m_Rec.Fields("Tecla") = Tecla
                m_Rec.Fields("Tag") = CodigoNativo
            m_Rec.UpdateBatch
        End If
    Else
        mensaje.mensaje "Error al grabar, verifique si todos los campos estan llenos"
        erroralguardar = True
    End If
    
    PConexion.CommitTrans
    
    Exit Sub
    
Error:

    If Transaccion = True Then
        PConexion.RollbackTrans
    End If
    
    MsgBox ERR.Description
    
End Sub


Public Sub eliminar(PConexion As ADODB.Connection, pNomTablaTemp As String, ByRef p_resp As String)
    Dim m_con As New ADODB.Connection
    Dim m_Rec As New ADODB.Recordset
    Dim m_SQL1, m_SQL2 As String
       
    m_SQL1 = "Select * From " & pNomTablaTemp + " where Relacion='" & Codigo + "'"
    m_SQL2 = "Select * from " & pNomTablaTemp + " where Clave='" & Codigo + "'"
           
    m_Rec.CursorLocation = adUseServer
    m_Rec.Open m_SQL1, PConexion, adOpenStatic, adLockReadOnly
    
    If Not m_Rec.EOF Then
        mensaje.mensaje "Registro no se puede eliminar, tiene botones asignados"
        p_resp = vbNo
    Else
        m_Rec.Close
        m_Rec.Open m_SQL2, PConexion, adOpenDynamic, adLockOptimistic
        If m_Rec.EOF Then
            mensaje.mensaje "Error: no se enconctro el registro"
            p_resp = vbNo
        Else
            m_Rec.Delete
            mensaje.mensaje "Registro eliminado"
            p_resp = vbYes
        End If
    End If
        
End Sub

Public Sub IniciarBotones(ByRef pbotones As Object, PConexion As ADODB.Connection, psql As String, pmin As Long, pmax As Long)
    
    Dim m_Rec As New ADODB.Recordset
    Dim txt As String
    Dim Indice As Integer
    Dim numcaracteres As Long
    Dim elementos() As String
    Dim str As String
    Dim Teclas_Asignadas As String
    
    m_Rec.CursorLocation = adUseServer
    m_Rec.Open psql, PConexion, adOpenKeyset, adLockReadOnly
    
    For i = pmin To pmax
        pbotones(i).Caption = ""
        pbotones(i).ToolTipText = ""
        pbotones(i).Tag = ""
        pbotones(i).Picture = LoadPicture("")
    Next i
    
    Do While Not m_Rec.EOF
        Indice = m_Rec.Fields("Posicion") - 1
        numcaracteres = Len(m_Rec.Fields("Texto"))
        str = Mid(m_Rec.Fields("Clave"), 1, 1)
        Select Case str
            Case Is = "R"
                txt = "Grupo:"
            Case Is = "G"
                txt = "Grupo:"
            Case Else
                txt = "Producto:"
        End Select
        Select Case numcaracteres
            Case Is > 10
                elementos() = Split(m_Rec.Fields("Texto"))
                pbotones(Indice).Caption = Strings.Left(m_Rec!Texto, 40) 'elementos(0)
                If m_Rec.Fields("Mascara") <> "" Then
                    Teclas_Asignadas = "Tecla:" & m_Rec.Fields("Mascara") & " + " & m_Rec.Fields("Tecla")
                Else
                    Teclas_Asignadas = "Tecla:" & m_Rec.Fields("Tecla")
                End If
                pbotones(Indice).ToolTipText = txt & m_Rec.Fields("Texto") & " " & Teclas_Asignadas
                pbotones(Indice).Tag = m_Rec.Fields("Clave")
                If str <> "R" And str <> "G" Then
                    Call CargarImagenEnBoton(pbotones(Indice), m_Rec.Fields("TAG"), PConexion)
                End If
                m_Rec.MoveNext
            Case Else
                pbotones(Indice).Caption = m_Rec.Fields("Texto")
                If m_Rec.Fields("Mascara") <> "" Then
                    Teclas_Asignadas = "Tecla:" & m_Rec.Fields("Mascara") & " + " & m_Rec.Fields("Tecla")
                Else
                    Teclas_Asignadas = "Tecla:" & m_Rec.Fields("Tecla")
                End If
                pbotones(Indice).ToolTipText = txt & m_Rec.Fields("Texto") & " " & Teclas_Asignadas
                pbotones(Indice).Tag = m_Rec.Fields("Clave")
                If str <> "R" And str <> "G" Then
                    Call CargarImagenEnBoton(pbotones(Indice), m_Rec.Fields("TAG"), PConexion)
                End If
                m_Rec.MoveNext
        End Select
    Loop
    
    m_Rec.Close

End Sub

Public Function BuscarBoton(pconex As ADODB.Connection, psql As String) As Variant
    Dim elementos() As Variant
    Dim Rec As New ADODB.Recordset
    Dim ncampos As Integer
    
    Rec.Open psql, pconex, adOpenForwardOnly, adLockReadOnly
    ncampos = Rec.Fields.Count - 1
    ReDim elementos(ncampos)
    
    If Not Rec.EOF Then
        elementos(0) = Rec!relacion
        elementos(1) = Rec!clave
        elementos(2) = Rec!Texto
        elementos(3) = Rec!Imagen
        elementos(4) = Rec!Tag
        elementos(5) = Rec!Posicion
        elementos(6) = Rec!id
        elementos(7) = Rec!codigoperfil
        elementos(8) = Rec!Mascara
        elementos(9) = Rec!Tecla
        
'        For i = 0 To rec.Fields.Count - 1
'            elementos(i) = rec.Fields(i)
'        Next i
    End If
    BuscarBoton = elementos()
    
End Function

Public Sub CargarImagenEnBoton(ByRef VBoton As CommandButton, Codigo As String, Conexion As Object)
    Dim LFHand As Long, Buffers As Variant, RsProductos As New ADODB.Recordset
    On Error GoTo Falla_Local
    RsProductos.Open "select * from ma_productos where c_codigo = '" & Codigo & "'", Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not RsProductos.EOF Then
        LFHand = FreeFile()
        If Dir("IMGTMP.TMP") <> "" Then Kill "IMGTMP.TMP"
        Open "IMGTMP.TMP" For Output Access Write As LFHand
        Buffers = RsProductos.Fields("c_fileimagen").GetChunk(RsProductos.Fields("c_fileimagen").ActualSize - 1)
        Print #LFHand, Buffers
        Close LFHand
        Set VBoton.Picture = LoadPicture("IMGTMP.TMP")
        Kill "IMGTMP.TMP"
    End If
    RsProductos.Close
    Exit Sub
Falla_Local:
    If LFHand <> 0 Then Close LFHand
    Exit Sub
End Sub

Public Sub ValidarTeclasBoton(pconex As ADODB.Connection, pNomTabla As String)
    Dim Rec As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "Select * from " & pNomTabla + " where relacion='" & Grupo + "'"
    
    If Mascara <> "" And Tecla = "" Then
        erroralguardar = True
        mensaje.mensaje "Si asigna una tecla mascara,debe asignar una tecla"
    ElseIf Mascara = "Alt" And Tecla = "C" Then
        erroralguardar = True
        mensaje.mensaje "Combinacion no permitida Alt+C"
    Else
        Rec.Open mSQL, pconex, adOpenForwardOnly, adLockReadOnly
        Do While Not Rec.EOF
            If Rec("Clave") <> Codigo Then
                If Rec.Fields("Mascara") = Mascara And Rec.Fields("Mascara") <> "" Then
                    If Rec.Fields("Tecla") = Tecla And Rec.Fields("Tecla") <> "" Then
                        mensaje.mensaje "Ya existe un boton con esa combinacion de teclas"
                        erroralguardar = True
                        
                        Exit Do
                    End If
                End If
            End If
            Rec.MoveNext
        Loop
        Rec.Close
    End If
    
End Sub

Private Sub Class_Initialize()
    Set mensaje = CreateObject("recsun.obj_MENSAJERIA")
    
    erroralguardar = False
End Sub

Private Sub Class_Terminate()
    Set mensaje = Nothing
End Sub
