VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_BtDigitales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Sub ConfigurarBotonesDigitales(Optional PConexion, Optional SConexion As String = "", Optional isWeb As Boolean = False)
    
    Main
        
    Dim CerrarAlSalir As Boolean
    On Error GoTo Falla_Iniciar
    CerrarAlSalir = False
    If IsMissing(PConexion) Then
        'LA VARIABLE NO FUE TRANSFERIA
        CerrarAlSalir = True
        Variables.PosConexion.ConnectionString = SConexion
        Variables.SPosConexion = SConexion
        Variables.PosConexion.Open
    Else
        'SE TRANSFIRIO, PERO NO SE SABE AUN SI ESTA ABIERTA
        If PConexion.State = adStateOpen Then 'LA CONEXION ESTA ABIERTA
            Set Variables.PosConexion = PConexion
            Variables.SPosConexion = PConexion.ConnectionString
        Else 'LA CONEXION TIENE OTRO ESTATUS
            CerrarAlSalir = True
            If PConexion.State <> adStateClosed Then PConexion.Close
            Variables.SPosConexion = SConexion
            Variables.PosConexion.Open
        End If
    End If
    If Not isWeb Then
        Frm_Perfiles.Show vbModal
    End If
    If CerrarAlSalir Then
        Variables.PosConexion.Close
    End If
    Exit Sub
Falla_Iniciar:
    If CerrarAlSalir Then
        Variables.PosConexion.Close
    End If
    Exit Sub
End Sub

Public Function ConfigCajaDigital(NCaja As String, Optional PConexion, Optional SConexion As String = "", Optional isWeb As Boolean = False) As Boolean
    
    Main
    
    Dim CerrarAlSalir As Boolean
    On Error GoTo Falla_Iniciar
    ConfigCajaDigital = False
    CerrarAlSalir = False
    If IsMissing(PConexion) Then
        'LA VARIABLE NO FUE TRANSFERIA
        CerrarAlSalir = True
        Variables.PosConexion.ConnectionString = SConexion
        Variables.SPosConexion = SConexion
        Variables.PosConexion.Open
    Else
        'SE TRANSFIRIO, PERO NO SE SABE AUN SI ESTA ABIERTA
        If PConexion.State = adStateOpen Then 'LA CONEXION ESTA ABIERTA
            Set Variables.PosConexion = PConexion
            Variables.SPosConexion = PConexion.ConnectionString
        Else 'LA CONEXION TIENE OTRO ESTATUS
            CerrarAlSalir = True
            If PConexion.State <> adStateClosed Then
                Variables.SPosConexion = PConexion.ConnectionString
                Variables.PosConexion.Open
            Else
                'TOMAR SCONEXION PARA ABRIR
                If Trim(SConexion) = "" Then GoTo Falla_Iniciar
                Variables.SPosConexion = SConexion
                Variables.PosConexion.Open
            End If
        End If
    End If
    If Not isWeb Then
        Set ModConfigDigitalCaja.Mensajes = CreateObject("recsun.OBJ_MENSAJERIA")
        Set ModConfigDigitalCaja.Arboles = CreateObject("recsun.obj_treeview")
        Variables.NCaja = NCaja
        FrmConfigDigitalCaja.Show vbModal
        Set ModConfigDigitalCaja.Mensajes = Nothing
        Set ModConfigDigitalCaja.Arboles = Nothing
    End If
    If CerrarAlSalir Then
        Variables.PosConexion.Close
    End If
    ConfigCajaDigital = Variables.StatusConfig
    Exit Function
Falla_Iniciar:
    ConfigCajaDigital = Variables.StatusConfig
    If CerrarAlSalir Then
        PConexion.Close
    End If
    Exit Function
End Function

Private Sub Class_Initialize()
    Variables.NCaja = ""
    Variables.SPosConexion = ""
    Set Variables.PosConexion = Nothing
End Sub
