Attribute VB_Name = "ModConfigDigitalCaja"
Public NPerfil              As String
Public Mensajes             As Object
Public Arboles              As Object
Public NodoPadre            As Node
Public NodoActual           As Node
Public Consecutivo          As String
Public ArregloNodos         As Variant
Public QueNivel             As String 'P = Producto, G = Grupo, R = Ra�z Cualquier otra cosa
Public Accion               As Byte '0 = Agregar 1 = Modificar
Public Indice               As Integer

Public SalidaForzada        As Boolean

Public Enum OperatingSystemArchitecture
    [32Bits]
    [64Bits]
    [OperatingSystemArchitecture_Count]
End Enum

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Public Sub Main()
    
    SalidaForzada = False
    
    TmpVal = GetEnvironmentVariable("ProgramW6432")
    
    Select Case Len(Trim(TmpVal))
        Case 0 ' Default / No Especificado.
            WindowsArchitecture = [32Bits]
        Case Else
            WindowsArchitecture = [64Bits]
    End Select
    
End Sub

Public Sub Cancelar(Forma As Object, Optional CargarTmp As Boolean = True)
    
    Dim BCont As Integer
    
    For BCont = 0 To Forma.cmd_Botones.Count - 1
        Forma.cmd_Botones(BCont).Caption = ""
        Forma.cmd_Botones(BCont).Picture = LoadPicture("")
        Forma.cmd_Botones(BCont).Tag = ""
        Forma.cmd_Botones(BCont).ToolTipText = ""
    Next BCont
    
    Variables.PosConexion.Execute "delete from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where codigoperfil = '" & nCaja & "'"
    
    'Arboles.crearRaiztw Forma.tvw_Arbol, "RAIZ", "BOTONES", "DPTO CERRADO"
    If CargarTmp Then
        Call CopiarPerfil(nCaja, Forma, PosConexion)
        Forma.tvw_Arbol.Nodes.Clear
        Call CargarDatos(Forma)
        Forma.tvw_Arbol.Nodes(1).Selected = True
        Set NodoActual = Forma.tvw_Arbol.SelectedItem
        Set NodoPadre = NodoActual.Parent
        Call BotonesToolBar(Forma.tlb_Config)
    End If
    
End Sub

Public Function Correlativos(PConexion As ADODB.Connection, pcampo As String, Optional psumar As Boolean = True) As String
    
    On Error GoTo Error
    
    Dim m_Obj_correlativo As Object
    
    Set m_Obj_correlativo = CreateObject("recsun.cls_datos")
    
    Correlativos = m_Obj_correlativo.NO_CONSECUTIVO(IDProducto, PConexion, pcampo, psumar)
    
    Exit Function
    
Error:
    
    MsgBox "Ha ocurrido un error con los correlativos. El configurador de botones se cerrar� a continuaci�n."
    
    ForcedExit
    
End Function

Public Sub BotonesToolBar(TBar As Object, Optional BBuscar As Boolean = True, Optional BCancelar As Boolean = True, Optional BGrabar As Boolean = False, Optional BSalir As Boolean = True, Optional BAyuda As Boolean = True)
    TBar.Buttons("buscar").Enabled = BBuscar
    TBar.Buttons("cancelar").Enabled = BCancelar
    TBar.Buttons("grabar").Enabled = BGrabar
    TBar.Buttons("salir").Enabled = BSalir
    TBar.Buttons("ayuda").Enabled = BAyuda
End Sub

Public Sub GrabarEstructura(PConexion As ADODB.Connection, Forma As Object)
    
    Dim RsTemp As New ADODB.Recordset, MARsBotonesCaja As New ADODB.Recordset, TRRsBotonesCaja As New ADODB.Recordset
    
    Call Apertura_RecordSet(RsTemp)
    Call Apertura_RecordSet(MARsBotonesCaja)
    Call Apertura_RecordSet(TRRsBotonesCaja)
    
    RsTemp.Open "select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where codigoperfil = '" & nCaja & "'", PConexion, adOpenForwardOnly, adLockPessimistic, adCmdText
    
    If Not RsTemp.EOF Then
        PConexion.BeginTrans
        
        'ACTUALIZACION DE LA MAESTRA DE BOTONES DE CAJA DIGITAL
        MARsBotonesCaja.Open "select * from ma_caja_botones_digitales where caja = '" & nCaja & "'", PConexion, adOpenForwardOnly, adLockBatchOptimistic
        If MARsBotonesCaja.EOF Then MARsBotonesCaja.AddNew
        MARsBotonesCaja!Caja = nCaja
        MARsBotonesCaja!codigoperfil = ModConfigDigitalCaja.NPerfil
        MARsBotonesCaja!fijar = (Forma.chk_fijar = 1)
        MARsBotonesCaja.UpdateBatch
        
        'ACTUALIZACION DE LA TRANSACCIONAL DE BOTONES DE CAJA DIGITAL
        PosConexion.Execute "delete from tr_caja_botones_digitales where caja = '" & nCaja & "'"
        TRRsBotonesCaja.Open "select * from tr_caja_botones_digitales where caja = '" & nCaja & "'", PConexion, adOpenForwardOnly, adLockBatchOptimistic
        While Not RsTemp.EOF
            TRRsBotonesCaja.AddNew
            TRRsBotonesCaja!Caja = nCaja
            TRRsBotonesCaja!Relacion = RsTemp!Relacion
            TRRsBotonesCaja!Clave = RsTemp!Clave
            TRRsBotonesCaja!Texto = RsTemp!Texto
            TRRsBotonesCaja!Tag = RsTemp!Tag
            TRRsBotonesCaja!Imagen = RsTemp!Imagen
            TRRsBotonesCaja!Posicion = RsTemp!Posicion
            TRRsBotonesCaja!Mascara = RsTemp!Mascara
            TRRsBotonesCaja!Tecla = RsTemp!Tecla
            TRRsBotonesCaja.UpdateBatch
            RsTemp.MoveNext
        Wend
        TRRsBotonesCaja.UpdateBatch
        'PosConexion.Execute "delete from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where codigoperfil = '" & NCaja & "'"
        PosConexion.Execute "delete from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
        PConexion.CommitTrans
        Call Cancelar(Forma)
    End If
    
    RsTemp.Close
    
End Sub

Public Function isNothing(Variable As Variant) As Boolean
    isNothing = Variable Is Nothing
End Function

Public Sub Modulo_Modificar(Forma As Object)
    Dim SQL As String
       
    SQL = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where Clave='" & ArregloNodos(2) + "'"
    BuscarDatosBoton SQL, Cadena
    Forma.txt_Codigo = DatosBoton.Codigo
    Forma.txt_Nombre = DatosBoton.Nombre
    Forma.txt_Posicion = DatosBoton.Posicion
    If DatosBoton.Mascara <> "" Then
        For I = 0 To Forma.cbo_Mascara.ListCount - 1
            Forma.cbo_Mascara.ListIndex = I
            If Forma.cbo_Mascara.Text = DatosBoton.Mascara Then
                Exit For
            End If
        Next I
    Else
        Forma.cbo_Mascara.ListIndex = 0
    End If
    If DatosBoton.Tecla <> "" Then
        For I = 0 To Forma.cbo_tecla.ListCount - 1
            Forma.cbo_tecla.ListIndex = I
            If Forma.cbo_tecla.Text = DatosBoton.Tecla Then
                Exit For
            End If
        Next I
    Else
        Forma.cbo_tecla.ListIndex = 0
    End If
         
End Sub

Public Sub Apertura_RecordSet(ByRef Rec As ADODB.Recordset, Optional LCursor As CursorLocationEnum = adUseServer)
    If Rec.State = adStateOpen Then Rec.Close
    Rec.CursorLocation = LCursor
End Sub

Public Sub CopiarPerfil(nCaja As String, Forma As Object, PConexion As ADODB.Connection)
    Dim RsMaCajaBotones As New ADODB.Recordset, RsTrCajaBotones As New ADODB.Recordset, RsTemp As New ADODB.Recordset
    Call Apertura_RecordSet(RsMaCajaBotones)
    Call Apertura_RecordSet(RsTrCajaBotones)
    Call Apertura_RecordSet(RsTemp)
    RsMaCajaBotones.Open "select * from ma_caja_botones_digitales where caja = '" & nCaja & "'", PConexion, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    If Not RsMaCajaBotones.EOF Then
        RsTrCajaBotones.Open "select * from tr_caja_botones_digitales where caja = '" & nCaja & "'", PConexion, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
        PConexion.Execute "delete from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where codigoperfil = '" & nCaja & "'"
        RsTemp.Open "select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where codigoperfil = '" & nCaja & "'", PConexion, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
        PConexion.BeginTrans
        While Not RsTrCajaBotones.EOF
            RsTemp.AddNew
            RsTemp!codigoperfil = nCaja
            RsTemp!Relacion = RsTrCajaBotones!Relacion
            RsTemp!Clave = RsTrCajaBotones!Clave
            RsTemp!Texto = RsTrCajaBotones!Texto
            RsTemp!Imagen = RsTrCajaBotones!Imagen
            RsTemp!Tag = RsTrCajaBotones!Tag
            RsTemp!Posicion = RsTrCajaBotones!Posicion
            RsTemp!Mascara = RsTrCajaBotones!Mascara
            RsTemp!Tecla = RsTrCajaBotones!Tecla
            RsTemp.UpdateBatch
            RsTrCajaBotones.MoveNext
        Wend
        RsTrCajaBotones.Close
        RsTemp.Close
        PConexion.CommitTrans
    End If
    RsMaCajaBotones.Close
End Sub

Public Sub CargarDatos(Forma As Object, Optional Cargar As Boolean = True)
    
    Dim boton As cls_Boton
    Dim Min As Long, Max As Long
    Dim m_SQL As String
    
    If Cargar Then
        'RECONTRUYE EL ARBOL
        m_SQL = "SELECT  * FROM TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP ORDER BY id"
        Forma.tvw_Arbol.Nodes.Clear
        DibujarArbol Forma.tvw_Arbol, Cadena, m_SQL
    End If
    
    Forma.tvw_Arbol.Nodes(1).Selected = True

    'MUESTRA LOS BOTONES
    Min = Forma.cmd_Botones.LBound
    Max = Forma.cmd_Botones.UBound
    
    Set boton = New cls_Boton
    ArregloNodos = Arboles.TomarDataTw(Forma.tvw_Arbol, Forma.tvw_Arbol.SelectedItem.Index)
    
    m_SQL = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where relacion='" & CStr(ArregloNodos(2)) + "'"
    boton.IniciarBotones Forma.cmd_Botones, PosConexion, m_SQL, Min, Max
    
    Set boton = Nothing

End Sub

Public Sub RedibujarDatos(Forma As Object)
    
    Dim boton As cls_Boton
    Dim Min As Long, Max As Long
    Dim m_SQL As String
    
    'MUESTRA LOS BOTONES
    Min = Forma.cmd_Botones.LBound
    Max = Forma.cmd_Botones.UBound
    
    Set boton = New cls_Boton
    ArregloNodos = Arboles.TomarDataTw(Forma.tvw_Arbol, Forma.tvw_Arbol.SelectedItem.Index)
    
    m_SQL = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where relacion='" & CStr(ArregloNodos(2)) + "'"
    boton.IniciarBotones Forma.cmd_Botones, PosConexion, m_SQL, Min, Max
    
    Set boton = Nothing

End Sub

Public Sub ForcedExit()
    On Error GoTo Finish
    SalidaForzada = True
    Dim PrevForm As Object
    While (Not Screen.ActiveForm Is Nothing)
        If Not (PrevForm Is Screen.ActiveForm) Then
            Set PrevForm = Screen.ActiveForm
            DoEvents
            Unload Screen.ActiveForm
        Else
            GoTo Finish
        End If
    Wend
Finish:
End Sub

Public Sub Mensaje(Activo As Boolean, CadMen As String, Optional BotonCancelar = False)

    'Uno = Activo
    'frm_MENSAJERIA.Mensaje.Text = Texto
    'frm_MENSAJERIA.Show vbModal
    
    On Error GoTo ErrorMensaje
    
    If Not PrimerShow Then
        PrimerShow = True
        inactivo = True
    End If
    
    If inactivo Then
        If BotonCancelar Then
            frm_Mensajeria.Cancelar.Visible = True
        Else
            frm_Mensajeria.Aceptar.Left = frm_Mensajeria.Cancelar.Left
            frm_Mensajeria.Cancelar.Visible = False
        End If
    
        frm_Mensajeria.Mensaje = CadMen
        frm_Mensajeria.Show vbModal
    End If
    
    Set FrmMensajes = Nothing
    
    Exit Sub
    
ErrorMensaje:
    
    'Unload FrmMensajes
    Retorno = False
    
End Sub

Public Function Stellar_Mensaje(Mensaje As Long, Optional pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    Texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = Texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje = "CHK_RES"
    
End Function

Public Function StellarMensaje(pResourceID As Long, Optional Devolver As Boolean = True) As String
    StellarMensaje = Stellar_Mensaje(pResourceID, True)
End Function

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    'Si la pantalla es igual a 1024x768px, se ajusta el alto del form para cubrir la pantalla.
    'Las medidas est�n en twips.
    If Screen.Height = "11520" And Forma.Height = 10920 Then 'And Screen.Width = "15360" Then
        Forma.Height = Screen.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
        Forma.Top = 0
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
        If Forma.Left < 0 Then Forma.Left = 0
        'El form navegador tiene un footer que siempre debe quedar en bottom.
        If GetTaskBarHeight = 0 And EsFormNavegador Then
            Forma.Frame1.Top = (Screen.Height - Forma.Frame1.Height)
        End If
    Else
        'Si no es la resoluci�n m�nima de Stellar, se centra el form.
        Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
    End If
End Sub

Public Function MsjErrorRapido(pDescripcion As String, Optional pMsjIntro As String = "[StellarMensaje]")
    
    If pMsjIntro = "[StellarMensaje]" Then
        pMsjIntro = Replace(pMsjIntro, "[StellarMensaje]", StellarMensaje(286)) & GetLines(2) '"Ha ocurrido un error, por favor reporte lo siguiente: " & vbNewLine & vbNewLine
    End If
    
    Mensaje True, pMsjIntro & pDescripcion
    
End Function

Public Function PuedeObtenerFoco(Objeto As Object) As Boolean
    If Not Objeto Is Nothing Then
        PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
    End If
End Function

Public Function GetEnvironmentVariable(Expression) As String
    On Error Resume Next
    GetEnvironmentVariable = Environ$(Expression)
End Function

Public Sub TecladoWindows(Optional pControl As Object)
    
    On Error Resume Next
    
    ' Abrir el Teclado.
    
    Dim Ruta As String
    
    If WindowsArchitecture = [32Bits] Then
                    
        Ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(Ruta) Then res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
            
    ElseIf WindowsArchitecture = [64Bits] Then
    
        Ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramW6432") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(Ruta) Then
            res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
        Else
            
            Ruta = FindPath("TabTip32.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles(X86)") & "\Common Files\Microsoft Shared\Ink")
            
            If PathExists(Ruta) Then
                res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
            End If
            
        End If
        
    End If
    
    If PuedeObtenerFoco(pControl) Then pControl.SetFocus
    
End Sub

Public Function PathExists(pPath As String) As Boolean
    'On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then PathExists = True
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim POS As Long
    
    POS = InStr(1, pPath, "\")
    
    If POS <> 0 Then
        GetDirectoryRoot = Strings.Left(pPath, POS - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim POS As Long
    
    POS = InStrRev(pPath, "\")
    
    If POS <> 0 Then
        GetDirParent = Strings.Left(pPath, POS - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    ERR.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Strings.Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For I = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next I
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For I = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(I)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next I
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function

Public Function Rellenar_SpaceR(intValue, intDigits, car)
    '*** ESPACIOA A LA DER
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), car)
End Function

Public Function GetLines(Optional HowMany As Long = 1)
        
    Dim HowManyLines As Integer
    
    HowManyLines = ValidarNumeroIntervalo(HowMany, , 1)
        
    For I = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next I
    
End Function

Public Function GetTab(Optional HowMany As Long = 1)
        
    Dim HowManyTabs As Integer
    
    HowManyTabs = ValidarNumeroIntervalo(HowMany, , 1)
        
    For I = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next I
    
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant

    On Error GoTo ERR

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
ERR:

    ValidarNumeroIntervalo = pValor

End Function
