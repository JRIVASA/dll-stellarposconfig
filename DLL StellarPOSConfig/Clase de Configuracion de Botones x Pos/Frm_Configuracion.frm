VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Configuracion 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8685
   ClientLeft      =   255
   ClientTop       =   795
   ClientWidth     =   13800
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   8685
   ScaleWidth      =   13800
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame6 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   41
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   11655
         TabIndex        =   43
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Stellar Platform - Configuraci�n de Botones del POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   13905
      TabIndex        =   38
      Top             =   360
      Width           =   13935
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1065
         Left            =   0
         TabIndex        =   39
         Top             =   -50
         Width           =   12825
         Begin MSComctlLib.Toolbar tlb_Config 
            Height          =   810
            Left            =   195
            TabIndex        =   40
            Top             =   190
            Width           =   10320
            _ExtentX        =   18203
            _ExtentY        =   1429
            ButtonWidth     =   1032
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Wrappable       =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "Salir"
                  Description     =   "Salir"
                  Object.ToolTipText     =   "Salir de la Configuracion de Botones"
                  Object.Tag             =   "Salir"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Menu"
                  Key             =   "Menu"
                  Description     =   "Menu"
                  Object.ToolTipText     =   "Menu"
                  Object.Tag             =   "Menu"
                  ImageIndex      =   4
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList Treeimagen 
      Left            =   150
      Top             =   6690
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":0000
            Key             =   "DPTO CERRADO2"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":0452
            Key             =   "DPTO ABIERTO"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":07EC
            Key             =   "DPTO CERRADO"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":0B86
            Key             =   "DPTO ABIERTO2"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   825
      Top             =   6690
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":0FD8
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":2D6A
            Key             =   "imgMenu"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":3A28
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Configuracion.frx":4702
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Caption         =   "Botones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   6810
      Left            =   5340
      TabIndex        =   1
      Top             =   1710
      Width           =   8265
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   30
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   6960
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   31
         Left            =   975
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   6960
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   32
         Left            =   1830
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   6960
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   33
         Left            =   2685
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   6960
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   34
         Left            =   3540
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   6960
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   35
         Left            =   4395
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   6960
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   29
         Left            =   6870
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   5370
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   28
         Left            =   5550
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   27
         Left            =   4230
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   26
         Left            =   2910
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   25
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   24
         Left            =   270
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   23
         Left            =   6870
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   22
         Left            =   5550
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   21
         Left            =   4230
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   20
         Left            =   2910
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   19
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   18
         Left            =   270
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   17
         Left            =   6870
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   16
         Left            =   5550
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   15
         Left            =   4230
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   14
         Left            =   2910
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   13
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   12
         Left            =   270
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   11
         Left            =   6870
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   10
         Left            =   5550
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   9
         Left            =   4230
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   8
         Left            =   2910
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   7
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   6
         Left            =   270
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   5
         Left            =   6870
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   4
         Left            =   5550
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   3
         Left            =   4230
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   2
         Left            =   2910
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   1
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1290
         Index           =   0
         Left            =   270
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   270
         Width           =   1335
      End
   End
   Begin MSComctlLib.TreeView tvw_Arbol 
      Height          =   6705
      Left            =   180
      TabIndex        =   0
      Top             =   1800
      Width           =   4995
      _ExtentX        =   8811
      _ExtentY        =   11827
      _Version        =   393217
      Indentation     =   212
      LabelEdit       =   1
      Style           =   5
      SingleSel       =   -1  'True
      ImageList       =   "Treeimagen"
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDragMode     =   1
      OLEDropMode     =   1
   End
   Begin VB.Menu menu 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu mnuAgregar 
         Caption         =   "&Agregar"
         Begin VB.Menu mnuGrupo 
            Caption         =   "&Grupos"
         End
         Begin VB.Menu mnuproducto 
            Caption         =   "&Producto"
         End
      End
      Begin VB.Menu mnuModificar 
         Caption         =   "&Modificar"
      End
      Begin VB.Menu sepa 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuborrar 
         Caption         =   "&Eliminar"
      End
   End
End
Attribute VB_Name = "Frm_Configuracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim POS As Integer          ' Variable que guarda la posicion del nodo
Dim Obj As Object             ' Variable para manejo del treeview New recsun.obj_treeview '

Public fNodoArr_Sel As Variant          'Variable que guarda los datos del nodo seleccionado
Public addnode As Boolean   'Variable que indica  si se va a�adir un nodo o no

Dim lcKey As String         ' Variable que indica dependiendo del nodo seleccionado si se pueden a�adir productos o grupos

Dim fcargado As Boolean     'Variable utilizada para controlar el evento mouse click y mouse down
                            ' el evento mouse down se dispara primero y para el correcto funcionamiento del programa se debia
                            'dispara primero el nodeclick entonces se utilizo esta bandera
                            
Private Sub cmd_Botones_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    Dim lcKey As String, Mensaje As Object
    Indice = Index
        
    If Trim(cmd_Botones(Index).Tag) <> "" Then
        fNodoArr_Sel = Obj.TomarDataTw(Me.tvw_Arbol, tvw_Arbol.Nodes("l" & cmd_Botones(Index).Tag).Index)
    Else
        If tvw_Arbol.SelectedItem Is Nothing Then
            Set tvw_Arbol.SelectedItem = tvw_Arbol.Nodes(1).Root
        End If
        
        fNodoArr_Sel = Obj.TomarDataTw(Me.tvw_Arbol, tvw_Arbol.Nodes(tvw_Arbol.SelectedItem.Key).Index)
    End If
    
    fcargado = True
    
    Set Mensaje = CreateObject("recsun.OBJ_MENSAJERIA")
    If fcargado = True Then
        lcKey = Mid(CStr(fNodoArr_Sel(2)), 1, 1)
        Select Case Button
            Case Is = 1
            Case Is = 2
                Select Case lcKey
                    Case Is = "R"
                        
                        mnuGrupo.Enabled = cmd_Botones(Index).Tag = ""
                        mnuAgregar.Enabled = tvw_Arbol.SelectedItem.Children < 35
                        mnuproducto.Enabled = cmd_Botones(Index).Tag = ""
                        mnuModificar.Enabled = cmd_Botones(Index).Tag <> ""
                        mnuborrar.Enabled = cmd_Botones(Index).Tag <> ""
                        DatosBoton.Posicion = Indice + 1
                    Case Is = "G"
                         mnuAgregar.Enabled = tvw_Arbol.SelectedItem.Children < 35
                         mnuGrupo.Enabled = cmd_Botones(Index).Tag = ""
                         mnuproducto.Enabled = cmd_Botones(Index).Tag = ""
                         mnuModificar.Enabled = cmd_Botones(Index).Tag <> ""
                         mnuborrar.Enabled = cmd_Botones(Index).Tag <> ""
                         DatosBoton.Posicion = Indice + 1
                    Case Else
                        mnuAgregar.Enabled = tvw_Arbol.SelectedItem.Children < 35
                        mnuModificar.Enabled = cmd_Botones(Index).Tag <> ""
                        mnuborrar.Enabled = cmd_Botones(Index).Tag <> ""
                        mnuGrupo.Enabled = cmd_Botones(Index).Tag = ""
                        mnuproducto.Enabled = cmd_Botones(Index).Tag = ""
                        DatosBoton.Posicion = Indice + 1
                End Select
                PopupMenu Menu
        End Select
    End If
    fcargado = False
End Sub

Private Sub Form_Activate()
    If SalidaForzada Then ForcedExit: Exit Sub
End Sub

Private Sub Form_Load()
    
    Dim mSQL As String
    
    Set Obj = New Obj_Treeview 'CreateObject("recsun.obj_treeview")
    
    mSQL = "SELECT  * FROM TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP ORDER BY id"
    
    DibujarArbol Me.tvw_Arbol, Cadena, mSQL
    
    fcargado = False
    
    tvw_Arbol_NodeClick tvw_Arbol.Nodes(1).Root
    Me.tlb_Config.Buttons(2).Caption = StellarMensaje(107, True)
    Me.tlb_Config.Buttons(4).Caption = StellarMensaje(10304, True)
    Me.mnuAgregar.Caption = StellarMensaje(10120, True) 'agregar
    Me.mnuborrar.Caption = StellarMensaje(208, True) 'borrar
    Me.mnuGrupo.Caption = StellarMensaje(161, True) 'grupo
    Me.mnuModificar.Caption = StellarMensaje(207, True) 'modificar
    Me.mnuproducto.Caption = StellarMensaje(5010, True) 'producto
    'Me.mn
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set Obj = Nothing
End Sub

Private Sub mnuborrar_Click()
    
    Dim boton As cls_Boton
    'Dim Mensaje As Object
    Dim mresp As String
    Dim mConexion As New ADODB.Connection
    Dim m_Tabla As String
    
    m_Tabla = "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
    
    mConexion.Open Cadena
    'Set Mensaje = CreateObject("recsun.OBJ_MENSAJERIA")
    'Mensaje.Mensaje "Esta seguro ", True
    'Mensaje True, "�Esta seguro que desea eliminar el item?"
    Mensaje True, StellarMensaje(10317, True), True ' traducir
    'If Mensaje.PressBoton = True Then
    If Retorno = True Then
        Set boton = New cls_Boton
        boton.Codigo = CStr(fNodoArr_Sel(2))
        boton.eliminar mConexion, m_Tabla, mresp
        If mresp = vbYes Then
            Obj.BorrarTw Me.tvw_Arbol, POS
            Call tvw_Arbol_NodeClick(tvw_Arbol.SelectedItem)
        End If
        Set boton = Nothing
    End If
    'Set Mensaje = Nothing
    mConexion.Close
 
End Sub

Private Sub mnuGrupo_Click()
    
    addnode = False
    frm_NewGrupo.Show vbModal
   
    If addnode = True Then
        A = tvw_Arbol.SelectedItem
        'Me.tvw_Arbol.Nodes.Add tvw_Arbol.SelectedItem, tvwChild, DatosBoton.Codigo, DatosBoton.Nombre, "DPTO CERRADO"
        Obj.AdicionarTw Me.tvw_Arbol, DatosBoton.Grupo, DatosBoton.Codigo, DatosBoton.Nombre, "DPTO ABIERTO", "DPTO ABIERTO"
        Call tvw_Arbol_NodeClick(tvw_Arbol.SelectedItem)
    End If
    
End Sub

Private Sub mnuModificar_Click()
    Dim mSQL As String
    addnode = False
    mSQL = "SELECT  * FROM TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP ORDER BY id"
    Frm_Modificar.Show vbModal
   
    If addnode = True Then
        Me.tvw_Arbol.Nodes.Clear
        DibujarArbol Me.tvw_Arbol, Cadena, mSQL
    End If
End Sub

Private Sub mnuproducto_Click()
    
    addnode = False
    clavepadre = fNodoArr_Sel(2)
    Frm_NewProducto.Show vbModal
           
    If addnode = True Then
        Obj.AdicionarTw Me.tvw_Arbol, DatosBoton.Grupo, DatosBoton.Codigo, DatosBoton.Nombre, , "DPTO CERRADO"
        Call tvw_Arbol_NodeClick(tvw_Arbol.SelectedItem)
    End If
    
End Sub

Private Sub tlb_Config_ButtonClick(ByVal Button As MSComctlLib.Button)
     Select Case Button.Index
        Case Is = 2
            Unload Me
        Case Is = 4
                                    
            If Not Me.ActiveControl Is Nothing Then
                If TypeName(Me.ActiveControl) = "CommandButton" Then
                    cmd_Botones_MouseDown Me.ActiveControl.Index, 2, 0, Me.ActiveControl.Left + Frame2.Left, Me.ActiveControl.Top + Frame2.Top
                Else
                    If tvw_Arbol.SelectedItem Is Nothing Then
                        Set tvw_Arbol.SelectedItem = tvw_Arbol.Nodes(0).Root
                    End If
                
                    tvw_Arbol_NodeClick tvw_Arbol.SelectedItem
                    tvw_Arbol_MouseDown 2, 0, Button.Left, Button.Top
                End If
            End If
    End Select
End Sub

Private Sub tvw_Arbol_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lcKey As String 'Mensaje As Object
    'Set Mensaje = CreateObject("recsun.OBJ_MENSAJERIA")
    If fcargado = True Then
        lcKey = Mid(CStr(fNodoArr_Sel(2)), 1, 1)
        Select Case Button
            Case Is = 1
            Case Is = 2
                Select Case lcKey
                    Case Is = "R"
                        If tvw_Arbol.SelectedItem.Children > 28 Then '34 Then
                            Mensaje True, "No hay m�s puestos de botones e este nivel." ' traducir
                            Exit Sub
                        End If
                        mnuGrupo.Enabled = True
                        mnuAgregar.Enabled = True
                        mnuproducto.Enabled = True
                        mnuModificar.Enabled = True
                        mnuborrar.Enabled = False
                    Case Is = "G"
                        If tvw_Arbol.SelectedItem.Children > 28 Then '34 Then
                            Mensaje True, "No hay m�s puestos de botones e este nivel." 'traducir
                            Exit Sub
                        End If
                         mnuAgregar.Enabled = True
                         mnuGrupo.Enabled = True
                         mnuproducto.Enabled = True
                         mnuModificar.Enabled = True
                         mnuborrar.Enabled = True
                    Case Else
                        If tvw_Arbol.SelectedItem.Children > 28 Then '34 Then
                            Mensaje True, "No hay m�s puestos de botones e este nivel." 'traducir
                            Exit Sub
                        End If
                        mnuAgregar.Enabled = False
                        mnuModificar.Enabled = True
                        mnuborrar.Enabled = True
                End Select
                PopupMenu Menu
        End Select
    End If
    fcargado = False
End Sub

Private Sub tvw_Arbol_NodeClick(ByVal Node As MSComctlLib.Node)
    
    Dim boton As cls_Boton
    Dim Min As Long, Max As Long
    Dim CONEX As New ADODB.Connection
    Dim m_SQL As String
    
    CONEX.Open Cadena
    Min = 0
    Max = Me.cmd_Botones.Count - 1
    
    Set boton = New cls_Boton
    
    fNodoArr_Sel = Obj.TomarDataTw(tvw_Arbol, Node.Index)
    
    POS = Node.Index
    
    fcargado = True
    
    m_SQL = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where relacion = '" & CStr(fNodoArr_Sel(2)) + "'"
    
    boton.IniciarBotones Me.cmd_Botones, CONEX, m_SQL, Min, Max
    
    CONEX.Close
    
    Set boton = Nothing
    
End Sub
