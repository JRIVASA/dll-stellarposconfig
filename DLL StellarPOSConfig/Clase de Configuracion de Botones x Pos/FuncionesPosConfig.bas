Attribute VB_Name = "FuncionesPosConfig"
Public Const IDProducto = 20389

' Funcion que proporciona la cadena de conexion para la conexion

Function Cadena() As String
    Cadena = Variables.SPosConexion
End Function

' Sub que dibuja el arbol toma los datos de la tabla temporal
Public Sub DibujarArbol(ptv As TreeView, pcadenaconexion As String, ptextoSQL As String)
    
    Dim m_rs As New ADODB.Recordset
    Dim m_conex As New ADODB.Connection
    Dim Obj As Object
    
    Set Obj = New Obj_Treeview 'CreateObject("recsun.obj_treeview")
    
    On Error GoTo Error
    
    Set m_conex = New ADODB.Connection
    Set m_rs = New ADODB.Recordset
    
    m_conex.ConnectionString = pcadenaconexion
    m_conex.Open
    
    m_rs.CursorLocation = adUseServer
    m_rs.Open ptextoSQL, m_conex, adOpenKeyset, adLockReadOnly
    
    ptv.Nodes.Clear
    
    'Obj.CrearRaizTw ptv, "RAIZ", "BOTONES", "DPTO CERRADO"
    Obj.CrearRaizTw ptv, "RAIZ", StellarMensaje(10304, True), "DPTO CERRADO"
    
        While Not m_rs.EOF
            'If LCase(m_rs!texto) = "bebidas" Then
                'A = B
            'End If
            Obj.AdicionarTw ptv, m_rs!Relacion, m_rs!Clave, m_rs!Texto, m_rs!Imagen, m_rs!Imagen
            m_rs.MoveNext
            
            'DoEvents
        Wend
        
        m_rs.Close
        m_conex.Close
        
        ExpandirArbol ptv
        
        Set Obj = Nothing
        
    Exit Sub
    
Error:
    
    MsgBox ERR.Description
    
End Sub

' Crea un nuevo correlativo para un Grupo, producto o Perfil
Public Function micorrelativo(PConexion As ADODB.Connection, pcampo As String, Optional psumar As Boolean = True) As String
    
    On Error GoTo Error
    
    Dim m_Obj_correlativo As Object
    Dim m_Correlativo As String
    
    Set m_Obj_correlativo = CreateObject("recsun.cls_datos")
    m_Correlativo = m_Obj_correlativo.NO_CONSECUTIVO(IDProducto, PConexion, pcampo, psumar)
    micorrelativo = m_Correlativo
    
    Exit Function
    
Error:
    
    MsgBox "Ha ocurrido un error con los correlativos. El configurador de botones se cerrar� a continuaci�n."
    
    ForcedExit
    
End Function

'Llena los combos de Asignacion de Teclas
Public Sub LlenarCombos(ByRef pcbo_mascara As ComboBox, ByRef pcbo_Tecla As ComboBox)
    Dim I As Integer
    
    With pcbo_mascara
        .AddItem "Ninguna"
        .AddItem "Ctrl"
        .AddItem "Alt"
        .AddItem "Shift"
        .ListIndex = 0
    End With
    With pcbo_Tecla
        .AddItem "Ninguna"
        For I = 65 To 90
            .AddItem Chr$(I)
        Next I
        .ListIndex = 0
    End With
    
End Sub

Public Sub ExpandirArbol(arbol As TreeView)
    
    For I = 1 To arbol.Nodes.Count ' Expande todos los nodos.
        
        If I = 1 Then
             arbol.Nodes(I).Expanded = True
        Else
             arbol.Nodes(I).Expanded = False
        End If
        
        If arbol.Nodes(I).Text = "INACTIVOS" Then
             arbol.Nodes(I).Expanded = False
        End If
        
    Next I
    
End Sub
