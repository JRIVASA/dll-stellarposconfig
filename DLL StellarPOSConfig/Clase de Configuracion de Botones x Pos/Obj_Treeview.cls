VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Obj_Treeview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Sub CrearRaizTw(ByRef Tw As Object, Codigo As String, Texto As String, Optional Imagen As String = "", Optional ImagenSel As String = "")
    
    On Error Resume Next
    
    Dim NodX As Node
    
    If Imagen = "" Then
        Set NodX = Tw.Nodes.Add(, , "l" & Codigo, Texto)
    ElseIf ImagenSel = "" Then
        Set NodX = Tw.Nodes.Add(, , "l" & Codigo, Texto, Imagen)
    Else
        Set NodX = Tw.Nodes.Add(, , "l" & Codigo, Texto, Imagen, ImagenSel)
    End If
    
End Sub
 
Function AdicionarTw(ByRef Tw As Object, CodPadre As String, Codigo As String, Texto As String, Optional Tag As String = "", Optional Imagen As String = "", Optional ImagenSel As String = "") As Integer

    'On Error Resume Next
    
    Dim NodX As Node
    
    If Imagen = "" Then
        Set NodX = Tw.Nodes.Add("l" & CodPadre, tvwChild, "l" & Codigo, Texto)
    ElseIf ImagenSel = "" Then
        Set NodX = Tw.Nodes.Add("l" & CodPadre, tvwChild, "l" & Codigo, Texto, Imagen)
    Else
        Set NodX = Tw.Nodes.Add("l" & CodPadre, tvwChild, "l" & Codigo, Texto, Imagen, ImagenSel)
    End If
    
    NodX.Tag = Tag
    NodX.EnsureVisible
    
    'AdicionarTw = Err.Number
    
End Function

Function AdicionarTwsucursal(ByRef Tw As Object, CodPadre As String, Codigo As String, Texto As String, Optional Tag As String = "", Optional Imagen As String = "", Optional ImagenSel As String = "") As Integer
    
    'On Error Resume Next
    
    Dim NodX As Node
    
    If Imagen = "" Then
        Set NodX = Tw.Nodes.Add("l" & CodPadre, , "l" & Codigo, Texto)
    ElseIf ImagenSel = "" Then
        Set NodX = Tw.Nodes.Add("l" & CodPadre, , "l" & Codigo, Texto, Imagen)
    Else
        Set NodX = Tw.Nodes.Add("l" & CodPadre, , "l" & Codigo, Texto, Imagen, ImagenSel)
    End If
    
    NodX.Tag = Tag
    NodX.EnsureVisible
    
    'AdicionarTw = Err.Number
    
End Function

Function TomarDataTw(ByRef Tw As Object, POS As Integer) As Variant
    
    Dim Tmp(10)
    Dim Nodo As Node
    
    Tmp(0) = Tw.Nodes(POS).Index                                                ' numero del nodo
    Tmp(2) = Mid(Tw.Nodes(POS).Key, 2, Len(Tw.Nodes(POS).Key))                  ' codigo del nodo
    Tmp(3) = Tw.Nodes(POS).Text                                                 ' codigo del texto
    Tmp(4) = Tw.Nodes(POS).Tag                                                  ' tag uso del programado
    
    Set Nodo = Tw.Nodes(1).Root
    
    If Tw.Nodes(POS) <> Nodo Then
        Tmp(1) = Mid(Tw.Nodes(POS).Parent.Key, 2, Len(Tw.Nodes(POS).Parent.Key))    ' codigo del padre
        Tmp(5) = Tw.Nodes(POS).Parent.Index                                         ' numero del padre
    End If
    
    TomarDataTw = Tmp
    
End Function

Sub ActualizarTw(ByRef Tw As Object, CodPadre As String, Codigo As String, Texto As String, POS As Integer, Optional Tag As String = "")
   Tw.Nodes(POS).Parent.Key = "l" & CodPadre
   Tw.Nodes(POS).Key = "l" & Codigo
   Tw.Nodes(POS).Text = Texto
   Tw.Nodes(POS).Tag = Tag
End Sub

Sub BorrarTw(ByRef Tw As Object, POS As Integer)
    Tw.Nodes.Remove (POS)
End Sub

'Purpose     :  Performs a recallable search on a treeview.
'Inputs      :  tvFind                  The treeview to search.
'               sFindItem               The text to search for (supports wildcards)
'               [bSearchAll]            If True will search all nodes of the treeview, else
'                                       will only search top level nodes.
'               [lItemIndex]            If specified will return this matching index.
'                                       eg, if set to 2 then will return the second node which matches the search criteria.
'Outputs     :  Returns the a matching treeview node or nothing if the item is not found

Function TreeViewFindNode(tvFind As Object, ByVal sFindItem As String, Optional bSearchAll As Boolean = True, _
Optional lItemIndex As Long = 1, Optional ExactMatch As Boolean = False) As Node

    Dim oThisNode As Node, bSearch As Boolean, lInstance As Long
    
    bSearch = True
    
    Set TreeViewFindNode = Nothing
    
    For Each oThisNode In tvFind.Nodes
        If bSearchAll = False Then
            'Only Search Top Level Nodes
            If (oThisNode.Parent Is Nothing) = False Then
                bSearch = False
            Else
                bSearch = True
            End If
        End If
        If bSearch Then
            If ExactMatch Then
                If (oThisNode.Text = sFindItem) Then
                    lInstance = lInstance + 1
                    If lInstance >= lItemIndex Then
                        'Found matching item
                        Set TreeViewFindNode = oThisNode
                        Exit For
                    End If
                End If
            Else
                If (UCase(oThisNode.Text) Like UCase("*" & sFindItem & "*")) Then
                    lInstance = lInstance + 1
                    If lInstance >= lItemIndex Then
                        'Found matching item
                        Set TreeViewFindNode = oThisNode
                        Exit For
                    End If
                End If
            End If
        End If
    Next
    
End Function

'Purpose     :  Performs a recallable search on a treeview.
'Inputs      :  tvFind                  The treeview to search.
'               sFindItem               The text to search for (supports wildcards)
'               [bSearchAll]            If True will search all nodes of the treeview, else
'                                       will only search top level nodes.
'               [lItemIndex]            If specified will return this matching index.
'                                       eg, if set to 2 then will return the second node which matches the search criteria.
'Outputs     :  Returns the a matching treeview node or nothing if the item is not found

Function TreeViewFindNodeByKey(tvFind As Object, ByVal sFindItem As String, Optional bSearchAll As Boolean = True, _
Optional lItemIndex As Long = 1, Optional ExactMatch As Boolean = True) As Node

    Dim oThisNode As Node, bSearch As Boolean, lInstance As Long
    
    bSearch = True
    
    Set TreeViewFindNodeByKey = Nothing
    
    For Each oThisNode In tvFind.Nodes
        If bSearchAll = False Then
            'Only Search Top Level Nodes
            If (oThisNode.Parent Is Nothing) = False Then
                bSearch = False
            Else
                bSearch = True
            End If
        End If
        If bSearch Then
            If ExactMatch Then
                If (oThisNode.Key = sFindItem) Then
                    lInstance = lInstance + 1
                    If lInstance >= lItemIndex Then
                        'Found matching item
                        Set TreeViewFindNodeByKey = oThisNode
                        Exit For
                    End If
                End If
            Else
                If (UCase(oThisNode.Key) Like UCase("*" & sFindItem & "*")) Then
                    lInstance = lInstance + 1
                    If lInstance >= lItemIndex Then
                        'Found matching item
                        Set TreeViewFindNodeByKey = oThisNode
                        Exit For
                    End If
                End If
            End If
        End If
    Next
    
End Function
