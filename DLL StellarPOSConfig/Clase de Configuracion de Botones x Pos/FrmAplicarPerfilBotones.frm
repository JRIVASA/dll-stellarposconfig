VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "comct332.ocx"
Begin VB.Form FrmAplicarPerfilBotones 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6000
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   6780
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "FrmAplicarPerfilBotones.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6000
   ScaleWidth      =   6780
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   4665
         TabIndex        =   6
         Top             =   75
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   75
         Width           =   5295
      End
   End
   Begin MSComctlLib.ImageList image_menu_prin 
      Index           =   0
      Left            =   360
      Top             =   1380
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAplicarPerfilBotones.frx":628A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAplicarPerfilBotones.frx":801C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAplicarPerfilBotones.frx":9DAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAplicarPerfilBotones.frx":BB40
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar CoolBar 
      Height          =   1140
      Left            =   0
      TabIndex        =   0
      Top             =   420
      Width           =   6795
      _ExtentX        =   11986
      _ExtentY        =   2011
      BandCount       =   1
      _CBWidth        =   6795
      _CBHeight       =   1140
      _Version        =   "6.7.8862"
      Child1          =   "Frame1"
      MinHeight1      =   1080
      Width1          =   8445
      NewRow1         =   0   'False
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   1080
         Left            =   30
         TabIndex        =   1
         Top             =   30
         Width           =   6675
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   180
            TabIndex        =   2
            Top             =   150
            Width           =   6405
            _ExtentX        =   11298
            _ExtentY        =   1429
            ButtonWidth     =   1402
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "image_menu_prin(0)"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "cancelar"
                  Object.ToolTipText     =   "Cancela los datos en la configuración"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "grabar"
                  Object.ToolTipText     =   "Guarda la información del Usuario"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   4
                  Object.Width           =   1e-4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "salir"
                  Object.ToolTipText     =   "Cancela y Cierre la Configuracion de Usuarios"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Todos"
                  Key             =   "Check"
                  ImageIndex      =   4
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ListView lw_Tablas 
      Height          =   4005
      Left            =   180
      TabIndex        =   3
      Top             =   1740
      Width           =   5835
      _ExtentX        =   10292
      _ExtentY        =   7064
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FlatScrollBar   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   5790296
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Codigo"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Descripción"
         Object.Width           =   7585
      EndProperty
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   0
      Picture         =   "FrmAplicarPerfilBotones.frx":10BCF
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   845
      Picture         =   "FrmAplicarPerfilBotones.frx":11899
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image CmdUp 
      Height          =   480
      Left            =   6165
      Picture         =   "FrmAplicarPerfilBotones.frx":12563
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image CmdDown 
      Height          =   480
      Left            =   6165
      Picture         =   "FrmAplicarPerfilBotones.frx":1322D
      Top             =   4080
      Width           =   480
   End
End
Attribute VB_Name = "FrmAplicarPerfilBotones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private TmpConex As ADODB.Connection
Private ObjLw As New Obj_Listview
Private Cancelar As Boolean

Private Sub CmdUp_Click()
    
    If lw_Tablas.ListItems.Count > 0 Then
        
        Dim CantSaltos As Long, CurrentIndex As Long, CurrentPageIndex As Long, NextIndex As Long, NextPageIndex As Long
        CantSaltos = 9
        
        CurrentIndex = lw_Tablas.SelectedItem.Index
        CurrentPageIndex = CurrentIndex Mod CantSaltos
        CurrentPageIndex = Fix(CurrentIndex / CantSaltos)
        NextPageIndex = CurrentPageIndex - 1
        NextIndex = (NextPageIndex * CantSaltos) - CantSaltos + 1
        
        If (NextIndex) <= 1 Then
            Set lw_Tablas.SelectedItem = lw_Tablas.ListItems(1)
            lw_Tablas.SelectedItem.EnsureVisible
            CmdUp.Visible = False
        Else
            Set lw_Tablas.SelectedItem = lw_Tablas.ListItems(NextIndex)
            lw_Tablas.SelectedItem.EnsureVisible
        End If
        
        CmdDown.Visible = True
        lw_Tablas.SetFocus
        
    End If
    
End Sub

Private Sub CmdDown_Click()
    
    If lw_Tablas.ListItems.Count > 0 Then
        
        Dim CantSaltos As Long, CurrentIndex As Long, CurrentPageIndex As Long, NextIndex As Long, NextPageIndex As Long
        CantSaltos = 9
        
        CurrentIndex = lw_Tablas.SelectedItem.Index
        CurrentPageIndex = CurrentIndex Mod CantSaltos
        CurrentPageIndex = Fix(CurrentIndex / CantSaltos) + IIf(CurrentPageIndex <> 0, 1, 0)
        NextPageIndex = CurrentPageIndex + 1
        NextIndex = NextPageIndex * CantSaltos
        
        If (NextIndex) > lw_Tablas.ListItems.Count Then
            Set lw_Tablas.SelectedItem = lw_Tablas.ListItems(lw_Tablas.ListItems.Count)
            lw_Tablas.SelectedItem.EnsureVisible
            CmdDown.Visible = False
        Else
            Set lw_Tablas.SelectedItem = lw_Tablas.ListItems(NextIndex)
            lw_Tablas.SelectedItem.EnsureVisible
        End If
        
        CmdUp.Visible = True
        lw_Tablas.SetFocus
        
    End If
    
End Sub

Private Sub Form_Activate()
    If SalidaForzada Then ForcedExit: Exit Sub
    If Cancelar Then Unload Me
End Sub

Private Sub Form_Load()
    
    'On Error GoTo error_formload
    'Me.Caption = "Datos a Sincronizar"
    
    lbl_Organizacion.Caption = StellarMensaje(10323) '"Aplicar configuración en cajas."
    
    Toolbar1.Buttons(1).Caption = Stellar_Mensaje(6) 'cancelar
    Toolbar1.Buttons(2).Caption = Stellar_Mensaje(103) 'grabar
    Toolbar1.Buttons(5).Caption = StellarMensaje(2545) 'todos
    Toolbar1.Buttons(4).Caption = Stellar_Mensaje(107) 'salir"
    
    lw_Tablas.ColumnHeaders(1).Text = Stellar_Mensaje(142) 'Codigo
    lw_Tablas.ColumnHeaders(2).Text = Stellar_Mensaje(143) 'Descripcion
    
    CargarCajas
    
    Exit Sub

error_formload:
    
    Unload Me
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo error_formkeydown
    
    Select Case KeyCode

        Case Is = vbKeyF1
            ' Llamar Ayuda

        Case Is = vbKeyF4
            Call Grabar

        Case Is = vbKeyF6
            Marcar IIf(Toolbar1.Buttons("Check").Caption = StellarMensaje(2545), True, False) ' Todos

        Case Is = vbKeyF12
            Unload Me
            
    End Select
    
    Exit Sub
    
error_formkeydown:
    
    Unload Me
    
End Sub

Private Sub lw_Tablas_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            Call Form_KeyDown(vbKeyF12, 0)
    End Select
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    On Error GoTo error_buttonclick
    
    Select Case UCase(Button.Key)
        Case Is = "CANCELAR"
            Call Form_KeyDown(vbKeyF7, 0)
        Case Is = "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
        Case Is = "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
        Case Is = "CHECK"
            Call Form_KeyDown(vbKeyF6, 0)
            
    End Select

    Exit Sub

error_buttonclick:
    
    Unload Me
    
End Sub

Private Sub Marcar(pBool As Boolean)
    
    With lw_Tablas
        For I = 1 To .ListItems.Count
            DoEvents
            .ListItems(I).Checked = pBool
        Next I
    End With
    
    Toolbar1.Buttons("Check").Caption = _
    IIf(pBool, StellarMensaje(10248), StellarMensaje(2545)) ' Todos / Ninguno.
    
End Sub

Private Sub Grabar()

    On Error GoTo Error_Grabar
    
    Dim I As Integer
    Dim IsBegin As Boolean
    Dim TmpRs As ADODB.Recordset
    Dim mRs As ADODB.Recordset
    Set mRs = New ADODB.Recordset
    
    Set TmpRs = New Recordset
    TmpRs.CursorLocation = adUseClient
    
    TmpRs.Open "SELECT * FROM TR_CAJA_BOTONES_DIGITALES_PERFIL WHERE CodigoPerfil = '" & Frm_Perfiles.txt_CodigoPerfil.Text & "' ORDER BY ID", TmpConex, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set TmpRs.ActiveConnection = Nothing
    
    TmpConex.BeginTrans
    
        IsBegin = True
        
        mCant = Me.lw_Tablas.ListItems.Count
        
        mRs.Open "SELECT * FROM TR_CAJA_BOTONES_DIGITALES WHERE 1 = 2", TmpConex, adOpenKeyset, adLockBatchOptimistic, adCmdText
        
        For I = 1 To mCant
        
            mValores = ObjLw.TomarDataLw(Me.lw_Tablas, I)
            
            If Me.lw_Tablas.ListItems(I).Checked Then
                
                TmpConex.Execute "DELETE FROM MA_CAJA_BOTONES_DIGITALES WHERE (Caja = '" & mValores(0) & "')"
                TmpConex.Execute "INSERT INTO MA_CAJA_BOTONES_DIGITALES (CAJA, CodigoPerfil, fijar) VALUES ('" & mValores(0) & "','" & Frm_Perfiles.txt_CodigoPerfil.Text & "', 0)"
                
                TmpConex.Execute "DELETE FROM TR_CAJA_BOTONES_DIGITALES WHERE (Caja = '" & mValores(0) & "')"
                
                With mRs
                                                          
                    TmpRs.MoveFirst
                                                          
                    While Not TmpRs.EOF
                        .AddNew
                        !Caja = mValores(0)
                        !Relacion = TmpRs!Relacion
                        !Clave = TmpRs!Clave
                        !Texto = TmpRs!Texto
                        !Imagen = TmpRs!Imagen
                        !Tag = TmpRs!Tag
                        !Posicion = TmpRs!Posicion
                        !Mascara = TmpRs!Mascara
                        !Tecla = TmpRs!Tecla
                        TmpRs.MoveNext
                    Wend
                    
                End With
                
            End If
            
        Next
        
        mRs.UpdateBatch adAffectAll
        
    TmpConex.CommitTrans
    
    'MsgBox "Proceso culminado."
    Mensaje True, StellarMensaje(10322)
    
    Unload Me
    
    Exit Sub

Error_Grabar:
    
    If IsBegin Then
        TmpConex.RollbackTrans
    End If
    
    MsjErrorRapido ERR.Description
    
    Unload Me

End Sub

Private Sub CargarCajas()
    
    On Error GoTo Error
    
    Dim TmpRs As ADODB.Recordset
    
    Set TmpConex = New ADODB.Connection
    Set TmpRs = New ADODB.Recordset
    
    TmpConex.Open Cadena
    
    Cancelar = False
    
    If TmpConex.Execute("SELECT * FROM TR_CAJA_BOTONES_DIGITALES_PERFIL WHERE CodigoPerfil = '" & Frm_Perfiles.txt_CodigoPerfil.Text & "'").EOF Then
        Cancelar = True
        'MsgBox "Perfil inválido o no contiene datos."
        Mensaje True, StellarMensaje(10321)
        Exit Sub
    End If
    
    TmpRs.Open "SELECT * FROM MA_CAJA", _
    TmpConex, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Dim I As Long
    I = 1
    
    While Not TmpRs.EOF
        
        Call ObjLw.AdicionarLw(Me.lw_Tablas, Array(TmpRs!c_Codigo, TmpRs!c_Desc_Caja))
        
        I = I + 1
        
        TmpRs.MoveNext
        
    Wend
    
    TmpRs.Close
    
    Exit Sub
    
Error:
    
End Sub
