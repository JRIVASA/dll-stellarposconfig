VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Obj_Listview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Sub AdicionarLw(ByRef Lw As Object, Valores As Variant)
    
    Dim ItmX As ListItem
    
    Set ItmX = Lw.ListItems.Add(, , CStr(IIf(IsNull(Valores(0)), "", Valores(0))))
    
    Dim I
    
    LonValores = UBound(Valores)
    
    If LonValores > 0 Then
        For I = 1 To LonValores
            ItmX.SubItems(I) = IIf(IsNull(Valores(I)), "", Valores(I))
        Next
    End If
    
End Sub
   
Sub BorrarLw(ByRef Lw As Object, ByVal POS As Long)
    Lw.ListItems.Remove (POS)
End Sub

Sub ActualizarLw(ByRef Lw As Object, ByVal POS As Long, Valores As Variant)
    
    Dim ItmX As ListItem
    
    Lw.ListItems(POS).Text = Valores(0)
    
    Set ItmX = Lw.ListItems(POS)
    
    LonValores = UBound(Valores)
    
    If LonValores > 0 Then
        For I = 1 To LonValores
            ItmX.SubItems(I) = Valores(I)
        Next
    End If
    
End Sub

Function TomarDataLw(ByRef Lw As Object, ByVal POS As Long) As Variant
    
    Dim ItmX As ListItem
    Dim LonCol As Integer
    Dim Tmp()
    
    LonCol = Lw.ColumnHeaders.Count
    
    ReDim Tmp(LonCol - 1)
    
    Set ItmX = Lw.ListItems(POS)
    
    Tmp(0) = Lw.ListItems(POS)
    
    If LonCol > 1 Then
        For I = 1 To LonCol - 1
            Tmp(I) = ItmX.SubItems(I)
        Next
    End If
    
    TomarDataLw = Tmp
    
End Function

