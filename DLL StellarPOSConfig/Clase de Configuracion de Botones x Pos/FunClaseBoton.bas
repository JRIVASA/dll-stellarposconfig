Attribute VB_Name = "FunClaseBoton"
Type DatosBoton             'Estructura de los datos del Boton
        Codigo As String
        CodigoNativo As String
        Posicion As Integer
        Nombre As String
        Grupo As String
        Mascara As String
        Tecla As String
        Imagen As String
        perfil As String
End Type
Public DatosBoton As DatosBoton

'Busca los datos de un boton y los asigna a la variable DatosBoton
Public Sub BuscarDatosBoton(SQL As String, cadenadeconexion As String)
    Dim boton As cls_Boton
    Dim mibusqueda As Variant
    Dim CONEX As New ADODB.Connection
  
    
    Set boton = New cls_Boton
    CONEX.Open Cadena
    mibusqueda = boton.BuscarBoton(CONEX, SQL)
        If Not IsEmpty(mibusqueda) Then
            For I = LBound(mibusqueda) To UBound(mibusqueda)
                If IsNull(mibusqueda(I)) Then
                    mibusqueda(I) = " "
                End If
            Next I
            DatosBoton.Grupo = mibusqueda(0)
            DatosBoton.Codigo = mibusqueda(1)
            DatosBoton.Nombre = mibusqueda(2)
            DatosBoton.Imagen = mibusqueda(3)
            DatosBoton.CodigoNativo = mibusqueda(4)
            DatosBoton.Posicion = mibusqueda(5)
            If mibusqueda(8) <> "" Then
                DatosBoton.Mascara = mibusqueda(8)
            Else
                DatosBoton.Mascara = "Ninguna"
            End If
            If mibusqueda(9) <> "" Then
                DatosBoton.Tecla = mibusqueda(9)
            Else
                DatosBoton.Tecla = "Ninguna"
            End If
            DatosBoton.perfil = mibusqueda(7)
    End If
    Set boton = Nothing

End Sub

' Graba los datos de la estructura a la clase Boton
Public Sub GrabarBoton(cadenaconexion As String, NomTablaTemp As String, ByRef Resp As Boolean, Optional ByVal pModificar = False)
    
    Dim boton As New cls_Boton
    Dim Conexion As New ADODB.Connection
    Dim m_Codigo As String
    Dim Nomtabla As String
    Dim m_CodNativo As String
    
    Conexion.ConnectionString = cadenaconexion
    Conexion.Open
        
    boton.Codigo = DatosBoton.Codigo
    boton.Grupo = DatosBoton.Grupo
    boton.Posicion = DatosBoton.Posicion
    boton.Texto = DatosBoton.Nombre
    boton.Tecla = DatosBoton.Tecla
    boton.Mascara = DatosBoton.Mascara
    boton.Imagen = DatosBoton.Imagen
    If boton.Imagen = "" Then boton.Imagen = "DPTO CERRADO"
    boton.CodigoNativo = DatosBoton.CodigoNativo
    boton.ValidarTeclasBoton Conexion, NomTablaTemp
    boton.perfil = IIf(g_CodigoPerfil = "", DatosBoton.perfil, g_CodigoPerfil)
    
    boton.Guardar Conexion, NomTablaTemp, boton.CodigoNativo, "PRODUCTO", pModificar
    Resp = boton.erroralguardar
    
    Conexion.Close

End Sub
