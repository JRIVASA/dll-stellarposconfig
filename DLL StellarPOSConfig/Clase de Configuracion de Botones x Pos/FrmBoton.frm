VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmBoton 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "POS Configuracion de Botones"
   ClientHeight    =   7995
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7290
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7995
   ScaleWidth      =   7290
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Asignacion de Teclas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   960
      Left            =   2055
      TabIndex        =   42
      Top             =   1770
      Width           =   5085
      Begin VB.ComboBox cbo_Mascara 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   375
         Style           =   2  'Dropdown List
         TabIndex        =   44
         Top             =   540
         Width           =   1665
      End
      Begin VB.ComboBox cbo_tecla 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2670
         Style           =   2  'Dropdown List
         TabIndex        =   43
         Top             =   540
         Width           =   1545
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tecla"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   2670
         TabIndex        =   46
         Top             =   255
         Width           =   615
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Mascara"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   375
         TabIndex        =   45
         Top             =   255
         Width           =   750
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   45
      TabIndex        =   40
      Top             =   15
      Width           =   7155
      Begin MSComctlLib.Toolbar tlb_Perfiles 
         Height          =   810
         Left            =   90
         TabIndex        =   41
         Top             =   180
         Width           =   7000
         _ExtentX        =   12356
         _ExtentY        =   1429
         ButtonWidth     =   1296
         ButtonHeight    =   1429
         Style           =   1
         ImageList       =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   5
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "Buscar"
               Object.ToolTipText     =   "Buscar Perfil"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "Grabar"
               Object.ToolTipText     =   "Graba Perfil"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "Salir"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Teclado"
               Key             =   "Teclado"
               Description     =   "Teclado"
               ImageIndex      =   4
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Posiciones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   5205
      Left            =   210
      TabIndex        =   3
      Top             =   2775
      Width           =   6930
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   0
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   270
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   1
         Left            =   1260
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   270
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   2
         Left            =   2355
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   270
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   3
         Left            =   3450
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   270
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   4
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   270
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   5
         Left            =   5640
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   270
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   6
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   1245
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   7
         Left            =   1260
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   1245
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   8
         Left            =   2355
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   1245
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   9
         Left            =   3450
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   1245
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   10
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   1245
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   11
         Left            =   5640
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   1245
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   12
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   2220
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   13
         Left            =   1260
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   2220
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   14
         Left            =   2355
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   2220
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   15
         Left            =   3450
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   2220
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   16
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   2220
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   17
         Left            =   5640
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   2220
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   18
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   3195
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   19
         Left            =   1260
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   3195
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   20
         Left            =   2355
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   3195
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   21
         Left            =   3450
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   3195
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   22
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   3195
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   23
         Left            =   5640
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   3195
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   24
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   4170
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   25
         Left            =   1260
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   4170
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   26
         Left            =   2355
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   4170
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   27
         Left            =   3450
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   4170
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   28
         Left            =   4545
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   4170
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   29
         Left            =   5640
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   4170
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   30
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   5025
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   31
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   5025
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   32
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   5025
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   33
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   5025
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   34
         Left            =   3585
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   5025
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   35
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   5025
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.TextBox txt_Nombre 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   405
      Left            =   2055
      TabIndex        =   2
      Top             =   1350
      Width           =   5085
   End
   Begin VB.TextBox txt_Posicion 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   405
      Left            =   210
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   2070
      Width           =   1755
   End
   Begin VB.TextBox txt_Codigo 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   405
      Left            =   210
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   1350
      Width           =   1755
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   0
      Top             =   6120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmBoton.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmBoton.frx":1D92
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmBoton.frx":3B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmBoton.frx":58B6
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
   Begin VB.Label lbl_codigo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Codigo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   210
      TabIndex        =   49
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Posicion"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   210
      TabIndex        =   48
      Top             =   1770
      Width           =   765
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   255
      Left            =   2055
      TabIndex        =   47
      Top             =   1095
      Width           =   735
   End
End
Attribute VB_Name = "FrmBoton"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Salir As Boolean

Private Sub Form_Activate()
    If SalidaForzada Then ForcedExit: Exit Sub
    If Salir Then Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            Select Case KeyCode
                Case vbKeyReturn
                    'Send_Keys Chr(vbKeyTab), True
                    'SendKeys Chr(vbKeyTab), True
                    
                Case vbKeyF1            'AYUDA
                
                Case vbKeyF2            'BUSCAR
                    Call BuscarProductos
                Case vbKeyF4            'GRABAR
                    Call Grabar
                Case vbKeyF12           'SALIR
                    Unload Me
            End Select
    End Select
End Sub

Private Sub Form_Load()
    
    Dim m_SQL As String, Min As Long, Max As Long
    Dim Botones As New cls_Boton
    
    Salir = False
    
    If InStr(1, "PG", ModConfigDigitalCaja.QueNivel) = 0 Then
        Mensajes.Mensaje "S�lo puede modificar o agregar productos o grupos", False
        'Unload Me
        Salir = True
    End If
    
    Min = Me.cmd_Botones.LBound
    Max = Me.cmd_Botones.UBound
    
    Call FuncionesPosConfig.LlenarCombos(cbo_Mascara, cbo_tecla)
    If ModConfigDigitalCaja.Accion = Variables.agregar Then
        
        If ModConfigDigitalCaja.QueNivel = "P" Then
            
            txt_Codigo.Text = micorrelativo(PosConexion, "cs_COD_PRODUCTO", False)
            If SalidaForzada Then ForcedExit: Exit Sub
            
            m_SQL = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where relacion='" & CStr(ArregloNodos(2)) + "'"
            Botones.IniciarBotones Me.cmd_Botones, PosConexion, m_SQL, Min, Max
            txt_Nombre.Locked = True
            
        ElseIf ModConfigDigitalCaja.QueNivel = "G" Then
            txt_Codigo.Text = micorrelativo(PosConexion, "cs_COD_GRUPO", False)
            If SalidaForzada Then ForcedExit: Exit Sub
            
            m_SQL = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where relacion='" & CStr(ArregloNodos(2)) + "'"
            Botones.IniciarBotones Me.cmd_Botones, PosConexion, m_SQL, Min, Max
            txt_Nombre.Locked = False
'            Me.tlb_Perfiles.Buttons("Buscar").Enabled = False
            
        End If
        
    Else
        
        m_SQL = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where relacion='" & ArregloNodos(1) + "'"
        Botones.IniciarBotones Me.cmd_Botones, PosConexion, m_SQL, Min, Max
        
        Call Modulo_Modificar(Me)
    End If
End Sub

Private Sub tlb_Perfiles_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case LCase(Button.Key)
        Case "buscar"
            'Send_Keys Chr(vbKeyF2), True
            Form_KeyDown vbKeyF2, 0
        Case "grabar"
            'Send_Keys Chr(vbKeyF4), True
            Form_KeyDown vbKeyF4, 0
        Case "salir"
            'Send_Keys Chr(vbKeyF12), True
            Form_KeyDown vbKeyF12, 0
        Case LCase("Teclado")
            TecladoWindows txt_Nombre
    End Select
End Sub

Private Sub Grabar()
    
    Dim SQL As String
    Dim Respuesta As Boolean
    Dim m_Correlativo As String
    Dim nVariable As String
    
    If ModConfigDigitalCaja.QueNivel = "P" And ModConfigDigitalCaja.Accion = Variables.agregar Then
            
        nVariable = "cs_COD_PRODUCTO"
        g_CodigoPerfil = nCaja
        DatosBoton.Codigo = ModConfigDigitalCaja.Correlativos(PosConexion, nVariable, True)
        If SalidaForzada Then ForcedExit: Exit Sub
        DatosBoton.Grupo = ArregloNodos(2)
            
    ElseIf ModConfigDigitalCaja.QueNivel = "G" And ModConfigDigitalCaja.Accion = Variables.agregar Then
        
        nVariable = "cs_COD_GRUPO"
        g_CodigoPerfil = nCaja
        DatosBoton.Codigo = Me.txt_Codigo.Text
        DatosBoton.Grupo = ArregloNodos(2)
    
    End If
    
    If ModConfigDigitalCaja.Accion = Variables.agregar Then
        DatosBoton.CodigoNativo = Me.txt_Codigo.Text
    Else
        DatosBoton.CodigoNativo = DatosBoton.CodigoNativo
    End If
    
    DatosBoton.Imagen = "DPTO CERRADO"
    DatosBoton.Mascara = Me.cbo_Mascara.Text
    DatosBoton.Nombre = Me.txt_Nombre.Text
    DatosBoton.Posicion = Val(Me.txt_Posicion.Text)
    DatosBoton.Tecla = Me.cbo_tecla
    
    GrabarBoton PosConexion.ConnectionString, "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP", Respuesta, CBool(ModConfigDigitalCaja.Accion)
    If Respuesta = False Then
        
        If ModConfigDigitalCaja.Accion = Variables.agregar Then
            Me.txt_Codigo.Text = ModConfigDigitalCaja.Correlativos(PosConexion, nVariable)
            If SalidaForzada Then ForcedExit: Exit Sub
        End If
        
        Unload Me
        
    End If
    
End Sub

Private Sub cmd_Botones_Click(Index As Integer)
    If Me.cmd_Botones(Index).Caption = "" Or Me.cmd_Botones(Index).Tag = ArregloNodos(2) Then
        Me.txt_Posicion.Text = Index + 1
    Else
        Mensajes.Mensaje "Boton asignado, seleccione otra posicion o modifique el boton actual "
    End If
End Sub

Private Sub BuscarProductos()
    
    On Error GoTo Error
    
    Dim mSQL As String
    Dim mConexion As New ADODB.Connection
    
    mConexion.Open Cadena
    
    With Frm_Super_Consultas
        mSQL = "SELECT c_codigo, c_descri FROM MA_PRODUCTOS"
        .Inicializar mSQL, "PRODUCTOS", mConexion
        .Add_ItemLabels "CODIGO", "C_CODIGO", 2000, 0
        .Add_ItemLabels "DESCRIPCION", "C_DESCRI", 6000, 0
        .Add_ItemSearching "Descripci�n", "c_descri"
        .Add_ItemSearching "C�digo", "c_codigo"
        .Show vbModal
        mProducto = .ArrResultado()
    End With
    
    If Not IsEmpty(mProducto) Then
        ModConfigDigitalCaja.QueNivel = "P"
        txt_Nombre.Locked = True
        Me.txt_Codigo.Text = mProducto(0)
        Me.txt_Nombre = mProducto(1)
    End If
    
    Exit Sub
    
Error:
    
    'MsgBox Err.Description
    Mensajes.Mensaje ERR.Description
    
End Sub

Private Sub txt_Codigo_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim RsProducto As Object, BProducto As New recsuna.cls_productos

    If ModConfigDigitalCaja.QueNivel = "P" And ModConfigDigitalCaja.Accion = Variables.agregar Then
    
        If Shift = 0 Then
            If KeyCode = vbKeyReturn Then
                If Trim(Me.txt_Codigo.Text) <> "" Then
                    Set BProducto = CreateObject("recsuna.cls_productos")
                    '000000000100004
                    BProducto.Inicializar Cadena
                    BProducto.CampoBusquedaCodigos sCodigoAlterno
                    On Error GoTo ERR
                    Set RsProducto = BProducto.Buscar_ProductosRs(BProducto.CampoBusquedaCodigos(sCodigoAlterno) & " = '" & Me.txt_Codigo.Text & "'")
                    If Not RsProducto.EOF Then
                        Me.txt_Nombre.Text = RsProducto!Descripcion
                    End If
                    RsProducto.Close
                End If
            End If
        End If
        
    End If
    
    Exit Sub
    
ERR:
    
    
End Sub
