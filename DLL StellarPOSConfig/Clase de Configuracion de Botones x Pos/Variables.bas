Attribute VB_Name = "Variables"
Public PosConexion As New ADODB.Connection
Public sPosConexion As String
Public nCaja As String
Public StatusConfig As Boolean

Public Const LIndice = 0
Public Const LClavePadre = 1
Public Const LClave = 2
Public Const LTexto = 3
Public Const LTag = 4
Public Const LIndicePadre = 5

Public Retorno As Boolean

Public Const agregar = 0
Public Const modificar = 1

Global WindowsArchitecture          As OperatingSystemArchitecture

Public Function CrearObjetoConexion() As Object
    Set CrearObjetoConexion = CreateObject("ADODB.connection")
End Function

Public Function CrearObjetoRecordSet() As Object
    Set CrearObjetoConexion = CreateObject("ADODB.recordset")
End Function

Public Function CrearObjetoCommand() As Object
    Set CrearObjetoConexion = CreateObject("ADODB.command")
End Function

Public Function CrearObjetoMensajeria() As Object
    Set CrearObjetoConexion = CreateObject("recsun.OBJ_MENSAJERIA")
End Function

Public Function CrearObjetoTreeView() As Object
    Set CrearObjetoConexion = CreateObject("recsun.obj_treeview")
End Function
