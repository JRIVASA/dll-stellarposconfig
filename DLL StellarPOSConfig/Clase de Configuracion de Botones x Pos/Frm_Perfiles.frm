VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Perfiles 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4620
   ClientLeft      =   -45
   ClientTop       =   0
   ClientWidth     =   7380
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4620
   ScaleWidth      =   7380
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   2655
      Left            =   240
      TabIndex        =   6
      Top             =   1680
      Width           =   6855
      Begin VB.CommandButton cmd_buscar 
         Height          =   405
         Left            =   4080
         Picture         =   "Frm_Perfiles.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   660
         Width           =   435
      End
      Begin VB.TextBox txt_CodigoPerfil 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   660
         Width           =   3675
      End
      Begin VB.TextBox txt_Descripcion 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   630
         Left            =   240
         TabIndex        =   7
         Top             =   1815
         Width           =   5640
      End
      Begin VB.Label lbl_Codigo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo del Perfil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   10
         Top             =   240
         Width           =   2265
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Descripcion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   9
         Top             =   1365
         Width           =   2025
      End
   End
   Begin VB.PictureBox Coolbar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   7665
      TabIndex        =   3
      Top             =   421
      Width           =   7700
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1020
         Left            =   0
         TabIndex        =   4
         Top             =   0
         Width           =   7250
         Begin MSComctlLib.Toolbar tbr_Perfiles 
            Height          =   810
            Left            =   315
            TabIndex        =   5
            Top             =   150
            Width           =   13500
            _ExtentX        =   23813
            _ExtentY        =   1429
            ButtonWidth     =   1296
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Nuevo"
                  Key             =   "Nuevo"
                  Object.ToolTipText     =   "Agregar Nuevo Perfil"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Buscar"
                  Key             =   "Buscar"
                  Object.ToolTipText     =   "Buscar Perfil"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "Grabar"
                  Object.ToolTipText     =   "Graba Perfil"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Aplicar"
                  Key             =   "Agregar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   6
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame6 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Perfiles"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   75
         Width           =   3615
      End
      Begin VB.Label lbl_Website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   11655
         TabIndex        =   1
         Top             =   75
         Width           =   1815
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   5655
      Top             =   1170
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":0802
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":14DC
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":326E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":5000
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":6D92
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Perfiles.frx":8B24
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Frm_Perfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim objmens As Object

Private Sub cmd_buscar_Click()
    Call Buscar
End Sub

Private Sub Form_Activate()
    If SalidaForzada Then ForcedExit: Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Call Buscar
        Case Is = vbKeyF4
            Call Grabar
        Case Is = vbKeyF6
            Call Aplicar
        Case Is = vbKeyF7
            Call Cancelar
        Case Is = vbKeyF12
            Unload Me
    End Select
End Sub

Private Sub Form_Load()
    
    Set objmens = CreateObject("recsun.OBJ_MENSAJERIA")
    BorrarPerfilTemp PosConexion, "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
    
    With tbr_Perfiles
        For I = 1 To .Buttons.Count - 1
            If .Buttons(I).Caption = "" Then
                .Buttons(I).Enabled = False
            End If
        Next I
    End With
    
    Me.lbl_Organizacion.Caption = StellarMensaje(10311, True) 'perfil
    Me.tbr_Perfiles.Buttons(1).Caption = StellarMensaje(10120, True) 'nuevo
    Me.tbr_Perfiles.Buttons(2).Caption = StellarMensaje(102, True) 'buscar
    Me.tbr_Perfiles.Buttons(3).Caption = StellarMensaje(103, True) 'grabar
    Me.tbr_Perfiles.Buttons(4).Caption = StellarMensaje(10312, True) 'aplicar
    Me.tbr_Perfiles.Buttons(5).Caption = StellarMensaje(54, True) 'salir
    Me.tbr_Perfiles.Buttons(6).Caption = StellarMensaje(10080, True) ' Teclado
    
    Me.lbl_codigo.Caption = StellarMensaje(10096, True) 'codigo
    Me.Label1.Caption = StellarMensaje(143, True) 'descripcion
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If SalidaForzada Then ForcedExit: Exit Sub
    
    Dim Tabla As String
    
    Tabla = "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
    
    If VerificarPerfil(PosConexion, Tabla) = False Then
        'objmens.Mensaje "El perfil actual contiene datos,Desea Salir", True
        Mensaje True, StellarMensaje(10315, True), True 'traducir
        
        'If objmens.PressBoton = False Then
        If Not Retorno Then
            Cancel = True
        Else
            BorrarPerfilTemp PosConexion, "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
            Set objmens = Nothing
        End If
    End If
    
End Sub

Private Sub tbr_Perfiles_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case Is = "Nuevo"
            Call Nuevo
        Case Is = "Buscar"
            Call Buscar
        Case Is = "Grabar"
            Call Grabar
        Case Is = "Agregar"
            Call Aplicar
        Case Is = "Salir"
            Unload Me
        Case Is = "Teclado"
            
            Dim mCtl As Object
            
            If Not Me.ActiveControl Is Nothing Then
                If TypeOf Me.ActiveControl Is TextBox Then
                    Set mCtl = Me.ActiveControl
                End If
            End If
            
            If mCtl Is Nothing Then Set mCtl = txt_CodigoPerfil
            
            TecladoWindows mCtl
            
    End Select
End Sub

Private Sub Nuevo()
    
    Dim TablaTemporal As String
    Dim mdatos(1) As String
    Dim misDatosPerfil As Variant
    
    TablaTemporal = "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
    mdatos(0) = g_CodigoPerfil
    mdatos(1) = Me.txt_Descripcion.Text
    misDatosPerfil = mdatos()
    Call NuevoPerfil(misDatosPerfil, TablaTemporal, Cadena)
    Me.txt_CodigoPerfil.Text = g_CodigoPerfil
    Frm_Configuracion.Show vbModal
    
End Sub

Private Sub Grabar()
    
    Dim Tabla As String
    Dim m_datos As Variant
    Dim m_Temp(1) As String
    Dim Error As Boolean
    
    'objmens.Mensaje "Desea conservar el mismo perfil", True
    'Mensaje True, StellarMensaje(10313, True), True
    'If objmens.PressBoton = True Then
    'If Retorno = True Then
        m_Temp(0) = g_CodigoPerfil
    'Else
        'g_CodigoPerfil = micorrelativo(PosConexion, "cs_COD_PERFIL")
        'If SalidaForzada Then ForcedExit: Exit Sub
         'm_Temp(0) = g_CodigoPerfil
    'End If
    
    m_Temp(1) = Me.txt_Descripcion.Text
    m_datos = m_Temp
    Tabla = "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
    Call GrabarPerfil(m_datos, Tabla, Cadena, Error)
            
End Sub

Private Sub Buscar()
    
    Dim mBusqueda As Variant
    Dim m_SQL As String
    Dim MisDatos As Variant
    Dim perfil(1) As String
    
    m_SQL = "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
    
    If Trim(g_CodigoPerfil) = "" Then
        g_CodigoPerfil = micorrelativo(PosConexion, "cs_cod_perfil", True)
        If SalidaForzada Then ForcedExit: Exit Sub
    End If
    
    perfil(0) = g_CodigoPerfil
    perfil(1) = Me.txt_Descripcion.Text
    
    MisDatos = perfil()
    
    mBusqueda = BuscarPerfil(m_SQL, Cadena, MisDatos)
    
    If Not IsEmpty(mBusqueda) Then
        Me.txt_CodigoPerfil = mBusqueda(0)
        Me.txt_Descripcion = mBusqueda(1)
        g_CodigoPerfil = CStr(mBusqueda(0))
        Frm_Configuracion.Show vbModal
    End If
    
End Sub

Private Sub Aplicar()
    If Trim(txt_CodigoPerfil.Text) = vbNullString Then
        'MsgBox "Debe seleccionar un perfil."
        Mensaje True, StellarMensaje(10314, True)
    Else
        FrmAplicarPerfilBotones.Show vbModal
    End If
End Sub

Private Sub Cancelar()
    
    Dim CONEX As New ADODB.Connection
    
    CONEX.Open Cadena
    
    'objmens.Mensaje "Esta seguro, si tiene algun perfil cargado perdera su informacion "
    Mensaje True, StellarMensaje(10316, True), True
    'If objmens.PressBoton = True Then
    If Retorno Then
        BorrarPerfilTemp CONEX, "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
        Me.txt_CodigoPerfil = ""
        Me.txt_Descripcion.Text = ""
    End If
    
End Sub

