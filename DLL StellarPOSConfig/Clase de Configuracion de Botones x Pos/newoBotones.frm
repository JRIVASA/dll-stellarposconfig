VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frm_NewGrupo 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9930
   ClientLeft      =   120
   ClientTop       =   -225
   ClientWidth     =   8985
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   9930
   ScaleWidth      =   8985
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   1695
      Left            =   120
      TabIndex        =   43
      Top             =   1680
      Width           =   8655
      Begin VB.TextBox txt_Codigo 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Left            =   480
         Locked          =   -1  'True
         TabIndex        =   51
         Top             =   390
         Width           =   1635
      End
      Begin VB.TextBox txt_Posicion 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Left            =   480
         Locked          =   -1  'True
         TabIndex        =   50
         Top             =   1230
         Width           =   1635
      End
      Begin VB.TextBox txt_Nombre 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Left            =   2685
         TabIndex        =   49
         Top             =   390
         Width           =   5070
      End
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Caption         =   "Asignacion de Teclas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   960
         Left            =   2520
         TabIndex        =   44
         Top             =   840
         Width           =   5085
         Begin VB.ComboBox cbo_tecla 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   2790
            Style           =   2  'Dropdown List
            TabIndex        =   46
            Top             =   420
            Width           =   1785
         End
         Begin VB.ComboBox cbo_Mascara 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   360
            Left            =   375
            Style           =   2  'Dropdown List
            TabIndex        =   45
            Top             =   420
            Width           =   1665
         End
         Begin VB.Label Label4 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Mascara"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Left            =   375
            TabIndex        =   48
            Top             =   135
            Width           =   750
         End
         Begin VB.Label Label5 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Tecla"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   195
            Left            =   2790
            TabIndex        =   47
            Top             =   135
            Width           =   615
         End
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   2655
         TabIndex        =   54
         Top             =   135
         Width           =   735
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Posici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   480
         TabIndex        =   53
         Top             =   930
         Width           =   765
      End
      Begin VB.Label lbl_codigo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   480
         TabIndex        =   52
         Top             =   120
         Width           =   615
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   9105
      TabIndex        =   40
      Top             =   421
      Width           =   9135
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1200
         Left            =   240
         TabIndex        =   41
         Top             =   -120
         Width           =   8750
         Begin MSComctlLib.Toolbar tlb_Perfiles 
            Height          =   810
            Left            =   90
            TabIndex        =   42
            Top             =   210
            Width           =   8500
            _ExtentX        =   15002
            _ExtentY        =   1429
            ButtonWidth     =   1296
            ButtonHeight    =   1429
            Style           =   1
            ImageList       =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Buscar"
                  Key             =   "Buscar"
                  Object.ToolTipText     =   "Buscar Perfil"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "Grabar"
                  Object.ToolTipText     =   "Graba Perfil"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "Salir"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  Description     =   "Teclado"
                  ImageIndex      =   4
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame6 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   37
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "POS Configuracion de Botones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   39
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_Website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6615
         TabIndex        =   38
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Caption         =   "Posiciones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   6285
      Left            =   210
      TabIndex        =   36
      Top             =   3495
      Width           =   8490
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   0
         Left            =   285
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   150
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   1
         Left            =   1620
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   150
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   2
         Left            =   2955
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   150
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   3
         Left            =   4290
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   150
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   4
         Left            =   5625
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   150
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   5
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   150
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   6
         Left            =   285
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   1365
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   7
         Left            =   1620
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1365
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   8
         Left            =   2955
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   1365
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   9
         Left            =   4290
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   1365
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   10
         Left            =   5625
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1365
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   11
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1365
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   12
         Left            =   285
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   2580
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   13
         Left            =   1620
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   2580
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   14
         Left            =   2955
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   2580
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   15
         Left            =   4290
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   2580
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   16
         Left            =   5625
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2580
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   17
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   2580
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   18
         Left            =   285
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   3795
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   19
         Left            =   1620
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   3795
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   20
         Left            =   2955
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   3795
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   21
         Left            =   4290
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   3795
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   22
         Left            =   5625
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   3795
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   23
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   3795
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   24
         Left            =   285
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   5010
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Index           =   25
         Left            =   1620
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   4890
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   26
         Left            =   2955
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   5010
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   27
         Left            =   4290
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   5010
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   28
         Left            =   5625
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   5010
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   29
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   5010
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   30
         Left            =   165
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   5985
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   31
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   5985
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   32
         Left            =   1875
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   5985
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   33
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   5985
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   34
         Left            =   3585
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   5985
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmd_Botones 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   35
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   5985
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   0
      Top             =   6960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newoBotones.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newoBotones.frx":1D92
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newoBotones.frx":3B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "newoBotones.frx":58B6
            Key             =   "Teclado"
         EndProperty
      EndProperty
   End
   Begin VB.Menu menu 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu eliminar 
         Caption         =   "Eliminar"
      End
      Begin VB.Menu cambiar 
         Caption         =   "Cambiar Nombre"
      End
   End
End
Attribute VB_Name = "frm_NewGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Objeto As Object

Private Sub barra_menu_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
    
    Case Is = "grabar" ''boton salvar
        Call Grabar
        
    Case Is = "cancelar"
        txt_Nombre = ""
    
    Case Is = "salir"
        Frm_Configuracion.addnode = False
        Unload Me
End Select

End Sub

Private Function ValidarPosicion(Index As Integer) As Boolean
    If Me.cmd_Botones(Index).Caption = "" Or Me.cmd_Botones(Index).Tag = Frm_Configuracion.fNodoArr_Sel(2) Then
        ValidarPosicion = True
    End If
End Function

Private Sub cmd_Botones_Click(Index As Integer)
    If Me.cmd_Botones(Index).Caption = "" Or Me.cmd_Botones(Index).Tag = Frm_Configuracion.fNodoArr_Sel(2) Then
        Me.txt_Posicion.Text = Index + 1
    Else
        'objeto.Mensaje "Boton asignado, seleccione otra posicion o modifique el boton actual "
        Mensaje True, StellarMensaje(10310, True)
    End If
End Sub

Private Sub BuscarProductos()
    Dim Buscar As Object
    Dim mProducto As Variant
    
    Set Buscar = CreateObject("recsuna.cls_productos")
    
    mProducto = Buscar.Buscar_ProductosInterfaz(Cadena)
    If Not IsEmpty(mProducto) Then
        Me.txt_Codigo.Text = mProducto(0)
        Me.txt_Nombre = mProducto(1)
    End If
    
    Set Buscar = Nothing
End Sub

Private Sub Form_Activate()
    If SalidaForzada Then ForcedExit: Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF1
        Case Is = vbKeyF2
            'Call BuscarProductos
        Case Is = vbKeyF7
            Call Cancelar
        Case Is = vbKeyF4
            Call Grabar
        Case Is = vbKeyF12
            Frm_Configuracion.addnode = False
            Unload Me
    End Select
End Sub

Private Sub Form_Load()

    Dim Botones As New cls_Boton
    Dim Min As Long, Max As Long
    Dim CONEX As New ADODB.Connection
    Dim m_SQL As String
    
    Min = 0
    Max = Me.cmd_Botones.Count - 1
    CONEX.Open Cadena
    
    Me.txt_Codigo.Text = micorrelativo(CONEX, "cs_COD_GRUPO", False)
    If SalidaForzada Then ForcedExit: Exit Sub
    
    'Set Objeto = CreateObject("recsun.obj_MENSAJERIA")
    
    Call LlenarCombos(Me.cbo_Mascara, Me.cbo_tecla)
    
    Me.txt_Codigo.Text = micorrelativo(CONEX, "cs_COD_GRUPO", False)
    If SalidaForzada Then ForcedExit: Exit Sub
    
    m_SQL = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP where relacion='" & CStr(Frm_Configuracion.fNodoArr_Sel(2)) + "'"
    Botones.IniciarBotones Me.cmd_Botones, CONEX, m_SQL, Min, Max
           
    CONEX.Close
    
    Me.tlb_Perfiles.Buttons(1).Enabled = False
    
    If IsNumeric(DatosBoton.Posicion) Then
        If DatosBoton.Posicion > 0 Then
            If ValidarPosicion(DatosBoton.Posicion) Then
                Me.txt_Posicion.Text = DatosBoton.Posicion
            End If
        End If
    End If
    
    Me.txt_Nombre.TabIndex = 0
    Me.txt_Posicion.TabIndex = 1
    Me.txt_Codigo.TabIndex = 2
    
    Me.tlb_Perfiles.Buttons(1).Caption = StellarMensaje(102, True) 'buscar
    Me.tlb_Perfiles.Buttons(2).Caption = StellarMensaje(103, True) 'grabar
    Me.tlb_Perfiles.Buttons(4).Caption = StellarMensaje(54, True) 'salir
    Me.tlb_Perfiles.Buttons(5).Caption = StellarMensaje(10080, True) ' Teclado
    
    Me.lbl_codigo.Caption = StellarMensaje(10096, True) 'codigo
    Me.Label1.Caption = StellarMensaje(10305, True) 'nombre
    Me.Label3.Caption = StellarMensaje(10306, True) 'posicion
    Me.Label4.Caption = StellarMensaje(10307, True) 'mascara
    Me.Label5.Caption = StellarMensaje(10308, True) 'tecla
    Me.lbl_Organizacion.Caption = StellarMensaje(10309, True) 'configuracion de botones
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set Objeto = Nothing
    Set Newdpto = Nothing
End Sub

Private Sub tlb_Perfiles_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case Is = "Buscar"
            'Call BuscarProductos
        Case Is = "Grabar"
            Call Grabar
        Case Is = "Salir"
            Unload Me
        Case Is = "Teclado"
            TecladoWindows txt_Nombre
    End Select
End Sub
Private Sub Cancelar()
    Me.txt_Codigo.Text = ""
    Me.txt_Nombre.Text = ""
    Me.txt_Posicion.Text = ""
    Me.cbo_Mascara.ListIndex = 0
    Me.cbo_tecla.ListIndex = 0
End Sub

Private Sub Grabar()
    
    Dim SQL As String
    Dim Respuesta As Boolean
    Dim m_Correlativo As String
    
    DatosBoton.Codigo = Me.txt_Codigo.Text
    DatosBoton.CodigoNativo = Me.txt_Codigo.Text
    DatosBoton.Grupo = Frm_Configuracion.fNodoArr_Sel(2)
    DatosBoton.Imagen = "DPTO ABIERTO"
    DatosBoton.Mascara = Me.cbo_Mascara.Text
    DatosBoton.Nombre = Me.txt_Nombre.Text
    DatosBoton.Posicion = Val(Me.txt_Posicion.Text)
    DatosBoton.Tecla = Me.cbo_tecla
    
    GrabarBoton Cadena, "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP", Respuesta
    
    If Respuesta = False Then
        Me.txt_Codigo.Text = micorrelativo(PosConexion, "cs_COD_GRUPO")
        If SalidaForzada Then ForcedExit: Exit Sub
        Frm_Configuracion.addnode = True
        Unload Me
    End If
    
End Sub

Private Sub txt_Codigo_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim RsProducto As Object, BProducto As New recsuna.cls_productos
    If Shift = 0 Then
        If KeyCode = vbKeyReturn Then
            If Trim(Me.txt_Codigo.Text) <> "" Then
                Set BProducto = CreateObject("recsuna.cls_productos")
                '000000000100004
                BProducto.Inicializar Cadena
                BProducto.CampoBusquedaCodigos sCodigoAlterno
                Set RsProducto = BProducto.Buscar_ProductosRs(BProducto.CampoBusquedaCodigos(sCodigoAlterno) & " = '" & Me.txt_Codigo.Text & "'")
                If Not RsProducto.EOF Then
                    Me.txt_Nombre.Text = RsProducto!Descripcion
                End If
                RsProducto.Close
            End If
        End If
    End If
End Sub
