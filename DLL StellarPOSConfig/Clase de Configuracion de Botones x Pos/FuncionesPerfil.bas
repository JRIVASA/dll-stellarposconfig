Attribute VB_Name = "FuncionesPerfil"
Dim ErrorGrabar As Boolean          'Variable utilizada para Saber si ocurrio un error al grabar
Public g_CodigoPerfil As String     'Variable global que almacena el codigo del perfil actual

Public Sub BorrarPerfilTemp(ByRef PConexion As ADODB.Connection, pNomTabTemp As String)
    Dim SQL As String
    SQL = "Delete from " & pNomTabTemp
    PConexion.Execute SQL
End Sub

Public Function VerificarPerfil(PConexion As ADODB.Connection, psql As String) As Boolean
    
    Dim Rec As New ADODB.Recordset
    
    Rec.Open psql, PConexion
    
    If Rec.EOF Then
        VerificarPerfil = True
    Else
        VerificarPerfil = False
    End If
    
    Rec.Close
    
End Function

' Sub rutina para Buscar un perfil creado previamente, Esta Sub rutina es llamada desde una funcion llamada BuscarPerfil que es publica
Public Sub BuscarP(ByRef pconex As ADODB.Connection, pNomTabTemp As String, ByRef pResultado As Variant)

    Dim rec_Origen As New ADODB.Recordset
    Dim rec_Temp As New ADODB.Recordset
    Dim TextoSql As String
    Dim Mensaje As Object
    Dim Buscar As Object
    
    Set Mensaje = CreateObject("recsun.OBJ_MENSAJERIA")
    
    Set Buscar = New Frm_Super_Consultas
    
    Buscar.Inicializar "Select * From MA_CAJA_BOTONES_DIGITALES_PERFIL", "Busqueda de Perfil", pconex
    Buscar.Add_ItemLabels "Codigo", "CodigoPerfil", 2500, 0
    Buscar.Add_ItemLabels "Descripcion", "Descripcion", 6500, 0
    Buscar.Add_ItemSearching "Codigo", "CodigoPerfil"
    Buscar.Add_ItemSearching "Descripcion", "Descripcion"
    
    Buscar.txtDato.Text = "%"
    Buscar.BusquedaInstantanea = True
    
    Buscar.Show vbModal
    
    pResultado = Buscar.ArrResultado
    
    If Not IsEmpty(pResultado) Then
        
        TextoSql = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL where CodigoPerfil='" & CStr(pResultado(0)) + "'"
        
        rec_Origen.Open TextoSql, pconex        'Abriendo recorset origen Tabla de transaccion de botones
        
        If rec_Origen.EOF Then
            Mensaje.Mensaje "No se cargo el perfil,por favor introduzca otra vez el Codigo"
        Else
            
            rec_Temp.Open pNomTabTemp, pconex, adOpenDynamic, adLockOptimistic  'Abriendo Tabla temporal para vaciado de informacion
            
            Do While Not rec_Origen.EOF
                rec_Temp.AddNew
                rec_Temp!Relacion = rec_Origen!Relacion
                rec_Temp!Clave = rec_Origen!Clave
                rec_Temp!Texto = rec_Origen!Texto
                rec_Temp!Imagen = rec_Origen!Imagen
                rec_Temp.Fields("tag") = rec_Origen.Fields("tag")
                rec_Temp!Posicion = rec_Origen!Posicion
                rec_Temp!codigoperfil = rec_Origen!codigoperfil
                rec_Temp!Mascara = rec_Origen!Mascara
                rec_Temp!Tecla = rec_Origen!Tecla
                rec_Temp.Update
                rec_Origen.MoveNext
            Loop
            
            rec_Temp.Close
            
        End If
        
        rec_Origen.Close
        
    End If
    
    Set Mensaje = Nothing
    Set Buscar = Nothing
    
End Sub

' Sub que Guarda el Perfil esta funcion es llamada desde una Sub llamada GrabarPerfil
Private Sub GuardarPerfil(ByRef PConexion As ADODB.Connection, ByRef pdatos As Variant, pNomTablaBotTemp As String)
    Dim m_RecPerfil As New ADODB.Recordset
    Dim m_RecOrigen As New ADODB.Recordset
    Dim SQL, SQLOrigen As String
    Dim mCodigoPerfil, mDescripcionPerfil As String
    Dim TransaccionenProceso  As Boolean
    
    TransaccionenProceso = False
    
    mCodigoPerfil = pdatos(0)
    mDescripcionPerfil = pdatos(1)
    If mDescripcionPerfil = "" Then
        mDescripcionPerfil = "Sin Descripcion Fecha:" & Format(Now, "short date")
    End If
'*************************** EMPIEZA TRANSACCION  ***************************************
    PConexion.CursorLocation = adUseServer
    PConexion.BeginTrans
    ErrorGrabar = False
    TransaccionenProceso = True
    On Error GoTo ManejadorError

'**************************** GRABANDO PERFIL EN MAESTRO DE BOTONES********************************
        SQL = "Delete from MA_CAJA_BOTONES_DIGITALES_PERFIL where CodigoPerfil='" & mCodigoPerfil + "'"
        PConexion.Execute SQL
        m_RecPerfil.Open "Select * From MA_CAJA_BOTONES_DIGITALES_PERFIL where CodigoPerfil='" & mCodigoPerfil + "'", PConexion, adOpenDynamic, adLockOptimistic
        m_RecPerfil.AddNew
            m_RecPerfil.Fields("CodigoPerfil") = mCodigoPerfil
            m_RecPerfil.Fields("Descripcion") = mDescripcionPerfil
        m_RecPerfil.Update
        m_RecPerfil.Close
    
        
    
'Abriendo Recorsets para vaciado de informacion
      SQL = "Delete from TR_CAJA_BOTONES_DIGITALES_PERFIL where CodigoPerfil='" & mCodigoPerfil + "'"
        PConexion.Execute SQL
      
      SQLOrigen = "Select * from " & pNomTablaBotTemp + " order by id"
        m_RecOrigen.Open SQLOrigen, PConexion, adOpenDynamic, adLockOptimistic
        
      SQL = "Select * from TR_CAJA_BOTONES_DIGITALES_PERFIL where CodigoPerfil='" & mCodigoPerfil + "'"
        m_RecPerfil.Open SQL, PConexion, adOpenDynamic, adLockOptimistic, adCmdText
    
    'Vaciando Recordset
            
                Do While Not m_RecOrigen.EOF
                    m_RecPerfil.AddNew
                        m_RecPerfil!Relacion = m_RecOrigen!Relacion
                        m_RecPerfil!Clave = m_RecOrigen!Clave
                        m_RecPerfil!Texto = m_RecOrigen!Texto
                        m_RecPerfil!Imagen = m_RecOrigen!Imagen
                        m_RecPerfil.Fields("tag") = m_RecOrigen.Fields("tag")
                        m_RecPerfil!Posicion = m_RecOrigen!Posicion
                        m_RecPerfil!codigoperfil = mCodigoPerfil
                        m_RecPerfil!Mascara = m_RecOrigen!Mascara
                        m_RecPerfil!Tecla = m_RecOrigen!Tecla
                    m_RecPerfil.Update
                    m_RecOrigen.MoveNext
                Loop
            
'******************** Cerrando Recordsets *********************
    PConexion.CommitTrans
    ErrorGrabar = False
    m_RecOrigen.Close
    m_RecPerfil.Close
    Exit Sub
ManejadorError:
    If TransaccionenProceso = True Then
        PConexion.RollbackTrans
        ErrorGrabar = True
    End If
    
End Sub
'*************************** Funciones de la Forma ************************************

'Crea un nuevo perfil, primero revisa si la tabla temporal no tiene datos, si no tiene crea nuevo perfil
' si existen datos en la tabla temporal pregunta si se van a guardar o no, Dependiendo de la respuesta graba,borra y crea o borra y crea

Public Sub NuevoPerfil(pmisdatos As Variant, pNomTabTemp As String, cadenadeconexion As String)
    Dim Conexion As New ADODB.Connection
    Dim mDatosPerfil As Variant
    Dim objmensaje As Object
    
    'Set objmensaje = CreateObject("recsun.obj_MENSAJERIA")
    Conexion.Open cadenadeconexion
    If VerificarPerfil(Conexion, pNomTabTemp) Then
        g_CodigoPerfil = micorrelativo(Conexion, "cs_COD_PERFIL")
        If SalidaForzada Then ForcedExit: Exit Sub
        Conexion.Close
    Else
        'objmensaje.Mensaje "El perfil actual contiene datos desea grabar", True
        'If objmensaje.PressBoton = True Then
        Mensaje True, StellarMensaje(10320), True
        If Retorno Then
            mDatosPerfil = pmisdatos(0)
            GuardarPerfil Conexion, pmisdatos, "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
            BorrarPerfilTemp Conexion, "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
            g_CodigoPerfil = micorrelativo(Conexion, "cs_COD_PERFIL")
            If SalidaForzada Then ForcedExit: Exit Sub
        Else
            BorrarPerfilTemp Conexion, "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
            g_CodigoPerfil = micorrelativo(Conexion, "cs_COD_PERFIL")
            If SalidaForzada Then ForcedExit: Exit Sub
        End If
        Conexion.Close
        'Set objmensaje = Nothing
    End If

End Sub

'Valida los datos antes de grabar y Utiliza la sub Guardar perfil para grabar
Public Sub GrabarPerfil(pdatos As Variant, NomTablaTemp As String, cadenadeconexion As String, ByRef pErrorGrabar As Boolean)
    
    Dim Conexion As New ADODB.Connection
    Dim objmensaje As Object
        
    pErrorGrabar = ErrorGrabar
    'Set objmensaje = CreateObject("recsun.obj_MENSAJERIA")
    Conexion.Open cadenadeconexion
    If VerificarPerfil(Conexion, NomTablaTemp) = False Then         'Verifica si hay datos en la tabla temporal para grabarlos en la de transaccion
        GuardarPerfil Conexion, pdatos, NomTablaTemp
        If ErrorGrabar = False Then
            BorrarPerfilTemp Conexion, NomTablaTemp
            'objmensaje.Mensaje "Perfil Guardado"
            Mensaje True, StellarMensaje(10318, True)
        End If
    Else
        'objmensaje.Mensaje "No hay un perfil para Grabar, o ya grabo el actual"
        Mensaje True, StellarMensaje(10319, True)
    End If
    Conexion.Close
    'Set objmensaje = Nothing
    
End Sub

'Funcion que devuelve los datos del perfil cargado. Utiliza la Subrutina BuscarP para buscar todos los datos del perfil y guardarlos en la tabla de perfil temporal
Public Function BuscarPerfil(pNombreTablaTemp As String, CadenadeConex As String, pdatos As Variant) As Variant
    
    Dim miconexion As New ADODB.Connection
    Dim mibusqueda As Variant
    Dim objmens As Object
        
    'Set objmens = CreateObject("recsun.obj_Mensajeria")
    miconexion.Open CadenadeConex
    
    If VerificarPerfil(miconexion, pNombreTablaTemp) Then
        BuscarP miconexion, pNombreTablaTemp, mibusqueda
    Else
        'objmens.Mensaje "El perfil actual contiene datos desea grabar", True
        Mensaje True, StellarMensaje(10320, True), True
        
        'If objmens.PressBoton = True Then
        If Retorno Then
            GuardarPerfil miconexion, pdatos, pNombreTablaTemp
            BorrarPerfilTemp miconexion, pNombreTablaTemp
            BuscarP miconexion, pNombreTablaTemp, mibusqueda
        Else
            BorrarPerfilTemp miconexion, pNombreTablaTemp
            BuscarP miconexion, pNombreTablaTemp, mibusqueda
        End If
    End If
    
    BuscarPerfil = mibusqueda

    'Set objmens = Nothing
    miconexion.Close
        
End Function
