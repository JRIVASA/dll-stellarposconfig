VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmConfigDigitalCaja 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n de los botones del POS"
   ClientHeight    =   8805
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15270
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   587
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1018
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      Caption         =   "Botones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   6810
      Left            =   6900
      TabIndex        =   9
      Top             =   1920
      Width           =   8265
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   0
         Left            =   135
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   1
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   2
         Left            =   2790
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   3
         Left            =   4110
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   4
         Left            =   5430
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   5
         Left            =   6750
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   270
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   6
         Left            =   135
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   7
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   8
         Left            =   2790
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   9
         Left            =   4110
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   10
         Left            =   5430
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   11
         Left            =   6750
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   1545
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   12
         Left            =   135
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   13
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   14
         Left            =   2790
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   15
         Left            =   4110
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   16
         Left            =   5430
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   17
         Left            =   6750
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   2820
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   18
         Left            =   135
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   19
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   20
         Left            =   2790
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   21
         Left            =   4110
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   22
         Left            =   5430
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   23
         Left            =   6750
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   4095
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   24
         Left            =   135
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   25
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   26
         Left            =   2790
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   27
         Left            =   4110
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   28
         Left            =   5430
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   5370
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   1290
         Index           =   29
         Left            =   6750
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   5370
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   885
         Index           =   30
         Left            =   135
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   6645
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   885
         Index           =   31
         Left            =   1230
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   6645
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   885
         Index           =   32
         Left            =   2310
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   6645
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   885
         Index           =   33
         Left            =   3390
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   6645
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CommandButton cmd_Botones 
         Height          =   885
         Index           =   34
         Left            =   4470
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   6645
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CommandButton cmd_Botones 
         Enabled         =   0   'False
         Height          =   885
         Index           =   35
         Left            =   5550
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   6645
         Visible         =   0   'False
         Width           =   1035
      End
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      Caption         =   "Perfil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   780
      Left            =   60
      TabIndex        =   2
      Top             =   1095
      Width           =   15105
      Begin VB.CheckBox chk_fijar 
         Appearance      =   0  'Flat
         Caption         =   "Fijar Botones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   13080
         TabIndex        =   7
         Top             =   315
         Width           =   1620
      End
      Begin VB.TextBox txt_CodigoPerfil 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1695
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   285
         Width           =   2115
      End
      Begin VB.TextBox txt_Descripcion 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5550
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   285
         Width           =   7065
      End
      Begin VB.Label lbl_Codigo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo del Perfil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   105
         TabIndex        =   6
         Top             =   330
         Width           =   1395
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Descripcion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4425
         TabIndex        =   5
         Top             =   330
         Width           =   975
      End
   End
   Begin MSComctlLib.ImageList Treeimagen 
      Left            =   165
      Top             =   8385
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDigitalCaja.frx":0000
            Key             =   "DPTO CERRADO"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDigitalCaja.frx":0452
            Key             =   "DPTO ABIERTO"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1500
      Top             =   8385
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDigitalCaja.frx":08A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDigitalCaja.frx":2636
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDigitalCaja.frx":43C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDigitalCaja.frx":615A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDigitalCaja.frx":7EEC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1065
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   15105
      Begin MSComctlLib.Toolbar tlb_Config 
         Height          =   810
         Left            =   90
         TabIndex        =   1
         Top             =   180
         Width           =   10590
         _ExtentX        =   18680
         _ExtentY        =   1429
         ButtonWidth     =   1402
         ButtonHeight    =   1429
         Style           =   1
         ImageList       =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   5
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "buscar"
               Object.ToolTipText     =   "Buscar un Perfil [F2]"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Cancelar"
               Key             =   "cancelar"
               Object.ToolTipText     =   "Cancelar la configuraci�n actual [F7]"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "grabar"
               Object.ToolTipText     =   "Grabar esta configuraci�n [F4]"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "salir"
               Object.ToolTipText     =   "Salir del configurador [F12]"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Ayuda"
               Key             =   "ayuda"
               Object.ToolTipText     =   "Ayuda del Configurador [F1]"
               ImageIndex      =   4
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.TreeView tvw_Arbol 
      Height          =   6705
      Left            =   60
      TabIndex        =   8
      Top             =   1950
      Width           =   6675
      _ExtentX        =   11774
      _ExtentY        =   11827
      _Version        =   393217
      Indentation     =   212
      LabelEdit       =   1
      Style           =   5
      SingleSel       =   -1  'True
      ImageList       =   "Treeimagen"
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDragMode     =   1
      OLEDropMode     =   1
   End
   Begin VB.Menu Menu 
      Caption         =   "Menu"
      Visible         =   0   'False
      Begin VB.Menu agregar 
         Caption         =   "&Agregar"
         Begin VB.Menu grupos 
            Caption         =   "&Grupos"
         End
         Begin VB.Menu productos 
            Caption         =   "&Producto"
         End
      End
      Begin VB.Menu modificar 
         Caption         =   "&Modificar"
      End
      Begin VB.Menu separador 
         Caption         =   "-"
      End
      Begin VB.Menu eliminar 
         Caption         =   "&Eliminar"
      End
   End
End
Attribute VB_Name = "FrmConfigDigitalCaja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Obj  As Object          ' Variable para manejo del treeview
Private ClickProgramado As Boolean
Private FormaCargada As Boolean

Private Sub ChequearNodo()
    ClickProgramado = True
    If Not tvw_Arbol.SelectedItem Is Nothing Then tvw_Arbol_NodeClick tvw_Arbol.SelectedItem
End Sub

Private Sub chk_fijar_Click()
    
    ChequearNodo
    
    On Error Resume Next
    
    If NodoActual.Children < 1 Then
        chk_fijar.Value = 0
    End If
    
End Sub

Private Sub cmd_Botones_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    ChequearNodo
    
    Dim lcKey As String, Mensaje As Object
    
    On Error GoTo Error
    
    'Validacion para evitar que hagan click derecho sobre los botones directamente.
    If Button = vbRightButton Then Exit Sub
    
    Indice = Index
    
    If Trim(cmd_Botones(Index).Tag) <> "" Then
        fNodoArr_Sel = Obj.TomarDataTw(Me.tvw_Arbol, tvw_Arbol.Nodes("l" & cmd_Botones(Index).Tag).Index)
    Else
        fNodoArr_Sel = Obj.TomarDataTw(Me.tvw_Arbol, tvw_Arbol.Nodes(tvw_Arbol.SelectedItem.Key).Index)
    End If
    
    fcargado = True
    
    Set Mensaje = CreateObject("recsun.OBJ_MENSAJERIA")
    If fcargado = True Then
        lcKey = Mid(CStr(fNodoArr_Sel(2)), 1, 1)
        Select Case Button
            Case Is = 1
            Case Is = 2
                Select Case lcKey
                    Case Is = "R"
                        
                        grupos.Enabled = cmd_Botones(Index).Tag = ""
                        agregar.Enabled = tvw_Arbol.SelectedItem.Children < 28 '< 35
                        productos.Enabled = cmd_Botones(Index).Tag = ""
                        modificar.Enabled = cmd_Botones(Index).Tag <> ""
                        eliminar.Enabled = cmd_Botones(Index).Tag <> ""
                        DatosBoton.Posicion = Indice
                        
                    Case Is = "G"
                        Debug.Print cmd_Botones(Index).Tag <> ""
                         agregar.Enabled = tvw_Arbol.SelectedItem.Children < 28 ' < 35
                         grupos.Enabled = cmd_Botones(Index).Tag = ""
                         productos.Enabled = cmd_Botones(Index).Tag = ""
                         modificar.Enabled = cmd_Botones(Index).Tag <> ""
                         eliminar.Enabled = cmd_Botones(Index).Tag <> ""
                         DatosBoton.Posicion = Indice
                    Case Else
                        agregar.Enabled = tvw_Arbol.SelectedItem.Children < 28 ' < 35
                        modificar.Enabled = cmd_Botones(Index).Tag <> ""
                        eliminar.Enabled = cmd_Botones(Index).Tag <> ""
                        grupos.Enabled = cmd_Botones(Index).Tag = ""
                        productos.Enabled = cmd_Botones(Index).Tag = ""
                        DatosBoton.Posicion = Indice
                End Select
                PopupMenu Menu
        End Select
    End If
    fcargado = False
    Exit Sub
Error:
    MsgBox Err.Description
End Sub

Private Sub eliminar_Click()
    
    Dim boton As cls_Boton
    Dim mresp As String
    Dim m_Tabla As String
    
    ChequearNodo
    
    m_Tabla = "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
    
    Mensajes.Mensaje "Esta seguro ", True
    
    Set NodoActual = tvw_Arbol.SelectedItem.Parent
    
    If Mensajes.PressBoton = True Then
        Set boton = New cls_Boton
        boton.Codigo = CStr(ArregloNodos(2))
        boton.eliminar PosConexion, m_Tabla, mresp
        If mresp = vbYes Then
'            'Me.tvw_Arbol.Nodes.Clear
'            'Call CargarDatos(Me)
            Call tvw_Arbol.Nodes.Remove(tvw_Arbol.SelectedItem.Index)
        End If
        Set boton = Nothing
    End If
    
    Call tvw_Arbol_NodeClick(NodoActual)
    
End Sub

Private Sub Form_Activate()
    If SalidaForzada Then ForcedExit: Exit Sub
    If Not FormaCargada Then
        Call CargarDatos(Me, False)
    Else
        Call CargarDatos(Me)
    End If
    'Call CargarDatos(Me)
    Call tvw_Arbol_NodeClick(tvw_Arbol.Nodes(1))
    FormaCargada = True
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
            Select Case KeyCode
                
            End Select
        Case vbCtrlMask
            Select Case KeyCode
                
            End Select
        Case Else
            Select Case KeyCode
                Case vbKeyF1        'ayuda
                Case vbKeyF2        'buscar
                    
                    Dim Datos As Variant
                    
                    Me.tvw_Arbol.Nodes.Clear
                    
                    Call Cancelar(Me, False)
                    Call BuscarP(Variables.PosConexion, "TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP", Datos)
                    
                    If Not IsEmpty(Datos) Then
                        NPerfil = Datos(0)
                        PosConexion.Execute "update TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP set codigoperfil = '" & nCaja & "'"
                        txt_CodigoPerfil = Datos(0)
                        txt_Descripcion = Datos(1)
                        Call CargarDatos(Me)
                        '0 numero de perfil
                        '1 descripcion del perfil
                    Else
                        Call CargarDatos(Me)
                    End If
                    
                    tvw_Arbol_NodeClick tvw_Arbol.Nodes(1).Root
                    
                Case vbKeyF4        'grabar
                    Call GrabarEstructura(PosConexion, Me)
                    Unload Me
                    Exit Sub
                Case vbKeyF7        'cancelar
                    If tvw_Arbol.Nodes.Count > 1 Then
                        Mensajes.Mensaje "Ya defini� una estructura de botones, �Desea grabar esta?", True
                        If Mensajes.PressBoton = True Then
                            Call GrabarEstructura(PosConexion, Me)
                        End If
                    End If
                    Call Cancelar(Me)
                Case vbKeyF12       'salir
                    If tvw_Arbol.Nodes.Count > 1 Then
                        'Mensajes.mensaje "Ya defini� una estructura de botones, �Desea grabar esta?", True
                        'If Mensajes.PressBoton = True Then
                            Call GrabarEstructura(PosConexion, Me)
                        'End If
                    End If
                    Variables.StatusConfig = False
                    Call Cancelar(Me, False)
                    Unload Me
                Case vbKeyReturn
                    'refresh
                    'Send_Keys Chr(vbKeyTab), True
                    'SendKeys Chr(vbKeyTab), True
            End Select
    End Select
End Sub

Private Sub Form_Load()
    FormaCargada = False
    Set Obj = CreateObject("recsun.obj_treeview")
    PosConexion.Execute "delete from TR_CAJA_BOTONES_DIGITALES_PERFIL_TEMP"
    Call Cancelar(Me)
End Sub

Private Sub grupos_Click()
    ChequearNodo
    ModConfigDigitalCaja.Accion = Variables.agregar
    ArregloNodos = Arboles.TomarDataTw(tvw_Arbol, NodoActual.Index)
    ModConfigDigitalCaja.QueNivel = "G"
    FrmBoton.Show vbModal
End Sub

Private Sub menu_Click()
    If chk_fijar.Value <> 0 Then
        eliminar.Enabled = False
        modificar.Enabled = False
        productos.Enabled = False
        grupos.Enabled = False
    End If
End Sub

Private Sub modificar_Click()
    ChequearNodo
    ModConfigDigitalCaja.Accion = Variables.modificar
    ArregloNodos = Arboles.TomarDataTw(tvw_Arbol, NodoActual.Index)
    ModConfigDigitalCaja.QueNivel = Mid(ArregloNodos(Variables.LClave), 1, 1)
    FrmBoton.Show vbModal
End Sub

Private Sub productos_Click()
    ChequearNodo
    ModConfigDigitalCaja.Accion = Variables.agregar
    ArregloNodos = Arboles.TomarDataTw(tvw_Arbol, NodoActual.Index)
    ModConfigDigitalCaja.QueNivel = "P"
    FrmBoton.Show vbModal
End Sub

Private Sub tlb_Config_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case LCase(Button.Key)
        Case "buscar"
            'Send_Keys Chr(vbKeyF2), True
           Form_KeyDown vbKeyF2, 0
        Case "cancelar"
            'Send_Keys Chr(vbKeyF7), True
            Form_KeyDown vbKeyF7, 0
        Case "grabar"
            'Send_Keys Chr(vbKeyF4), True
            Form_KeyDown vbKeyF4, 0
        Case "salir"
            'Send_Keys Chr(vbKeyF12), True
            Form_KeyDown vbKeyF12, 0
        Case "ayuda"
            'Send_Keys Chr(vbKeyF1), True
            Form_KeyDown vbKeyF1, 0
    End Select
End Sub

Private Sub tvw_Arbol_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
            Select Case KeyCode
                
            End Select
        Case vbCtrlMask
            Select Case KeyCode
                
            End Select
        Case Else
            Select Case KeyCode
                Case 93, vbKeyMenu, vbKeyRButton
                    PopupMenu Menu
                    'If Not (NodoActual = tvw_Arbol.Nodes(1).Root) Then
                        'NodoActual.Parent.Selected = True
                    'Else
                        'NodoActual.Selected = True
                    'End If
                    'RedibujarDatos Me
                    'Form_Activate
            End Select
    End Select
End Sub

Private Sub tvw_Arbol_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Button
        Case vbRightButton
            Select Case Shift
                Case vbAltMask
                Case vbCtrlMask
                Case Else
                
                'If tvw_Arbol.SelectedItem = "BOTONES" Then Exit Sub
                    'Send_Keys Chr(93), True
                    tvw_Arbol_KeyDown vbKeyMenu, 0
            End Select
        Case vbLeftButton
        Case vbMiddleButton
    End Select
End Sub

Public Sub tvw_Arbol_NodeClick(ByVal Node As MSComctlLib.Node)
    
    Set NodoPadre = Node.Parent
    Set NodoActual = Node
    
    ArregloNodos = Arboles.TomarDataTw(tvw_Arbol, NodoActual.Index)
    
    If Not ClickProgramado Then
        Me.tlb_Config.Buttons("grabar").Enabled = (tvw_Arbol.SelectedItem.Root.Children > 0)
        Call RedibujarDatos(Me)
    End If
    
    ClickProgramado = False
    
End Sub
